start .\start_infrastructure.bat

timeout 10

cd .\src\
dotnet build -c Debug

cd .\WebApi\bin\Debug\net8.0\
set ASPNETCORE_ENVIRONMENT=Development
start WebApi.exe
cd ../../../../

cd .\IODataModule\IODataModule\bin\Debug\net8.0\
start IODataModule.exe
cd ../../../../../

cd .\StrategyProcessor\StrategyProcessor\bin\Debug\net8.0\
start StrategyProcessor.exe
cd ../../../../../../