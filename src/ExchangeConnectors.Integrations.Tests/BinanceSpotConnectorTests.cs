using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;
using ExchangeConnectors.Connectors;
using FluentAssertions;
using Infrastructure.FluentAssertions;
using Microsoft.Extensions.Logging;
using MoreLinq;

namespace ExchangeConnectors.Integrations.Tests;

[Parallelizable(ParallelScope.Fixtures)]
public class BinanceSpotConnectorTests
{
    [OneTimeTearDown]
    public async Task DisposeConnector() => await _binanceSpotConnector.DisposeAsync();

    [Test]
    public async Task GetPrices_ReturnSomeValues()
    {
        var priceByTicker = await _binanceSpotConnector.GetPrices();
        priceByTicker.Should().NotBeEmpty();
        priceByTicker.Keys.ForEach(x =>
            x.ToString().Should().NotBeNullOrWhiteSpace().And.NotBeEmpty().And.Contain("/"));
        priceByTicker.Values.ForEach(x => x.Should().BePositive());
    }

    [TestCase(MarketOrder.Buy, 0.001)]
    [TestCase(MarketOrder.Sell, 0.001)]
    public async Task CreateMarketOrder_CreateOrderSuccessfully(MarketOrder orderType, decimal amount)
    {
        var order = await _binanceSpotConnector.CreateMarketOrder(_btcUsdt, orderType, amount);
        order.Should().NotBeNull();
        order.Id.Should().NotBeNull();
    }

    private static IEnumerable<object[]> LimitOrderTestData()
    {
        yield return new object[] {LimitOrder.BuyLimit, new PlacementOrder(10_000, 0.01m)};
        yield return new object[] {LimitOrder.SellLimit, new PlacementOrder(100_000, 0.01m)};
    }

    [TestCaseSource(nameof(LimitOrderTestData))]
    public async Task CreateLimitOrder_CreateOrderSuccessfully(LimitOrder orderType, PlacementOrder placementOrder)
    {
        if (orderType == LimitOrder.SellLimit)
        {
            await _binanceSpotConnector.CreateMarketOrder(_btcUsdt, MarketOrder.Buy,
                placementOrder.Amount); // buy to create limit sell
        }

        var order = await _binanceSpotConnector.CreateLimitOrder(_btcUsdt, orderType, placementOrder);
        order.Should().NotBeNull();
        order.Id.Should().NotBeNull();
    }

    [TestCase(MarketOrder.Buy, 0.001)]
    [TestCase(MarketOrder.Sell, 0.001)]
    public async Task GetOrderInfo_ShouldReturnValidDataForMarketOrder(MarketOrder orderType, decimal amount)
    {
        var order = await _binanceSpotConnector.CreateMarketOrder(_btcUsdt, orderType, amount);

        var orderInfo = await _binanceSpotConnector.GetOrderInfo(_btcUsdt, order);
        orderInfo.OrderType.Should().Be(orderType.ToOrderType());
        orderInfo.Price.Should().BePositive();
        orderInfo.Amount.Should().BeInRadius(amount);
        orderInfo.FilledAmount.Should().BeInRadius(amount);
        orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
        orderInfo.CreateTime.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromHours(1));
        orderInfo.UpdateTime.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromHours(1));
    }

    [TestCaseSource(nameof(LimitOrderTestData))]
    public async Task GetOrderInfo_ShouldReturnValidDataForLimitOrder(LimitOrder orderType,
        PlacementOrder placementOrder)
    {
        if (orderType == LimitOrder.SellLimit)
        {
            await _binanceSpotConnector.CreateMarketOrder(_btcUsdt, MarketOrder.Buy,
                placementOrder.Amount); // buy to create limit sell
        }

        var order = await _binanceSpotConnector.CreateLimitOrder(_btcUsdt, orderType, placementOrder);

        var orderInfo = await _binanceSpotConnector.GetOrderInfo(_btcUsdt, order);
        orderInfo.OrderType.Should().Be(orderType.ToOrderType());
        orderInfo.Price.Should().BeInRadius(placementOrder.Price);
        orderInfo.Amount.Should().BeInRadius(placementOrder.Amount);
        orderInfo.CreateTime.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromHours(1));
        orderInfo.UpdateTime.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromHours(1));
    }

    [TestCase(LimitOrder.BuyLimit)]
    [TestCase(LimitOrder.SellLimit)]
    public async Task CloseOrder_ShouldReturnValidDataForLimitOrder(LimitOrder orderType)
    {
        if (orderType == LimitOrder.SellLimit)
        {
            await _binanceSpotConnector.CreateMarketOrder(_btcUsdt, MarketOrder.Buy,
                0.001m); // buy to create limit sell
        }


        var price = (double) (await _binanceSpotConnector.GetPrices()).Single(x => x.Key == _btcUsdt).Value;
        price *= orderType == LimitOrder.BuyLimit ? 0.7 : 1.3;

        var order = await _binanceSpotConnector.CreateLimitOrder(_btcUsdt, orderType,
            new PlacementOrder((int) price, 0.001m));

        var orderInfo = await _binanceSpotConnector.GetOrderInfo(_btcUsdt, order);
        orderInfo.OrderStatus.Should().Be(OrderStatus.Open);

        await _binanceSpotConnector.CancelOrder(_btcUsdt, order);

        var orderInfoAfterClose = await _binanceSpotConnector.GetOrderInfo(_btcUsdt, order);
        orderInfoAfterClose.OrderStatus.Should().Be(OrderStatus.Close);
    }

    [Test]
    public async Task GetOrderInfo_UnknownId_ThrowException()
    {
        var getOrderInfo = async () =>
            await _binanceSpotConnector.GetOrderInfo(_btcUsdt, new OrderId(Guid.NewGuid().ToString()));

        await getOrderInfo.Should().ThrowAsync<Exception>("Order Not Found");
    }

    [Test]
    public async Task GetCoinsAmount()
    {
        var coinsAmount = await _binanceSpotConnector.GetCoinsAmount();
        coinsAmount.Should().NotBeEmpty();
        coinsAmount.ForEach(x =>
        {
            x.Asset.Name.Should().NotBeNullOrEmpty();
            x.Asset.Name.Length.Should().BeGreaterOrEqualTo(1);
            x.Available.Should().BeGreaterOrEqualTo(0);
            x.Locked.Should().BeGreaterOrEqualTo(0);
            x.Total.Should().BePositive();
        });
    }

    [Test]
    [NonParallelizable]
    public async Task GetOrdersPerPair_ShouldReturnTrades() //todo тест проходит только в дебаге
    {
        var trades = await _binanceSpotConnector.GetOrdersPerPair(_btcUsdt);
        var amount = trades.Count;

        await _binanceSpotConnector.CreateMarketOrder(_btcUsdt, MarketOrder.Buy, 0.001m);
        await Task.Delay(TimeSpan.FromSeconds(60));
        var newTrades = await _binanceSpotConnector.GetOrdersPerPair(_btcUsdt);
        newTrades.Count.Should().BeGreaterOrEqualTo(amount + 1);
        var lastOrder = newTrades.Last();
        lastOrder.OrderType.Should().Be(OrderType.Buy);
        lastOrder.Amount.Should().Be(0.001m);
    }


    private static IEnumerable<object[]> GetCandlesTestData()
    {
        return new[]
        {
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h1, 24},
            new object[] {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h4, 6},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h12, 2},
            new object[] {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.D1, 1},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.m30, 48},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.m15, 96},
        };
    }

    [TestCaseSource(nameof(GetCandlesTestData))]
    public async Task GetCandles_ByPreviousDay_ShouldReturn24H1Candle(DateTimeRange range, TimeFrame timeFrame,
        int candleAmount)
    {
        var candles = await _binanceSpotConnector.GetCandles(_btcUsdt, timeFrame, range);

        candles.Count.Should().Be(candleAmount);
    }

    [Test]
    public async Task SubscribeOnNewKlines_OnePairOneTimeframe_WhenDataReceivedInvokeAction()
    {
        var isReceived = false;
        await _binanceSpotConnector.SubscribeOnNewKlines(new[] {_btcUsdt}, new[] {TimeFrame.m1},
            kline =>
            {
                kline.Pair.Should().Be(_btcUsdt);
                kline.TimeFrame.Should().Be(TimeFrame.m1);
                isReceived = true;
            });
        await Task.Delay(TimeSpan.FromSeconds(60 - DateTime.Now.Second + 5));
        isReceived.Should().BeTrue();
    }

    [Test]
    public async Task SubscribeOnNewKlines_ThreePairOneTimeframe_WhenDataReceivedInvokeAction()
    {
        var pairs = new[] {_btcUsdt, new Pair("LTC", UsdtTicker), new Pair("ETH", UsdtTicker)};
        var receivedAmount = 0;
        await _binanceSpotConnector.SubscribeOnNewKlines(pairs, new[] {TimeFrame.m1},
            kline =>
            {
                pairs.Should().Contain(kline.Pair);
                kline.TimeFrame.Should().Be(TimeFrame.m1);
                Interlocked.Increment(ref receivedAmount);
            });
        await Task.Delay(TimeSpan.FromSeconds(60 - DateTime.Now.Second + 5));
        receivedAmount.Should().Be(pairs.Length);
    }

    [Test]
    [Explicit("тулза на случай, если начали валиться тесты с Reach max open order limit")]
    [Description("Cancel all opened orders")]
    public async Task PrepareAccount()
    {
        var coinsAmount = await _binanceSpotConnector.GetCoinsAmount();
        foreach (var coins in coinsAmount.Where(x => x.Asset != UsdtTicker && x.Available > 0))
        {
            try
            {
                await _binanceSpotConnector.CreateMarketOrder(new Pair(coins.Asset, UsdtTicker), MarketOrder.Sell,
                    coins.Available);
            }
            catch
            {
                TestContext.WriteLine($"Smth went wrong {coins.Asset}");
            }
        }

        var prices = await _binanceSpotConnector.GetPrices();
        foreach (var coins in prices)
        {
            var tt = await _binanceSpotConnector.GetOrdersPerPair(coins.Key);
            foreach (var order in tt.Where(x => x.OrderStatus != OrderStatus.Close))
            {
                await _binanceSpotConnector.CancelOrder(coins.Key, order.OrderId);
            }
        }
    }


    private const string Key = "MTPsvWG2ZP7WvfZFgxO2oLAxt9jt7okSSJaqi6DxIXR8TkBZXXhJMLMi7rjzv56S";
    private const string Secret = "4ednBqrrW9lhA1m8mB3GVYuL6p5XJtBCxS0JKkqhdQQdCEyfO2mS5mHcEpvkGz0m";
    private readonly BinanceSpotConnector _binanceSpotConnector = new(Key, Secret, new LoggerFactory(), true);
    private readonly Pair _btcUsdt = new(BtcTicker, UsdtTicker);
    private static readonly Ticker UsdtTicker = new("USDT");
    private static readonly Ticker BtcTicker = new("BTC");
}