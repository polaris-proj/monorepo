﻿using System.ComponentModel.DataAnnotations;
using AutoFixture.NUnit3;
using Domain;
using Infrastructure.Tests;
using IODataModule.Infrastructure.DAL;
using IODataModule.Infrastructure.DAL.Models;
using IODataModule.Infrastructure.DAL.Repositories;
using IODataModule.Infrastructure.Mappers;
using Microsoft.Extensions.DependencyInjection;

namespace IoDataModule.Integrations.Tests;

public class CandleRepositoryTests : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup(); 
        DropTables().Wait();
        Scope.ServiceProvider.GetRequiredService<Migrations>().Start().Wait();

        _repository = Scope.ServiceProvider.GetService<ICandleRepository>() ??
                      throw new InvalidOperationException();
    }

    [TearDown]
    public async Task ClearDataBase()
    {
        await DropTables();
        base.TearDown();
    }

    [Test, CustomAutoData]
    public async Task SaveCandle_SavesCandle(Exchange exchange, CandleDbo candle, Pair pair)
    {
        await _repository.SaveCandle(exchange, pair, candle);

        var candleFromDb = await _repository
            .GetRangeStream((exchange, pair, candle.TimeFrame), candle.TimeStamp, candle.TimeStamp)
            .FirstOrDefaultAsync();
        candleFromDb.Should().Be(candle.ToModel());
    }

    [Test, CustomAutoData]
    public async Task SaveCandles_SavesMultipleCandles(
        [Frozen] TimeFrame tf,
        [MinLength(50)] CandleDbo[] candles,
        Exchange exchange,
        Pair pair)
    {
        candles = candles.DistinctBy(x => x.TimeStamp).ToArray();
        var minTimeStamp = candles.Min(x => x.TimeStamp);
        var maxTimeStamp = candles.Max(x => x.TimeStamp);

        await _repository.SaveCandles(exchange, pair, candles);

        var candlesFromDb = await _repository.GetRangeStream((exchange, pair, tf), minTimeStamp, maxTimeStamp)
            .ToListAsync();

        candlesFromDb.Count.Should().Be(candles.Length);
        candlesFromDb.Should().BeEquivalentTo(candles.Select(x => x.ToModel()));
    }

    [Test, CustomAutoData]
    public async Task GetMaxTimestamp_ReturnCorrectMaxTimeStamp(
        [Frozen] TimeFrame tf,
        [Length(2, 5)] CandleDbo[] candles,
        Exchange exchange,
        Pair pair)
    {
        var orderedCandles = candles.DistinctBy(x => x.TimeStamp).OrderBy(x => x.TimeStamp);
        long maxTimeStamp = 0;
        foreach (var candle in orderedCandles)
        {
            maxTimeStamp = Math.Max(candle.TimeStamp, maxTimeStamp);

            await _repository.SaveCandle(exchange, pair, candle);

            var maxValue = await _repository.GetMaxTimestamp(exchange, pair, tf);
            maxValue.Should().Be(maxTimeStamp);
        }
    }

    [Test, CustomAutoData]
    public async Task IsOldDataSaved_ReturnsTrueIfOldDataSaved(Exchange exchange, Pair pair, TimeFrame timeFrame)
    {
        await _repository.SetOldDataSaved(exchange, pair, timeFrame);

        // Act
        var isOldDataSaved = await _repository.IsOldDataSaved(exchange, pair, timeFrame);

        // Assert
        isOldDataSaved.Should().BeTrue();
    }

    [Test, CustomAutoData]
    public async Task IsOldDataSaved_ReturnsFalseIfOldDataNotSaved(Exchange exchange, Pair pair, TimeFrame timeFrame)
    {
        // Act
        var isOldDataSaved = await _repository.IsOldDataSaved(exchange, pair, timeFrame);

        // Assert
        isOldDataSaved.Should().BeFalse();
    }

    private ICandleRepository _repository = null!;
}