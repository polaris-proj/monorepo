﻿using Confluent.Kafka;
using Domain;
using Domain.Patterns.Objects;
using Domain.StrategyProcessor;
using ExchangeConnectors.Connectors;
using Infrastructure.RTQConnector;
using Infrastructure.Tests;
using IODataModule.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace IoDataModule.Integrations.Tests.Services;

public class NewCandleEventHandlerTests : BaseTests
{
    [OneTimeSetUp]
    public void OneTimeSetup()
    {
        _kafkaParameters = App.Services.GetRequiredService<KafkaParameters>();
        _consumer = GetCandleConsumer();
        _consumer.Subscribe(Topic.Candle.ToString());
        var currentOffset = _consumer.Position(new TopicPartition(Topic.Candle.ToString(), new Partition(0)));
        if (!currentOffset.IsSpecial)
        {
            _consumer.Seek(new TopicPartitionOffset(Topic.Candle.ToString(), new Partition(0), Offset.End));
            _consumer.Commit();
        }

        SkipOldMessagesInKafka();
    }

    [OneTimeTearDown]
    public override void OneTimeTearDown()
    {
        _consumer.Dispose();
        base.OneTimeTearDown();
    }

    [SetUp]
    public override void Setup()
    {
        base.Setup();
    }

    [Test, CustomAutoData]
    public async Task ShouldSendNewCandlesToKafka(Pair[] pairs, TimeFrame[] timeFrames, Kline kline)
    {
        var ct = new CancellationTokenSource(TimeSpan.FromSeconds(60)).Token;

        Action<Kline> action = null!;

        var connector = new Mock<IInternalConnector>();
        connector.Setup(x => x.SubscribeOnNewKlines(It.IsAny<IReadOnlyList<Pair>>(),
                It.IsAny<IReadOnlyList<TimeFrame>>(), It.IsAny<Action<Kline>>()))
            .Callback(new Action<IReadOnlyList<Pair>, IReadOnlyList<TimeFrame>, Action<Kline>>((_, _, x) =>
                action = x));


        var handler = new NewCandleEventHandler(connector.Object, Exchange.BinanceSpot, pairs, timeFrames,
            _kafkaParameters, new LoggerFactory());

        await handler.StartAsync();

        connector.Verify(x => x.SubscribeOnNewKlines(pairs, timeFrames, It.IsAny<Action<Kline>>()));

        action(kline);

        await Task.Run(() =>
        {
            var candleFromKafke = _consumer.Consume(ct);
            _consumer.Commit();
            candleFromKafke.Message.Value.Candle.Should().BeEquivalentTo(kline as Candle);
            candleFromKafke.Message.Value.Pair.Should().Be(kline.Pair);
            candleFromKafke.Message.Value.TimeFrame.Should().Be(kline.TimeFrame);
        }, ct);
    }

    private IConsumer<Null, NewCandleEvent> GetCandleConsumer()
    {
        var cc = _kafkaParameters.GetClientConfig();
        var config = new ConsumerConfig(cc)
        {
            GroupId = $"{Topic.Candle.ToString()}_consumer",
            AutoOffsetReset = AutoOffsetReset.Latest,
            AllowAutoCreateTopics = true,
            EnableAutoCommit = false
        };
        return new ConsumerBuilder<Null, NewCandleEvent>(config)
            .SetValueDeserializer(new MyProtoDeserializer<NewCandleEvent>())
            .Build();
    }

    private void SkipOldMessagesInKafka()
    {
        var isSmthCommited = false;
        while (true)
        {
            var ct = new CancellationTokenSource(TimeSpan.FromSeconds(10)).Token;
            try
            {
                _consumer.Consume(ct);
                isSmthCommited = true;
            }
            catch (OperationCanceledException)
            {
                break;
            }
        }

        if (isSmthCommited)
        {
            _consumer.Commit();
        }
    }

    private KafkaParameters _kafkaParameters = null!;
    private IConsumer<Null, NewCandleEvent> _consumer = null!;
}