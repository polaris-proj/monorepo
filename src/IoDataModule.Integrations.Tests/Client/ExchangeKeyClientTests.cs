﻿using System.ComponentModel.DataAnnotations;
using Grpc.Core;
using Grpc.Net.Client;
using Infrastructure.Tests;
using IODataModule.Client;
using IODataModule.Client.Clients;
using IODataModule.Infrastructure.DAL;
using IODataModule.Shared.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace IoDataModule.Integrations.Tests.Client;

public class UserExchangeKeysClientTests : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();

        DropTables().GetAwaiter().GetResult();
        Scope.ServiceProvider.GetRequiredService<Migrations>().Start().GetAwaiter().GetResult();

        var contextFactory = Scope.ServiceProvider.GetRequiredService<IDbContextFactory<IoDataModuleContext>>();

        var context = contextFactory.CreateDbContext();
        context.UserExchangeKeys.RemoveRange(context.UserExchangeKeys);

        var channel = GrpcChannel.ForAddress("http://localhost:9999", new GrpcChannelOptions
        {
            MaxReceiveMessageSize = null,
        });
        var wrappedChannel = new IoDataModuleClientChannel {Channel = channel};
        _repository = new UserExchangeKeysClient(wrappedChannel);
    }

    [TearDown]
    public async Task ClearDataBase()
    {
        await DropTables();
        base.TearDown();
    }

    [Test]
    [CustomAutoData]
    public async Task UpsertExchangeKey_ShouldUpdateExchangeKey(
        Guid userId,
        ExchangeKeyDto firstExchangeKey,
        ExchangeKeyDto secondExchangeKey
    )
    {
        secondExchangeKey.Exchange = firstExchangeKey.Exchange;
        await _repository.AddOrUpdateExchangeKey(userId, firstExchangeKey.Exchange, firstExchangeKey.Key,
            firstExchangeKey.Secret);

        var result = await _repository.GetExchangeKey(userId, firstExchangeKey.Exchange);
        result.Should().Be(firstExchangeKey);

        await _repository.AddOrUpdateExchangeKey(userId, secondExchangeKey.Exchange, secondExchangeKey.Key,
            secondExchangeKey.Secret);

        var resultAfter = await _repository.GetExchangeKey(userId, firstExchangeKey.Exchange);
        resultAfter.Should().Be(secondExchangeKey);
    }

    [Test]
    [CustomAutoData]
    public async Task UpsertExchangeKeyAndGetExchangeKeysAsync_ShouldReturnCorrectExchangeKey(
        Guid userId,
        [Length(5, 20)] ExchangeKeyDto[] exchangeKeys
    )
    {
        exchangeKeys = exchangeKeys.DistinctBy(x => x.Exchange).ToArray();
        await Task.WhenAll(exchangeKeys.Select(x =>
            _repository.AddOrUpdateExchangeKey(userId, x.Exchange, x.Key, x.Secret)));

        foreach (var key in exchangeKeys)
        {
            var result = await _repository.GetExchangeKey(userId, key.Exchange);
            result.Should().Be(key);
        }
    }

    [Test]
    [CustomAutoData]
    public async Task UpsertExchangeKeyAndGetAllExchangeKeysAsync_ShouldReturnsAllExchangeKey(
        Guid userId,
        [Length(5, 10)] ExchangeKeyDto[] exchangeKeys
    )
    {
        exchangeKeys = exchangeKeys.DistinctBy(x => x.Exchange).ToArray();

        await Task.WhenAll(exchangeKeys.Select(x =>
            _repository.AddOrUpdateExchangeKey(userId, x.Exchange, x.Key, x.Secret)));

        var result = (await _repository.GetAllExchangeKeys(userId)).ToArray();

        exchangeKeys.Length.Should().Be(result.Length);
        result.Should().BeEquivalentTo(exchangeKeys);
    }

    [Test]
    [CustomAutoData]
    public async Task RemoveExchangeKeyAsync_ShouldRemoveKey(
        Guid userId,
        ExchangeKeyDto firstExchangeKey
    )
    {
        await _repository.AddOrUpdateExchangeKey(userId, firstExchangeKey.Exchange, firstExchangeKey.Key,
            firstExchangeKey.Secret);

        var result = await _repository.GetExchangeKey(userId, firstExchangeKey.Exchange);
        result.Should().Be(firstExchangeKey);

        await _repository.RemoveExchangeKey(userId, firstExchangeKey.Exchange);

        var getKey = () => _repository.GetExchangeKey(userId, firstExchangeKey.Exchange);

        await getKey.Should().ThrowExactlyAsync<RpcException>("Key not found");
    }

    private IUserExchangeKeysClient _repository = null!;
}