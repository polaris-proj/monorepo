﻿using System.Text.Json;
using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;
using Grpc.Net.Client;
using Infrastructure.FluentAssertions;
using IODataModule.Client;
using IODataModule.Client.Clients;
using IODataModule.Infrastructure.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MoreLinq.Extensions;

namespace IoDataModule.Integrations.Tests.Client;

public class RemoteSpotConnectorTests : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();

        DropTables().GetAwaiter().GetResult();
        Scope.ServiceProvider.GetRequiredService<Migrations>().Start().Wait();

        var contextFactory = Scope.ServiceProvider.GetService<IDbContextFactory<IoDataModuleContext>>() ??
                             throw new InvalidOperationException();

        var context = contextFactory.CreateDbContext();
        context.UserExchangeKeys.RemoveRange(context.UserExchangeKeys);
        
        var key = "MTPsvWG2ZP7WvfZFgxO2oLAxt9jt7okSSJaqi6DxIXR8TkBZXXhJMLMi7rjzv56S";
        var secret = "4ednBqrrW9lhA1m8mB3GVYuL6p5XJtBCxS0JKkqhdQQdCEyfO2mS5mHcEpvkGz0m";
        var userId = Guid.NewGuid();

        var channel = GrpcChannel.ForAddress("http://localhost:9999", new GrpcChannelOptions
        {
            MaxReceiveMessageSize = null,
            MaxSendMessageSize = null,
        });
        var wrappedChannel = new IoDataModuleClientChannel {Channel = channel};
        var userExchangeKeys = new UserExchangeKeysClient(wrappedChannel);

        userExchangeKeys.AddOrUpdateExchangeKey(userId, Exchange.BinanceSpot, key, secret).GetAwaiter().GetResult();

        _connector = new RemoteSpotConnector(userId, Exchange.BinanceSpot, wrappedChannel);
    }

    [TearDown]
    public async Task ClearDataBase()
    {
        await _connector.DisposeAsync();
        await DropTables();
        base.TearDown();
    }


    [Test]
    public async Task GetPrices_ReturnSomeValues()
    {
        var priceByTicker = await _connector.GetPrices();
        priceByTicker.Should().NotBeEmpty();
        priceByTicker.Keys.ForEach(x =>
            x.ToString().Should().NotBeNullOrWhiteSpace().And.NotBeEmpty().And.Contain("/"));
        priceByTicker.Values.ForEach(x => x.Should().BePositive());
    }

    [TestCase(MarketOrder.Buy, 0.001)]
    [TestCase(MarketOrder.Sell, 0.001)]
    public async Task CreateMarketOrder_CreateOrderSuccessfully(MarketOrder orderType, decimal amount)
    {
        var order = await _connector.CreateMarketOrder(_btcUsdt, orderType, amount);
        order.Should().NotBeNull();
        order.Id.Should().NotBeNull();
    }

    private static IEnumerable<object[]> LimitOrderTestData()
    {
        yield return new object[] {LimitOrder.BuyLimit, new PlacementOrder(10_000, 0.01m)};
        yield return new object[] {LimitOrder.SellLimit, new PlacementOrder(100_000, 0.01m)};
    }

    [TestCaseSource(nameof(LimitOrderTestData))]
    public async Task CreateLimitOrder_CreateOrderSuccessfully(LimitOrder orderType, PlacementOrder placementOrder)
    {
        if (orderType == LimitOrder.SellLimit)
        {
            await _connector.CreateMarketOrder(_btcUsdt, MarketOrder.Buy,
                placementOrder.Amount); // buy to create limit sell
        }

        var order = await _connector.CreateLimitOrder(_btcUsdt, orderType, placementOrder);
        order.Should().NotBeNull();
        order.Id.Should().NotBeNull();
    }

    [TestCase(MarketOrder.Buy, 0.001)]
    [TestCase(MarketOrder.Sell, 0.001)]
    public async Task GetOrderInfo_ShouldReturnValidDataForMarketOrder(MarketOrder orderType, decimal amount)
    {
        var order = await _connector.CreateMarketOrder(_btcUsdt, orderType, amount);

        var orderInfo = await _connector.GetOrderInfo(_btcUsdt, order);
        orderInfo.OrderType.Should().Be(orderType.ToOrderType());
        orderInfo.Price.Should().BePositive();
        orderInfo.Amount.Should().BeInRadius(amount);
        orderInfo.FilledAmount.Should().BeInRadius(amount);
        orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
        orderInfo.CreateTime.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromHours(1));
        orderInfo.UpdateTime.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromHours(1));
    }

    [TestCaseSource(nameof(LimitOrderTestData))]
    public async Task GetOrderInfo_ShouldReturnValidDataForLimitOrder(LimitOrder orderType,
        PlacementOrder placementOrder)
    {
        if (orderType == LimitOrder.SellLimit)
        {
            await _connector.CreateMarketOrder(_btcUsdt, MarketOrder.Buy,
                placementOrder.Amount); // buy to create limit sell
        }

        var order = await _connector.CreateLimitOrder(_btcUsdt, orderType, placementOrder);

        var orderInfo = await _connector.GetOrderInfo(_btcUsdt, order);
        orderInfo.OrderType.Should().Be(orderType.ToOrderType());
        orderInfo.Price.Should().BeInRadius(placementOrder.Price);
        orderInfo.Amount.Should().BeInRadius(placementOrder.Amount);
        orderInfo.CreateTime.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromHours(1));
        orderInfo.UpdateTime.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromHours(1));
    }

    [TestCase(LimitOrder.BuyLimit)]
    [TestCase(LimitOrder.SellLimit)]
    public async Task CloseOrder_ShouldReturnValidDataForLimitOrder(LimitOrder orderType)
    {
        if (orderType == LimitOrder.SellLimit)
        {
            await _connector.CreateMarketOrder(_btcUsdt, MarketOrder.Buy,
                0.001m); // buy to create limit sell
        }


        var price = (double) (await _connector.GetPrices()).Single(x => x.Key == _btcUsdt).Value;
        price *= orderType == LimitOrder.BuyLimit ? 0.7 : 1.3;

        var order = await _connector.CreateLimitOrder(_btcUsdt, orderType,
            new PlacementOrder((int) price, 0.001m));

        var orderInfo = await _connector.GetOrderInfo(_btcUsdt, order);
        orderInfo.OrderStatus.Should().Be(OrderStatus.Open);

        await _connector.CancelOrder(_btcUsdt, order);

        var orderInfoAfterClose = await _connector.GetOrderInfo(_btcUsdt, order);
        orderInfoAfterClose.OrderStatus.Should().Be(OrderStatus.Close);
    }

    [Test]
    public async Task GetOrderInfo_UnknownId_ThrowException()
    {
        var getOrderInfo = async () =>
            await _connector.GetOrderInfo(_btcUsdt, new OrderId(Guid.NewGuid().ToString()));

        await getOrderInfo.Should().ThrowAsync<Exception>("Order Not Found");
    }

    [Test]
    public async Task GetCoinsAmount()
    {
        var coinsAmount = await _connector.GetCoinsAmount();
        coinsAmount.Should().NotBeEmpty();
        coinsAmount.ForEach(x =>
        {
            x.Asset.Name.Should().NotBeNullOrEmpty();
            x.Asset.Name.Length.Should().BeGreaterOrEqualTo(1);
            x.Available.Should().BeGreaterOrEqualTo(0);
            x.Locked.Should().BeGreaterOrEqualTo(0);
            x.Total.Should().BePositive();
        });
    }

    [Test]
    [NonParallelizable]
    public async Task GetOrdersPerPair_ShouldReturnTrades() //todo тест проходит только в дебаге
    {
        var trades = await _connector.GetOrdersPerPair(_btcUsdt);
        var amount = trades.Count;

        await _connector.CreateMarketOrder(_btcUsdt, MarketOrder.Buy, 0.001m);
        await Task.Delay(TimeSpan.FromSeconds(10));
        var newTrades = await _connector.GetOrdersPerPair(_btcUsdt);
        newTrades.Count.Should().BeGreaterOrEqualTo(amount + 1);
        var lastOrder = newTrades.Last();
        lastOrder.OrderType.Should().Be(OrderType.Buy);
        lastOrder.Amount.Should().Be(0.001m);
    }


    private static IEnumerable<object[]> GetCandlesTestData()
    {
        return new[]
        {
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h1, 24},
            new object[] {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h4, 6},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h12, 2},
            new object[] {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.D1, 1},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.m30, 48},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.m15, 96},
        };
    }

    [TestCaseSource(nameof(GetCandlesTestData))]
    public async Task GetCandles_ByPreviousDay_ShouldReturn24H1Candle(DateTimeRange range, TimeFrame timeFrame,
        int candleAmount)
    {
        var candles = await _connector.GetCandles(_btcUsdt, timeFrame, range);

        candles.Count.Should().Be(candleAmount);
    }

    //  [OneTimeSetUp]
    [Explicit]
    public async Task PrepairAccount()
    {
        var coinsAmount = await _connector.GetCoinsAmount();
        foreach (var coins in coinsAmount.Where(x => x.Asset != UsdtTicker && x.Available > 0))
        {
            try
            {
                await _connector.CreateMarketOrder(new Pair(coins.Asset, UsdtTicker), MarketOrder.Sell,
                    coins.Available);
            }
            catch
            {
                TestContext.WriteLine($"Smth went wrong {coins.Asset}");
            }
        }
    }

    [Test]
    [Explicit]
    public async Task SaveAllCandles()
    {
       var tfs = Enum.GetValues<TimeFrame>().Where(x=>x>0).ToArray();

       var pairs = (await _connector.GetPrices()).Select(x=>x.Key);

       foreach (var pair in pairs)
       {
           foreach (var timeFrame in tfs)
           {
               var candles = await _connector.GetCandles(pair, timeFrame,
                   new DateTimeRange(new DateTime(2017, 1, 1), new DateTime(2024, 3, 1)));

               var json = JsonSerializer.Serialize(candles);
               await File.WriteAllTextAsync($"{pair.ToStringWithOutSlash()}_{timeFrame.ToString()}.json", json);
           }
       }


    }

    private readonly Pair _btcUsdt = new(BtcTicker, UsdtTicker);
    private static readonly Ticker UsdtTicker = new("USDT");
    private static readonly Ticker BtcTicker = new("BTC");


    private RemoteSpotConnector _connector = null!;
}