﻿using System.ComponentModel.DataAnnotations;
using AutoFixture.NUnit3;
using IODataModule.Infrastructure.DAL;
using IODataModule.Infrastructure.DAL.Repositories;
using IODataModule.Shared.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace IoDataModule.Integrations.Tests;

public class UserExchangeKeysRepositoryTests : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();

        DropTables().GetAwaiter().GetResult();
        Scope.ServiceProvider.GetRequiredService<Migrations>().Start().GetAwaiter().GetResult();
        
        var contextFactory = Scope.ServiceProvider.GetService<IDbContextFactory<IoDataModuleContext>>() ??
                      throw new InvalidOperationException();

        var context = contextFactory.CreateDbContext();
        context.UserExchangeKeys.RemoveRange(context.UserExchangeKeys);

        _repository = App.Services.GetService<IUserExchangeKeysRepository>() ??
                      throw new InvalidOperationException();
    }

    [TearDown]
    public async Task ClearDataBase()
    {
        await DropTables();
        base.TearDown();
    }

    [Test]
    [AutoData]
    public async Task UpsertExchangeKey_ShouldUpdateExchangeKey(
        Guid userId,
        ExchangeKeyDto firstExchangeKey,
        ExchangeKeyDto secondExchangeKey
    )
    {
        secondExchangeKey.Exchange = firstExchangeKey.Exchange;
        var ct = new CancellationTokenSource(TimeSpan.FromSeconds(500)).Token;
        await _repository.UpsertExchangeKeyAsync(userId, firstExchangeKey, ct);

        var result = await _repository.GetExchangeKeyAsync(userId, firstExchangeKey.Exchange);
        result.Should().Be(firstExchangeKey);

        await _repository.UpsertExchangeKeyAsync(userId, secondExchangeKey, ct);

        var resultAfter = await _repository.GetExchangeKeyAsync(userId, firstExchangeKey.Exchange);
        resultAfter.Should().Be(secondExchangeKey);
    }

    [Test]
    [AutoData]
    public async Task UpsertExchangeKeyAndGetExchangeKeysAsync_ShouldReturnCorrectExchangeKey(
        Guid userId,
        [Length(5, 20)] ExchangeKeyDto[] exchangeKeys
    )
    {
        var ct = new CancellationTokenSource(TimeSpan.FromSeconds(5)).Token;
        await Task.WhenAll(exchangeKeys.Select(x => _repository.UpsertExchangeKeyAsync(userId, x, ct)));

        foreach (var key in exchangeKeys)
        {
            var result = await _repository.GetExchangeKeyAsync(userId, key.Exchange);
            result.Should().Be(key);
        }
    }

    [Test]
    [AutoData]
    public async Task UpsertExchangeKeyAndGetAllExchangeKeysAsync_ShouldReturnsAllExchangeKey(
        Guid userId,
        [Length(5, 20)] ExchangeKeyDto[] exchangeKeys
    )
    {
        var ct = new CancellationTokenSource(TimeSpan.FromSeconds(5)).Token;
        await Task.WhenAll(exchangeKeys.Select(x => _repository.UpsertExchangeKeyAsync(userId, x, ct)));

        var result = (await _repository.GetAllExchangeKeysAsync(userId)).ToArray();

        exchangeKeys.Length.Should().Be(result.Length);
        result.Should().BeEquivalentTo(exchangeKeys);
    }

    [Test]
    [AutoData]
    public async Task RemoveExchangeKeyAsync_ShouldRemoveKey(
        Guid userId,
        ExchangeKeyDto firstExchangeKey
    )
    {
        var ct = new CancellationTokenSource(TimeSpan.FromSeconds(500)).Token;
        await _repository.UpsertExchangeKeyAsync(userId, firstExchangeKey, ct);

        var result = await _repository.GetExchangeKeyAsync(userId, firstExchangeKey.Exchange);
        result.Should().Be(firstExchangeKey);

        await _repository.RemoveExchangeKeyAsync(userId, firstExchangeKey.Exchange, ct);

        var resultAfter = await _repository.GetExchangeKeyAsync(userId, firstExchangeKey.Exchange);
        resultAfter.Should().BeNull();
    }

    private IUserExchangeKeysRepository _repository = null!;
}