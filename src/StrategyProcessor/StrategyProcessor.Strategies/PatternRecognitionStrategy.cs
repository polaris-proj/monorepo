﻿using Domain.Connectors;
using Domain.Patterns.Objects;
using Domain.StrategyProcessor;

namespace StrategyProcessor.Strategies
{
    public record PatternRecognitionStrategy : BaseStrategy<IFuturesConnector>
    {
        [StrategyParameter("takeProfitMultiplier")] public decimal TakeProfitMultiplier { get; set; } = 0.5m;
        private OrderId? _activeOrder;
        
        public override async Task EventsCatalog_NewPattern(NewPatternEvent obj)
        {
            var usedCandles = obj.RecognitionResult.UsedCandles;
            var patternType = obj.RecognitionResult.MostProbablePattern;

            if (!obj.RecognitionResult.IsSuccessful || usedCandles.Length < 5 || patternType == PatternType.NoPattern)
                return;

            var highestPrice = usedCandles.Max(c => c.Close);
            var lowestPrice = usedCandles.Min(c => c.Close);
            var patternLength = highestPrice - lowestPrice;

            var point2Price = usedCandles[1].Close;
            var point4Price = usedCandles[3].Close;
            var point5Price = usedCandles[4].Close;

            var takeProfitPrice = patternType switch
            {
                PatternType.UpImpulseOrDiagExpand or PatternType.UpDiagShrink => point4Price - patternLength * TakeProfitMultiplier,
                PatternType.DownDiagShrink or PatternType.DownImpulseOrDiagExpand => point4Price + patternLength * TakeProfitMultiplier,
                _ => 0m
            };

            var stopLossPrice = point5Price;

            var orderSide = patternType switch
            {
                PatternType.UpImpulseOrDiagExpand or PatternType.UpDiagShrink => FuturesOrder.Short,
                PatternType.DownImpulseOrDiagExpand or PatternType.DownDiagShrink => FuturesOrder.Long,
                _ => FuturesOrder.Long
            };
            
            // TODO implement entry price when 2-4 line crossing, not just 4 point
            var entryPrice = point4Price;

            var total = GetOrderTotal();
            var amount = total / entryPrice;

            try
            {
                if (_activeOrder is not null)
                {
                    await Connector.CancelOrder(obj.Pair, _activeOrder);
                    _activeOrder = null;
                }
            }
            catch (Exception)
            {
                
            }

            _activeOrder = await Connector.CreateFuturesOrder(obj.Pair, orderSide, 1,
                new PlacementOrder(entryPrice, amount),
                new PlacementOrder(takeProfitPrice, amount),
                new PlacementOrder(stopLossPrice, amount));
        }

        private static decimal GetOrderTotal()
        {
            return 100;
        }

        public override LifeTime LifeTime => LifeTime.SingleInstancePerExchangePairTimeframe;
    }
}