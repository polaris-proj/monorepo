﻿using Domain.Connectors;
using Domain.Patterns.Objects;
using Domain.StrategyProcessor;
using QuantConnect.Indicators;
using static StrategyProcessor.Strategies.IndicatorDataPointToDomainDataPointMapper.OHLCV;

namespace StrategyProcessor.Strategies;

public record ExampleEmaStrategy : BaseStrategy<ISpotConnector>
{
    public override LifeTime LifeTime => LifeTime.SingleInstance;

    [StrategyParameter("longMovingPeriod")]
    public int LongMovingPeriod { get; set; } = 180;

    [StrategyParameter("shortMovingPeriod")]
    public int ShortMovingPeriod { get; set; } = 30;

    // define a small tolerance on our checks to avoid bouncing
    [StrategyParameter("Tolerance")] public decimal Tolerance { get; set; } = 0.00015m;


    public override void Start()
    {
        _fast = new ExponentialMovingAverage(ShortMovingPeriod);
        _slow = new ExponentialMovingAverage(LongMovingPeriod);
        Status = StrategyStatus.Opened;
    }

    public override async Task EventsCatalog_NewCandle(NewCandleEvent obj)
    {
        this.Update(_fast, $"Ema {ShortMovingPeriod}", "Description1", obj, Close);
        this.Update(_slow, $"Ema {LongMovingPeriod}", "Description2", obj, Close);

        if (!_slow.IsReady) return;

        var holdings = (await Connector.GetCoinsAmount()).FirstOrDefault(x => x.Asset == obj.Pair.First)?.Available??0;

        // we only want to go long if we're currently short or flat
        if (holdings <= 0)
        {
            // if the fast is greater than the slow, we'll go long
            if (_fast > _slow * (1 + Tolerance))
            {
                var toBuy = 0.001m;
              //  Console.WriteLine($"BUY >> {obj.Pair} amount: {toBuy}");
                await Connector.CreateMarketOrder(obj.Pair, MarketOrder.Buy, toBuy);
            }
        }

        // we only want to liquidate if we're currently long
        // if the fast is less than the slow we'll liquidate our long
        if (holdings > 0 && _fast < _slow)
        {
           // Console.WriteLine($"Sell >> {obj.Pair} amount: {holdings}");
            await Connector.CreateMarketOrder(obj.Pair, MarketOrder.Sell, 0.01m);
        }
    }

    private ExponentialMovingAverage _fast = null!;
    private ExponentialMovingAverage _slow = null!;

    /*
        private bool IsHighCrossLow()
        {
            return _maHigh.Count > 1 && _maHigh[^1] >= _maLow[^1] && _maHigh[^0] <= _maLow[^0];
        }

        private bool IsLowCrossHigh()
        {
            return _maHigh.Count > 1 && _maHigh[^1] <= _maLow[^1] && _maHigh[^0] >= _maLow[^0];
        }

        private bool IsTouch()
        {
            return _maHigh.Count > 0 && _maHigh[^0] == _maLow[^0];
        }

        public void EntryHandler(NewCandleEvent candle) // на входе Event class, который содержит много разной инфы
        {
            prevlastCandle = lastCandle;
            lastCandle = candle;
            foreach(var deal in deals.Where(x=>x.Status!= Status.Close))
            {
                var tk = deal.Takes;
                var st = deal.Stops;
                var ent = deal.Entryes;
                if (deal.TimeFrame == TimeFrames.TimeFrame.D1)
                {
                    var entry = Tools.GetNearestLevels(TimeFrames.TimeFrame.D1, Direction.Both, dot, 2, LevelType.Value);
                    var takes = Tools.GetNearestLevels(TimeFrames.TimeFrame.h4, Direction.Up, dot, 5, LevelType.Struct);
                    var stops = Tools.GetNearestLevels(TimeFrames.TimeFrame.h1, Direction.Down, dot, 10, LevelType.Value);

                    if (CheckRiskProfitFactor(entry, takes, stops))
                    {
                        //SendOrder();
                    }
                }
            }
        }

        public void StopHandler(Dot dot)
        {
            //Реализация динамического стопа
            //Например, сделка выполнена на Н4 , я хочу пододвигать стоп ,при образовании на
            //меньшем тф (Н1) слома или пп.
            //я подписываюсь на событие обнаружения пп\слома на этой монете,
            //далее я проверяю ,что этот уровень выше ,чем текущий стоп и ниже текущей цены?

            //тогда я отправляю запрос на биржу на изменение цены стопа
            //но как мне отправить запрос на биржу?
            //как биржа поймет какой именно ордер надо менять?
            //я где-то храню ид ордеров?
            //мне где-то нужно хранить локальные копии всех сделок и их ид на бирже,
            //но как я буду синхронизировать их с биржей?
            //я могу получить с биржи все заявки, но мне это не поможет
            //т.к не получится сопоставить заявку с конкретным ордером
            //поэтому я буду локально хранить ордер
            //ордер в этом контексте будет называться группа из заявок на покупку и продажу
            //связанные в рамках одной торговой ситуации
            //далее имея их id я могу поддерживать их в актуальном состоянии при необходимости
            //перед тем как двигать стоп или ещё что-то я буду сначала обновлять состояние сделок
            //в текущем ордере т.е должен быть список локальных ордеров.
            //и только после этого принимать решение об сдвигании.
        }

        public void TakeHandler()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Order> GetStops()
        {
            throw new NotImplementedException();
        }

        private static bool CheckRiskProfitFactor(decimal entry, decimal take, decimal stop)
        {
            return Math.Abs(take - entry) / Math.Abs(entry - stop) > 2;
        }

        private static bool CheckRiskProfitFactor(List<decimal> entry, List<decimal> take, List<decimal> stop)
        {
            var entryAvg = entry.Sum() / entry.Count;
            var takeAvg = take.Sum() / take.Count;
            var stopAvg = stop.Sum() / stop.Count;

            return CheckRiskProfitFactor(entryAvg, takeAvg, stopAvg);
        }
        */
}