﻿using System.Text.Json;
using Domain.Connectors;
using Domain.StrategyProcessor;
using Infrastructure.Json;
using Microsoft.Extensions.DependencyInjection;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories;
using StrategyProcessor.Services;

namespace StrategyProcessor.Integrations.Tests;

public class AddDeclaredStrategiesToDatabaseTests : BaseTests
{
    [SetUp] 
    public override void Setup()
    {
        base.Setup();
        _strategyRepository = Scope.ServiceProvider.GetService<IStrategyRepository>() ??
                              throw new InvalidOperationException();
    }

    [Test]
    public async Task ServiceShouldAddStrategiesToDatabase_OneStrategyOneProperty()
    {
        var mock = new Mock<IDarkMagic>();
        mock.Setup(x => x.GetAllStrategiesTypes()).Returns(new[] {typeof(AddDeclaredStrategiesToDatabaseStrategy)});
        var properties = new Dictionary<string, Property>
        {
            {"Prop1", new Property(typeof(string), "str")}
        };
        mock.Setup(x => x.GetStrategyProperties(It.IsAny<Type>())).Returns(properties);

        var service = new AddDeclaredStrategiesToDatabase(_strategyRepository, mock.Object);
        await service.Start();

        var strategyDbo = await _strategyRepository.ReadByNameAsync(typeof(AddDeclaredStrategiesToDatabaseStrategy).FullName!);

        CompareStrategyDbo(strategyDbo, typeof(AddDeclaredStrategiesToDatabaseStrategy), properties);
    }
    
    [Test]
    public async Task ServiceShouldAddStrategiesToDatabase_OneStrategyOneProperty_AfterUpdateWithTwoProps()
    {
        var mock = new Mock<IDarkMagic>();
        mock.Setup(x => x.GetAllStrategiesTypes()).Returns(new[] {typeof(AddDeclaredStrategiesToDatabaseStrategy)});
        var properties = new Dictionary<string, Property>
        {
            {"Prop1", new Property(typeof(string), "str")}
        };
        mock.Setup(x => x.GetStrategyProperties(It.IsAny<Type>())).Returns(properties);

        var service = new AddDeclaredStrategiesToDatabase(_strategyRepository, mock.Object);
        await service.Start();

        var strategyDbo = await _strategyRepository.ReadByNameAsync(typeof(AddDeclaredStrategiesToDatabaseStrategy).FullName!);
        CompareStrategyDbo(strategyDbo, typeof(AddDeclaredStrategiesToDatabaseStrategy), properties);



        properties.Add("Prop2", new Property(typeof(int), 12));
        mock.Setup(x => x.GetStrategyProperties(It.IsAny<Type>())).Returns(properties);

        var serviceUpdated = new AddDeclaredStrategiesToDatabase(_strategyRepository, mock.Object);
        await serviceUpdated.Start();

        var strategyDboUpdated = await _strategyRepository.ReadByNameAsync(typeof(AddDeclaredStrategiesToDatabaseStrategy).FullName!);
        CompareStrategyDbo(strategyDboUpdated, typeof(AddDeclaredStrategiesToDatabaseStrategy), properties);
    }
    
    [Test]
    public async Task ServiceShouldAddStrategiesToDatabase_TwoStrategyDifferentProperties()
    {
        var mock = new Mock<IDarkMagic>();
        mock.Setup(x => x.GetAllStrategiesTypes()).Returns(new[] {typeof(AddDeclaredStrategiesToDatabaseStrategy), typeof(AddDeclaredStrategiesToDatabaseStrategyFutures)});
        var properties1 = new Dictionary<string, Property>
        {
            {"Prop1", new Property(typeof(string), "str")}
        };
        mock.Setup(x => x.GetStrategyProperties(It.Is<Type>(y=>y == typeof(AddDeclaredStrategiesToDatabaseStrategy)))).Returns(properties1);
        
        var properties2 = new Dictionary<string, Property>
        {
            {"Prop1", new Property(typeof(double), 10.1)},
            {"Prop2", new Property(typeof(long), 1090000L)}
        };
        mock.Setup(x => x.GetStrategyProperties(It.Is<Type>(y=>y == typeof(AddDeclaredStrategiesToDatabaseStrategyFutures)))).Returns(properties2);

        var service = new AddDeclaredStrategiesToDatabase(_strategyRepository, mock.Object);
        await service.Start();

        var strategyDbo = await _strategyRepository.ReadByNameAsync(typeof(AddDeclaredStrategiesToDatabaseStrategy).FullName!);
        CompareStrategyDbo(strategyDbo, typeof(AddDeclaredStrategiesToDatabaseStrategy), properties1);
        
        var strategyDbo2 = await _strategyRepository.ReadByNameAsync(typeof(AddDeclaredStrategiesToDatabaseStrategyFutures).FullName!);
        CompareStrategyDbo(strategyDbo2, typeof(AddDeclaredStrategiesToDatabaseStrategyFutures), properties2);
    }

    private void CompareStrategyDbo(StrategyDbo? strategyDbo, Type strategyType, Dictionary<string, Property> properties)
    {
        strategyDbo.Should().NotBeNull();
        strategyDbo!.InternalName.Should().Be(strategyType.FullName);
        
        strategyDbo.Properties.Count.Should().Be(properties.Count);

        var propsOrderedByName = properties.Select(x => (x.Key, x.Value)).OrderBy(x => x.Key).ToList();
        var strategyDboProps = strategyDbo.Properties.OrderBy(x => x.Name).ToList();

        for (var i = 0; i < strategyDboProps.Count; i++)
        {
            var fromDbo = strategyDboProps[i];
            var expected = propsOrderedByName[i];

            fromDbo.Name.Should().Be(expected.Key);

            fromDbo.TypeName.Should().Be(expected.Value.Type.FullName);

            Compare(fromDbo.ParameterValue, JsonSerializer.SerializeToDocument(expected.Value.Value, expected.Value.Type, JsonOptions.JsonSerializerOptions));
        }
    }

    private void Compare(JsonDocument first, JsonDocument second)
    {
        var t1 = new MemoryStream();
        var tt1 = new Utf8JsonWriter(t1);
        first.WriteTo(tt1);

        var t2 = new MemoryStream();
        var tt2 = new Utf8JsonWriter(t2);
        second.WriteTo(tt2);

        t1.ToArray().Should().BeEquivalentTo(t2.ToArray());
    }

    private IStrategyRepository _strategyRepository = null!;

    private record AddDeclaredStrategiesToDatabaseStrategy : BaseStrategy<ISpotConnector>;
    private record AddDeclaredStrategiesToDatabaseStrategyFutures : BaseStrategy<IFuturesConnector>;
} 