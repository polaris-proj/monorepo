﻿using System.Reflection;
using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;
using Infrastructure.Tests;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Infrastructure.DAL;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;
using StrategyProcessor.Infrastructure.Mappers;
using StrategyProcessor.Services;
using StrategyProcessor.Strategy;

namespace StrategyProcessor.Integrations.Tests;

public class StrategyManagerRestoreAfterFailureTests : BaseTests
{
    [SetUp] 
    public override void Setup()
    {
        base.Setup();
        ClearStrategyManagers();
    }
    
    //создаем и запускаем через StartStopStrategyService
    //через TradingStrategyManagerStorage / TestingStrategyManagerStorage засылаем эвенты
    //очищаем все стратегии в TradingStrategyManagerStorage / TestingStrategyManagerStorage, эмулируя перезапуск приложения
    //запускаем RestoreStrategyManagerState
    //проверяем содержимое TradingStrategyManagerStorage / TestingStrategyManagerStorage

    [Test]
    [CustomAutoData]
    public async Task ShouldRestoreStateOfRealStrategyAfterFail(Guid strategyId, Guid userId, DateTimeRange dateTimeRange)
    {
        var startStopService = Scope.ServiceProvider
            .GetRequiredService<StrategyService<TestStrategy1, ISpotConnector>>();
        var strategyParameters = new StartStrategyParameters(
            strategyId,
            userId,
            RunMode.Real,
            false,
            new List<ExchangePairTimeFrame> {new(Exchange.BinanceSpot, new Pair("1", "2"), TimeFrame.h1)},
            new List<StrategyParameterDbo>
            {
                new StrategyParameter<DateOnly>("Date", DateOnly.FromDateTime(DateTime.Now)).ToDbo(),
                new StrategyParameter<int>("Number", 312).ToDbo()
            },
            new List<Balance> {new Balance("USDT", 100, 0)},
            dateTimeRange);

        var runId = await startStopService.StartNewStrategyAsync(strategyParameters);
        var strategyManagerStorage = Scope.ServiceProvider.GetRequiredService<TradingStrategyManagerStorage>();
        strategyManagerStorage.StrategyManagers.Count.Should().Be(1);
        var strategyManager = strategyManagerStorage.StrategyManagers.First();
        strategyManager.RunId.Should().Be(runId);
        strategyManager.Should().BeOfType<StrategyManagerWithStoreInDb<TestStrategy1, ISpotConnector>>();

        ((List<IStrategyManager>) strategyManagerStorage.StrategyManagers).Clear();

        var restoreStrategyManagerState = CreateRestoreStrategyManagerState();
        await restoreStrategyManagerState.Start();

        strategyManagerStorage.StrategyManagers.Count.Should().Be(1);
        var strategyManagerRestored = strategyManagerStorage.StrategyManagers.First();
        strategyManagerRestored.Should().BeEquivalentTo(strategyManager);
    }
    
    [Test]
    [CustomAutoData]
    public async Task ShouldRestoreStateOfRealTimeTestStrategyAfterFail(Guid strategyId, Guid userId, DateTimeRange dateTimeRange)
    {
        var startStopService = Scope.ServiceProvider
            .GetRequiredService<StrategyService<TestStrategy1, ISpotConnector>>();
        var strategyParameters = new StartStrategyParameters(
            strategyId,
            userId,
            RunMode.RealDataTest,
            false,
            new List<ExchangePairTimeFrame> {new(Exchange.BinanceSpot, new Pair("1", "2"), TimeFrame.h1)},
            new List<StrategyParameterDbo>
            {
                new StrategyParameter<DateOnly>("Date", DateOnly.FromDateTime(DateTime.Now)).ToDbo(),
                new StrategyParameter<int>("Number", 312).ToDbo()
            },
            new List<Balance> {new Balance("USDT", 100, 0)},
            dateTimeRange);

        var runId = await startStopService.StartNewStrategyAsync(strategyParameters);
        var strategyManagerStorage = Scope.ServiceProvider.GetRequiredService<TestingStrategyManagerStorage>();
        strategyManagerStorage.StrategyManagers.Count.Should().Be(1);
        var strategyManager = strategyManagerStorage.StrategyManagers.First();
        strategyManager.RunId.Should().Be(runId);
        strategyManager.Should().BeOfType<StrategyManagerWithStoreInDb<TestStrategy1, ISpotConnector>>();

        ((List<IStrategyManager>) strategyManagerStorage.StrategyManagers).Clear();

        var restoreStrategyManagerState = CreateRestoreStrategyManagerState();
        await restoreStrategyManagerState.Start();

        strategyManagerStorage.StrategyManagers.Count.Should().Be(1);
        var strategyManagerRestored = strategyManagerStorage.StrategyManagers.First();
        strategyManagerRestored.Should().BeEquivalentTo(strategyManager);
    }

    private RestoreStrategyManagerState CreateRestoreStrategyManagerState()
    {
        var currentAssembly = Assembly.GetExecutingAssembly();
        var darkMagic = new DarkMagic(currentAssembly);
        
        var strategyManagerRepository = Scope.ServiceProvider.GetRequiredService<IStrategyManagerRepository>();
        var startStopStrategyServiceTypedFactory = Scope.ServiceProvider.GetRequiredService<IStartStopStrategyServiceTypedFactory>();
        
        return new RestoreStrategyManagerState(strategyManagerRepository, startStopStrategyServiceTypedFactory, darkMagic, new LoggerFactory());
    }
    
    [TearDown] 
    public override void TearDown()
    {
        ClearStrategyManagers();
        base.TearDown();
    }

    private void ClearStrategyManagers()
    {
        var context = Scope.ServiceProvider.GetRequiredService<StrategyProcessorContext>();
        context.StrategyManagers.RemoveRange(context.StrategyManagers);
        
        context.SaveChanges();
    }

    public record TestStrategy1 : BaseStrategy<ISpotConnector>
    {
        [StrategyParameter("Date")] public DateOnly Date { get; set; }
        [StrategyParameter("Number")] public int Num { get; set; }
    }
}