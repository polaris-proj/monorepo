﻿using Microsoft.Extensions.DependencyInjection;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

namespace StrategyProcessor.Integrations.Tests.RepositoryTests;

public class RunStrategyRepositoryTests : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        _repository = Scope.ServiceProvider.GetService<IRunStrategyRepository>() ??
                      throw new InvalidOperationException();
    }


    [Test]
    public async Task CreateAndReadAsync_ShouldReturnCorrectObject()
    {
        var dbo = CreateTestOfStrategyDbo();

        var id = await _repository.CreateAsync(dbo);

        var resultFromDb = await _repository.ReadAsync(id);
        resultFromDb.Should().BeEquivalentTo(dbo);
    }


    [Test]
    public async Task ReadAsync_ObjectNotExists_ShouldReturnNull()
    {
        (await _repository.ReadAsync(Guid.NewGuid())).Should().BeNull();
    }

    private RunOfStrategyDbo CreateTestOfStrategyDbo()
    {
        var runId = Guid.NewGuid();
        var dbo = new RunOfStrategyDbo
        {
            Id = runId,
            StrategyId = Guid.NewGuid()
        };
        return dbo;
    }

    private IRunStrategyRepository _repository = null!;
}