﻿using Microsoft.Extensions.DependencyInjection;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

namespace StrategyProcessor.Integrations.Tests.RepositoryTests;

public abstract class StrategyArtifactsBaseTests : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        RootRepository = Scope.ServiceProvider.GetRequiredService<IRunStrategyRepository>();
    }

    protected async Task<RunOfStrategyDbo> CreateTestOfStrategyDboAsync()
    {
        var runId = Guid.NewGuid();
        var dbo = new RunOfStrategyDbo
        {
            Id = runId,
            StrategyId = Guid.NewGuid()
        };

        await RootRepository.CreateAsync(dbo);
        
        return dbo;
    }

    protected IRunStrategyRepository RootRepository = null!;
}