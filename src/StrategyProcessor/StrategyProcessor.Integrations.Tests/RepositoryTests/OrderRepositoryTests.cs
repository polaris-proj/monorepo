﻿using AutoFixture;
using Domain;
using Domain.Connectors;
using FluentAssertions.Equivalency;
using Infrastructure;
using Infrastructure.Tests;
using Microsoft.Extensions.DependencyInjection;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Integrations.Tests.RepositoryTests;

public class OrderRepositoryTests : StrategyArtifactsBaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        _repository = Scope.ServiceProvider.GetRequiredService<IOrderRepository>();
    }

    [Test]
    public async Task AddOrdersAsync_ShouldAddOrdersToTest()
    {
        var dbo = await CreateTestOfStrategyDboAsync();
        var runId = dbo.Id;

        var pair1 = new Pair("BTC", "USD");
        var orders1 = CreateExpectedOrders(pair1).ToList();
        await _repository.AddOrdersAsync(runId, orders1);

        var pair2 = new Pair("ETH", "USD");
        var orders2 = CreateExpectedOrders(pair2).ToList();
        await _repository.AddOrdersAsync(runId, orders2);

        (await _repository.ReadAsync(runId, pair1, TimeFrame.h4, TimeConstants.WholeDataTimeRange)).Should().BeEquivalentTo(orders1, Comparer);
        (await _repository.ReadAsync(runId, pair2, TimeFrame.h4, TimeConstants.WholeDataTimeRange)).Should().BeEquivalentTo(orders2, Comparer);
    }

    [Test]
    public async Task CloseOrderAsync_ShouldUpdate()
    {
        var dbo = await CreateTestOfStrategyDboAsync();
        var runId = dbo.Id;

        var pair = new Pair("BTC", "USD");
        var order = CreateExpectedOrders(pair).First();
        await _repository.AddOrdersAsync(runId, new[] {order});
        (await _repository.ReadAsync(runId, pair, TimeFrame.h4, TimeConstants.WholeDataTimeRange)).Single().Should().BeEquivalentTo(order, Comparer);
        
        await _repository.CloseOrderAsync(new OrderId(order.OrderId));
        
        order.Status = OrderStatus.Close;
        (await _repository.ReadAsync(runId, pair, TimeFrame.h4, TimeConstants.WholeDataTimeRange)).Single().Should().BeEquivalentTo(order, Comparer);
    }

    protected virtual IEnumerable<OrderDto> CreateExpectedOrders(Pair pair)
    {
        var fixture = CustomAutoDataAttribute.CreateFixture();
        fixture.Customizations.Add(new CommonDbTools.UtcConverter(new RandomDateTimeSequenceGenerator(TimeConstants.Jan1St1970, DateTime.UtcNow)));

        return fixture.Build<OrderDto>()
            .With(x => x.Pair, pair)
            .CreateMany();
    }

    private static EquivalencyAssertionOptions<OrderDto> Comparer(
        EquivalencyAssertionOptions<OrderDto> c) =>
        c.Using<DateTime>(ctx => ctx.Subject.Should().BeCloseTo(ctx.Expectation, TimeSpan.FromMilliseconds(1)))
            .WhenTypeIs<DateTime>();


    private IOrderRepository _repository = null!;
}