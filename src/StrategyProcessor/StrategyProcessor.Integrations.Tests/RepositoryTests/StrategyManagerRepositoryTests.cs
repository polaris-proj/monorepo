﻿using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;
using Json.More;
using Microsoft.Extensions.DependencyInjection;
using MoreLinq;
using StrategyProcessor.Infrastructure.DAL;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;
using StrategyProcessor.Infrastructure.Mappers;
using StrategyProcessor.Services;
using StrategyProcessor.Strategy;

namespace StrategyProcessor.Integrations.Tests.RepositoryTests;

[NonParallelizable]
public class StrategyManagerRepositoryTests : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        _repository = Scope.ServiceProvider.GetRequiredService<IStrategyManagerRepository>();

        var context = Scope.ServiceProvider.GetRequiredService<StrategyProcessorContext>();
        context.StrategyManagers.RemoveRange(context.StrategyManagers);
        context.SaveChanges();
    }

    [Test]
    public async Task ReadAsync_NoObjectWithThisId_ReturnNull()
    {
        var model = await _repository.ReadAsync(Guid.NewGuid());
        model.Should().BeNull();
    }

    [Test]
    public async Task CreateAndRead_ReturnValidObject()
    {
        var strategyManager = CreateStrategyManagerDbo();
        var id = await _repository.CreateAsync(strategyManager);

        var model = await _repository.ReadAsync(id);
        CompareStrategyManager(model!, strategyManager);
    }

    [Test]
    public async Task CreateAndReadAll_ReturnValidObjects()
    {
        var strategyManagers = Enumerable.Repeat(0, 6)
            .Select(_ => CreateStrategyManagerDbo())
            .OrderBy(x => x.RunId)
            .ToArray();
        foreach (var strategyManager in strategyManagers)
        {
            await _repository.CreateAsync(strategyManager);
        }

        var models = (await _repository.ReadAll())
            .OrderBy(x => x.RunId)
            .ToArray();
        for (var i = 0; i < models.Length; i++)
        {
            var model = models[i];
            var strategyManager = strategyManagers[i];
            CompareStrategyManager(model, strategyManager);
        }
    }

    [Test]
    public async Task GetStateToHandle_ReturnAllStatesIfStrategiesDontHandleEventsMoreTime()
    {
        var strategyManagers = Enumerable.Range(0, 6)
            .Select(i => (i, CreateStrategyManagerDbo()))
            .Pipe(x =>
            {
                x.Item2.m1MaxHandledTimeStamp = x.i * 1000;
                x.Item2.m3MaxHandledTimeStamp = x.i * 1000 + 1;
                x.Item2.m5MaxHandledTimeStamp = x.i * 1000 + 2;
                x.Item2.m15MaxHandledTimeStamp = x.i * 1000 + 3;
                x.Item2.m30MaxHandledTimeStamp = x.i * 1000 + 4;
                x.Item2.h1MaxHandledTimeStamp = x.i * 1000 + 5;
                x.Item2.h4MaxHandledTimeStamp = x.i * 1000 + 6;
                x.Item2.h12MaxHandledTimeStamp = x.i * 1000 + 7;
                x.Item2.D1MaxHandledTimeStamp = x.i * 1000 + 8;
                x.Item2.D3MaxHandledTimeStamp = x.i * 1000 + 9;
                x.Item2.W1MaxHandledTimeStamp = x.i * 1000 + 10;
                x.Item2.Mo1MaxHandledTimeStamp = x.i * 1000 + 11;
            })
            .Select(x => x.Item2)
            .OrderBy(x => x.RunId)
            .ToArray();

        foreach (var strategyManager in strategyManagers)
        {
            await _repository.CreateAsync(strategyManager);
        }

        var pointerThatSelectAllStates = new TimeStampByTimeFramePointer()
        {
            m1TimeStamp = 999999,
            m3TimeStamp = 999999,
            m5TimeStamp = 999999,
            m15TimeStamp = 999999,
            m30TimeStamp = 999999,
            h1TimeStamp = 999999,
            h4TimeStamp = 999999,
            h12TimeStamp = 999999,
            D1TimeStamp = 999999,
            D3TimeStamp = 999999,
            W1TimeStamp = 999999,
            Mo1TimeStamp = 999999
        };

        await _repository.HandleStates(pointerThatSelectAllStates, x =>
        {
            var models = x.OrderBy(b => b.RunId).ToArray();
            models.Length.Should().Be(strategyManagers.Length);
            for (var i = 0; i < models.Length; i++)
            {
                var model = models[i];
                var strategyManager = strategyManagers[i];
                CompareStrategyManager(model, strategyManager);
            }

            return Task.FromResult(Enumerable.Empty<StrategyManagerDbo>());
        });
    }

    [Test]
    public async Task GetStateToHandle_ReturnAllStates()
    {
        var strategyManagers = Enumerable.Range(0, 6)
            .Select(i => (i, CreateStrategyManagerDbo()))
            .Pipe(x =>
            {
                x.Item2.m1MaxHandledTimeStamp = x.i * 1000;
                x.Item2.m3MaxHandledTimeStamp = x.i * 1000 + 1;
                x.Item2.m5MaxHandledTimeStamp = x.i * 1000 + 2;
                x.Item2.m15MaxHandledTimeStamp = x.i * 1000 + 3;
                x.Item2.m30MaxHandledTimeStamp = x.i * 1000 + 4;
                x.Item2.h1MaxHandledTimeStamp = x.i * 1000 + 5;
                x.Item2.h4MaxHandledTimeStamp = x.i * 1000 + 6;
                x.Item2.h12MaxHandledTimeStamp = x.i * 1000 + 7;
                x.Item2.D1MaxHandledTimeStamp = x.i * 1000 + 8;
                x.Item2.D3MaxHandledTimeStamp = x.i * 1000 + 9;
                x.Item2.W1MaxHandledTimeStamp = x.i * 1000 + 10;
                x.Item2.Mo1MaxHandledTimeStamp = x.i * 1000 + 11;
            })
            .Select(x => x.Item2)
            .OrderBy(x => x.RunId)
            .ToArray();

        foreach (var strategyManager in strategyManagers)
        {
            await _repository.CreateAsync(strategyManager);
        }

        var pointerThatSelectZeroStates = new TimeStampByTimeFramePointer
        {
            m1TimeStamp = 0,
            m3TimeStamp = 0,
            m5TimeStamp = 0,
            m15TimeStamp = 0,
            m30TimeStamp = 0,
            h1TimeStamp = 0,
            h4TimeStamp = 0,
            h12TimeStamp = 0,
            D1TimeStamp = 0,
            D3TimeStamp = 0,
            W1TimeStamp = 0,
            Mo1TimeStamp = 0
        };
        await _repository.HandleStates(pointerThatSelectZeroStates, x =>
        {
            x.Count().Should().Be(0);
            return Task.FromResult(Enumerable.Empty<StrategyManagerDbo>());
        });
    }

    [Test]
    public async Task GetStateToHandle_PointerSelectOneStateByMultipleShards_ReturnOneStates()
    {
        var strategyManagers = Enumerable.Range(0, 6)
            .Select(i => (i, CreateStrategyManagerDbo()))
            .Pipe(x =>
            {
                x.Item2.m1MaxHandledTimeStamp = x.i * 1000;
                x.Item2.m3MaxHandledTimeStamp = x.i * 1000 + 1;
                x.Item2.m5MaxHandledTimeStamp = x.i * 1000 + 2;
                x.Item2.m15MaxHandledTimeStamp = x.i * 1000 + 3;
                x.Item2.m30MaxHandledTimeStamp = x.i * 1000 + 4;
                x.Item2.h1MaxHandledTimeStamp = x.i * 1000 + 5;
                x.Item2.h4MaxHandledTimeStamp = x.i * 1000 + 6;
                x.Item2.h12MaxHandledTimeStamp = x.i * 1000 + 7;
                x.Item2.D1MaxHandledTimeStamp = x.i * 1000 + 8;
                x.Item2.D3MaxHandledTimeStamp = x.i * 1000 + 9;
                x.Item2.W1MaxHandledTimeStamp = x.i * 1000 + 10;
                x.Item2.Mo1MaxHandledTimeStamp = x.i * 1000 + 11;
            })
            .Select(x => x.Item2)
            .OrderBy(x => x.RunId)
            .ToArray();

        foreach (var strategyManager in strategyManagers)
        {
            await _repository.CreateAsync(strategyManager);
        }

        var pointerThatSelectOneStates = new TimeStampByTimeFramePointer
        {
            m1TimeStamp = 20,
            m3TimeStamp = 20,
            m5TimeStamp = 20,
            m15TimeStamp = 20,
            m30TimeStamp = 20,
            h1TimeStamp = 20,
            h4TimeStamp = 20,
            h12TimeStamp = 20,
            D1TimeStamp = 20,
            D3TimeStamp = 20,
            W1TimeStamp = 20,
            Mo1TimeStamp = 20
        };
        await _repository.HandleStates(pointerThatSelectOneStates, x =>
        {
            x.Count().Should().Be(1);
            return Task.FromResult(Enumerable.Empty<StrategyManagerDbo>());
        });
    }

    [Test]
    public async Task GetStateToHandle_PointerSelectOneStateByOneShards_ReturnOneStates()
    {
        var strategyManagers = Enumerable.Range(0, 6)
            .Select(i => (i, CreateStrategyManagerDbo()))
            .Pipe(x =>
            {
                x.Item2.m1MaxHandledTimeStamp = x.i * 1000;
                x.Item2.m3MaxHandledTimeStamp = x.i * 1000 + 1;
                x.Item2.m5MaxHandledTimeStamp = x.i * 1000 + 2;
                x.Item2.m15MaxHandledTimeStamp = x.i * 1000 + 3;
                x.Item2.m30MaxHandledTimeStamp = x.i * 1000 + 4;
                x.Item2.h1MaxHandledTimeStamp = x.i * 1000 + 5;
                x.Item2.h4MaxHandledTimeStamp = x.i * 1000 + 6;
                x.Item2.h12MaxHandledTimeStamp = x.i * 1000 + 7;
                x.Item2.D1MaxHandledTimeStamp = x.i * 1000 + 8;
                x.Item2.D3MaxHandledTimeStamp = x.i * 1000 + 9;
                x.Item2.W1MaxHandledTimeStamp = x.i * 1000 + 10;
                x.Item2.Mo1MaxHandledTimeStamp = x.i * 1000 + 11;
            })
            .Select(x => x.Item2)
            .OrderBy(x => x.RunId)
            .ToArray();

        foreach (var strategyManager in strategyManagers)
        {
            await _repository.CreateAsync(strategyManager);
        }

        var pointerThatSelectOneStates = new TimeStampByTimeFramePointer
        {
            m1TimeStamp = 20,
            m3TimeStamp = 1,
            m5TimeStamp = 1,
            m15TimeStamp = 1,
            m30TimeStamp = 1,
            h1TimeStamp = 1,
            h4TimeStamp = 1,
            h12TimeStamp = 1,
            D1TimeStamp = -1,
            D3TimeStamp = -1,
            W1TimeStamp = -1,
            Mo1TimeStamp = -1
        };
        await _repository.HandleStates(pointerThatSelectOneStates, x =>
        {
            x.Count().Should().Be(1);
            return Task.FromResult(Enumerable.Empty<StrategyManagerDbo>());
        });
    }

    [Test]
    public async Task GetStateToHandle_PointerSelectTwoStateByOneShards_ReturnOneStates()
    {
        var strategyManagers = Enumerable.Range(0, 6)
            .Select(i => (i, CreateStrategyManagerDbo()))
            .Pipe(x =>
            {
                x.Item2.m1MaxHandledTimeStamp = x.i * 1000;
                x.Item2.m3MaxHandledTimeStamp = x.i * 1000 + 1;
                x.Item2.m5MaxHandledTimeStamp = x.i * 1000 + 2;
                x.Item2.m15MaxHandledTimeStamp = x.i * 1000 + 3;
                x.Item2.m30MaxHandledTimeStamp = x.i * 1000 + 4;
                x.Item2.h1MaxHandledTimeStamp = x.i * 1000 + 5;
                x.Item2.h4MaxHandledTimeStamp = x.i * 1000 + 6;
                x.Item2.h12MaxHandledTimeStamp = x.i * 1000 + 7;
                x.Item2.D1MaxHandledTimeStamp = x.i * 1000 + 8;
                x.Item2.D3MaxHandledTimeStamp = x.i * 1000 + 9;
                x.Item2.W1MaxHandledTimeStamp = x.i * 1000 + 10;
                x.Item2.Mo1MaxHandledTimeStamp = x.i * 1000 + 11;
            })
            .Select(x => x.Item2)
            .OrderBy(x => x.RunId)
            .ToArray();

        foreach (var strategyManager in strategyManagers)
        {
            await _repository.CreateAsync(strategyManager);
        }

        var pointerThatSelectTwoStates = new TimeStampByTimeFramePointer
        {
            m1TimeStamp = 1001,
            m3TimeStamp = 1,
            m5TimeStamp = 1,
            m15TimeStamp = 1,
            m30TimeStamp = 1,
            h1TimeStamp = 1,
            h4TimeStamp = 1,
            h12TimeStamp = 1,
            D1TimeStamp = -1,
            D3TimeStamp = -1,
            W1TimeStamp = -1,
            Mo1TimeStamp = -1
        };
        await _repository.HandleStates(pointerThatSelectTwoStates, x =>
        {
            x.Count().Should().Be(2);
            return Task.FromResult(Enumerable.Empty<StrategyManagerDbo>());
        });
    }

    [Test]
    public async Task GetStateToHandle_HideStatesFromRepeatedRead()
    {
        var strategyManagers = Enumerable.Range(0, 6)
            .Select(i => (i, CreateStrategyManagerDbo()))
            .Pipe(x =>
            {
                x.Item2.m1MaxHandledTimeStamp = x.i * 1000;
                x.Item2.m3MaxHandledTimeStamp = x.i * 1000 + 1;
                x.Item2.m5MaxHandledTimeStamp = x.i * 1000 + 2;
                x.Item2.m15MaxHandledTimeStamp = x.i * 1000 + 3;
                x.Item2.m30MaxHandledTimeStamp = x.i * 1000 + 4;
                x.Item2.h1MaxHandledTimeStamp = x.i * 1000 + 5;
                x.Item2.h4MaxHandledTimeStamp = x.i * 1000 + 6;
                x.Item2.h12MaxHandledTimeStamp = x.i * 1000 + 7;
                x.Item2.D1MaxHandledTimeStamp = x.i * 1000 + 8;
                x.Item2.D3MaxHandledTimeStamp = x.i * 1000 + 9;
                x.Item2.W1MaxHandledTimeStamp = x.i * 1000 + 10;
                x.Item2.Mo1MaxHandledTimeStamp = x.i * 1000 + 11;
            })
            .Select(x => x.Item2)
            .OrderBy(x => x.RunId)
            .ToArray();

        foreach (var strategyManager in strategyManagers)
        {
            await _repository.CreateAsync(strategyManager);
        }

        var pointerThatSelectTwoStates = new TimeStampByTimeFramePointer
        {
            m1TimeStamp = 1001,
            m3TimeStamp = 1,
            m5TimeStamp = 1,
            m15TimeStamp = 1,
            m30TimeStamp = 1,
            h1TimeStamp = 1,
            h4TimeStamp = 1,
            h12TimeStamp = 1,
            D1TimeStamp = -1,
            D3TimeStamp = -1,
            W1TimeStamp = -1,
            Mo1TimeStamp = -1
        };

        await _repository.HandleStates(pointerThatSelectTwoStates, async x =>
        {
            x.Count().Should().Be(2);

            await _repository.HandleStates(pointerThatSelectTwoStates, y =>
            {
                y.Count().Should().Be(0);
                return Task.FromResult(Enumerable.Empty<StrategyManagerDbo>());
            });
            return [];
        });
        
        await _repository.HandleStates(pointerThatSelectTwoStates, x =>
        {
            x.Count().Should().Be(2);
            return Task.FromResult(Enumerable.Empty<StrategyManagerDbo>());
        });
    }

    private static void CompareStrategyManager(StrategyManagerDbo updatedModel, StrategyManagerDbo strategyManager)
    {
        updatedModel.Should().BeEquivalentTo(strategyManager,
            options => options.Excluding(x => x.StartStrategyParameters)
                .Excluding(x => x.StrategyManagerState));

        updatedModel.StartStrategyParameters.Should().BeEquivalentTo(strategyManager.StartStrategyParameters);
        updatedModel.StrategyManagerState.IsEquivalentTo(strategyManager.StrategyManagerState);
    }


    private static StrategyManagerDbo CreateStrategyManagerDbo()
    {
        var userId = Guid.NewGuid();
        var exchangePairTimeFrame = new List<ExchangePairTimeFrame>
            {
                new(Exchange.Poloniex, new Pair("BTC", "USDT"), TimeFrame.m1)
            };
        
        return new StrategyManagerDbo()
        {
            UserId = userId,
            RunId = Guid.NewGuid(),
            StrategyType = typeof(TestStrategy1),
            ExchangePairTimeFrameDbos = exchangePairTimeFrame.Select(x=>x.ToDbo()).ToList(),
            StartStrategyParameters = new StartStrategyParameters(Guid.NewGuid(),userId, RunMode.Real, false, exchangePairTimeFrame,[],[], new DateTimeRange() ),
            StrategyManagerState = new StrategyManagerState<TestStrategy1>().Serialize()
        };
    }


    private IStrategyManagerRepository _repository = null!;

    private record TestStrategy1 : BaseStrategy<ISpotConnector>
    {
    }
}