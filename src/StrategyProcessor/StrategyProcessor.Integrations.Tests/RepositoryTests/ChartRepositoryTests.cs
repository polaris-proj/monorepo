﻿using AutoFixture;
using Domain.StrategyProcessor;
using Infrastructure;
using Infrastructure.Tests;
using Microsoft.Extensions.DependencyInjection;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Integrations.Tests.RepositoryTests;

public class ChartRepositoryTests : StrategyArtifactsBaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        _repository = Scope.ServiceProvider.GetRequiredService<IChartRepository>();
    }

    [Test]
    public async Task AddChartsAsync_ShouldAddChartsToTest()
    {
        var dbo = await CreateTestOfStrategyDboAsync();
        var runId = dbo.Id;

        var chart1 = CreateExpectedChartWithoutDots();
        var points1 = CreateExpectedDots().ToList();
        chart1.Chart = points1.ToList();

        var chart2 = CreateExpectedChartWithoutDots();
        chart2.Pair = chart1.Pair;
        chart2.TimeFrame = chart1.TimeFrame;
        var points2 = CreateExpectedDots().ToList();
        chart2.Chart = points2.ToList();

        await _repository.AddChartsAsync(runId, new[] {chart1, chart2});

        var chart1FromDb = (await _repository.ReadAsync(dbo.Id, chart1.Pair, chart1.TimeFrame, TimeConstants.WholeDataTimeRange)).ToList();
        chart1FromDb.Count.Should().Be(2);

        chart1FromDb[0].Should().BeEquivalentTo(chart1);
        chart1FromDb[1].Should().BeEquivalentTo(chart2);
    }

    [Test]
    public async Task AddDotToChartAsync_ShouldAddDotToTest()
    {
        var dbo = await CreateTestOfStrategyDboAsync();
        var runId = dbo.Id;

        var chart1 = CreateExpectedChartWithoutDots();
        var points1 = CreateExpectedDots().ToList();
        chart1.Chart = points1.ToList();

        await _repository.AddChartsAsync(runId, new[] {chart1});

        var dot = CreateExpectedDots().First();
        await _repository.AddDotToChartAsync(runId, chart1.Name, chart1.Description, chart1.Pair,
            chart1.TimeFrame, dot);

        var chart1FromDb = (await _repository.ReadAsync(dbo.Id, chart1.Pair, chart1.TimeFrame, TimeConstants.WholeDataTimeRange)).ToList();
        chart1FromDb.Count.Should().Be(1);
        chart1.Chart = chart1.Chart.Concat(new[] {dot}).ToList();
        chart1FromDb[0].Should().BeEquivalentTo(chart1);
    }

    private ChartDto CreateExpectedChartWithoutDots()
    {
        var fixture = CustomAutoDataAttribute.CreateFixture();
        return fixture.Build<ChartDto>()
            .Without(x => x.Chart)
            .Create();
    }

    private IEnumerable<Point> CreateExpectedDots()
    {
        var fixture = CustomAutoDataAttribute.CreateFixture();
        return fixture.Build<Point>()
            .CreateMany(50);
    }

    private IChartRepository _repository = null!;
}