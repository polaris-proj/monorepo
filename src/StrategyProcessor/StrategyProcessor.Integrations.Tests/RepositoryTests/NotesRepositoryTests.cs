﻿using AutoFixture;
using Domain;
using Domain.StrategyProcessor;
using Infrastructure;
using Infrastructure.Tests;
using Microsoft.Extensions.DependencyInjection;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

namespace StrategyProcessor.Integrations.Tests.RepositoryTests;

public class NotesRepositoryTests : StrategyArtifactsBaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        _repository = Scope.ServiceProvider.GetRequiredService<INotesRepository>();
    }

    [Test]
    public async Task AddNotesAsync_ShouldAddNotesToTest()
    {
        var dbo = await CreateTestOfStrategyDboAsync();
        var runId = dbo.Id;

        var pair1 = new Pair("BTC", "USD");
        var notes1 = CreateExpectedNotes(pair1, TimeFrame.h1).ToList();
        var notes3 = CreateExpectedNotes(pair1, TimeFrame.h4).ToList();
        var pair2 = new Pair("ETH", "USD");
        var notes2 = CreateExpectedNotes(pair2, TimeFrame.h1).ToList();

        await _repository.AddNotesAsync(runId, notes1);
        await _repository.AddNotesAsync(runId, notes2);
        await _repository.AddNotesAsync(runId, notes3);

        (await _repository.ReadAsync(runId, pair1, TimeFrame.h1, TimeConstants.WholeDataTimeRange)).Should().BeEquivalentTo(notes1);
        (await _repository.ReadAsync(runId, pair1, TimeFrame.h4, TimeConstants.WholeDataTimeRange)).Should().BeEquivalentTo(notes3);
        (await _repository.ReadAsync(runId, pair2, TimeFrame.h1, TimeConstants.WholeDataTimeRange)).Should().BeEquivalentTo(notes2);
    }

    private IEnumerable<Note> CreateExpectedNotes(Pair pair, TimeFrame timeFrame)
    {
        var fixture = CustomAutoDataAttribute.CreateFixture();
        return fixture.Build<Note>()
            .With(x => x.Pair, pair)
            .With(x => x.TimeFrame, timeFrame)
            .CreateMany(10);
    }


    private INotesRepository _repository = null!;
}