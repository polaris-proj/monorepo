﻿using System.Text.Json;
using AutoFixture;
using Domain.StrategyProcessor;
using Microsoft.Extensions.DependencyInjection;
using StrategyProcessor.Infrastructure.DAL;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories;

namespace StrategyProcessor.Integrations.Tests.RepositoryTests;

public class StrategyRepositoryTests : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        var context = Scope.ServiceProvider.GetService<StrategyProcessorContext>() ?? throw new Exception("Can't get StrategyProcessorContext");
        var set = context.Set<StrategyDbo>();
        set.RemoveRange(set.ToList()); //todo удалить, когда для тестов бдует отдельная бд
        _repository = Scope.ServiceProvider.GetService<IStrategyRepository>() ??
                      throw new InvalidOperationException();
    }
    
    [OneTimeTearDown]
    public async Task OneTimeTearDownAsync()
    {
        using var scope = App.Services.CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<StrategyProcessorContext>();
        await context.Database.EnsureDeletedAsync();
        await context.SaveChangesAsync();
        base.OneTimeTearDown();
    }

    [Test]
    public async Task CreateAndReadByName_ShouldReturnStrategy()
    {
        // Arrange
        var expectedName = "TestStrategy";
        var expectedStrategy = new StrategyDbo {InternalName = expectedName, StrategyConnectorType = StrategyConnectorType.Futures};

        await _repository.CreateAsync(expectedStrategy);
        await _repository.SaveAsync();
        // Act
        var result = await _repository.ReadByNameAsync(expectedName);

        // Assert
        result.Should().BeEquivalentTo(expectedStrategy);
    }

    [Test]
    public async Task CreateAndReadById_ShouldReturnStrategy()
    {
        // Arrange
        var expectedName = "TestStrategy1231231213";
        var expectedStrategy = new StrategyDbo {InternalName = expectedName, StrategyConnectorType = StrategyConnectorType.Futures};

        var id = await _repository.CreateAsync(expectedStrategy);
        await _repository.SaveAsync();
        // Act
        var result = await _repository.ReadByIdAsync(id);

        // Assert
        result.Should().BeEquivalentTo(expectedStrategy);
    }

    [Test]
    public async Task ReadAllAsync_ShouldReturnListOfStrategies()
    {
        var strategyId1 = Guid.NewGuid();
        var strategyId2 = Guid.NewGuid();
        // Arrange
        var expectedStrategies = new List<StrategyDbo>
        {
            new()
            {
                Id = strategyId1, InternalName = "Strategy1",
                Properties = Enumerable.Repeat(0, 5).Select(_ => CreateParameterDbo(strategyId1)).ToList(),
                StrategyConnectorType = StrategyConnectorType.Futures
            },
            new()
            {
                Id = strategyId2, InternalName = "Strategy2",
                Properties = Enumerable.Repeat(0, 5).Select(_ => CreateParameterDbo(strategyId2)).ToList(),
                StrategyConnectorType = StrategyConnectorType.Futures
            }
        };
        foreach (var expectedStrategy in expectedStrategies)
        {
            await _repository.CreateAsync(expectedStrategy);
        }

        await _repository.SaveAsync();
        // Act
        var result = await _repository.ReadAllAsync();

        // Assert
        result.Should().BeEquivalentTo(expectedStrategies);
    }

    [Test]
    public async Task CreateAsync_ShouldReturnGeneratedGuid()
    {
        // Arrange
        var strategy = new StrategyDbo {InternalName = "NewStrategy", StrategyConnectorType = StrategyConnectorType.Futures};

        // Act
        var result = await _repository.CreateAsync(strategy);
        await _repository.SaveAsync();
        // Assert
        result.Should().NotBe(Guid.Empty);
    }

    [Test]
    public async Task UpdateAsync_ShouldNotThrowException()
    {
        // Arrange
        var expectedName = "TestStrategy";
        var expectedStrategy = new StrategyDbo {InternalName = expectedName, StrategyConnectorType = StrategyConnectorType.Spot};

        var id = await _repository.CreateAsync(expectedStrategy);
        await _repository.SaveAsync();
        // Act
        var result = await _repository.ReadByNameAsync(expectedName);
        result.Should().BeEquivalentTo(expectedStrategy);


        expectedStrategy.Properties = new List<ParameterDbo>
        {
            CreateParameterDbo(id)
        };
        _repository.Update(expectedStrategy);
        await _repository.SaveAsync();
        var resultAfterUpdate = await _repository.ReadByNameAsync(expectedName);
        resultAfterUpdate.Should().BeEquivalentTo(expectedStrategy);
    }

    private static ParameterDbo CreateParameterDbo(Guid id)
    {
        var fixture = new Fixture();

        return fixture.Build<ParameterDbo>()
            .Without(x => x.Id)
            .With(x => x.ParameterValue, JsonSerializer.SerializeToDocument("asdasdasdas"))
            .With(x => x.StrategyId, id)
            .Create();
    }

    private IStrategyRepository _repository = null!;
}