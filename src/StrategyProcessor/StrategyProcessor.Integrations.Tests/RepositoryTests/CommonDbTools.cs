﻿using AutoFixture.Kernel;
using Infrastructure;
using Infrastructure.DAL.Dapper;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;

namespace StrategyProcessor.Integrations.Tests.RepositoryTests;

public class CommonDbTools
{
    public static NpgsqlDataSource GetNpgsqlDataSource(PostgresParameters postgresParameters)
    {
        var collection = new ServiceCollection();
        collection.AddSingleton(postgresParameters);
        collection.ConfigureNpgSql();
        return collection.BuildServiceProvider().GetRequiredService<NpgsqlDataSource>();
    }
    
    public class UtcConverter : ISpecimenBuilder
    {
        public UtcConverter(ISpecimenBuilder builder)
        {
            _builder = builder;
        }

        public object Create(object request, ISpecimenContext context)
        {
            var t = request as Type;

            // Abort if request not valid for this type
            if (t == null || t != typeof(DateTime))
                return new NoSpecimen();

            var specimen = _builder.Create(request, context);
            if (!(specimen is DateTime))
                return new NoSpecimen();
            return ((DateTime)specimen).ToUniversalTime();
        }

        private readonly ISpecimenBuilder _builder;
    }
}