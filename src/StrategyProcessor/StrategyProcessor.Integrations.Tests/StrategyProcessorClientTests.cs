using AutoFixture;
using Domain;
using Domain.StrategyProcessor;
using Grpc.Net.Client;
using Infrastructure;
using Infrastructure.Tests;
using Microsoft.Extensions.DependencyInjection;
using MoreLinq;
using StrategyProcessor.Client;
using StrategyProcessor.Infrastructure.Mappers;
using StrategyProcessor.Services;
using StrategyProcessor.Shared;
using StrategyProcessor.Strategies;

#pragma warning disable CS0618 // Type or member is obsolete

namespace StrategyProcessor.Integrations.Tests;

[Parallelizable(ParallelScope.All)]
public class StrategyProcessorClientTests : BaseTests
{
    public StrategyProcessorClientTests()
    {
        var channel = GrpcChannel.ForAddress("http://localhost:10042", new GrpcChannelOptions
        {
            MaxReceiveMessageSize = null,
        });
        var wrappedChannel = new StrategyProcessorClientChannel {Channel = channel};

        _strategyProcessorClient = new StrategyProcessorClient(wrappedChannel);

        using var scope = App.Services.CreateScope();
        var addDeclaredStrategiesToDatabase =
            scope.ServiceProvider.GetRequiredService<AddDeclaredStrategiesToDatabase>();
        addDeclaredStrategiesToDatabase.Start().Wait();
    }

    [Test]
    public async Task StrategyProcessorClient_StartStrategy()
    {
        var strategies = await _strategyProcessorClient.GetAllStrategies();
        var emaStrategy = strategies.First(x => x.InternalName == typeof(ExampleEmaStrategy).FullName);
        var id = await StartStrategy(emaStrategy);
        id.Should().NotBeEmpty();
    }

    [Test]
    public async Task StrategyProcessorClient_GetAllStrategies()
    {
        var strategies = await _strategyProcessorClient.GetAllStrategies();
        strategies.Should().NotBeEmpty();
        strategies.ForEach(x =>
        {
            x.Parameters.Should().NotBeNull();
            x.InternalName.Should().NotBeNullOrWhiteSpace().And.NotBeEmpty();
            x.Id.Should().NotBeEmpty();
        });
    }

    [Test]
    public async Task StrategyProcessorClient_GetTestResult()
    {
        var strategies = await _strategyProcessorClient.GetAllStrategies();
        var emaStrategy = strategies.First(x => x.InternalName == typeof(ExampleEmaStrategy).FullName);
        var runid = await StartStrategy(emaStrategy);

        var tcs = new CancellationTokenSource(TimeSpan.FromSeconds(10));
        TestOfStrategyDto result = null;
        while (result?.Status != StatusOfRun.Completed && !tcs.IsCancellationRequested)
        {
            result = await _strategyProcessorClient.GetTestResult(runid, new Pair("BTC", "USDT"), TimeFrame.h4, TimeConstants.WholeDataTimeRange);
            await Task.Delay(1000);
        }

        //result.Orders.Should().NotBeEmpty();
        //result.Notes.Should().NotBeEmpty();
        result.Charts.Should().NotBeEmpty();
    }

    [Test]
    public async Task StrategyProcessorClient_StopStrategy()
    {
        var strategies = await _strategyProcessorClient.GetAllStrategies();
        var emaStrategy = strategies.First(x => x.InternalName == typeof(ExampleEmaStrategy).FullName);
        var id = await StartStrategy(emaStrategy);
        await _strategyProcessorClient.StopStrategy(new StrategyStopDto {StrategyTestId = id});
    }


    [TestCase(1)]
    [TestCase(10)]
    [TestCase(10_000)]
    [Explicit]
    public async Task StrategyProcessorClient_LoadTestingHistoryTest(int amount)
    {
        var strategies = await _strategyProcessorClient.GetAllStrategies();
        var emaStrategy = strategies.First(x => x.InternalName == typeof(ExampleEmaStrategy).FullName);

        var fixture = CustomAutoDataAttribute.CreateFixture();
        var dataSources = fixture.CreateMany<ExchangePairTimeFrame>(100)
            .Select(x => x.ToDto())
            .Pipe(x => x.Exchange = Exchange.BinanceSpot)
            .Distinct()
            .ToArray();


        var startStrategyDbo = new StrategyStartDto
        {
            StrategyId = emaStrategy.Id,
            Parameters = emaStrategy.Parameters,
            DataSources = dataSources,
            UserId = Guid.Parse("9b257c73-241c-438e-9d2d-46834afcb244"),
            RunMode = RunMode.HistoryTest,
            InitialBalance = 10000,
            StartTime = new DateTime(2017, 03, 12),
            EndTime = new DateTime(2024, 08, 11),
        };
        for (var _ = 0; _ < amount; _++)
        {
            var t = _strategyProcessorClient.StartStrategy(startStrategyDbo);
            await Task.Delay(1);
        }
    }


    private async Task<Guid> StartStrategy(StrategyDto dto)
    {
        var connectorType = dto.StrategyConnectorType == StrategyConnectorType.Spot
            ? Exchange.BinanceSpot
            : Exchange.BinanceFutures;

        var startStrategyDbo = new StrategyStartDto
        {
            StrategyId = dto.Id,
            Parameters = dto.Parameters,
            DataSources = new[] {new ExchangePairTimeFrameDto(connectorType, new Pair("BTC", "USDT"), TimeFrame.h4)},
            UserId = Guid.Parse("9b257c73-241c-438e-9d2d-46834afcb244"),
            RunMode = RunMode.HistoryTest,
            InitialBalance = 10000,
            EndTime = new DateTime(2021, 08, 11),
            StartTime = new DateTime(2020, 03, 12),
        };
        return (await _strategyProcessorClient.StartStrategy(startStrategyDbo)).Value;
    }

    private readonly IStrategyProcessorClient _strategyProcessorClient;
}