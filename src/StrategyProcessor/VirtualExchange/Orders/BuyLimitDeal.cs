﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;
using static System.Math;


namespace VirtualExchange.Orders;

internal sealed record BuyLimitDeal : DealBase
{
    public BuyLimitDeal(OrderType orderType, Account owner, decimal lastPrice, Pair pair, List<Order> entry,
        List<Order>? takes = null, List<Order>? stops = null) : base(owner, lastPrice, pair, entry, takes, stops)
    {
        FeeFactor = 0.025m / 100m;
        //todo можно добавить проверку, что если ордер тайп байлимит, то все стопы и тейки это селл лимиты
        // аналогично для селл лимит
        var usedMoney = entry.Select(x => x.Amount * x.Price).Sum();
        var totalAmount = entry.Select(x => x.Amount).Sum();
        Amount = totalAmount;

        if (orderType == OrderType.BuyLimit && owner.CoinsAmount[pair.Second].Free < usedMoney * (1 + FeeFactor)
            || orderType == OrderType.SellLimit && owner.CoinsAmount[pair.First].Free < totalAmount)
            throw new Exception("No money");

        owner.UpdateCoinsAmountFree(pair.Second, -usedMoney * (1 + FeeFactor));
        owner.UpdateCoinsAmountLock(pair.Second, usedMoney);

        //todo зачем все эти ордера снова в ордера переводить?
        EntryOrders.AddRange(entry.Select(x => new Order(x.Price, x.Amount, orderType)));

        if (takes is not null)
            TakeOrders.AddRange(takes.Select(x => new Order(x.Price, x.Amount, OrderType.SellLimit)));

        if (stops is not null)
            StopOrders.AddRange(stops.Select(x => new Order(x.Price, x.Amount, OrderType.SellLimit)));
    }

    protected override void Buy(Order order)
    {
        Owner.CoinsAmount.TryGetValue(Pair.Second, out var realBalance);
        var balance = Min(order.Amount * order.Price, realBalance.Locked);
        // AveragePrice = (AveragePrice * Owner.CoinsAmount[Pair] + amount * deal.Price) /
        //                (Owner.CoinsAmount[Pair] + amount);

        Balance -= balance;
        Owner.UpdateCoinsAmountLock(Pair.Second, -balance);

        var amount = balance / order.Price;

        Amount += amount;
        Owner.UpdateCoinsAmountFree(Pair.First, amount);

        order.FilledAmount = amount;
        if (Abs(amount - order.Amount) < Eps)
            order.OrderStatus = OrderStatus.Close;
    }

    protected override void Sell(Order order)
    {
        Owner.CoinsAmount.TryGetValue(Pair.First, out var realAmount);
        var amount = Min(order.Amount, realAmount.Free);

        // AveragePrice = (AveragePrice * Owner.CoinsAmount[Pair] + amount * deal.Price)
        //                (Owner.CoinsAmount[Pair] + amount);
        Owner.UpdateCoinsAmountFree(Pair.First, -amount);
        Owner.UpdateCoinsAmountFree(Pair.Second, amount * order.Price);

        Amount -= amount;
        Balance += amount * order.Price;

        order.FilledAmount = amount;
        order.OrderStatus = OrderStatus.Close;
    }

    public override void UpdateStatusOfOrder(decimal price)
    {
        if (DealStatus == DealStatus.Close) return;

        if (LastPrice == 0)
        {
            UpdateLastPrice(price);
            return;
        }

        UpdateAllDeals(price);

        if (EntryOrders.All(x => x.OrderStatus == OrderStatus.Close) &&
            StopOrders.All(x => x.OrderStatus == OrderStatus.Close) &&
            TakeOrders.All(x => x.OrderStatus == OrderStatus.Close) &&
            Amount < Eps)
        {
            CloseDeal();
            return;
        }

        UpdateLastPrice(price);
    }

    public override void CloseDeal()
    {
        DealStatus = DealStatus.Close;

        var usedMoney = EntryOrders.Where(x => x.OrderStatus == OrderStatus.Close)
            .Select(x => x.Amount * x.Price)
            .Sum();

        Owner.UpdateCoinsAmountFree(Pair.Second, usedMoney);
        Owner.UpdateCoinsAmountLock(Pair.Second, -usedMoney);


        Owner.CloseDeal(Id);
    }
}