﻿using System.Collections.Generic;
using Domain;
using Domain.StrategyProcessor;

namespace VirtualExchange.Orders;

internal abstract record FuturesDealBase : DealBase
{
    protected decimal LiquidationPrice;
    public int Leverage { get; init; } = 1;

    protected FuturesDealBase(Account owner, decimal lastPrice, Pair pair, List<Order> entry, List<Order>? takes = null,
        List<Order>? stops = null)
        : base(owner, lastPrice, pair, entry, takes, stops)
    {
    }
}