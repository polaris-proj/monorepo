using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;

namespace VirtualExchange.Orders;

internal abstract record DealBase
{
    protected decimal AveragePrice;
    protected decimal LastPrice;
    protected decimal StartBalance = 0;
    protected decimal Amount;
    protected decimal Balance;

    protected virtual decimal FeeFactor { get; set; } = 0.025m / 100m;
    protected const decimal Eps = 10e-8m;

    public List<Order> TakeOrders = new();
    public List<Order> StopOrders = new();
    public List<Order> EntryOrders = new();
    protected Account Owner { get; }
    public Pair Pair { get; }
    public DealStatus DealStatus { get; protected set; }
    public readonly Guid Id;

    protected DealBase(Account owner, decimal lastPrice, Pair pair, List<Order> entry, List<Order>? takes = null,
        List<Order>? stops = null)
    {
        LastPrice = lastPrice;
        if (!IsPositive(entry) || (takes != null && !IsPositive(takes))
                               || (stops != null && !IsPositive(stops)))
            throw new Exception("Incorrect input");

        Owner = owner;
        Pair = pair;
        DealStatus = DealStatus.Open;

        Owner.CoinsAmount.TryAdd(pair.First, (0, 0));
        Owner.CoinsAmount.TryAdd(pair.Second, (0, 0));
        Id = Guid.NewGuid();
    }

    public virtual void UpdateStatusOfOrder(decimal price)
    {
        throw new NotImplementedException();
    }

    protected virtual void UpdateAllDeals(decimal price)
    {
        //todo много аллоки
        UpdateStatusOfDeals(EntryOrders, price);
        UpdateStatusOfDeals(TakeOrders, price);
        UpdateStatusOfDeals(StopOrders, price);
    }

    protected virtual void UpdateStatusOfDeals(List<Order> deals, decimal price)
    {
        if (deals.Count == 0) return;

        foreach (var deal in deals.Where(x => x.OrderStatus == OrderStatus.Open
                                              && IsPriceCrossedLevel(x.Price, price)))
        {
            if (deal.OrderType == OrderType.BuyLimit)
                Buy(deal);
            else if (deal.OrderType == OrderType.SellLimit)
                Sell(deal);
            else
                throw new NotImplementedException();
        }
    }


    protected bool IsPriceCrossedLevel(decimal level, decimal price)
    {
        return (LastPrice >= price && level <= LastPrice && level >= price)
               || (LastPrice <= price && level >= LastPrice && level <= price);
    }

    protected static bool IsPositive(List<Order> orders)
    {
        if (orders == null)
            throw new ArgumentNullException(nameof(orders));

        return orders.All(x => x.Amount > 0) && orders.All(x => x.Price > 0);
    }

    protected virtual void UpdateLastPrice(decimal price) => LastPrice = price;

    public virtual void CloseDeal()
    {
        DealStatus = DealStatus.Close;
        Owner.CloseDeal(Id);
    }

    protected virtual void Buy(Order order) => throw new NotImplementedException();

    protected virtual void Sell(Order order) => throw new NotImplementedException();
}