﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;

namespace VirtualExchange.Orders;

internal sealed record FuturesDealShort : FuturesDealBase
{
    public FuturesDealShort(Account owner, decimal lastPrice, Pair pair, List<Order> entry, int leverage,
        List<Order>? takes = null, List<Order>? stops = null)
        : base(owner, lastPrice, pair, entry, takes, stops)
    {
        if (leverage < 1)
            throw new Exception("Leverage should be more when 1");

        var totalSum = entry.Sum(x => x.Price * x.Amount);

        if (owner.CoinsAmount[pair.Second].Free * leverage < totalSum * (1 + FeeFactor))
            throw new Exception("Not enough money");

        Leverage = leverage;
        LiquidationPrice = decimal.MaxValue;
        StartBalance = totalSum;

        var usedMoney = totalSum / leverage;
        Owner.UpdateCoinsAmountFree(pair.Second, -usedMoney * (1 + FeeFactor));

        EntryOrders.AddRange(entry.Select(x => new Order(x.Price, x.Amount, OrderType.SellLimit)));

        if (takes != null)
            TakeOrders.AddRange(takes.Select(x => new Order(x.Price, x.Amount, OrderType.BuyLimit)));
        if (stops != null)
            StopOrders.AddRange(stops.Select(x => new Order(x.Price, x.Amount, OrderType.BuyLimit)));
    }

    public override void UpdateStatusOfOrder(decimal price)
    {
        if (DealStatus == DealStatus.Close) return;

        if (LastPrice == 0)
        {
            UpdateLastPrice(price);
            return;
        }

        UpdateAllDeals(price);

        if (Math.Abs(Amount) < 10e-3m && EntryOrders.All(x => x.OrderStatus == OrderStatus.Close))
        {
            CloseDeal();
            return;
        }

        //if(EntryDeals.All(x => x.Status == Status.Close)
        //    && TakeDeals.All(x => x.Status == Status.Close)
        //    && StopDeals.All(x => x.Status == Status.Close))
        //    CloseOrder();
        if (LiquidationPrice <= price && Amount > 0)
        {
            //Liquidation
            //CloseOrder();
            DealStatus = DealStatus.Close;
            return;
            // Amount = 0;
        }

        UpdateLastPrice(price);
    }

    protected override void Buy(Order order)
    {
        var amount = order.Amount;
        order.FilledAmount = amount;
        Amount += amount;
        var basePrice = AveragePrice * amount;
        Balance += basePrice - amount * order.Price;
        order.OrderStatus = OrderStatus.Close;
    }

    protected override void Sell(Order order)
    {
        AveragePrice = (AveragePrice * Amount + order.Amount * order.Price) /
                       (Amount + order.Amount);
        var amount = order.Amount;
        order.FilledAmount = amount;
        Amount -= amount;
        var priceOfSell = amount * order.Price;

        var totalSpend = Math.Abs(AveragePrice * Amount);
        if (Math.Abs(Amount) < 0.00001m)
            LiquidationPrice = decimal.MaxValue;
        else
            LiquidationPrice = (totalSpend + totalSpend / Leverage) / Math.Abs(Amount) * FeeFactor;
        Balance += priceOfSell;
        order.OrderStatus = OrderStatus.Close;
    }

    public override void CloseDeal()
    {
        Owner.UpdateCoinsAmountFree(Pair.Second, Balance - StartBalance + StartBalance / Leverage);
        DealStatus = DealStatus.Close;
        Owner.CloseDeal(Id);
    }
}