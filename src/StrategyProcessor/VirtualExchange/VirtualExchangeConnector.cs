﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;
using ReadOnlyCollectionsExtensions;

namespace VirtualExchange
{
    internal class VirtualExchangeConnector : BaseConnector<IConnector>, ISpotConnector, IFuturesConnector
    {
        internal VirtualExchangeConnector(IConnector connector, Account acc) : base(connector)
        {
            _acc = acc;
        }

        public Task<OrderId> CreateOrder(Pair pair, OrderType orderType, decimal amount, decimal? price = null)
        {
            if (price is null && orderType is not (OrderType.Buy or OrderType.Sell))
                throw new Exception("Price can't be null on order of this type");

            return Task.FromResult(
                new OrderId(_acc.PostMarketOrder(orderType.ToMarketOrder(), pair, amount).ToString()));
        }

        public Task<OrderId> CreateLimitOrder(Pair pair, OrderType orderType, List<Order> prices,
            List<Order>? takes = null, List<Order>? stops = null)
        {
            if (orderType is not (OrderType.BuyLimit or OrderType.SellLimit))
                throw new Exception($"OrderType should be BuyLimit or SellLimit, not {orderType}");

            return Task.FromResult(new OrderId(_acc.PostLimitOrder(orderType.ToLimitOrder(), pair, prices, takes, stops)
                .ToString()));
        }

        public Task<OrderId> CreateFuturesOrder(Pair pair, OrderType orderType, int leverage, List<Order> prices,
            List<Order>? takes = null, List<Order>? stops = null)
        {
            if (orderType is not (OrderType.Long or OrderType.Short))
                throw new Exception($"OrderType should be short or long, not {orderType}");

            return Task.FromResult(new OrderId(_acc
                .PostFuturesOrder(orderType.ToFuturesOrder(), pair, leverage, prices, takes, stops)
                .ToString()));
        }

        public Task<OrderId> CreateMarketOrder(Pair pair, MarketOrder orderType, decimal amount)
        {
            if (orderType is not (MarketOrder.Buy or MarketOrder.Sell))
                throw new Exception("Price can't be null on order of this type");

            return Task.FromResult(new OrderId(_acc.PostMarketOrder(orderType, pair, amount).ToString()));
        }

        public Task<OrderId> CreateLimitOrder(Pair pair, LimitOrder orderType, PlacementOrder entry)
        {
            if (orderType is not (LimitOrder.BuyLimit or LimitOrder.SellLimit))
                throw new Exception($"OrderType should be BuyLimit or SellLimit, not {orderType}");

            var entries = new List<Order> {new(entry.Price, entry.Amount)};
            return Task.FromResult(new OrderId(_acc.PostLimitOrder(orderType, pair, entries).ToString()));
        }

        public Task<OrderId> CreateFuturesOrder(Pair pair, FuturesOrder orderType, int leverage, PlacementOrder entry,
            PlacementOrder? take = null, PlacementOrder? stop = null)
        {
            if (orderType is not (FuturesOrder.Long or FuturesOrder.Short))
                throw new Exception($"OrderType should be short or long, not {orderType}");

            var entries = new List<Order> {new(entry.Price, entry.Amount)};
            var takes = take is not null ? new List<Order> {new(take.Price, take.Amount)} : null;
            var stops = stop is not null ? new List<Order> {new(stop.Price, stop.Amount)} : null;

            return Task.FromResult(new OrderId(_acc.PostFuturesOrder(orderType, pair, leverage, entries, takes, stops)
                .ToString()));
        }

        public override Task<IReadOnlyList<Balance>> GetCoinsAmount()
            => Task.FromResult((IReadOnlyList<Balance>) _acc.CoinsAmount
                .Select(x => new Balance(x.Key, x.Value.Free, x.Value.Locked)).ToList());


        public override Task CancelOrder(Pair pair, OrderId orderId)
        {
            _acc.CancelOrder(Guid.Parse(orderId.Id ?? throw new ArgumentNullException()));
            return Task.CompletedTask;
        }


        public override Task<IReadOnlyList<Order>> GetOrdersPerPair(Pair pair)
        {
            var t = _acc.CurrentDeals.Values.Where(x => x.Pair == pair).ToArray();
            return Task.FromResult(t.SelectMany(x => x.TakeOrders)
                .Union(t.SelectMany(x => x.StopOrders))
                .Union(t.SelectMany(x => x.EntryOrders))
                .ToReadOnlyList());
        }

        public override Task<Order> GetOrderInfo(Pair pair, OrderId id)
        {
            var guid = Guid.Parse(id.Id ?? throw new ArgumentNullException());
            if (_acc.CurrentDeals.TryGetValue(guid, out var currentDeal))
                return Task.FromResult(currentDeal.EntryOrders.First());

            if (_acc.ClosedDeals.TryGetValue(guid, out var closedDeal))
                return Task.FromResult(closedDeal.EntryOrders.First());

            throw new Exception("Order not found");
        }

        private readonly Account _acc;
    }
}