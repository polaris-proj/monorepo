using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Connectors;
using Domain.Patterns.Objects;
using Domain.StrategyProcessor;
using VirtualExchange.Orders;

namespace VirtualExchange;

internal class Account
{
    public string Name { get; }
    public ConcurrentDictionary<Guid, DealBase> CurrentDeals { get; } = new();
    public ConcurrentDictionary<Guid, DealBase> ClosedDeals { get; } = new();
    public ConcurrentDictionary<Ticker, (decimal Free, decimal Locked)> CoinsAmount { get; } = new();
    private readonly Dictionary<Pair, decimal> _lastPrices = new();
    

    internal Account(string name, Ticker balance, decimal amount)
    {
        Name = name;
        CoinsAmount.TryAdd(balance, (amount, 0));
    }

    internal Account(string name, IEnumerable<Balance> initialBalance)
    {
        Name = name;
        foreach (var balance in initialBalance)
            CoinsAmount.TryAdd(balance.Asset, (balance.Available, balance.Locked));
    }

    internal Guid PostMarketOrder(MarketOrder orderType, Pair pair, decimal amount)
    {
        if (orderType is not (MarketOrder.Buy or MarketOrder.Sell))
            throw new Exception("Incorrect type of order");

        _lastPrices.TryGetValue(pair, out var lastPrice);

        var order = new MarketDeal(orderType.ToOrderType(), this, lastPrice, pair, amount);
        var guid = order.Id;

        if (!ClosedDeals.TryAdd(guid, order))
            throw new Exception("Can't add order to history");
        

        return guid;
    }

    internal Guid PostFuturesOrder(FuturesOrder orderType, Pair pair, int leverage, List<Order> prices,
        List<Order>? takes = null, List<Order>? stops = null)
    {
        if (orderType is not (FuturesOrder.Long or FuturesOrder.Short))
            throw new Exception("Incorrect type of order");

        _lastPrices.TryGetValue(pair, out var lastPrice);

        DealBase order = orderType == FuturesOrder.Long
            ? new FuturesDealLong(this, lastPrice, pair, prices, leverage, takes, stops)
            : new FuturesDealShort(this, lastPrice, pair, prices, leverage, takes, stops);

        var guid = order.Id;
        CurrentDeals.TryAdd(guid, order);

        return guid;
    }

    internal Guid PostLimitOrder(LimitOrder orderType, Pair pair, List<Order> prices,
        List<Order>? takes = null, List<Order>? stops = null)
    {
        if (orderType is not (LimitOrder.BuyLimit or LimitOrder.SellLimit))
            throw new Exception("Incorrect type of order");

        _lastPrices.TryGetValue(pair, out var lastPrice);
        DealBase order;

        if (orderType == LimitOrder.BuyLimit)
            order = new BuyLimitDeal(orderType.ToOrderType(), this, lastPrice, pair, prices, takes, stops);
        else
            order = new SellLimitDeal(orderType.ToOrderType(), this, lastPrice, pair, prices, takes, stops);


        var guid = order.Id;
        CurrentDeals.TryAdd(guid, order);

        return guid;
    }

    internal void UpdateCoinsAmountFree(Ticker ticker, decimal amount)
    {
        CoinsAmount.AddOrUpdate(ticker, _ => (amount, 0), (_, v) => (v.Free + amount, v.Locked));
    }

    internal void UpdateCoinsAmountLock(Ticker ticker, decimal amount)
    {
        CoinsAmount.AddOrUpdate(ticker, _ => (amount, 0), (_, v) => (v.Free, v.Locked + amount));
    }

    internal void ChangeStops(Guid orderId, List<Order> stops)
    {
        if (!CurrentDeals.ContainsKey(orderId))
            throw new Exception("Unknown orderId");

        CurrentDeals[orderId].StopOrders = stops;
    }

    internal void ChangeEntries(Guid orderId, List<Order> entries)
    {
        if (!CurrentDeals.ContainsKey(orderId))
            throw new Exception("Unknown orderId");

        CurrentDeals[orderId].EntryOrders = entries;
    }

    internal void ChangeTakes(Guid orderId, List<Order> takes)
    {
        if (!CurrentDeals.ContainsKey(orderId))
            throw new Exception("Unknown orderId");

        CurrentDeals[orderId].TakeOrders = takes;
    }

    internal void CancelOrder(Guid orderId)
    {
        if (CurrentDeals.TryRemove(orderId, out _))
        {

        }
        else
        {
            throw new Exception("Order Not found");
        }
    }
    
    internal void CloseDeal(Guid orderId)
    {
        if (CurrentDeals.TryRemove(orderId, out var deal))
        {
            if (!ClosedDeals.TryAdd(orderId, deal))
                throw new Exception("Can't add order to closed");
        }
        else
        {
            throw new Exception("Order Not found");
        }
    }

    internal void DataReceiver(NewCandleEvent candleEvent)
    {
        if (_lastPrices.ContainsKey(candleEvent.Pair))
            _lastPrices[candleEvent.Pair] = candleEvent.Candle.Close;
        else
            _lastPrices.Add(candleEvent.Pair, candleEvent.Candle.Close);
        // _candleQueue.GetOrAdd(candleEvent.Pair, new ConcurrentQueue<NewCandleEvent>())
        //             .Enqueue(candleEvent);

        foreach (var deal in CurrentDeals.Values.Where(x => x.DealStatus is DealStatus.Created or DealStatus.Open &&
                                                            x.Pair == candleEvent.Pair))
        {
            if (RandomBool())
            {
                deal.UpdateStatusOfOrder(candleEvent.Candle.Low);
                deal.UpdateStatusOfOrder(candleEvent.Candle.High);
            }
            else
            {
                deal.UpdateStatusOfOrder(candleEvent.Candle.High);
                deal.UpdateStatusOfOrder(candleEvent.Candle.Low);
            }
        }
    }

    private static bool RandomBool() => Rnd.Next() % 2 == 1;

    private static readonly Random Rnd = new();
}