﻿using Grpc.Net.Client;
using Infrastructure.Grpc;

namespace StrategyProcessor.Client;

public class StrategyProcessorClientChannel : INamedGrpcChannelWrapper
{
    public GrpcChannel Channel { get; init; } = null!;
}