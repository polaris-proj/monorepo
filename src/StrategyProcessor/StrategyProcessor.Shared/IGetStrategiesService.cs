﻿using System.ServiceModel;
using ProtoBuf.Grpc;

namespace StrategyProcessor.Shared;

[ServiceContract]
public interface IGetStrategiesService
{
    [OperationContract]
    Task<StrategyDto[]> GetStrategies(CallContext context = default);
}