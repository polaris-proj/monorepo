﻿using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using Domain;

namespace StrategyProcessor.Shared;

[DataContract]
public class ExchangePairTimeFrameDto
{
    private ExchangePairTimeFrameDto()
    {
        
    }
    public ExchangePairTimeFrameDto(Exchange exchange, Pair pair, TimeFrame timeframe)
    {
        Exchange = exchange;
        Pair = pair;
        Timeframe = timeframe;
    }
    
    [JsonConstructor]
    public ExchangePairTimeFrameDto(Exchange exchange, string pair, TimeFrame timeframe)
    {
        Exchange = exchange;
        Pair = pair;
        Timeframe = timeframe;
    }

    [DataMember(Order = 1)] public Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public string Pair { get; set; }
    [DataMember(Order = 3)] public TimeFrame Timeframe { get; set; }
}