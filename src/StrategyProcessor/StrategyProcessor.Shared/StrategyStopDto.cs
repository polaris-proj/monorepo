﻿using System.Runtime.Serialization;

namespace StrategyProcessor.Shared;

[DataContract]
public class StrategyStopDto
{
    [DataMember(Order = 1)] public required Guid StrategyTestId { get; set; }
}