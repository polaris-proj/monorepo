﻿using System.ServiceModel;
using ProtoBuf.Grpc;

namespace StrategyProcessor.Shared;

[ServiceContract]
public interface IRunOfStrategyService
{
    [OperationContract]
    Task<TestOfStrategyDto>GetRunResult(TestOfStrategyArgument arg, CallContext context = default);
}