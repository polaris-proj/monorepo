﻿using System.ServiceModel;
using Infrastructure.Grpc;
using ProtoBuf.Grpc;

namespace StrategyProcessor.Shared;

[ServiceContract]
public interface IStrategyServiceGrpc
{
    [OperationContract]
    Task<Answer<Guid>> Start(StrategyStartDto arguments, CallContext context = default);

    [OperationContract]
    Task StopStrategy(StrategyStopDto arguments, CallContext context = default);
}