﻿using System.Runtime.Serialization;
using Domain;

namespace StrategyProcessor.Shared;

[DataContract]
public class TestOfStrategyArgument
{
    [DataMember(Order = 1)] public required Guid RunId { get; set; }
    [DataMember(Order = 2)] public required Pair Pair { get; set; }
    [DataMember(Order = 3)] public required TimeFrame TimeFrame { get; set; }
    [DataMember(Order = 4)] public required DateTimeRange DateTimeRange { get; set; }
}