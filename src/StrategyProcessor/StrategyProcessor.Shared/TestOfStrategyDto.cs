﻿using System.Runtime.Serialization;
using Domain.StrategyProcessor;

namespace StrategyProcessor.Shared;

[DataContract]
public class TestOfStrategyDto
{
    [DataMember(Order = 1)] public List<Note> Notes { get; set; } = new();
    [DataMember(Order = 2)] public List<OrderDto> Orders { get; set; } = new();
    [DataMember(Order = 3)] public List<ChartDto> Charts { get; set; } = new();
    [DataMember(Order = 4)] public StatusOfRun Status { get; set; }
}