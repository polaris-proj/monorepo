﻿using System.Runtime.Serialization;
using Domain.StrategyProcessor;

namespace StrategyProcessor.Shared;

[DataContract]
public class StrategyStartDto
{
    [DataMember(Order = 1)] public required Guid StrategyId { get; set; }
    [DataMember(Order = 2)] public required StrategyParameterDto[] Parameters { get; set; }
        = Array.Empty<StrategyParameterDto>();

    [DataMember(Order = 3)] public required ExchangePairTimeFrameDto[] DataSources { get; set; }
    [DataMember(Order = 4)] public required Guid UserId { get; set; }
    [DataMember(Order = 5)] public bool EnableOrderConfirmation { get; set; }
    [DataMember(Order = 6)] public required RunMode RunMode { get; set; }
    [DataMember(Order = 7)] public decimal InitialBalance { get; set; } // only for test mode
    [DataMember(Order = 8)] public DateTime StartTime { get; set; } // only for test mode
    [DataMember(Order = 9)] public DateTime EndTime { get; set; } // only for test mode
}