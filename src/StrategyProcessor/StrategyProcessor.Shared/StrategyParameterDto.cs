﻿using System.Runtime.Serialization;

#pragma warning disable CS8618

namespace StrategyProcessor.Shared;

[DataContract]
public class StrategyParameterDto
{
    [DataMember(Order = 1)] public required string Name { get; set; }
    [DataMember(Order = 2)] public required string JsonValue { get; set; }
    [DataMember(Order = 3)] public required Type ParameterType { get; set; }
}