﻿using System.Runtime.Serialization;
using Domain.StrategyProcessor;

namespace StrategyProcessor.Shared;

[DataContract]
public class StrategyDto
{
    [DataMember(Order = 1)] public required Guid Id { get; set; }

    [Obsolete("в будущем будет удалено, для идентификации сипользуйте Id")]
    [DataMember(Order = 2)]
    public string InternalName { get; set; } = string.Empty;
    [DataMember(Order = 3)] public required StrategyConnectorType StrategyConnectorType { get; set; }
    [DataMember(Order = 4)] public required StrategyParameterDto[] Parameters { get; set; } = Array.Empty<StrategyParameterDto>();
}