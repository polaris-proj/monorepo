﻿using System.Runtime.Serialization;
using Domain;
using Domain.StrategyProcessor;

namespace StrategyProcessor.Shared;

[DataContract]
public class ChartDto
{
    [DataMember(Order = 1)] public required string Name { get; set; }
    [DataMember(Order = 2)] public string? Description { get; set; }
    [DataMember(Order = 3)] public required Pair Pair { get; set; }
    [DataMember(Order = 4)] public required TimeFrame TimeFrame { get; set; }
    [DataMember(Order = 5)] public required List<Point> Chart { get; set; } = new List<Point>();
}