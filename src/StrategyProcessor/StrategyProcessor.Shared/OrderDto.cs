﻿using System.Runtime.Serialization;
using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;


namespace StrategyProcessor.Shared;

[DataContract]
public class OrderDto
{
    [DataMember(Order = 1)] public required Pair Pair { get; set; }
    [DataMember(Order = 2)] public OrderType Type { get; set; }
    [DataMember(Order = 3)] public OrderStatus Status { get; set; }
    [DataMember(Order = 4)] public decimal? Price { get; set; }
    [DataMember(Order = 5)] public decimal Amount { get; set; }
    [DataMember(Order = 6)] public string? OrderId { get; set; }
    [DataMember(Order = 7)] public DateTime CreatedAt { get; set; }
    [DataMember(Order = 8)] public DateTime? UpdatedAt { get; set; }
    [DataMember(Order = 9)] public int? Leverage { get; set; }
    [DataMember(Order = 10)] public decimal? TakePrice { get; set; }
    [DataMember(Order = 11)] public decimal? StopPrice { get; set; }
}