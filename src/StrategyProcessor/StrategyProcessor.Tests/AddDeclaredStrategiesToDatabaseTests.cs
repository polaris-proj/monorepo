﻿using System.Reflection;
using Domain.Connectors;
using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Infrastructure.DAL.Repositories;
using StrategyProcessor.Services;
using StrategyProcessor.Tests.Common;

namespace StrategyProcessor.Tests;

[NonParallelizable]
[TestFixture]
public class AddDeclaredStrategiesToDatabaseTests
{
    [SetUp]
    public void Setup()
    {
        _mockedStrategyRepository = new MockedStrategyRepository();
        _strategyRepository = _mockedStrategyRepository.GetStrategyDataStorage();
        _addDeclaredStrategiesToDatabase = new AddDeclaredStrategiesToDatabase(_strategyRepository, _darkMagic);
    }

    [Test]
    public async Task CheckThatAllStrategiesAddedToDatabase()
    {
        var allStrategies = _darkMagic.GetAllStrategiesTypes();
        await _addDeclaredStrategiesToDatabase.Start();
        _mockedStrategyRepository.StrategyDbos.Count.Should().Be(allStrategies.Count());
    }

    [Test]
    public async Task AllStrategiesAddedInDatabase_ShouldHasFullName()
    {
        var allStrategies = _darkMagic.GetAllStrategiesTypes().ToArray();
        allStrategies.Should().NotBeEmpty();
        await _addDeclaredStrategiesToDatabase.Start();

        var namesFromDb = _mockedStrategyRepository.StrategyDbos.Select(x => x.InternalName);
        var strategyNames = allStrategies.Select(x => x.FullName);

        strategyNames.Should().BeEquivalentTo(namesFromDb);
    }

    [Test]
    public async Task AllStrategiesAddedInDatabase_ShouldHasStrategyType()
    {
        var allStrategies = _darkMagic.GetAllStrategiesTypes().ToArray();
        allStrategies.Should().NotBeEmpty();
        await _addDeclaredStrategiesToDatabase.Start();

        var namesFromDb = _mockedStrategyRepository.StrategyDbos.Select(x => (x.InternalName, StrategyType: x.StrategyConnectorType));

        var strategyNames = allStrategies.Select(x => (x.FullName,
            x.BaseType!.GetGenericArguments()[0] == typeof(ISpotConnector) ? StrategyConnectorType.Spot : StrategyConnectorType.Futures));

        strategyNames.Should().BeEquivalentTo(namesFromDb);
    }

    private IStrategyRepository _strategyRepository = null!;
    private readonly IDarkMagic _darkMagic = new DarkMagic(Assembly.GetExecutingAssembly());
    private AddDeclaredStrategiesToDatabase _addDeclaredStrategiesToDatabase = null!;
    private MockedStrategyRepository _mockedStrategyRepository = null!;
}