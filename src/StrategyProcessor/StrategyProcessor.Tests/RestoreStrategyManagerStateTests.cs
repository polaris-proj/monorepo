﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using EventStore.Client;
using Infrastructure.EventDriven;
using IODataModule.Client.Clients;
using Microsoft.Extensions.Logging;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Infrastructure.Mappers;
using StrategyProcessor.Services;
using StrategyProcessor.Strategy;
using StrategyProcessor.Tests.Common;

namespace StrategyProcessor.Tests;

[NonParallelizable]
[TestFixture]
public class RestoreStrategyManagerStateTests
{
    [SetUp]
    public void Setup()
    {
        _tradingStrategyManagerStorage = new TradingStrategyManagerStorage();
        _testingStrategyManagerStorage = new TestingStrategyManagerStorage();
        var spotMock = new Mock<ISpotConnector>();
        var futuresMock = new Mock<IFuturesConnector>();
        _remoteConnectorFactory = new MockedRemoteConnectorFactory(spotMock.Object, futuresMock.Object);

        var orderSenderFactory = new OrderSenderClientFactory(null!);

        _strategyManagerRepository = new InMemoryStrategyManagerRepository();
        var strategyManagerFactory = new StrategyManagerFactory(_strategyManagerRepository, new DarkMagic());

        _chartRepositoryMock = new Mock<IChartRepository>();
        _notesRepositoryMock = new Mock<INotesRepository>();
        _orderRepositoryMock = new Mock<IOrderRepository>();
        _runStrategyRepositoryMock = new Mock<IRunStrategyRepository>();

        var candleServiceMock = new Mock<ICandleService>();
        candleServiceMock.Setup(x => x.GetRange(It.IsAny<Guid>(), It.IsAny<ExchangePairTimeFrame>(),
                It.IsAny<long>(), It.IsAny<long>()))
            .ReturnsAsync((Guid _, (Exchange, Pair, TimeFrame ) _, long _, long _) => _candles);

        candleServiceMock.Setup(x => x.GetRangeStream(It.IsAny<Guid>(), It.IsAny<ExchangePairTimeFrame>(),
                It.IsAny<long>(), It.IsAny<long>()))
            .Returns((Guid _, (Exchange, Pair, TimeFrame ) _, long _, long _) => _candles.ToAsyncEnumerable());
        
        var startStopStrategyService = new StrategyService<TestStrategy1, ISpotConnector>(
            new RealTimeDataSource(), _runStrategyRepositoryMock.Object, _orderRepositoryMock.Object,
            _chartRepositoryMock.Object, _notesRepositoryMock.Object, _tradingStrategyManagerStorage,
            _testingStrategyManagerStorage, _strategyManagerRepository, strategyManagerFactory, _remoteConnectorFactory,
            orderSenderFactory, candleServiceMock.Object, new Mock<IEventStoreClient>().Object);

        var startStopStrategyFactory = new Mock<IStartStopStrategyServiceTypedFactory>();
        startStopStrategyFactory.Setup(x => x.GetInstance(It.IsAny<Type>())).Returns(startStopStrategyService);

        _restoreStrategyManagerState = new RestoreStrategyManagerState(_strategyManagerRepository,
            startStopStrategyFactory.Object, new DarkMagic(), new LoggerFactory());
    }

    [Test]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(16)]
    public async Task CheckThatRestoreRealRestoreStrategies(int amount)
    {
        for (var i = 0; i < amount; i++)
        {
            var sm = GetStrategyManagerDbo();
            await _strategyManagerRepository.CreateAsync(sm);
        }

        await _restoreStrategyManagerState.Start();
        _tradingStrategyManagerStorage.StrategyManagers.Count.Should().Be(amount);
    }

    private StrategyManagerDbo GetStrategyManagerDbo()
    {
        var strategyParameters = new StartStrategyParameters(
            Guid.NewGuid(),
            Guid.NewGuid(),
            RunMode.Real,
            false,
            new List<ExchangePairTimeFrame> {new(Exchange.BinanceSpot, new Pair("1", "2"), TimeFrame.h1)},
            new List<StrategyParameterDbo>
            {
                new StrategyParameter<DateOnly>("Date", DateOnly.FromDateTime(DateTime.Now)).ToDbo(),
                new StrategyParameter<int>("Number", 312).ToDbo()
            },
            new List<Balance> {new Balance("USDT", 100, 0)},
            new DateTimeRange(DateTime.Now, DateTime.Now));

        var strategy = new TestStrategy1
        {
            Num = 100,
            Date = DateOnly.FromDateTime(DateTime.Now),
        };
        var exchangeToStrategiesListJson = new Dictionary<Exchange, List<TestStrategy1>>
        {
            {
                Exchange.BinanceSpot,
                [strategy]
            }
        };

        return new StrategyManagerDbo
        {
            StrategyManagerState = new StrategyManagerState<TestStrategy1>
            {
                ActiveStrategiesByExchange = exchangeToStrategiesListJson,
                ActiveStrategiesByExchangePairTimeFrame = new Dictionary<ExchangePairTimeFrame, List<TestStrategy1>>(),
                LastHandledEventTimeStamp = new Dictionary<ExchangePairTimeFrame, long>()
                {
                    {new ExchangePairTimeFrame(Exchange.BinanceSpot, new Pair("1", "2"), TimeFrame.h1), 1516515},
                },
            }.Serialize(),
            RunId = Guid.NewGuid(),
            StrategyType = typeof(TestStrategy1),
            UserId = Guid.NewGuid(),
            StartStrategyParameters = strategyParameters
        };
    }

    public record TestStrategy1 : BaseStrategy<ISpotConnector>
    {
        [StrategyParameter("Date")] public DateOnly Date { get; set; }
        [StrategyParameter("Number")] public int Num { get; set; }
    }

    private IStrategyManagerRepository _strategyManagerRepository = null!;
    private RestoreStrategyManagerState _restoreStrategyManagerState = null!;
    private MockedRemoteConnectorFactory _remoteConnectorFactory = null!;
    private readonly List<Candle> _candles = new() {new(1, 1, 1, 1, 1)};
    private TradingStrategyManagerStorage _tradingStrategyManagerStorage = null!;
    private TestingStrategyManagerStorage _testingStrategyManagerStorage = null!;
    private Mock<IChartRepository> _chartRepositoryMock = null!;
    private Mock<INotesRepository> _notesRepositoryMock = null!;
    private Mock<IRunStrategyRepository> _runStrategyRepositoryMock = null!;
    private Mock<IOrderRepository> _orderRepositoryMock = null!;
}