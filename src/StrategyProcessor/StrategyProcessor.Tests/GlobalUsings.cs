﻿// Global using directives

global using Domain;
global using FluentAssertions;
global using Infrastructure;
global using Moq;
global using static Domain.TimeFrames;