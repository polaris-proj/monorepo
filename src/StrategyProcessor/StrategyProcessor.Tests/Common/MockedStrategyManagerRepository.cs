﻿using System.Collections.Concurrent;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;

namespace StrategyProcessor.Tests.Common
{
    public class InMemoryStrategyManagerRepository : IStrategyManagerRepository
    {
        private readonly ConcurrentDictionary<Guid, StrategyManagerDbo> _strategyManagers = new();

        public Task<IReadOnlyCollection<StrategyManagerDbo>> ReadAll()
        {
            return Task.FromResult<IReadOnlyCollection<StrategyManagerDbo>>(_strategyManagers.Values.ToList());
        }

        public Task<StrategyManagerDbo?> ReadAsync(Guid id)
        {
            _strategyManagers.TryGetValue(id, out var strategyManager);
            return Task.FromResult(strategyManager);
        }

        public async Task HandleStates(TimeStampByTimeFramePointer pointer,
            Func<IEnumerable<StrategyManagerDbo>, Task<IEnumerable<StrategyManagerDbo>>> doInTransaction)
        {
            throw new NotImplementedException();
        }

        public Task<Guid> CreateAsync(StrategyManagerDbo item)
        {
            _strategyManagers[item.RunId] = item;
            return Task.FromResult(item.RunId);
        }

        [Obsolete($"Use {nameof(HandleStates)}")]
        public Task UpdateAsync(StrategyManagerDbo item)
        {
            _strategyManagers[item.RunId] = item;
            return Task.CompletedTask;
        }
    }
}