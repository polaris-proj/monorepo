﻿using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories;

namespace StrategyProcessor.Tests.Common;

internal class MockedStrategyRepository
{
    public List<StrategyDbo> StrategyDbos { get; private set; } = null!;

    public IStrategyRepository GetStrategyDataStorage()
    {
        StrategyDbos = new List<StrategyDbo>();
        var mock = new Mock<IStrategyRepository>();
        mock.Setup(a => a.CreateAsync(It.IsAny<StrategyDbo>()))
            .Callback<StrategyDbo>(x => StrategyDbos.Add(x));
        mock.Setup(a => a.Update(It.IsAny<StrategyDbo>()))
            .Callback<StrategyDbo>(x => StrategyDbos.Add(x));
        return mock.Object;
    }
}