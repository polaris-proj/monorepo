﻿using Domain.Connectors;
using Infrastructure.Extensions;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Tests.Common.InMemoryStorages;

public class InMemoryOrderRepository : IOrderRepository
{
    public Task AddOrdersAsync(Guid runId, IEnumerable<OrderDto> orders)
    {
        if (!_orders.ContainsKey(runId))
        {
            _orders[runId] = new List<OrderDto>();
        }

        _orders[runId].AddRange(orders);
        return Task.CompletedTask;
    }

    public Task CloseOrderAsync(OrderId orderId)
    {
        foreach (var orderList in _orders.Values)
        {
            var order = orderList.FirstOrDefault(o => o.OrderId == orderId.Id);
            if (order != null)
            {
                order.Status = OrderStatus.Close;
                return Task.CompletedTask;
            }
        }

        throw new Exception("Order not found");
    }

    public Task<IEnumerable<OrderDto>> ReadAsync(Guid runId, Pair pair, TimeFrame timeFrame, DateTimeRange dateTimeRange)
    {
        if (_orders.TryGetValue(runId, out var orderList))
        {
            var orders = orderList.Where(o => o.Pair == pair && dateTimeRange.Contains(o.CreatedAt));
            return Task.FromResult(orders);
        }

        return Task.FromResult(Enumerable.Empty<OrderDto>());
    }

    private readonly Dictionary<Guid, List<OrderDto>> _orders = new();
}