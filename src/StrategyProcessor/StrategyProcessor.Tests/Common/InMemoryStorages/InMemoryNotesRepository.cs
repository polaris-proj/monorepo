﻿using Domain.StrategyProcessor;
using Infrastructure.Extensions;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

namespace StrategyProcessor.Tests.Common.InMemoryStorages;

public class InMemoryNotesRepository : INotesRepository
{
    private readonly Dictionary<Guid, List<Note>> _notes = new();

    public Task AddNotesAsync(Guid runId, IEnumerable<Note> notes)
    {
        if (!_notes.ContainsKey(runId))
        {
            _notes[runId] = new List<Note>();
        }

        _notes[runId].AddRange(notes);
        return Task.CompletedTask;
    }

    public Task<IEnumerable<Note>> ReadAsync(Guid runId, Pair pair, TimeFrame timeFrame, DateTimeRange dateTimeRange)
    {
        if (_notes.TryGetValue(runId, out var noteList))
        {
            var notes = noteList.Where(
                n => n.Pair == pair
                     && n.TimeFrame == timeFrame
                     && dateTimeRange.Contains(n.TimeStamp.ToDateTime())
            );
            return Task.FromResult(notes);
        }

        return Task.FromResult(Enumerable.Empty<Note>());
    }
}