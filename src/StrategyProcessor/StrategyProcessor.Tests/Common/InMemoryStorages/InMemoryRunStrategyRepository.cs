﻿using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

namespace StrategyProcessor.Tests.Common.InMemoryStorages;

internal class InMemoryRunStrategyRepository : IRunStrategyRepository
{
    public IEnumerable<RunOfStrategyDbo> ReadAll()
    {
        return _runStrategies;
    }

    public Task<List<RunOfStrategyDbo>> ReadAllAsync()
    {
        return Task.FromResult(_runStrategies);
    }

    public Guid Create(RunOfStrategyDbo item)
    {
        _runStrategies.Add(item);
        return item.Id;
    }

    public Task<Guid> CreateAsync(RunOfStrategyDbo item)
    {
        _runStrategies.Add(item);
        return Task.FromResult(item.Id);
    }


    public Task<RunOfStrategyDbo?> ReadAsync(Guid runId)
    {
        return Task.FromResult(_runStrategies.FirstOrDefault(rs => rs.Id == runId));
    }

    public Task UpdateStatusAsync(Guid id, StatusOfRun statusOfRun)
    {
        var item = _runStrategies.FirstOrDefault(rs => rs.Id == id);
        if (item == null)
        {
            throw new InvalidOperationException($"TestOfStrategyDbo with id {id} not found");
        }

        item.StatusOfRun = statusOfRun;
        return Task.CompletedTask;
    }
    
    private List<RunOfStrategyDbo> _runStrategies = new();
}