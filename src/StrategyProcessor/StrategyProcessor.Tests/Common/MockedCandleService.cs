﻿using IODataModule.Client.Clients;

namespace StrategyProcessor.Tests.Common;

internal class MockedCandleService
{
    public List<Candle> Candles { get; private set; } = new();

    public ICandleService GetStorage()
    {
        var mock = new Mock<ICandleService>();

        return mock.Object;
    }
}