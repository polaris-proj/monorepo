﻿using Domain.Connectors;
using StrategyProcessor.Infrastructure.Connectors.Configurators;

namespace StrategyProcessor.Tests.Common;

public class MockedRemoteConnectorFactory : IRemoteConnectorFactory
{
    public MockedRemoteConnectorFactory(ISpotConnector spotConnector, IFuturesConnector futuresConnector)
    {
        SpotConnector = spotConnector;
        FuturesConnector = futuresConnector;
    }

    public ISpotConnector SpotConnector { get; set; }
    public IFuturesConnector FuturesConnector { get; set; }

    public ISpotConnector CreateSpotConnector(Guid userId, Exchange exchange)
    {
        return SpotConnector;
    }

    public IFuturesConnector CreateFuturesConnector(Guid userId, Exchange exchange)
    {
        return FuturesConnector;
    }

    public TConnector CreateConnector<TConnector>(Guid userId, Exchange exchange)
    {
        return typeof(TConnector) switch
        {
            var t when t == typeof(ISpotConnector) => (TConnector) CreateSpotConnector(userId, exchange),
            var t when t == typeof(IFuturesConnector) => (TConnector) CreateFuturesConnector(userId, exchange),
            _ => throw new NotSupportedException()
        };
    }
}