﻿using Infrastructure.EventDriven;

namespace StrategyProcessor.Tests.Common;

internal static class MockedConsumerWithExternalHandlers<T>
{
    public static (IConsumerWithExternalHandlers<T> Consumer, ActionWrapper<T> Handle) GetObject()
    {
        ActionWrapper<T> wrapper = new();
        var mock = new Mock<IConsumerWithExternalHandlers<T>>();
        mock.Setup(foo => foo.RegisterHandler(It.IsAny<Func<T, Task>>()))
            .Callback((Func<T, Task> x) => wrapper.Handler = t=> x(t).GetAwaiter().GetResult());

        return (mock.Object, wrapper);
    }
}

public class ActionWrapper<T>
{
    public Action<T> Handler { get; set; } = null!;
}