﻿using Domain.Connectors;
using IODataModule.Client;
using IODataModule.Client.Clients;
using StrategyProcessor.Infrastructure.Connectors.Configurators;

namespace StrategyProcessor.Tests.ConnectorFactoryTests;

[Parallelizable(ParallelScope.Fixtures)]
public class RemoteConnectorFactoryTests
{
    [SetUp]
    public void Setup()
    {
        _factory = new RemoteConnectorFactory(new IoDataModuleClientChannel {Channel = null!});
    }

    [Test]
    public void CreateSpotConnector_Should_Return_RemoteSpotConnector_Instance()
    {
        var userId = Guid.NewGuid();
        var exchange = new Exchange();

        var connector = _factory.CreateSpotConnector(userId, exchange);

        connector.Should().NotBeNull();
        connector.Should().BeOfType<RemoteSpotConnector>();
    }

    [Test]
    public void CreateFuturesConnector_Should_Return_RemoteFuturesConnector_Instance()
    {
        var userId = Guid.NewGuid();
        var exchange = new Exchange();

        var connector = _factory.CreateFuturesConnector(userId, exchange);

        connector.Should().NotBeNull();
        connector.Should().BeOfType<RemoteFuturesConnector>();
    }

    [Test]
    public void CreateConnector_With_ISpotConnector_Should_Return_RemoteSpotConnector_Instance()
    {
        // Arrange
        var userId = Guid.NewGuid();
        var exchange = new Exchange();

        // Act
        var connector = _factory.CreateConnector<ISpotConnector>(userId, exchange);

        // Assert
        connector.Should().NotBeNull();
        connector.Should().BeOfType<RemoteSpotConnector>();
    }

    [Test]
    public void CreateConnector_With_IFuturesConnector_Should_Return_RemoteFuturesConnector_Instance()
    {
        // Arrange
        var userId = Guid.NewGuid();
        var exchange = new Exchange();

        // Act
        var connector = _factory.CreateConnector<IFuturesConnector>(userId, exchange);

        // Assert
        connector.Should().NotBeNull();
        connector.Should().BeOfType<RemoteFuturesConnector>();
    }

    [Test]
    public void CreateConnector_With_Invalid_GenericType_Should_Throw_NotSupportedException()
    {
        // Arrange
        var userId = Guid.NewGuid();
        var exchange = new Exchange();

        // Act
        Action act = () => _factory.CreateConnector<object>(userId, exchange);

        // Assert
        act.Should().Throw<NotSupportedException>();
    }

    private RemoteConnectorFactory _factory = null!;
}