﻿using Domain.Connectors;
using Infrastructure.EventDriven;
using OrderConfirmation.Client;
using StrategyProcessor.Infrastructure.Connectors.Configurators;
using StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderConfirmation;
using StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

namespace StrategyProcessor.Tests.ConnectorFactoryTests;

[Parallelizable(ParallelScope.Fixtures)]
public class ExchangeConnectorBuilderTests
{
    [Test]
    public void WithEmulatingTrading_Should_Return_SpotConnector_With_VirtualExchange()
    {
        var inputDataSource = new Mock<IDataSource>().Object;
        var initialBalance = new[] {new Balance(new Ticker("1"), 1, 1)};

             var connector = _spotConnectorMock.Object.WithEmulatingTrading(inputDataSource, initialBalance);
             
        connector.Should().NotBeNull();
        connector.Should().BeAssignableTo<ISpotConnector>();
    }

    [Test]
    public void WithEmulatingTrading_Should_Return_FuturesConnector_With_VirtualExchange()
    {
        var inputDataSource = new Mock<IDataSource>().Object;
        var initialBalance = new[] {new Balance(new Ticker("1"), 1, 1)};
        
        var connector = _futuresConnectorMock.Object.WithEmulatingTrading(inputDataSource, initialBalance);
        
        connector.Should().NotBeNull();
        connector.Should().BeAssignableTo<IFuturesConnector>();
    }

    [Test]
    public void WithTradeLogging_Should_Return_SpotConnector_With_TradeLogger()
    {
        var factoryTestOfStrategyMock = new Mock<IOrderRepository>();
        var runId = Guid.NewGuid();
        var isRealTime = true;
        
        var connector = _spotConnectorMock.Object.WithTradeLogging(factoryTestOfStrategyMock.Object, runId, isRealTime);
        
        connector.Should().NotBeNull();
        connector.Should().BeOfType<SpotConnectorWithOrderLogger>();
        connector.Should().BeAssignableTo<ISpotConnector>();
    }

    [Test]
    public void WithTradeLogging_Should_Return_FuturesConnector_With_TradeLogger()
    {
        var factoryTestOfStrategyMock = new Mock<IOrderRepository>();
        var runId = Guid.NewGuid();
        var isRealTime = true;
        
        var connector = _futuresConnectorMock.Object.WithTradeLogging(factoryTestOfStrategyMock.Object, runId, isRealTime);
        
        connector.Should().NotBeNull();
        connector.Should().BeOfType<FuturesConnectorWithOrderLogger>();
        connector.Should().BeAssignableTo<IFuturesConnector>();
    }

    [Test]
    public void WithOrderConfirmation_Should_Return_SpotConnector_With_OrderConfirmation()
    {
        var orderSenderClientMock = new Mock<IOrderSenderClient>().Object;
        
        var connector = _spotConnectorMock.Object.WithOrderConfirmation(orderSenderClientMock);
        
        connector.Should().NotBeNull();
        connector.Should().BeOfType<SpotConnectorWithOrderConfirmation>();
        connector.Should().BeAssignableTo<ISpotConnector>();
    }

    [Test]
    public void WithOrderConfirmation_Should_Return_FuturesConnector_With_OrderConfirmation()
    {
        var orderSenderClientMock = new Mock<IOrderSenderClient>().Object;
        
        var connector = _futuresConnectorMock.Object.WithOrderConfirmation(orderSenderClientMock);
        
        connector.Should().NotBeNull();
        connector.Should().BeOfType<FuturesConnectorWithOrderConfirmation>();
        connector.Should().BeAssignableTo<IFuturesConnector>();
    }
        
    [Test]
    public void WithOrderConfirmation_Generic_Should_Return_SpotConnector_With_OrderConfirmation()
    {
        var orderSenderClientMock = new Mock<IOrderSenderClient>().Object;
        
        var connector = _spotConnectorMock.Object.WithOrderConfirmation<ISpotConnector>(orderSenderClientMock);
        
        connector.Should().NotBeNull();
        connector.Should().BeOfType<SpotConnectorWithOrderConfirmation>();
        connector.Should().BeAssignableTo<ISpotConnector>();
    }

    [Test]
    public void WithOrderConfirmation_Generic_Should_Return_FuturesConnector_With_OrderConfirmation()
    {
        var orderSenderClientMock = new Mock<IOrderSenderClient>().Object;
        
        var connector = _futuresConnectorMock.Object.WithOrderConfirmation<IFuturesConnector>(orderSenderClientMock);
        
        connector.Should().NotBeNull();
        connector.Should().BeOfType<FuturesConnectorWithOrderConfirmation>();
        connector.Should().BeAssignableTo<IFuturesConnector>();
    }
        
    [Test]
    public void WithOrderConfirmation_Generic_Should_Throw_UnknownConnectorType()
    {
        var orderSenderClientMock = new Mock<IOrderSenderClient>().Object;
        
        Action test = () => _futuresConnectorMock.Object.WithOrderConfirmation<IConnector>(orderSenderClientMock);

        test.Should().Throw<NotSupportedException>();
    }
        
    [Test]
    public void WithTradeLogging_Generic_Should_Throw_UnknownConnectorType()
    {
        var orderRepositoryMock = new Mock<IOrderRepository>();
        var runId = Guid.NewGuid();
        var isRealTime = true;
            
        Action test = () => _futuresConnectorMock.Object.WithTradeLogging<IConnector>(orderRepositoryMock.Object, runId, isRealTime);
        
        test.Should().Throw<NotSupportedException>();
    }
        
        
    [Test]
    public void WithEmulatingTrading_Generic_Should_Throw_UnknownConnectorType()
    {
        var inputDataSource = new Mock<IDataSource>().Object;
        var initialBalance = new[] {new Balance(new Ticker("1"), 1, 1)};
            
        Action test = () => _futuresConnectorMock.Object.WithEmulatingTrading<IConnector>(inputDataSource, initialBalance);
        
        test.Should().Throw<NotSupportedException>();
    }

    private Mock<ISpotConnector> _spotConnectorMock = new();
    private Mock<IFuturesConnector> _futuresConnectorMock = new();
}