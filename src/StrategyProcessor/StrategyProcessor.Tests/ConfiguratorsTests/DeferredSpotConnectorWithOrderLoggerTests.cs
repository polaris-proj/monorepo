﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;
using StrategyProcessor.Tests.Common.InMemoryStorages;

namespace StrategyProcessor.Tests.ConfiguratorsTests;

public class DeferredSpotConnectorWithOrderLoggerTests
{
    [Test]
    public async Task CreateMarketOrder_CallsCreateMarketOrderInConnector_AndLogsOrder()
    {
        // Arrange
        var pair = new Pair("BTC", "USDT");
        var orderType = MarketOrder.Buy;
        var amount = 1;
        var expectedOrderId = new OrderId("xyz123");

        var runId = Guid.NewGuid();

        var connectorMock = new Mock<ISpotConnector>();
        var orderRepository = new InMemoryOrderRepository();
        var connectorWithOrderLogger = new DeferredSpotConnectorWithOrderLogger(connectorMock.Object, orderRepository, runId);
        
        connectorMock.Setup(x => x.CreateMarketOrder(pair, orderType, amount))
            .ReturnsAsync(expectedOrderId);

        // Act
        var result = await connectorWithOrderLogger.CreateMarketOrder(pair, orderType, amount);
        await connectorWithOrderLogger.DisposeAsync();

        // Assert
        result.Should().Be(expectedOrderId);
        connectorMock.Verify(x => x.CreateMarketOrder(pair, orderType, amount), Times.Once);

        (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange)).Count().Should().Be(1);
        var loggedOrder = (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange)).First();

        loggedOrder.Should().NotBeNull();
        loggedOrder.Pair.Should().Be(pair);
        loggedOrder.OrderId.Should().Be(expectedOrderId.Id);
        loggedOrder.Amount.Should().Be(amount);
        loggedOrder.Type.Should().Be(OrderType.Buy);
    }

    [Test]
    public async Task CreateLimitOrder_CallsCreateLimitOrderInConnector_AndLogsOrder()
    {
        // Arrange
        var pair = new Pair("BTC", "USDT");
        var orderType = LimitOrder.BuyLimit;
        var entry = new PlacementOrder(1, 100);
        var expectedOrderId = new OrderId("xyz123");

        var runId = Guid.NewGuid();

        var connectorMock = new Mock<ISpotConnector>();
        var orderRepository = new InMemoryOrderRepository();
        var connectorWithOrderLogger = new DeferredSpotConnectorWithOrderLogger(connectorMock.Object, orderRepository, runId);
        
        connectorMock.Setup(x => x.CreateLimitOrder(pair, orderType, entry)).ReturnsAsync(expectedOrderId);

        // Act
        var result = await connectorWithOrderLogger.CreateLimitOrder(pair, orderType, entry);
        await connectorWithOrderLogger.DisposeAsync();

        // Assert
        result.Should().Be(expectedOrderId);
        connectorMock.Verify(x => x.CreateLimitOrder(pair, orderType, entry), Times.Once);

        (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange)).Count().Should().Be(1);
        var loggedOrder =
            (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange)).First();

        loggedOrder.Should().NotBeNull();
        loggedOrder.Pair.Should().Be(pair);
        loggedOrder.OrderId.Should().Be(expectedOrderId.Id);
        loggedOrder.Amount.Should().Be(entry.Amount);
        loggedOrder.Price.Should().Be(entry.Price);
        loggedOrder.Type.Should().Be(OrderType.BuyLimit);
    }

    [Test]
    public async Task DisposeAsync_DisposeInnerConnector()
    {
        var runId = Guid.NewGuid();

        var connectorMock = new Mock<ISpotConnector>();
        var orderRepository = new InMemoryOrderRepository();
        var connectorWithOrderLogger = new DeferredSpotConnectorWithOrderLogger(connectorMock.Object, orderRepository, runId);
        
        connectorMock.Verify(x => x.DisposeAsync(), Times.Never);
        await connectorWithOrderLogger.DisposeAsync();
        connectorMock.Verify(x => x.DisposeAsync(), Times.Once());
    }
}