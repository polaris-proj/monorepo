﻿using AutoFixture;
using AutoFixture.Kernel;
using Domain.Connectors;
using MoreLinq;
using StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Shared;
using StrategyProcessor.Tests.Common.InMemoryStorages;

namespace StrategyProcessor.Tests.ConfiguratorsTests;

public class DeferredBaseConnectorWithOrderLoggerTests
{
    [Test]
    public async Task LogOrder_AddsOrderToOrderDbosList()
    {
        // Arrange
        var order = GetFixture().Create<OrderDto>();
        var runId = Guid.NewGuid();
        var connectorMock = new Mock<IConnector>();
        var orderRepository = new InMemoryOrderRepository();

        var connectorWithOrderLogger = new DeferredBaseConnectorWithOrderLogger<IConnector>(
            connectorMock.Object, orderRepository, runId);
        // Act
        var act = async () => await connectorWithOrderLogger.LogOrder(order);

        // Assert
        await act.Should().NotThrowAsync();
        (await orderRepository.ReadAsync(runId, order.Pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange)).Should().BeEmpty();
    }


    [Test]
    public async Task DisposeAsync_AddsOrdersToTestOfStrategyRepository_AndClearsOrderDbosList()
    {
        var pair = new Pair("BTC", "USDT");
        var runId = Guid.NewGuid();
        // Arrange
        var orders = GetFixture().CreateMany<OrderDto>().Pipe(x =>
            {
                x.Pair = pair;
                x.CreatedAt = DateTime.UtcNow.AddMinutes(-Random.Shared.Next(0,100));
            }
        ).ToArray();
        
        var connectorMock = new Mock<IConnector>();
        var orderRepository = new InMemoryOrderRepository();

        var connectorWithOrderLogger = new DeferredBaseConnectorWithOrderLogger<IConnector>(
            connectorMock.Object, orderRepository, runId);
        
        await Task.WhenAll(orders.Select(x => connectorWithOrderLogger.LogOrder(x)));

        await connectorWithOrderLogger.DisposeAsync();
        (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange))
            .Should()
            .Contain(orders);
    }

    [Test]
    public async Task DisposeAsync_DisposeInnerConnector()
    {
        var connectorMock = new Mock<IConnector>();
        var orderRepository = new InMemoryOrderRepository();
        var connectorWithOrderLogger = new DeferredBaseConnectorWithOrderLogger<IConnector>(
            connectorMock.Object, orderRepository, Guid.Empty);
        
        connectorMock.Verify(x => x.DisposeAsync(), Times.Never);
        await connectorWithOrderLogger.DisposeAsync();
        connectorMock.Verify(x => x.DisposeAsync(), Times.Once());
    }

    
    
    [Test]
    public async Task CancelOrder_CancelsOrder_UpdatesOrderStatusInStrategyTestRepository()
    {
        // Arrange
        var orderDbo = GetFixture().Create<OrderDto>();
        orderDbo.CreatedAt = DateTime.UtcNow.AddHours(-1);
        orderDbo.Pair = new Pair("12", "312");
        var runId = Guid.NewGuid();
        var orderId = new OrderId(orderDbo.OrderId);
        var pair = orderDbo.Pair;
        var connectorMock = new Mock<IConnector>();
        var orderRepository = new InMemoryOrderRepository();
        var connectorWithOrderLogger = new DeferredBaseConnectorWithOrderLogger<IConnector>(connectorMock.Object, orderRepository, runId);
        
        
        await connectorWithOrderLogger.LogOrder(orderDbo);
        // Act
        var action = async () => await connectorWithOrderLogger.CancelOrder(pair, orderId);

        // Assert
        await action.Should().NotThrowAsync();
        connectorMock.Verify(x => x.CancelOrder(pair, orderId), Times.Once);

        await connectorWithOrderLogger.DisposeAsync();
        (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange))
            .Should()
            .Contain(x => x.OrderId == orderId.Id);
        var order = (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange)).First();

        order.Status.Should().Be(OrderStatus.Close);
    }

    [Test]
    public async Task CancelOrder_NoSavedOrder_ThrowsException()
    {
        var runId = Guid.NewGuid();

        var connectorMock = new Mock<IConnector>();
        var orderRepository = new InMemoryOrderRepository();
        var connectorWithOrderLogger = new DeferredBaseConnectorWithOrderLogger<IConnector>(
            connectorMock.Object, orderRepository, runId);

        var pair = new Pair("BTC", "USDT");
        var orderId = new OrderId("12345");

        var action = async () => await connectorWithOrderLogger.CancelOrder(pair, orderId);

        await action.Should().ThrowAsync<ArgumentException>().WithMessage("Can't close not saved order");
        connectorMock.Verify(x => x.CancelOrder(pair, orderId), Times.Never);
    }

    private Fixture GetFixture()
    {
        var fixture = new Fixture();
        var orderDboCustomizer = SpecimenBuilderNodeFactory.CreateComposer<OrderDbo>().WithAutoProperties();
        var noteDboCustomizer = SpecimenBuilderNodeFactory.CreateComposer<NoteDbo>().WithAutoProperties();
        var chartDboCustomizer = SpecimenBuilderNodeFactory.CreateComposer<ChartDbo>().WithAutoProperties();
        var pairCustomizer =
            SpecimenBuilderNodeFactory.CreateComposer<Pair>().FromFactory(() => new Pair("BTC", "USDT"));

        fixture.Customizations.Add(orderDboCustomizer);
        fixture.Customizations.Add(noteDboCustomizer);
        fixture.Customizations.Add(chartDboCustomizer);
        fixture.Customizations.Add(pairCustomizer);
        return fixture;
    }
}