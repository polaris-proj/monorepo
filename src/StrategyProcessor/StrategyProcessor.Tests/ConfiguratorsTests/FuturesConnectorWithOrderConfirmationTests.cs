﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using OrderConfirmation.Client;
using StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderConfirmation;

namespace StrategyProcessor.Tests.ConfiguratorsTests;

[Parallelizable(ParallelScope.Fixtures)]
public class FuturesConnectorWithOrderConfirmationTests
{
    public FuturesConnectorWithOrderConfirmationTests()
    {
        _connectorMock = new Mock<IFuturesConnector>();
        _orderSenderClientMock = new Mock<IOrderSenderClient>();

        _futuresConnectorWithOrderConfirmation =
            new FuturesConnectorWithOrderConfirmation(_connectorMock.Object, _orderSenderClientMock.Object);
    }

    [OneTimeTearDown]
    public async Task OneTimeTearDown()
    {
        await _futuresConnectorWithOrderConfirmation.DisposeAsync();
    }

    [Test]
    public async Task CreateFuturesOrder_CallsOrderSenderClient()
    {
        // Arrange
        var pair = new Pair("BTC", "USDT");
        var orderType = FuturesOrder.Long;
        var leverage = 10;
        var entry = new PlacementOrder(1, 100);
        var take = new PlacementOrder(2, 200);
        var stop = new PlacementOrder(3, 300);
        var expectedOrderId = new OrderId("xyz123");

        _orderSenderClientMock.Setup(x => x.CreateFuturesOrder(pair, orderType, leverage, entry, take, stop))
            .ReturnsAsync(expectedOrderId);

        // Act
        var result = await _futuresConnectorWithOrderConfirmation.CreateFuturesOrder(pair, orderType, leverage, entry,
            take,
            stop);

        // Assert
        result.Should().Be(expectedOrderId);
        _orderSenderClientMock.Verify(x => x.CreateFuturesOrder(pair, orderType, leverage, entry, take, stop),
            Times.Once);
    }

    [Test]
    public async Task DisposeAsync_DisposeInnerConnector()
    {
        _connectorMock.Verify(x => x.DisposeAsync(), Times.Never);
        await _futuresConnectorWithOrderConfirmation.DisposeAsync();
        _connectorMock.Verify(x => x.DisposeAsync(), Times.Once());
    }

    private readonly FuturesConnectorWithOrderConfirmation _futuresConnectorWithOrderConfirmation;
    private readonly Mock<IOrderSenderClient> _orderSenderClientMock;
    private readonly Mock<IFuturesConnector> _connectorMock;
}