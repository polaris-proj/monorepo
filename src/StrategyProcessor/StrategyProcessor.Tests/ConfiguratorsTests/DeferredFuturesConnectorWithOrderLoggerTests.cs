﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;
using StrategyProcessor.Tests.Common.InMemoryStorages;

namespace StrategyProcessor.Tests.ConfiguratorsTests;

public class DeferredFuturesConnectorWithOrderLoggerTests
{
    [Test]
    public async Task CreateFuturesOrder_CallsCreateFuturesOrderInConnector_AndLogsOrder()
    {
        // Arrange
        var pair = new Pair("BTC", "USDT");
        var orderType = FuturesOrder.Long;
        var leverage = 10;
        var entry = new PlacementOrder(1, 100);
        var take = new PlacementOrder(2, 200);
        var stop = new PlacementOrder(3, 50);
        var expectedOrderId = new OrderId("xyz123");

        var runId = Guid.NewGuid();

        var connectorMock = new Mock<IFuturesConnector>();
        var orderRepository = new InMemoryOrderRepository();
        var connectorWithOrderLogger = new DeferredFuturesConnectorWithOrderLogger(connectorMock.Object, orderRepository, runId);
        
        connectorMock.Setup(x => x.CreateFuturesOrder(pair, orderType, leverage, entry, take, stop))
            .ReturnsAsync(expectedOrderId);

        // Act
        var result = await connectorWithOrderLogger.CreateFuturesOrder(pair, orderType, leverage, entry, take, stop);
        await connectorWithOrderLogger.DisposeAsync();

        // Assert
        result.Should().Be(expectedOrderId);
        connectorMock.Verify(x => x.CreateFuturesOrder(pair, orderType, leverage, entry, take, stop),
            Times.Once);

        (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange))
            .Count()
            .Should()
            .Be(1);
        var loggedOrder =
            (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange))
            .First();

        loggedOrder.Should().NotBeNull();
        loggedOrder.Pair.Should().Be(pair);
        loggedOrder.OrderId.Should().Be(expectedOrderId.Id);
        loggedOrder.Amount.Should().Be(entry.Amount);
        loggedOrder.Price.Should().Be(entry.Price);
        loggedOrder.TakePrice.Should().Be(take.Price);
        loggedOrder.StopPrice.Should().Be(stop.Price);
    }

    [Test]
    public async Task CreateFuturesOrder2_CallsCreateFuturesOrderInConnector_AndLogsOrder()
    {
        // Arrange
        var pair = new Pair("BTC", "USDT");
        var orderType = FuturesOrder.Long;
        var leverage = 10;
        var entry = new PlacementOrder(1, 100);
        PlacementOrder? take = null;
        PlacementOrder? stop = null;
        var expectedOrderId = new OrderId("xyz123");
        
        var runId = Guid.NewGuid();

        var connectorMock = new Mock<IFuturesConnector>();
        var orderRepository = new InMemoryOrderRepository();
        var connectorWithOrderLogger = new DeferredFuturesConnectorWithOrderLogger(connectorMock.Object, orderRepository, runId);

        connectorMock.Setup(x => x.CreateFuturesOrder(pair, orderType, leverage, entry, take, stop))
            .ReturnsAsync(expectedOrderId);

        // Act
        var result = await connectorWithOrderLogger.CreateFuturesOrder(pair, orderType, leverage, entry, take, stop);
        await connectorWithOrderLogger.DisposeAsync();

        // Assert
        result.Should().Be(expectedOrderId);
        connectorMock.Verify(x => x.CreateFuturesOrder(pair, orderType, leverage, entry, take, stop), Times.Once);

        (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange)).Count()
            .Should()
            .Be(1);
        var loggedOrder = (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange))
            .First();

        loggedOrder.Should().NotBeNull();
        loggedOrder.Pair.Should().Be(pair);
        loggedOrder.OrderId.Should().Be(expectedOrderId.Id);
        loggedOrder.Amount.Should().Be(entry.Amount);
        loggedOrder.Price.Should().Be(entry.Price);
        loggedOrder.TakePrice.Should().BeNull();
        loggedOrder.StopPrice.Should().BeNull();
    }

    [Test]
    public async Task DisposeAsync_DisposeInnerConnector()
    {
        var runId = Guid.NewGuid();

        var connectorMock = new Mock<IFuturesConnector>();
        var orderRepository = new InMemoryOrderRepository();
        var connectorWithOrderLogger = new DeferredFuturesConnectorWithOrderLogger(connectorMock.Object, orderRepository, runId);
        
        connectorMock.Verify(x => x.DisposeAsync(), Times.Never);
        await connectorWithOrderLogger.DisposeAsync();
        connectorMock.Verify(x => x.DisposeAsync(), Times.Once());
    }

}