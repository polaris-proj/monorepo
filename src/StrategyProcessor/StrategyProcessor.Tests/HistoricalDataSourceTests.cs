﻿using Infrastructure.EventDriven;
using StrategyProcessor.Tests.Common;

namespace StrategyProcessor.Tests;

public class HistoricalDataSourceTests
{
    [Test]
    public async Task CheckThatHistoricalDataSourceSendMessages()
    {
        var (consumer, _) = MockedConsumerWithExternalHandlers<int>.GetObject();

        var flags = new bool[3];
        var dataSource = new HistoricalDataSource();
        dataSource.RegisterExternalEventSource(consumer);

        dataSource.RegisterEventDestination<bool>(_ => flags[0] = true);
        dataSource.RegisterEventDestination<bool>(_ => flags[1] = true);
        dataSource.RegisterEventDestination<bool>(_ => flags[2] = true);

        await dataSource.SendEvents(new List<bool> {true});
        flags.Should().AllSatisfy(u => u.Should().Be(true));
    }

    [Test]
    public async Task CheckThatHistoricalDataSourceSendSpecificEventToCorrectTargets()
    {
        var (consumer, _) = MockedConsumerWithExternalHandlers<int>.GetObject();

        var flags = new bool[3];
        var dataSource = new HistoricalDataSource();
        dataSource.RegisterExternalEventSource(consumer);

        dataSource.RegisterEventDestination<bool>(_ => flags[0] = true);
        dataSource.RegisterEventDestination<int>(_ => flags[1] = true);
        dataSource.RegisterEventDestination<long>(_ => flags[2] = true);

        await dataSource.SendEvents(new List<bool> {true});
        flags[0].Should().Be(true);
        flags[1].Should().Be(false);
        flags[2].Should().Be(false);

        await dataSource.SendEvents(new List<int> {1});
        flags[0].Should().Be(true);
        flags[1].Should().Be(true);
        flags[2].Should().Be(false);

        await dataSource.SendEvents(new List<long> {1});
        flags[0].Should().Be(true);
        flags[1].Should().Be(true);
        flags[2].Should().Be(true);
    }


    [Test]
    public async Task CheckThatHistoricalDataSourceSendsTheSpecifiedNumberOfMessages()
    {
        var (consumer, _) = MockedConsumerWithExternalHandlers<int>.GetObject();

        var flags = new int[3];
        var dataSource = new HistoricalDataSource();
        dataSource.RegisterExternalEventSource(consumer);

        dataSource.RegisterEventDestination<int>(x => flags[0] += x);
        dataSource.RegisterEventDestination<int>(x => flags[1] += x);
        dataSource.RegisterEventDestination<int>(x => flags[2] += x);

        await dataSource.SendEvents(new List<int> {1});
        flags.Should().AllSatisfy(u => u.Should().Be(1));

        await dataSource.SendEvents(new List<int> {2, 2});
        flags.Should().AllSatisfy(u => u.Should().Be(5));

        await dataSource.SendEvents(new List<int> {3, 3, 3});
        flags.Should().AllSatisfy(u => u.Should().Be(14));
    }
}