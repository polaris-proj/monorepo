﻿using System.Reflection;
using Domain.Connectors;
using Domain.Patterns.Objects;
using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure;

namespace StrategyProcessor.Tests;

[Parallelizable(ParallelScope.Fixtures)]
internal class DarkMagicTests
{
    [Test]
    public void GetEditableProps_CheckIfGettingEditablePropsAmountCorrect()
    {
        _darkMagic.GetEditableProps(typeof(TestStrategy1)).Count.Should().Be(1);
        _darkMagic.GetEditableProps(typeof(TestStrategy2)).Count.Should().Be(2);
        _darkMagic.GetEditableProps(typeof(TestStrategy3)).Count.Should().Be(3);
    }

    [Test]
    public void GetEditableProps_CheckIfGettingEditablePropsValuesCorrect()
    {
        _darkMagic.GetEditableProps(typeof(TestStrategy1)).First().Key.Should().Be("Good Koef");

        _darkMagic.GetEditableProps(typeof(TestStrategy2)).First().Key.Should().Be("Good Koef");
        _darkMagic.GetEditableProps(typeof(TestStrategy2)).Last().Key.Should().Be("string");

        _darkMagic.GetEditableProps(typeof(TestStrategy3)).ToArray()[1].Key.Should().Be("Ints");
        _darkMagic.GetEditableProps(typeof(TestStrategy3)).First().Key.Should().Be("Good Koef");
        _darkMagic.GetEditableProps(typeof(TestStrategy3)).Last().Key.Should().Be("Time");
    }

    [Test]
    public void GetEditableProps_ReturnEmptyArrayIfNoProperties()
    {
        _darkMagic.GetEditableProps(typeof(TestStrategy11)).Should().BeEmpty();
    }

    [Test]
    public void GetAllStrategiesTypes_ReturnMoreThanZeroTypes()
    {
        _darkMagic.GetAllStrategiesTypes().Should().NotBeEmpty();
    }

    [TestCase(typeof(TestStrategy1), typeof(ISpotConnector))]
    [TestCase(typeof(TestStrategy33), typeof(IFuturesConnector))]
    public void GetConnectorType_ReturnsConnectorType(Type strategyType, Type expected)
    {
        _darkMagic.GetConnectorType(strategyType).Should().Be(expected);
    }
    
    [TestCase(typeof(TestStrategy1), StrategyConnectorType.Spot)]
    [TestCase(typeof(TestStrategy33), StrategyConnectorType.Futures)]
    public void GetTypeOfStrategy_ReturnsCorrectType(Type strategyType, StrategyConnectorType expected)
    {
        _darkMagic.GetTypeOfStrategy(strategyType).Should().Be(expected);
    }

    [Test]
    public void GetConnectorType_ThrowsException_WhenStrategyTypeDoesNotHaveBaseType()
    {
        // Arrange
        var strategyType = typeof(object);

        // Act & Assert
        Action act = () => _darkMagic.GetConnectorType(strategyType);
        act.Should().Throw<InvalidOperationException>().WithMessage("StrategyType should contains baseType");
    }

    [Test]
    public void GetConnectorType_ThrowsException_WhenBaseTypeDoesNotContainGeneric()
    {
        // Arrange
        var strategyType = typeof(MyStrategyWithoutGeneric);

        // Act & Assert
        Action act = () => _darkMagic.GetConnectorType(strategyType);
        act.Should().Throw<InvalidOperationException>().WithMessage("BaseType should contains generic");
    }

    [Test]
    public void GetTypeOfStrategy_ShouldResolveTypesForAllStrategy()
    {
        var allTypes = _darkMagic.GetAllStrategiesTypes();
        var types = new[]
        {
            typeof(TestStrategy1), typeof(TestStrategy2), typeof(TestStrategy3), typeof(TestStrategy11),
            typeof(TestStrategy22), typeof(TestStrategy33)
        };
        foreach (var type in types)
        {
            _darkMagic.GetTypeOfStrategy(type.FullName ?? throw new InvalidOperationException()).Should().Be(type);
        }

        foreach (var type in allTypes)
        {
            _darkMagic.GetTypeOfStrategy(type.FullName ?? throw new InvalidOperationException()).Should().Be(type);
        }
    }

    #region Strategies

    // ReSharper disable UnusedMember.Local
    private record TestStrategy1 : BaseStrategy<ISpotConnector>
    {
        [StrategyParameter("Good Koef")] public float Koef { get; set; } = 10f;
    }

    private record TestStrategy2 : BaseStrategy<ISpotConnector>
    {
        [StrategyParameter("Good Koef")] public float Koef { get; set; } = 10f;
        [StrategyParameter("string")] public string Gg { get; set; } = "11123csdcs";
    }

    private record TestStrategy3 : BaseStrategy<ISpotConnector>
    {
        [StrategyParameter("Good Koef")] public float Koef { get; set; } = 10f;
        [StrategyParameter("Ints")] public List<int> Ints { get; set; } = new();
        [StrategyParameter("Time")] public DateTime Time { get; set; } = DateTime.Now;
    }
    // ReSharper restore UnusedMember.Local

    private record TestStrategy11 : BaseStrategy<ISpotConnector>
    {
        public override Task EventsCatalog_MovingAverage(NewMovingAverageEvent obj)
        {
            return Task.CompletedTask;
        }
    }

    private record TestStrategy22 : BaseStrategy<ISpotConnector>
    {
        public override Task EventsCatalog_MovingAverage(NewMovingAverageEvent obj)
        {
            return Task.CompletedTask;
        }

        public override Task EventsCatalog_PP(Point obj)
        {
            return Task.CompletedTask;
        }
    }

    private record TestStrategy33 : BaseStrategy<IFuturesConnector>
    {
        public override Task EventsCatalog_MovingAverage(NewMovingAverageEvent obj)
        {
            return Task.CompletedTask;
        }

        public override Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            return Task.CompletedTask;
        }

        public override Task EventsCatalog_ReboundFromTheLevel(Point obj)
        {
            return Task.CompletedTask;
        }
    }

    private record MyStrategyWithoutBaseType;

    private record MyStrategyWithoutGeneric : MyStrategyWithoutBaseType;

    #endregion

    private readonly IDarkMagic _darkMagic = new DarkMagic(Assembly.GetExecutingAssembly());
}