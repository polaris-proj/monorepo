﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Services;

namespace StrategyProcessor.Tests;

public class StartStopStrategyServiceTypedFactoryTests
{
    [OneTimeTearDown]
    public void OneTimeTearDown()
    {
        _app.Dispose();
    }
    
    [TestCase(typeof(SomeStrategy<ISpotConnector>), typeof(ISpotConnector))]
    [TestCase(typeof(SomeStrategy<IFuturesConnector>), typeof(IFuturesConnector))]
    public void GetInstance_ShouldReturnStrategyServiceInstance(Type strategyType, Type connectorType)
    {
        var expectedServiceType = typeof(StrategyService<,>).MakeGenericType(strategyType, connectorType);
        using var scope = _app.Services.CreateScope();
        var factory = scope.ServiceProvider.GetRequiredService<IStartStopStrategyServiceTypedFactory>();
        
        var result = factory.GetInstance(strategyType);
        
        result.Should().BeOfType(expectedServiceType);
    }

    [Test]
    public void Dispose_NoCallGetInstance_ShouldNotDispose()
    {
        // Arrange
        var mockServiceScope = new Mock<IServiceScope>();
        mockServiceScope.Setup(p => p.ServiceProvider.GetService(typeof(IDisposable))).Returns(mockServiceScope.Object);


        var mock1 = new Mock<IServiceScopeFactory>();
        mock1.Setup(x => x.CreateScope()).Returns(mockServiceScope.Object);

        var mockServiceProvider = new Mock<IServiceProvider>();
        mockServiceProvider.Setup(p => p.GetService(typeof(IServiceScopeFactory)))
            .Returns(mock1.Object);

        var factory = new StartStopStrategyServiceTypedFactory(mockServiceProvider.Object, new DarkMagic());

        // Act
        factory.Dispose();

        // Assert
        mockServiceScope.Verify(s => s.Dispose(), Times.Never);
    }

    internal record SomeStrategy<TConnector> : BaseStrategy<TConnector> where TConnector : IConnector;

    private readonly IHost _app = Program.SetUp(Array.Empty<string>(), false);
}