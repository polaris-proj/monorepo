﻿using Domain.Patterns.Objects;
using Infrastructure.EventDriven;
using StrategyProcessor.Tests.Common;

namespace StrategyProcessor.Tests;

public class RealTimeDataSourceTests
{
    [Test]
    public void CheckThatRealTimeDataSourceDistributeDataToAll()
    {
        var (consumer, delegateMock) = MockedConsumerWithExternalHandlers<NewCandleEvent>.GetObject();

        var flags = new bool[3];

        var dataSource = new RealTimeDataSource();
        dataSource.RegisterExternalEventSource(consumer);

        dataSource.RegisterEventDestination<NewCandleEvent>(_ => flags[0] = true);
        dataSource.RegisterEventDestination<NewCandleEvent>(_ => flags[1] = true);
        dataSource.RegisterEventDestination<NewCandleEvent>(_ => flags[2] = true);

        delegateMock.Handler(new NewCandleEvent(new Candle(), new Pair("21","213"), TimeFrame.h1, Exchange.Poloniex, 12));

        flags.Should().AllSatisfy(u => u.Should().Be(true));
    }

    [Test]
    public void CheckThatHistoricalDataSourceSendMessages()
    {
        var (consumerB, delegateMockB) = MockedConsumerWithExternalHandlers<bool>.GetObject();

        var flags = new bool[3];
        var dataSource = new RealTimeDataSource();
        dataSource.RegisterExternalEventSource(consumerB);

        dataSource.RegisterEventDestination<bool>(_ => flags[0] = true);
        dataSource.RegisterEventDestination<bool>(_ => flags[1] = true);
        dataSource.RegisterEventDestination<bool>(_ => flags[2] = true);

        delegateMockB.Handler(true);
        flags.Should().AllSatisfy(u => u.Should().Be(true));
    }

    [Test]
    public void CheckThatRealTimeDataSourceSendSpecificEventToCorrectTargets()
    {
        var (consumerB, delegateMockB) = MockedConsumerWithExternalHandlers<bool>.GetObject();
        var (consumerI, delegateMockI) = MockedConsumerWithExternalHandlers<int>.GetObject();
        var (consumerL, delegateMockL) = MockedConsumerWithExternalHandlers<long>.GetObject();

        var flags = new bool[3];
        var dataSource = new RealTimeDataSource();
        dataSource.RegisterExternalEventSource(consumerB);
        dataSource.RegisterExternalEventSource(consumerI);
        dataSource.RegisterExternalEventSource(consumerL);

        dataSource.RegisterEventDestination<bool>(_ => flags[0] = true);
        dataSource.RegisterEventDestination<int>(_ => flags[1] = true);
        dataSource.RegisterEventDestination<long>(_ => flags[2] = true);

        delegateMockB.Handler(true);
        flags[0].Should().Be(true);
        flags[1].Should().Be(false);
        flags[2].Should().Be(false);

        delegateMockI.Handler(1);
        flags[0].Should().Be(true);
        flags[1].Should().Be(true);
        flags[2].Should().Be(false);

        delegateMockL.Handler(1);
        flags[0].Should().Be(true);
        flags[1].Should().Be(true);
        flags[2].Should().Be(true);
    }
}