﻿using Domain.StrategyProcessor;
using MoreLinq.Extensions;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Tests;

[TestFixture]
public class DeferredStrategyArtifactsRepositoryTests : StrategyArtifactsRepositoryTestsBase
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        _repository = new DeferredChartDataRepository(RunId, ChartRepositoryMock.Object, NotesRepositoryMock.Object);
    }

    [TearDown]
    public async Task OneTimeTearDown()
    {
        await _repository.DisposeAsync();
    }

    [Test]
    public async Task DisposeAsync_ShouldSaveChartsToRepository()
    {
        var expectedChart = CreateExpectedCharts().ToArray();

        expectedChart.SelectMany(x => x.Chart.Select(dot => (x.Name, x.Description, x.Pair, x.TimeFrame, dot)))
            .ForEach(x => _repository.AddChartDot(x.Name, x.Description, x.dot, x.Pair, x.TimeFrame));

        await _repository.DisposeAsync();

        ChartRepositoryMock.Verify(r => r.AddChartsAsync(It.Is<Guid>(x=>x == RunId),
            It.Is<IEnumerable<ChartDto>>(x => CompareChartDboLists(x, expectedChart))), Times.Once);
    }

    [Test]
    public async Task DisposeAsync_ShouldSaveNotesToRepository()
    {
        var expectedNote = CreateExpectedNotes().ToArray();

        expectedNote.ForEach(x => _repository.CreateNote(x));

        await _repository.DisposeAsync();

        NotesRepositoryMock.Verify(r => r.AddNotesAsync(It.Is<Guid>(x=>x == RunId),
            It.Is<IEnumerable<Note>>(x => CompareNoteDboLists(x, expectedNote))), Times.Once);
    }


    private DeferredChartDataRepository _repository = null!;
}