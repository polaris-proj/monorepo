using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;
using Domain.StrategyProcessor;
using FluentAssertions;
using Infrastructure.FluentAssertions;

namespace VirtualExchange.Tests
{
    [TestFixture]
    internal class LimitOrderTests : BaseOrderTest
    {
        [Test]
        public async Task BuySomeCoinsWhenNotEnoughMoneys()
        {
            var startBalance = 200m;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var pairName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);
            await MovePrice(new List<decimal> {9m}, pairName);

            Assert.Catch(() => connector.CreateOrder(pairName, OrderType.BuyLimit, 100, 10).GetAwaiter().GetResult());

            connector.GetBalance(secondTicker).Available.Should().Be(200);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
            connector.GetBalance(firstTicker).Available.Should().Be(0);
            connector.GetBalance(firstTicker).Locked.Should().Be(0);

            var priceGoals = new List<decimal> {9m, 10.9999m};
            await MovePrice(priceGoals, pairName);

            connector.GetBalance(secondTicker).Available.Should().Be(200);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
            connector.GetBalance(firstTicker).Available.Should().Be(0);
            connector.GetBalance(firstTicker).Locked.Should().Be(0);
        }

        [Test]
        public async Task TryBuySomeCoinsWhenPriceHasNotReached()
        {
            var startBalance = 2000m;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var pairName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);
            await MovePrice(new List<decimal> {9m}, pairName);
            var entry = new List<Order> {new Order(10, 100)};
            var orderId = await connector.CreateLimitOrder(pairName, OrderType.BuyLimit, entry);

            var priceGoals = new List<decimal> {9m, 9.9999m};
            await MovePrice(priceGoals, pairName);

            var order = await connector.GetOrderInfo(pairName, orderId);
            order.OrderStatus.Should().Be(OrderStatus.Open);
            order.FilledAmount.Should().Be(0);
            order.Amount.Should().Be(100);
            order.Price.Should().Be(10);

            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 100 * 10, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(100 * 10);
            connector.GetBalance(firstTicker).Available.Should().Be(0);
            connector.GetBalance(firstTicker).Locked.Should().Be(0);
        }

        [Test]
        public async Task BuySomeCoins()
        {
            var startBalance = 2000m;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var pairName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);
            await MovePrice(new List<decimal> {9m}, pairName);
            var entry = new List<Order> {new Order(10, 100)};
            var orderId = await connector.CreateLimitOrder(pairName, OrderType.BuyLimit, entry);

            var order = await connector.GetOrderInfo(pairName, orderId);
            order.OrderStatus.Should().Be(OrderStatus.Open);
            order.FilledAmount.Should().Be(0);
            order.Amount.Should().Be(100);
            order.Price.Should().Be(10);

            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 100 * 10, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(100 * 10);
            connector.GetBalance(firstTicker).Available.Should().Be(0);
            connector.GetBalance(firstTicker).Locked.Should().Be(0);


            var priceGoals = new List<decimal> {9m, 10.9999m};
            await MovePrice(priceGoals, pairName);

            order = await connector.GetOrderInfo(pairName, orderId);
            order.OrderStatus.Should().Be(OrderStatus.Close);
            order.FilledAmount.Should().Be(100);
            order.Amount.Should().Be(100);
            order.Price.Should().Be(10);

            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 100 * 10, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
            connector.GetBalance(firstTicker).Available.Should().Be(100);
            connector.GetBalance(firstTicker).Locked.Should().Be(0);
        }

        [Test]
        public async Task TrySellSomeCoinsWhenPriceHasNotReached()
        {
            var startBalance = 200m;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var pairName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", firstTicker, startBalance);
            await MovePrice(new List<decimal> {6m}, pairName);
            var entry = new List<Order> {new Order(8, 100)};
            var orderId = await connector.CreateLimitOrder(pairName, OrderType.SellLimit, entry);

            var order = await connector.GetOrderInfo(pairName, orderId);
            order.OrderStatus.Should().Be(OrderStatus.Open);
            order.FilledAmount.Should().Be(0);
            order.Amount.Should().Be(100);
            order.Price.Should().Be(8);

            connector.GetBalance(secondTicker).Available.Should().BeInRadius(0, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
            connector.GetBalance(firstTicker).Available.Should().Be(100);
            connector.GetBalance(firstTicker).Locked.Should().Be(100);

            var priceGoals = new List<decimal> {7m, 7.9999m};
            await MovePrice(priceGoals, pairName);

            order = await connector.GetOrderInfo(pairName, orderId);
            order.OrderStatus.Should().Be(OrderStatus.Open);
            order.FilledAmount.Should().Be(0);
            order.Amount.Should().Be(100);
            order.Price.Should().Be(8);

            connector.GetBalance(secondTicker).Available.Should().BeInRadius(0, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
            connector.GetBalance(firstTicker).Available.Should().Be(100);
            connector.GetBalance(firstTicker).Locked.Should().Be(100);
        }

        [Test]
        public async Task SellSomeCoins()
        {
            var startBalance = 200m;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var pairName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", firstTicker, startBalance);
            await MovePrice(new List<decimal> {9m}, pairName);
            var entry = new List<Order> {new Order(10, 100)};
            var orderId = await connector.CreateLimitOrder(pairName, OrderType.SellLimit, entry);


            var order = await connector.GetOrderInfo(pairName, orderId);
            order.OrderStatus.Should().Be(OrderStatus.Open);
            order.FilledAmount.Should().Be(0);
            order.Amount.Should().Be(100);
            order.Price.Should().Be(10);

            connector.GetBalance(secondTicker).Available.Should().BeInRadius(0, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
            connector.GetBalance(firstTicker).Available.Should().Be(100);
            connector.GetBalance(firstTicker).Locked.Should().Be(100);


            var priceGoals = new List<decimal> {9m, 10.9999m};
            await MovePrice(priceGoals, pairName);

            order = await connector.GetOrderInfo(pairName, orderId);
            order.OrderStatus.Should().Be(OrderStatus.Close);
            order.FilledAmount.Should().Be(100);
            order.Amount.Should().Be(100);
            order.Price.Should().Be(10);

            connector.GetBalance(secondTicker).Available.Should().BeInRadius(100 * 10, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
            connector.GetBalance(firstTicker).Available.Should().Be(100);
            connector.GetBalance(firstTicker).Locked.Should().Be(0);
        }

        [Test]
        public async Task BuySomeCoinsAndSellAllByTakes()
        {
            var startBalance = 2000m;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);


            var buy = new List<Order> {new Order(10, 100)};
            var take = new List<Order> {new Order(12, 25), new Order(15, 75),};
            var orderId = await connector.CreateLimitOrder(tickerName, OrderType.BuyLimit, buy, take);
            var priceGoals = new List<decimal> {9m, 10m, 11m, 20};
            await MovePrice(priceGoals, tickerName);
            var order = await connector.GetOrderInfo(tickerName, orderId);
            order.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(firstTicker).Available.Should().Be(0);
            connector.GetBalance(firstTicker).Locked.Should().Be(0);
            connector.GetBalance(secondTicker).Available.Should()
                .BeInRadius(startBalance - 10 * 100 + 12 * 25 + 15 * 75, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
        }

        [Test]
        public async Task BuySomeCoinsAndSellPartByTakes()
        {
            var startBalance = 2000m;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var take = new List<Order> {new Order(11, 25)};
            var orderId = await connector.CreateLimitOrder(tickerName, OrderType.BuyLimit, buy, take);
            var priceGoals = new List<decimal> {9m, 10m, 11m};
            await MovePrice(priceGoals, tickerName);

            await connector.GetOrderInfo(tickerName, orderId);
            // order.Status.Should().Be(Status.Open);
            connector.GetBalance(firstTicker).Available.Should().Be(75);
            connector.GetBalance(firstTicker).Locked.Should().Be(0);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 10 * 100 + 11 * 25, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
        }

        [Test]
        public async Task BuySomeCoinsAndSellPartByStop()
        {
            var startBalance = 2000m;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var stop = new List<Order> {new Order(9, 25)};
            var orderId = await connector.CreateLimitOrder(tickerName, OrderType.BuyLimit, buy, null, stop);
            var priceGoals = new List<decimal> {9.5m, 10m, 11m, 8m};
            await MovePrice(priceGoals, tickerName);
            await connector.GetOrderInfo(tickerName, orderId);
            //order.Status.Should().Be(Status.Open);
            connector.GetBalance(firstTicker).Available.Should().Be(75);
            connector.GetBalance(firstTicker).Locked.Should().Be(0);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 10 * 100 + 9 * 25, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
        }

        [Test]
        public async Task BuySomeCoinsAndSellAllByStop()
        {
            var startBalance = 2000m;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new(10, 100)};
            var stop = new List<Order> {new(9, 25), new(8.5m, 75)};
            var orderId = await connector.CreateLimitOrder(tickerName, OrderType.BuyLimit, buy, null, stop);
            var priceGoals = new List<decimal> {9.6m, 10m, 11m, 8m};
            await MovePrice(priceGoals, tickerName);
            var order = await connector.GetOrderInfo(tickerName, orderId);
            order.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(firstTicker).Available.Should().Be(0);
            connector.GetBalance(firstTicker).Locked.Should().Be(0);
            connector.GetBalance(secondTicker).Available.Should()
                .BeInRadius(startBalance - 10 * 100 + 9 * 25 + 8.5m * 75, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
        }

        [Test]
        public async Task BuySomeCoinsAndSellAllByTakeAndStop()
        {
            var startBalance = 2000m;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new(10, 100)};
            var take = new List<Order> {new(15, 20), new(17, 30), new(18, 50)};
            var stop = new List<Order> {new(9, 25)};
            var orderId = await connector.CreateLimitOrder(tickerName, OrderType.BuyLimit, buy, take, stop);
            var priceGoals = new List<decimal> {9.5m, 10m, 11m, 8.6m, 17, 9, 20};
            await MovePrice(priceGoals, tickerName);

            var order = await connector.GetOrderInfo(tickerName, orderId);
            order.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(firstTicker).Available.Should().Be(0);
            connector.GetBalance(firstTicker).Locked.Should().Be(0);
            connector.GetBalance(secondTicker).Available.Should()
                .BeInRadius(startBalance - 10 * 100 + 9 * 25 + 15 * 20 + 17 * 30 + 25 * 18, 1);
            connector.GetBalance(secondTicker).Locked.Should().Be(0);
        }

        [Test]
        public async Task BuySomeCoinsWhenHasMoneyOnlyOnPartOfCoins()
        {
            var startBalance = 2000m;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);


            var buy = new List<Order> {new(10, 100), new(5, 1000)};
            Assert.Catch(() => connector.CreateLimitOrder(tickerName, OrderType.BuyLimit, buy));

            var priceGoals = new List<decimal> {9m, 10m, 11m, 8m};
            await MovePrice(priceGoals, tickerName);
        }
    }
}