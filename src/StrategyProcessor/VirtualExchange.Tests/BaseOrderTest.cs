﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.Patterns.Objects;
using Infrastructure.EventDriven;

namespace VirtualExchange.Tests;

internal abstract class BaseOrderTest
{
    protected Dictionary<Pair, decimal> Prices = new();
    protected HistoricalDataSource DataSource = null!;
    protected VirtualExchange Exchange = null!;


    [SetUp]
    public void SetUp()
    {
        var mock = new Mock<IConnector>();
        mock.Setup(x => x.GetPrices())
            .Returns(Task.FromResult(Prices));

        var mockedExchange = mock.Object;
        DataSource = new HistoricalDataSource();
        Exchange = new VirtualExchange(mockedExchange, DataSource);
        Prices = new Dictionary<Pair, decimal>();
    }

    protected virtual async Task MovePrice(List<decimal> priceGoals, Pair tickerName)
    {
        //todo убедиться что с этим методом все нормально
        var previousPrice = priceGoals.First();
        foreach (var price in priceGoals)
        {
            var min = Math.Min(previousPrice, price);
            var max = Math.Max(previousPrice, price);

            if (Prices.ContainsKey(tickerName))
                Prices[tickerName] = price;
            else
                Prices.Add(tickerName, price);

            var candle = new Candle(0, previousPrice, max, min, price);
            await DataSource.SendEvent(new NewCandleEvent(candle, tickerName, TimeFrames.TimeFrame.h1,
                Domain.Exchange.BinanceSpot, 0));
            previousPrice = price;
        }
    }
}