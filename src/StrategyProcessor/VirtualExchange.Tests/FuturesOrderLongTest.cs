using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;
using Domain.StrategyProcessor;
using FluentAssertions;
using Infrastructure.FluentAssertions;

namespace VirtualExchange.Tests
{
    [TestFixture]
    internal class FuturesOrderLongTest : BaseOrderTest
    {
        [Test]
        public async Task JustBuySomeCoins()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy);
            var priceGoals = new List<decimal> {9m, 10m, 11m};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(10 * 100, 0.5m);
        }

        [Test]
        public async Task BuySomeCoinsWhenNotEnoughMoneys()
        {
            var startBalance = 200m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            Assert.Catch(
                delegate { connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy); });
            var priceGoals = new List<decimal> {9m, 10m, 11m, 8m};
            await MovePrice(priceGoals, tickerName);
        }

        [Test]
        public async Task BuySomeCoinsWhenPriceHasNotReached()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy);
            var priceGoals = new List<decimal> {9m, 9.9999m};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Open);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 10 * 100, 0.5m);
        }

        [Test]
        public async Task BuySomeCoinsAndSellAllByTakes()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var take = new List<Order> {new Order(12, 25), new Order(15, 75)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy, take);
            var priceGoals = new List<decimal> {9m, 10m, 11m, 20};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should()
                .BeInRadius(startBalance - 10 * 100 + 12 * 25 + 15 * 75, 0.5m);
        }

        [Test]
        public async Task BuySomeCoinsAndSellPartByTakes()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var take = new List<Order> {new Order(11, 25)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy, take);
            var priceGoals = new List<decimal> {9m, 10m, 11m};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 10 * 100, 0.5m);
        }

        [Test]
        public async Task BuySomeCoinsByFourEntryAndSellByThree()
        {
            var startBalance = 3000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(9, 50), new Order(10, 25), new Order(11, 100), new Order(11.5m, 100)};
            var take = new List<Order> {new Order(13, 125), new Order(15, 150)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy, take);
            var priceGoals = new List<decimal> {9m, 10m, 11m, 14m, 19m};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 2950 + 3875, 1m);
        }

        [Test]
        public async Task BuySomeCoinsAndSellPartByStop()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var stop = new List<Order> {new Order(9, 25)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy, null, stop);
            var priceGoals = new List<decimal> {9m, 10m, 11m, 8m};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 10 * 100, 0.5m);
        }

        [Test]
        public async Task BuySomeCoinsAndSellAllByStop()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var stop = new List<Order> {new Order(9, 25), new Order(8.5m, 75)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy, null, stop);
            var priceGoals = new List<decimal> {9.6m, 10m, 11m, 8m};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should()
                .BeInRadius(startBalance - 10 * 100 + 9 * 25 + 8.5m * 75, 0.5m);
        }

        [Test]
        public async Task BuySomeCoinsAndSellAllByTakeAndStop()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var take = new List<Order> {new Order(15, 20), new Order(17, 30), new Order(18, 50)};
            var stop = new List<Order> {new Order(9, 25)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy, take, stop);
            var priceGoals = new List<decimal> {9m, 10m, 11m, 8.6m, 17, 9, 20};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should()
                .BeInRadius(startBalance - 10 * 100 + 9 * 25 + 15 * 20 + 17 * 30 + 9 * 50, 0.5m);
        }

        [Test]
        public async Task BuySomeCoinsWhenHasMoneyOnlyOnPartOfCoins()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100), new Order(5, 1000)};
            Assert.Catch(() => connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy));
            var priceGoals = new List<decimal> {9m, 10m, 11m, 8m};
            await MovePrice(priceGoals, tickerName);
        }

        [Test]//todo разобраться почему тест флакает
        public async Task BuyCoinsAndGetLiquidation()
        {
            var startBalance = 2000m;
            var leverage = 10;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy);
            var priceGoals = new List<decimal> {9m, 10m, 11m, 7m};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(1900, 0.5m);
        }

        [Test]
        public async Task BuySomeCoinsByLeverageAndSellAllByStop()
        {
            var startBalance = 2000m;
            var leverage = 10;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var stop = new List<Order> {new Order(9.5m, 100)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy, null, stop);
            var priceGoals = new List<decimal> {9.6m, 10m, 11m, 8m};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should()
                .BeInRadius(startBalance - 10 * 100 + 9.5m * 100, 0.1m);
        }

        [Test]
        public async Task BuySomeCoinsByLeverageAndSellAllByTakes()
        {
            var startBalance = 2000m;
            var leverage = 10;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var take = new List<Order> {new Order(12, 25), new Order(15, 75),};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy, take);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 1000m / leverage, 0.5m);
            var priceGoals = new List<decimal> {9.5m, 10m, 11m, 20};
            await MovePrice(priceGoals, tickerName);
            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance + 2m * 25 + 5m * 75, 0.5m);
        }

        [Test]
        public async Task BuySomeCoinsByLeverageAndMultipleOrderAndSellAllByTakes()
        {
            var startBalance = 2000m;
            var leverage = 10;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 50), new Order(11, 50)};
            var take = new List<Order> {new Order(12m, 25), new Order(15, 75),};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Long, leverage, buy, take);
            var priceGoals = new List<decimal> {9.5m, 10, 11, 20};
            await MovePrice(priceGoals, tickerName);
            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should()
                .BeInRadius(startBalance - 10 * 50 - 11 * 50 + 12 * 25 + 15 * 75, 0.5m);
        }
    }
}