﻿using System.Linq;
using System.Text.Json;
using System.Threading;
using Confluent.Kafka;
using Domain.Patterns.Objects;
using Infrastructure;
using Infrastructure.EventDriven;
using Infrastructure.RTQConnector;
using Microsoft.Extensions.Logging;
using PatternRecognition.Shared;
using Prometheus;
using StrategyProcessor.Infrastructure.DAL.Repositories;

namespace StrategyProcessor.Services.Consumers;

internal class NewPatternHandler : IDisposable
{
    public NewPatternHandler(KafkaParameters kafkaParameters, TradingStrategyManagerStorage tradingStrategyManagerStorage,
        RealTimeDataSource realTimeDataSource, ILogger<NewPatternHandler> logger)
    {
        _tradingStrategyManagerStorage = tradingStrategyManagerStorage;
        _realTimeDataSource = realTimeDataSource;
        _logger = logger;
        _lastEvents = new CircularHashSet<(Exchange, Pair, TimeFrame, long)>(500_000);

        var cc = kafkaParameters.GetClientConfig();
        var config = new ConsumerConfig(cc)
        {
            GroupId = $"{_topicName}_consumer_strategyprocessor",
            AllowAutoCreateTopics = false,
            EnableAutoCommit = false
        };
        _consumer = new ConsumerBuilder<Ignore, string>(config)
            .Build();

        SubscribeOnMessages(_cancellationTokenSource.Token);
    }

    public void Dispose()
    {
        _cancellationTokenSource.Cancel();
        _consumer.Dispose();
    }

    private void SubscribeOnMessages(CancellationToken ct)
    {
        Task.Run(async () =>
        {
            _consumer.Subscribe(_topicName);
            _logger.LogInformation($"Поднимаю консюмер для {_topicName}");
            while (!ct.IsCancellationRequested)
            {
                var consumeResult = _consumer.Consume();
                var msgJson = consumeResult.Message;
                var msgDto = JsonSerializer.Deserialize<NewPatternEventDto>(msgJson.Value);
                var msg = msgDto.Map();
                var key = (msg.Exchange, msg.Pair, msg.TimeFrame, msg.TimeStamp);

                if (!_lastEvents.Contains(key))
                {
                    _lastEvents.Add(key);
                    await HandleEvent(msg);
                    _consumer.Commit();
                }
            }

            return Task.CompletedTask;
        });
    }

    private async Task HandleEvent(NewPatternEvent patternEvent)
    {
        Interlocked.Increment(ref _count);
       // SendMetric(candleEvent);

        await Task.WhenAll(_tradingStrategyManagerStorage.StrategyManagers// может сожрать весь тредпул
            .Select(x => x.HandlePatternEvent(patternEvent)));

        await _realTimeDataSource.SendDataToHandlers(patternEvent);
    }

    private static readonly Histogram CandleLag = Metrics.CreateHistogram("candle_lag", string.Empty,
        ["Exchange", "TimeFrame"],
        new HistogramConfiguration
        {
            Buckets = [0, 200, 500, 1000, 1500, 1750, 2000, 3000, 4000, 8000]
        });


    private static int _count;
    private readonly CircularHashSet<(Exchange, Pair, TimeFrame, long)> _lastEvents;
    private readonly CancellationTokenSource _cancellationTokenSource = new();

    private readonly string _topicName = Topic.PatternRecognition.ToString();
    private readonly IConsumer<Ignore, string> _consumer;
    private readonly TradingStrategyManagerStorage _tradingStrategyManagerStorage;
    private readonly RealTimeDataSource _realTimeDataSource;
    private readonly ILogger<NewPatternHandler> _logger;
}