﻿using System.Linq;
using System.Threading;
using Confluent.Kafka;
using Domain.Patterns.Objects;
using Infrastructure;
using Infrastructure.EventDriven;
using Infrastructure.Extensions;
using Infrastructure.RTQConnector;
using Microsoft.Extensions.Logging;
using Prometheus;
using StrategyProcessor.Infrastructure.DAL.Repositories;

namespace StrategyProcessor.Services.Consumers;

internal class CandleHandler : IDisposable
{
    public CandleHandler(KafkaParameters kafkaParameters, TradingStrategyManagerStorage tradingStrategyManagerStorage,
        RealTimeDataSource realTimeDataSource, ILogger<CandleHandler> logger)
    {
        _tradingStrategyManagerStorage = tradingStrategyManagerStorage;
        _realTimeDataSource = realTimeDataSource;
        _logger = logger;
        _lastEvents = new CircularHashSet<(Exchange, Pair, TimeFrame, long)>(500_000);

        var cc = kafkaParameters.GetClientConfig();
        var config = new ConsumerConfig(cc)
        {
            GroupId = $"{_topicName}_consumer_{Guid.NewGuid()}",
            AllowAutoCreateTopics = false,
            EnableAutoCommit = false,
            
        };
        _consumer = new ConsumerBuilder<Ignore, NewCandleEvent>(config)
            .SetValueDeserializer(new MyProtoDeserializer<NewCandleEvent>())
            .Build();

        SubscribeOnMessages(_cancellationTokenSource.Token);
    }

    public void Dispose()
    {
        _cancellationTokenSource.Cancel();
        _consumer.Dispose();
    }

    private void SubscribeOnMessages(CancellationToken ct)
    {
        Task.Factory.StartNew(async () =>
        {
            _consumer.Subscribe(_topicName);
            _logger.LogInformation($"Поднимаю консюмер для {_topicName}");
            while (!ct.IsCancellationRequested)
            {
                var consumeResult = _consumer.Consume();
                var msg = consumeResult.Message;
                var key = (msg.Value.Exchange, msg.Value.Pair, msg.Value.TimeFrame, msg.Value.TimeStamp);

                if (!_lastEvents.Contains(key))
                {
                    _lastEvents.Add(key);
                    await HandleEvent(consumeResult.Message.Value);
                    _consumer.Commit();
                }
            }

            return Task.CompletedTask;
        }, ct, TaskCreationOptions.LongRunning, TaskScheduler.Default);
    }

    private async ValueTask HandleEvent(NewCandleEvent candleEvent)
    {
        Interlocked.Increment(ref _count);
        SendMetric(candleEvent);

        await Task.WhenAll(_tradingStrategyManagerStorage.StrategyManagers
            .Select(x => x.HandleCandleEvent(candleEvent)));

        await _realTimeDataSource.SendDataToHandlers(candleEvent);
    }

    private static void SendMetric(NewCandleEvent msg)
    {
        var candleTime = GetCandleEndTime(msg);
        var lag = DateTime.UtcNow.ToMilliseconds() - candleTime;

        CandleLag.WithLabels(msg.Exchange.ToString(), msg.TimeFrame.ToString()).Observe(lag);
    }

    private static long GetCandleEndTime(NewCandleEvent @event)
    {
        return (long) Math.Round((double) @event.Candle.TimeStamp / (@event.TimeFrame.GetSeconds() * 1000))
               * @event.TimeFrame.GetSeconds() * 1000;
    }

    private static readonly Histogram CandleLag = Metrics.CreateHistogram("candle_lag", string.Empty,
        new[] {"Exchange", "TimeFrame"},
        new HistogramConfiguration
        {
            Buckets = new double[] {0, 200, 500, 1000, 1500, 1750, 2000, 3000, 4000, 8000}
        });


    private static int _count;
    private readonly CircularHashSet<(Exchange, Pair, TimeFrame, long)> _lastEvents;
    private readonly CancellationTokenSource _cancellationTokenSource = new();

    private readonly string _topicName = Topic.Candle.ToString();
    private readonly IConsumer<Ignore, NewCandleEvent> _consumer;
    private readonly TradingStrategyManagerStorage _tradingStrategyManagerStorage;
    private readonly RealTimeDataSource _realTimeDataSource;
    private readonly ILogger<CandleHandler> _logger;
}