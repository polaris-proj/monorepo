﻿namespace StrategyProcessor.Services;

internal interface IStartStopStrategyServiceTypedFactory : IDisposable
{
    IStrategyService GetInstance(Type strategyType);
}