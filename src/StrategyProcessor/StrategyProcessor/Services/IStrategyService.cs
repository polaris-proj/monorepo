﻿using StrategyProcessor.Shared;
using StrategyProcessor.Strategy;

namespace StrategyProcessor.Services;

internal interface IStrategyService
{
    Task<Guid> StartNewStrategyAsync(StartStrategyParameters startStrategyParameters);

    Task StopStrategyAsync(StrategyStopDto @event);

    public Task RunStrategyManager(
        Guid userId,
        StartStrategyParameters strategyParameters,
        Guid runId,
        IStrategyManager strategyManager
    );

    public IStrategyManager CreateStrategyManager(StartStrategyParameters startStrategyParameters, Guid runId);
}