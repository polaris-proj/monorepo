﻿using Grpc.Net.Client;
using OrderConfirmation.Client;

namespace StrategyProcessor.Services;

public interface IOrderSenderClientFactory
{
    public OrderSenderClient Create(Guid userId, Exchange exchange, Guid runId, GrpcChannel? channel);
}