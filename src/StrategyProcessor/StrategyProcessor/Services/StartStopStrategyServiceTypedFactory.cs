﻿using Infrastructure.DIExtension;
using Microsoft.Extensions.DependencyInjection;
using StrategyProcessor.Infrastructure;

namespace StrategyProcessor.Services;

internal class StartStopStrategyServiceTypedFactory(IServiceProvider serviceProvider, IDarkMagic darkMagic)
    : IStartStopStrategyServiceTypedFactory
{
    public IStrategyService GetInstance(Type strategyType)
    {
        var connectorType = darkMagic.GetConnectorType(strategyType);
        _scope = serviceProvider.CreateScope();
        return _scope.ServiceProvider.GetGenericService(StrategyServiceType, strategyType, connectorType) as IStrategyService
               ?? throw new InvalidOperationException();
    }

    public void Dispose()
    {
        _scope?.Dispose();
    }

    private IServiceScope? _scope;
    private static readonly Type StrategyServiceType = typeof(StrategyService<,>);
}