﻿using System.Linq;
using System.Text.Json;
using Domain.StrategyProcessor;
using Infrastructure;
using Infrastructure.Json;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories;

namespace StrategyProcessor.Services;

internal class AddDeclaredStrategiesToDatabase(IStrategyRepository strategyRepository, IDarkMagic darkMagic)
    : StartUpService
{
    public override async Task Start()
    {
        var allStrategies = darkMagic.GetAllStrategiesTypes();

        foreach (var strategy in allStrategies)
        {
            var props = darkMagic.GetStrategyProperties(strategy);
            var name = strategy.FullName!;
            var strategyType = darkMagic.GetTypeOfStrategy(strategy);

            var strategyDbo = await strategyRepository.ReadByNameAsync(name);
            var isNull = strategyDbo is null;
            strategyDbo ??= new StrategyDbo
                {InternalName = name, StrategyConnectorType = strategyType, Properties = new List<ParameterDbo>()};

            strategyDbo.Properties = props.Select(x => x.Value.ToDbo(x.Key)).ToList();

            if (isNull)
                await strategyRepository.CreateAsync(strategyDbo);
            else
                strategyRepository.Update(strategyDbo);

            await strategyRepository.SaveAsync();
        }
    }
}

public static class ParameterMapper
{
    public static ParameterDbo ToDbo(this Property property, string propertyName)
    {
        return new ParameterDbo
        {
            TypeName = property.Type.FullName ?? throw new ArgumentNullException(),
            Name = propertyName,
            ParameterValue =
                JsonSerializer.SerializeToDocument(property.Value, property.Type, JsonOptions.JsonSerializerOptions)
        };
    }
}