﻿using System.Diagnostics;
using System.Linq;
using Domain.Connectors;
using Domain.Patterns.Objects;
using Domain.StrategyProcessor;
using EventStore.Client;
using Infrastructure.EventDriven;
using Infrastructure.Extensions;
using IODataModule.Client.Clients;
using MoreLinq;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Infrastructure.Connectors.Configurators;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Infrastructure.Mappers;
using StrategyProcessor.Shared;
using StrategyProcessor.Strategy;


namespace StrategyProcessor.Services;

internal class StrategyService<TStrategy, TConnector>(
    RealTimeDataSource realDataSource,
    IRunStrategyRepository runStrategyRepository,
    IOrderRepository orderRepository,
    IChartRepository chartRepository,
    INotesRepository notesRepository,
    TradingStrategyManagerStorage tradingStrategyManagerStorage,
    TestingStrategyManagerStorage testingStrategyManagerStorage,
    IStrategyManagerRepository strategyManagerRepository,
    IStrategyManagerFactory strategyManagerFactory,
    IRemoteConnectorFactory remoteConnectorFactory,
    IOrderSenderClientFactory orderSenderClientFactory,
    ICandleService candleService,
    IEventStoreClient eventStoreClient
)
    : IStrategyService
    where TStrategy : BaseStrategy<TConnector>, new()
    where TConnector : class, IConnector
{
    public async Task<Guid> StartNewStrategyAsync(StartStrategyParameters startStrategyParameters)
    {
        var runId = Guid.NewGuid();

        var strategyManager = CreateStrategyManager(startStrategyParameters, runId);
        try
        {
            await RunStrategyManager(startStrategyParameters.UserId, startStrategyParameters, runId, strategyManager);
        }
        catch (Exception)
        {
            await runStrategyRepository.UpdateStatusAsync(runId, StatusOfRun.Failed);
            throw;
        }

        return runId;
    }

    public async Task RunStrategyManager(Guid userId, StartStrategyParameters strategyParameters, Guid runId,
        IStrategyManager strategyManager)
    {
        var (strategyId, _, runMode, _, allowedSources, _, _, timeRange) = strategyParameters;

        await CreateStrategyTestResult(runId, strategyId);
        if (runMode.IsRealTime())
        {
            await SaveToDb(userId, runId, strategyParameters);

            var storage = runMode == RunMode.Real ? tradingStrategyManagerStorage : testingStrategyManagerStorage;
            await runStrategyRepository.UpdateStatusAsync(runId, StatusOfRun.Running);
            storage.AddStrategyManager(strategyManager);
        }
        else if (runMode == RunMode.HistoryTest)
        {
            await runStrategyRepository.UpdateStatusAsync(runId, StatusOfRun.Running);
            await StartSendEventsForTest(strategyManager, userId, timeRange, allowedSources);
            var sw = Stopwatch.StartNew();
            await strategyManager.DisposeAsync();
            await runStrategyRepository.UpdateStatusAsync(runId, StatusOfRun.Completed);
            Console.WriteLine($"Dispose {sw.ElapsedMilliseconds}");
        }
        else
        {
            throw new InvalidOperationException("Unknown run mode");
        }
    }

    public IStrategyManager CreateStrategyManager(StartStrategyParameters startStrategyParameters, Guid runId)
    {
        var (_, ownerId, runMode, enableOrderConfirmation, allowedSources, parameters, initialBalance, _
            ) = startStrategyParameters;

        var parametersModel = parameters.Select(x => x.ToModel()).ToList();
        var storeInDatabase = runMode.IsRealTime();
        IChartDataStorage chartDataStorage = runMode == RunMode.HistoryTest
            ? new DeferredChartDataRepository(runId, chartRepository, notesRepository)
            : new ChartDataStorage(runId, chartRepository, notesRepository);

        var connectors = CreateConnectors(allowedSources, realDataSource, runId, ownerId, runMode, initialBalance,
            enableOrderConfirmation);

        var strategyManager = strategyManagerFactory.Create<TStrategy, TConnector>(runId, parametersModel, connectors,
            allowedSources, chartDataStorage, storeInDatabase);

        return strategyManager;
    }

    //todo вохвращать результат остановки в виде объекта

    public async Task StopStrategyAsync(StrategyStopDto @event)
    {
        var runId = @event.StrategyTestId;
        try
        {
            await tradingStrategyManagerStorage.RemoveStrategyManager(runId);
        }
        catch (KeyNotFoundException)
        {
            await testingStrategyManagerStorage.RemoveStrategyManager(runId);
        }
        await runStrategyRepository.UpdateStatusAsync(runId, StatusOfRun.Stopped);
    }

    private Dictionary<Exchange, TConnector> CreateConnectors(
        IEnumerable<ExchangePairTimeFrame> allowedSources,
        IDataSource inputDataSource,
        Guid runId,
        Guid ownerId,
        RunMode runMode,
        IReadOnlyList<Balance> initialBalance,
        bool enableOrderConfirmation)
    {
        return allowedSources.DistinctBy(x => x.Exchange)
            .ToDictionary(k => k.Exchange,
                v =>
                {
                    var connector = remoteConnectorFactory.CreateConnector<TConnector>(ownerId, v.Exchange);

                    if (runMode.IsTest())
                    {
                        connector = connector.WithEmulatingTrading(inputDataSource, initialBalance);
                    }

                    if (enableOrderConfirmation)
                    {
                        var orderConfirmationClient = orderSenderClientFactory.Create(ownerId, v.Exchange, runId, null);
                        connector = connector.WithOrderConfirmation(orderConfirmationClient);
                    }

                    return connector.WithTradeLogging(orderRepository, runId, runMode != RunMode.HistoryTest);
                });
    }

    private Task StartSendEventsForTest(IStrategyManager strategyManager, Guid userId, DateTimeRange period,
        IReadOnlyList<ExchangePairTimeFrame> allowedSources)
    {
        var sw = Stopwatch.StartNew();
        //todo пока нет полной поддержки в режиме тестирования на истории, есть только для свечей
        //var minStep = allowedSources.Min(x => (int) x.TimeFrame);

        var candleSources = allowedSources.SelectMany(s =>
                candleService
                    .GetRange(userId, new ExchangePairTimeFrame(s.Exchange, s.Pair, s.TimeFrame), period.Begin.ToMilliseconds(),
                        period.End.ToMilliseconds()).GetAwaiter().GetResult()
                    .Select(y => new NewCandleEvent(y, s.Pair, s.TimeFrame, s.Exchange, y.TimeStamp)))
            .OrderBy(x => x.TimeStamp)
            .ThenByDescending(x => x.TimeFrame)
            .ToArray();
        var patternsSources = allowedSources.SelectMany(s =>
                eventStoreClient.GetRange(userId, new ExchangePairTimeFrame(s.Exchange, s.Pair, s.TimeFrame), period.Begin.ToMilliseconds(), period.End.ToMilliseconds()).GetAwaiter().GetResult())
            .OrderBy(x => x.TimeStamp)
            .ThenByDescending(x => x.TimeFrame)
            .ToArray();
        patternsSources.ForEach(x => strategyManager.HandlePatternEvent(x));
        candleSources.ForEach(x => strategyManager.HandleCandleEvent(x));
        // var intSource = allowedSources.Select(_ => AsyncEnumerable.Empty<ITradeEvent>().GetAsyncEnumerator())
        //     .ToReadOnlyList();
        // foreach (var candlesAsync in candleSources)
        // {//todo ошибка в отправке эвентов если несколько пар
        //     var candles = await candlesAsync.ToListAsync();
        //     candles.ForEach(x => strategyManager.HandleCandleEvent(x));
        // }

        // for (var currentTimeStamp = period.StartTime.ToMilliseconds();
        //      currentTimeStamp <= period.EndTime.ToMilliseconds();
        //      currentTimeStamp += minStep)
        // {
        //    // for (var i = 0; i < allowedSources.Count; i++)
        //     //{
        //
        //     //    currentTimeStamp = Math.Max(await TrySendEvent(dataSource, t, currentTimeStamp), currentTimeStamp);
        //         //await TrySendEvent(dataSource, intSource[i], currentTimeStamp);
        //     //}
        //     //Console.WriteLine(currentTimeStamp.ToDateTime());
        // }
        Console.WriteLine($"свечи получал и отправлял {sw.ElapsedMilliseconds}");
        return Task.CompletedTask;
    }


    /*private static async Task<long> TrySendEvent<T>(HistoricalDataSource dataSource, IAsyncEnumerator<T> source,
        long currentTimeStamp) where T : ITradeEvent
    {
        var sw = Stopwatch.StartNew();
        var containsNextValue = await source.MoveNextAsync();
        var current = source.Current;
        if (containsNextValue && current.TimeStamp <= currentTimeStamp)
        {
            dataSource.SendEvent(current);
            return current.TimeStamp;
        }

        return 0;
    }*/

    private async Task SaveToDb(Guid userId, Guid runId, StartStrategyParameters strategyParameters)
    {
        var strategyManagerDbo = new StrategyManagerDbo
        {
            RunId = runId,
            StrategyType = typeof(TStrategy),
            StartStrategyParameters = strategyParameters,
            StrategyManagerState = new StrategyManagerState<TStrategy>().Serialize(),
            ExchangePairTimeFrameDbos = strategyParameters.AllowedSources.Select(x => x.ToDbo()).ToArray(),
            UserId = userId,
        };
        
        if (await strategyManagerRepository.ReadAsync(runId) is null)
        {
            await strategyManagerRepository.CreateAsync(strategyManagerDbo);
        }
    }

    private async Task CreateStrategyTestResult(Guid runId, Guid strategyId)
    {
        var testOfStrategyDbo = new RunOfStrategyDbo
        {
            Id = runId,
            StrategyId = strategyId,
            StatusOfRun = StatusOfRun.Created,
        };
        
        if (await runStrategyRepository.ReadAsync(runId) == null)
        {
            await runStrategyRepository.CreateAsync(testOfStrategyDbo);
        }
    }
}