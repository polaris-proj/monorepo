﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.DAL.Models;

namespace StrategyProcessor.Services;

public record StartStrategyParameters(
    Guid StrategyId,
    Guid UserId,
    RunMode RunMode,
    bool EnableOrderConfirmation,
    IReadOnlyList<ExchangePairTimeFrame> AllowedSources,
    IReadOnlyList<StrategyParameterDbo> Parameters,
    IReadOnlyList<Balance> InitialBalance,
    DateTimeRange TimeRange
);