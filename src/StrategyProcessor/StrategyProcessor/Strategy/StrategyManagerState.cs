﻿using System.Text.Json;
using Domain.StrategyProcessor;
using Infrastructure.Json;

namespace StrategyProcessor.Strategy;

public class StrategyManagerState<TStrategy> : ISerializable
{
    public StrategyManagerState()
    {
        ActiveStrategiesByExchange = new Dictionary<Exchange, List<TStrategy>>();
        ActiveStrategiesByExchangePairTimeFrame = new Dictionary<ExchangePairTimeFrame, List<TStrategy>>();
        LastHandledEventTimeStamp = new Dictionary<ExchangePairTimeFrame, long>();
    }

    public JsonDocument Serialize()
    {
        return JsonSerializer.SerializeToDocument(this, JsonOptions.JsonSerializerOptions);
    }

    // сджоинить 2 словаря ниже
    public Dictionary<Exchange, List<TStrategy>> ActiveStrategiesByExchange { get; set; }
    public Dictionary<ExchangePairTimeFrame, List<TStrategy>> ActiveStrategiesByExchangePairTimeFrame { get; set; }
    
    public Dictionary<ExchangePairTimeFrame, long> LastHandledEventTimeStamp { get; set; }
}