﻿using System.Runtime.Serialization;
using System.Text.Json;
using Domain.Connectors;
using Domain.Patterns;
using Domain.StrategyProcessor;
using Infrastructure.Json;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;

namespace StrategyProcessor.Strategy;

internal class StrategyManagerWithStoreInDb<TStrategy, TConnector> : StrategyManager<TStrategy, TConnector>
    where TStrategy : BaseStrategy<TConnector>, new()
    where TConnector : IConnector
{
    public StrategyManagerWithStoreInDb(StrategyManagerWithStoreInDbParameters<TConnector> parameters)
        : base(parameters)
    {
        _strategyManagerRepository = parameters.StrategyManagerRepository;
    }

    protected override async Task SendEvent<T>(Func<BaseStrategy<TConnector>, T, Task> send, T arg)
    {
        if (!AllowedSources.Contains(new ExchangePairTimeFrame(arg.Exchange, arg.Pair, arg.TimeFrame)))
            return; //todo если перестроить систему распространения эвентов, то не придется заниматься вот этим на уровне менеджера стратегии

        await SendEventToStrategiesInDb(send, arg);
    }

    //todo пересмотреть всю логику с обработкой эвентов. как-то все сложно. мб часть логики по конвертации унести в репозиторий
    // ещё из-за SendEventToStrategiesInMemory из базового класса получилось жесть какая-то
    private async Task SendEventToStrategiesInDb<T>(Func<BaseStrategy<TConnector>, T, Task> send, T arg)
        where T : ITradeEvent
    {
        var currentManager = await _strategyManagerRepository.ReadAsync(RunId);
        if (currentManager is null)
        {
            throw new Exception("StrategyManager not found in db");
        }

        //утащить эту хрень в репозиторий
        var strategyManagerState = currentManager.StrategyManagerState.Deserialize<StrategyManagerState<TStrategy>>(_options) ??
                                   throw new SerializationException("Can't deserialize StrategyManager State");
        
        var exchangePairTimeframe = arg.ToExchangePairTimeframe();
        
        if (!strategyManagerState.LastHandledEventTimeStamp.TryGetValue(exchangePairTimeframe, out var value))
        {
            strategyManagerState.LastHandledEventTimeStamp.Add(exchangePairTimeframe, arg.TimeStamp);
        }
        else if (arg.TimeStamp <= value)//эвент уже был обработан
        {
            return;
        }

        strategyManagerState.LastHandledEventTimeStamp[exchangePairTimeframe] = arg.TimeStamp;
        
        SetState(strategyManagerState);

        foreach (var (exchange, strategies) in State.ActiveStrategiesByExchange)
        {
            foreach (var strategyState in strategies)
            {
                UpdateDeps(strategyState, exchange);
            }
        }

        await SendEventToStrategiesInMemory(send, arg);

        currentManager.StrategyManagerState = State.Serialize();
        await _strategyManagerRepository.UpdateAsync(currentManager);
    }

    private readonly IStrategyManagerRepository _strategyManagerRepository;
    private readonly JsonSerializerOptions _options = JsonOptions.JsonSerializerOptions;
}