﻿using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure;

namespace StrategyProcessor.Strategy;

internal class StrategyManagerParameters<TConnector>
{
    internal StrategyManagerParameters(
        Guid runId,
        IReadOnlyList<StrategyParameter> parameters,
        IReadOnlyList<ExchangePairTimeFrame> allowedSources,
        Dictionary<Exchange, TConnector> exchangesConnectors,
        IChartDataStorage chartDataStorage,
        IDarkMagic darkMagic
        )
    {
        RunId = runId;
        Parameters = parameters;
        AllowedSources = allowedSources;
        ExchangesConnectors = exchangesConnectors;
        ChartDataStorage = chartDataStorage;
        DarkMagic = darkMagic;
    }

    public Guid RunId { get; }
    public IReadOnlyList<StrategyParameter> Parameters { get; }
    public IReadOnlyList<ExchangePairTimeFrame> AllowedSources { get; }
    public Dictionary<Exchange, TConnector> ExchangesConnectors { get; }
    public IChartDataStorage ChartDataStorage { get; }
    public IDarkMagic DarkMagic { get; }
}