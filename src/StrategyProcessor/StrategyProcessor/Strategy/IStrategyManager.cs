﻿using Domain.Patterns.Objects;

namespace StrategyProcessor.Strategy;

internal interface IStrategyManager : IAsyncDisposable
{
    public Guid RunId { get; }
    public Task HandleCandleEvent(NewCandleEvent candleEvent);
    public Task HandlePatternEvent(NewPatternEvent patternEvent);
}