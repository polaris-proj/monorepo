using System.IO;
using System.Linq;
using System.Text.Json;
using Infrastructure;
using Infrastructure.Json;
using Infrastructure.Metric;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Prometheus;
using StrategyProcessor.Shared;

namespace StrategyProcessor;

internal class Program
{
    public static async Task Main(string[] args)
    {
        var isIndependentModeEnable = /*true ||*/ args.Length > 0 && args.Contains("independentMode");

        var app = SetUp(args, isIndependentModeEnable);
        
        Metrics.SuppressDefaultMetrics();
        Metrics.DefaultRegistry.SetStaticLabels(new Dictionary<string, string>
        {
            {"service_name", "StrategyProcessor"}
        });
        
        await RunApplication(app, isIndependentModeEnable);
    }

    public static IHost SetUp(string[] args, bool isIndependentModeEnable)
    {
        var builder = WebApplication.CreateBuilder(args);
        builder.ConfigureOpenTelemetry("StrategyProcessor");
        
        builder.Services.ConfigureServices(builder.Environment);
        builder.Services.ConfigureForTest(isIndependentModeEnable);

        var app = builder.Build();
        app.ConfigureApp();

        return app;
    }

    static async Task RunApplication(IHost app, bool isIndependentMode)
    {
        using var scope = app.Services.CreateScope();
        var startUpServices = scope.ServiceProvider.GetServices<StartUpService>();

        if (isIndependentMode)
        {
            foreach (var s in startUpServices.Where(x => x.Priority>0))
            {
                await s.Start();
            }

            await RunInIndependentMode(app);
            var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();
            logger.LogInformation($"{Environment.NewLine} Test Completed");
        }
        else
        {
            foreach (var s in startUpServices.OrderByDescending(x => x.Priority))
            {
                await s.Start();
            }

            await app.RunAsync();
        }
    }

    static async Task RunInIndependentMode(IHost app)
    {
        using var scope = app.Services.CreateScope();
        var startStopStrategyServiceGrpc = scope.ServiceProvider.GetRequiredService<IStrategyServiceGrpc>();

        var json = await File.ReadAllTextAsync("startTestStrategyParameters.json");
        var parameters = JsonSerializer.Deserialize<StrategyStartDto>(json, JsonOptions.JsonSerializerOptions);
        if (parameters is null)
        {
            throw new Exception("Can't parse json");
        }

        await startStopStrategyServiceGrpc.Start(parameters);
    }
}