﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;
using StrategyProcessor.Strategy;

namespace StrategyProcessor.Infrastructure;

internal class StrategyManagerFactory : IStrategyManagerFactory
{
    public StrategyManagerFactory(IStrategyManagerRepository strategyManagerRepository, IDarkMagic darkMagic)
    {
        _strategyManagerRepository = strategyManagerRepository;
        _darkMagic = darkMagic;
    }

    public StrategyManager<TStrategy, TConnector> Create<TStrategy, TConnector>(
        Guid runId,
        IReadOnlyList<StrategyParameter> parameters,
        Dictionary<Exchange, TConnector> connectors,
        IReadOnlyList<ExchangePairTimeFrame> allowedSources,
        IChartDataStorage chartDataStorage,
        bool storeInDatabase
    )
        where TStrategy : BaseStrategy<TConnector>, new()
        where TConnector : IConnector
    {
        if (allowedSources.Count < 1)
        {
            throw new ArgumentOutOfRangeException($"AllowedSources amount should be more zero {nameof(allowedSources)}");
        }
        if (connectors.Count < 1)
        {
            throw new ArgumentOutOfRangeException($"Connectors amount should be more zero {nameof(connectors)}");
        }

        var startParams = new StrategyManagerWithStoreInDbParameters<TConnector>(runId, parameters, allowedSources,
            connectors, chartDataStorage, _strategyManagerRepository, _darkMagic);
        
        return storeInDatabase ? 
            new StrategyManagerWithStoreInDb<TStrategy, TConnector>(startParams) :
            new StrategyManager<TStrategy, TConnector>(startParams);
    }

    private readonly IStrategyManagerRepository _strategyManagerRepository;
    private readonly IDarkMagic _darkMagic;
}