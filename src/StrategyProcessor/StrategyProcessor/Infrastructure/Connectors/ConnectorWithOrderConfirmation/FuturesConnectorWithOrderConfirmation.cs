﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using OrderConfirmation.Client;

namespace StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderConfirmation;

internal class FuturesConnectorWithOrderConfirmation : BaseConnector<IFuturesConnector>, IFuturesConnector
{
    private readonly IOrderSenderClient _orderSenderClient;

    public FuturesConnectorWithOrderConfirmation(IFuturesConnector futuresConnector,
        IOrderSenderClient orderSenderClient) : base(futuresConnector)
    {
        _orderSenderClient = orderSenderClient;
    }

    public Task<OrderId> CreateFuturesOrder(Pair pair, FuturesOrder orderType, int leverage, PlacementOrder entry,
        PlacementOrder? take = null, PlacementOrder? stop = null)
    {
        return _orderSenderClient.CreateFuturesOrder(pair, orderType, leverage, entry, take, stop);
    }
}