﻿using Domain.Connectors;

namespace StrategyProcessor.Infrastructure.Connectors.Configurators;

public interface IRemoteConnectorFactory
{
    TConnector CreateConnector<TConnector>(Guid userId, Exchange exchange);
    ISpotConnector CreateSpotConnector(Guid userId, Exchange exchange);
    IFuturesConnector CreateFuturesConnector(Guid userId, Exchange exchange);
}