﻿using Domain.Connectors;
using IODataModule.Client;
using IODataModule.Client.Clients;

namespace StrategyProcessor.Infrastructure.Connectors.Configurators;

public class RemoteConnectorFactory(IoDataModuleClientChannel channel) : IRemoteConnectorFactory
{
    public ISpotConnector CreateSpotConnector(Guid userId, Exchange exchange)
        => new RemoteSpotConnector(userId, exchange, channel);


    public IFuturesConnector CreateFuturesConnector(Guid userId, Exchange exchange)
        => new RemoteFuturesConnector(userId, exchange, channel);

    public TConnector CreateConnector<TConnector>(Guid userId, Exchange exchange)
    {
        return typeof(TConnector) switch
        {
            var t when t == typeof(ISpotConnector) => (TConnector) CreateSpotConnector(userId, exchange),
            var t when t == typeof(IFuturesConnector) => (TConnector) CreateFuturesConnector(userId, exchange),
            _ => throw new NotSupportedException()
        };
    }
}