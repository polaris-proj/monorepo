﻿using System.Linq;
using Domain.Connectors;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;

public class DeferredBaseConnectorWithOrderLogger<TConnector> : BaseConnector<TConnector>
    where TConnector : IConnector
{
    public DeferredBaseConnectorWithOrderLogger(TConnector connector, IOrderRepository orderRepository, Guid runId) :
        base(connector)
    {
        RunId = runId;
        _orderRepository = orderRepository;
    }

    public Task LogOrder(OrderDto order)
    {
        _orderDbos.Add(order);
        return Task.CompletedTask;
    }

    public override async Task CancelOrder(Pair pair, OrderId orderId) //Todo точно все норм? закрытие это же ещё одна точка
    {
        var order = _orderDbos.FirstOrDefault(x => x.OrderId == orderId.Id);
        if (order is null)
        {
            throw new ArgumentException("Can't close not saved order");
        }

        order.Status = OrderStatus.Close;

        await Connector.CancelOrder(pair, orderId); //todo тут может быть несогласованность между бд и ордером
    }

    public override async ValueTask DisposeAsync()
    {
        var connector = Connector.DisposeAsync();
        if (_orderDbos.Count > 0)
        {
            await _orderRepository.AddOrdersAsync(RunId, _orderDbos);
            _orderDbos.Clear();
        }

        await connector;
    }

    private readonly IOrderRepository _orderRepository;
    private List<OrderDto> _orderDbos = new();
    protected readonly Guid RunId;
}