﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using Infrastructure.Extensions;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;

public class DeferredSpotConnectorWithOrderLogger : DeferredBaseConnectorWithOrderLogger<ISpotConnector>,
    ISpotConnector
{
    public DeferredSpotConnectorWithOrderLogger(ISpotConnector connector, IOrderRepository orderRepository, Guid runId)
        : base(connector, orderRepository, runId)
    {
    }
    
    public async Task<OrderId> CreateMarketOrder(Pair pair, MarketOrder orderType, decimal amount)
    {
        var order = await Connector.CreateMarketOrder(pair, orderType, amount);
        var dbo = new OrderDto
        {
            Pair = pair,
            OrderId = order.Id,
            Amount = amount,
            Type = orderType.ToOrderType(),
            CreatedAt = DateTime.UtcNow,
        };

        await LogOrder(dbo);
        return order;
    }

    public async Task<OrderId> CreateLimitOrder(Pair pair, LimitOrder orderType, PlacementOrder entry)
    {
        var order = await Connector.CreateLimitOrder(pair, orderType, entry);
        var dbo = new OrderDto
        {
            Pair = pair,
            OrderId = order.Id,
            Amount = entry.Amount,
            Price = entry.Price,
            Type = orderType.ToOrderType(),
            CreatedAt = DateTime.UtcNow,
        };

        await LogOrder(dbo);
        return order;
    }

    public async Task<OrderId> CreateLimitOrderWithLog(Pair pair, LimitOrder orderType, PlacementOrder entry,
        long timeStamp)
    {
        var order = await Connector.CreateLimitOrder(pair, orderType, entry);
        var dbo = new OrderDto
        {
            Pair = pair,
            OrderId = order.Id,
            Amount = entry.Amount,
            Price = entry.Price,
            Type = orderType.ToOrderType(),
            CreatedAt = timeStamp.ToDateTime(),
        };

        await LogOrder(dbo);
        return order;
    }
}