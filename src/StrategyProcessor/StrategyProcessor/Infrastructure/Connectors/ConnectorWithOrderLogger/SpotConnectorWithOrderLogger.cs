﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;

internal class SpotConnectorWithOrderLogger : BaseConnectorWithOrderLogger<ISpotConnector>, ISpotConnector
{
    public SpotConnectorWithOrderLogger(ISpotConnector connector, IOrderRepository orderRepository, Guid runId) : base(
        connector, orderRepository, runId)
    {
    }

    public async Task<OrderId> CreateMarketOrder(Pair pair, MarketOrder orderType, decimal amount)
    {
        var order = await Connector.CreateMarketOrder(pair, orderType, amount);
        var dbo = new OrderDto
        {
            Pair = pair,
            OrderId = order.Id,
            Amount = amount,
            Type = orderType.ToOrderType(),
            CreatedAt = DateTime.UtcNow,
        };
        
        await LogOrder(dbo);
        return order;
    }

    public async Task<OrderId> CreateLimitOrder(Pair pair, LimitOrder orderType, PlacementOrder entry)
    {
        var orderId = await Connector.CreateLimitOrder(pair, orderType, entry);
        var dbo = new OrderDto
        {
            Pair = pair,
            OrderId = orderId.Id,
            Amount = entry.Amount,
            Price = entry.Price,
            Type = orderType.ToOrderType(),
            CreatedAt = DateTime.UtcNow,
        };

        await LogOrder(dbo);
        return orderId;
    }
}