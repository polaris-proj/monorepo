﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;

internal class FuturesConnectorWithOrderLogger : BaseConnectorWithOrderLogger<IFuturesConnector>, IFuturesConnector
{
    public FuturesConnectorWithOrderLogger(IFuturesConnector connector, IOrderRepository orderRepository, Guid runId) :
        base(connector, orderRepository, runId)
    {
    }


    public async Task<OrderId> CreateFuturesOrder(Pair pair, FuturesOrder orderType, int leverage, PlacementOrder entry,
        PlacementOrder? take = null, PlacementOrder? stop = null)
    {
        var order = await Connector.CreateFuturesOrder(pair, orderType, leverage, entry, take, stop);
        var dbo = new OrderDto
        {
            Pair = pair,
            OrderId = order.Id,
            Amount = entry.Amount,
            Price = entry.Price,
            Type = orderType.ToOrderType(),
            Leverage = leverage,
            CreatedAt = DateTime.UtcNow
        };
        if (take is not null)
            dbo.TakePrice = take.Price;

        if (stop is not null)
            dbo.StopPrice = stop.Price;

        await LogOrder(dbo);
        return order;
    }
}