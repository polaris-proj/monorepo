﻿using System.Linq;
using System.Reflection;
using Domain.Connectors;
using Domain.StrategyProcessor;

namespace StrategyProcessor.Infrastructure;

public class DarkMagic(params Assembly[] assembliesWithStrategies) : IDarkMagic
{
    public Dictionary<string, Property> GetStrategyProperties(Type strategy)
    {
        var instance = Activator.CreateInstance(strategy);
        var props = GetEditableProps(strategy)
            .ToDictionary(k => k.Key,
                v => new Property(v.Value.PropertyType,
                    v.Value.GetValue(instance) ??
                    throw new InvalidOperationException($"Can't get default value of {v.Key} in strategy {strategy.FullName}")));
        return props;
    }

    public Dictionary<string, PropertyInfo> GetEditableProps(Type strategyType)
    {
        return strategyType.GetProperties()
            .Select(prop => (prop, prop.GetCustomAttribute<StrategyParameterAttribute>()))
            .Where(x => x.Item2 is not null)
            .ToDictionary(x => x.Item2!.Name, y => y.prop);
    }

    public IEnumerable<Type> GetAllStrategiesTypes()
    {
        return _assembliesWithStrategies.SelectMany(x => x.GetTypes())
                                        .Where(IsStrategyType) ?? throw new InvalidOperationException();
    }

    public Type GetConnectorType(Type strategyType)
    {
        return (strategyType.BaseType ?? throw new InvalidOperationException("StrategyType should contains baseType"))
                .GetGenericArguments()
                .FirstOrDefault() ?? throw new InvalidOperationException("BaseType should contains generic");
    }

    public StrategyConnectorType GetTypeOfStrategy(Type strategyType)
    {
        var connectorType = GetConnectorType(strategyType);
        return connectorType switch
        {
            _ when connectorType == typeof(ISpotConnector) => StrategyConnectorType.Spot,
            _ when connectorType == typeof(IFuturesConnector) => StrategyConnectorType.Futures,
            _ => throw new ArgumentOutOfRangeException($"Can't detect type of strategy {connectorType.FullName}")
        };
    }

    public Type GetTypeOfStrategy(string strategyName)
    {
        if (StrategyTypeByStrategyName.Count == 0)
        {
            AddStrategiesToComparison();
        }

        if (!StrategyTypeByStrategyName.TryGetValue(strategyName, out var strategyType))
        {
            throw new KeyNotFoundException($"Unknown type of strategy {strategyName}");
        }

        return strategyType;
    }

    private void AddStrategiesToComparison()
    {
        var allStrategies = GetAllStrategiesTypes();

        foreach (var strategy in allStrategies)
        {
            if (!IsStrategyType(strategy))
            {
                throw new ArgumentOutOfRangeException($"Type should be inherited from {typeof(BaseStrategy<>)}");
            }

            StrategyTypeByStrategyName.Add(strategy.FullName ?? throw new InvalidOperationException($"Cant' get full name of class {strategy}"), strategy);
        }
    }

    private static bool IsStrategyType(Type t)
    {
        return t is {IsClass: true, IsAbstract: false}
               && (t.IsSubclassOf(typeof(BaseStrategy<ISpotConnector>)) || t.IsSubclassOf(typeof(BaseStrategy<IFuturesConnector>)));
    }

    private readonly Assembly[] _assembliesWithStrategies = assembliesWithStrategies.Distinct().ToArray();
    private Dictionary<string, Type> StrategyTypeByStrategyName { get; } = new();
}