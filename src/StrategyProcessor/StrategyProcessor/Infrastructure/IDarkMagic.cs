﻿using System.Reflection;
using Domain.StrategyProcessor;

namespace StrategyProcessor.Infrastructure;

internal interface IDarkMagic
{
    Dictionary<string, Property> GetStrategyProperties(Type strategy);
    Dictionary<string, PropertyInfo> GetEditableProps(Type strategyType);
    IEnumerable<Type> GetAllStrategiesTypes();
    Type GetConnectorType(Type strategyType);
    StrategyConnectorType GetTypeOfStrategy(Type strategyType);
    Type GetTypeOfStrategy(string strategyName);
}