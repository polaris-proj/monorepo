﻿using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.Json;
using Domain.Connectors;
using Domain.StrategyProcessor;
using Infrastructure;
using Infrastructure.Json;
using Microsoft.Extensions.Logging;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;
using StrategyProcessor.Services;
using StrategyProcessor.Strategy;

namespace StrategyProcessor.Infrastructure;

internal class RestoreStrategyManagerState : StartUpService
{
    public RestoreStrategyManagerState(
        IStrategyManagerRepository strategyManagerRepository,
        IStartStopStrategyServiceTypedFactory startStopStrategyServiceTypedFactory,
        IDarkMagic darkMagic,
        ILoggerFactory loggerFactory
    )
    {
        _strategyManagerRepository = strategyManagerRepository;
        _startStopStrategyServiceTypedFactory = startStopStrategyServiceTypedFactory;
        _darkMagic = darkMagic;
        _logger = loggerFactory.CreateLogger<RestoreStrategyManagerState>();
    }

    public override async Task Start()
    {
        var strategies = await _strategyManagerRepository.ReadAll();
        await Task.WhenAll(strategies.Select(async x=>
        {
            try
            {
                await CallRestoreStrategyManager(x);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Can't restore state");
            }
        }));
    }

    private async Task RestoreStrategyManager<TStrategy, TConnector>(StrategyManagerDbo strategyManagerDbo)
        where TStrategy : BaseStrategy<TConnector>, new()
        where TConnector : IConnector
    {
        var runId = strategyManagerDbo.RunId;
        var userId = strategyManagerDbo.UserId;
        var json = strategyManagerDbo.StartStrategyParametersJson;
        var startStopStrategyService =
            _startStopStrategyServiceTypedFactory.GetInstance(strategyManagerDbo.StrategyType);

        var startStrategyParameters =
            json.Deserialize<StartStrategyParameters>(JsonOptions.JsonSerializerOptions) ??
            throw new SerializationException("Can't deserialize strategyParameters");

        var strategyManager =
            (StrategyManager<TStrategy, TConnector>) startStopStrategyService.CreateStrategyManager(
                startStrategyParameters, runId);

        //todo если что-то крашнется, то весь стретеджи мэнэджер упадет
        var runningStrategies =
            strategyManagerDbo.StrategyManagerState.Deserialize<StrategyManagerState<TStrategy>>(JsonOptions
                .JsonSerializerOptions) ??
            throw new SerializationException("Can't deserialize strategies");

        strategyManager.SetState(runningStrategies);

        await startStopStrategyService.RunStrategyManager(userId, startStrategyParameters, runId, strategyManager);
    }

    private Task CallRestoreStrategyManager(StrategyManagerDbo x)
    {
        var connectorType = _darkMagic.GetConnectorType(x.StrategyType);
        var method = GetType().GetMethod(nameof(RestoreStrategyManager), BindingFlags.NonPublic | BindingFlags.Instance);
        var generic = method!.MakeGenericMethod(x.StrategyType, connectorType);
        return generic.Invoke(this, [x]) as Task ?? throw new InvalidOperationException();
    }

    private readonly IStrategyManagerRepository _strategyManagerRepository;
    private readonly IStartStopStrategyServiceTypedFactory _startStopStrategyServiceTypedFactory;
    private readonly IDarkMagic _darkMagic;
    private readonly ILogger<RestoreStrategyManagerState> _logger;
}