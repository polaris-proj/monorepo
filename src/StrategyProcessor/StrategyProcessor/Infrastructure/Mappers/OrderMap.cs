﻿using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.Mappers;

public static class OrderMap
{
    public static OrderDto ToDto(this OrderDbo orderDbo)
    {
        return new OrderDto
        {
            Amount = orderDbo.Amount,
            CreatedAt = orderDbo.CreatedAt ?? DateTime.MinValue,//todo а почему так?
            Pair = orderDbo.Pair,
            OrderId = orderDbo.OrderId,
            Price = orderDbo.Price,
            Status = orderDbo.Status,
            Type = orderDbo.Type,
            UpdatedAt = orderDbo.UpdatedAt,
            Leverage = orderDbo.Leverage,
            StopPrice = orderDbo.StopPrice,
            TakePrice = orderDbo.TakePrice,
        };
    }
    
    public static OrderDbo ToDbo(this OrderDto orderDto, Guid runId)
    {
        return new OrderDbo
        {
            Amount = orderDto.Amount,
            CreatedAt = orderDto.CreatedAt,
            Pair = orderDto.Pair,
            Price = orderDto.Price,
            Status = orderDto.Status,
            Type = orderDto.Type,
            UpdatedAt = orderDto.UpdatedAt,
            Leverage = orderDto.Leverage,
            StopPrice = orderDto.StopPrice,
            TakePrice = orderDto.TakePrice,
            OrderId = orderDto.OrderId,
            RunOfStrategyId = runId
        };
    }
}