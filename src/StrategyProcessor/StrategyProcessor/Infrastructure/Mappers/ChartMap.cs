﻿using System.Linq;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.Mappers;

public static class ChartMap
{
    public static ChartDto ToDto(this ChartDbo dbo)
    {
        return new ChartDto
        {
            Name = dbo.Name,
            Description = dbo.Description,
            Pair = new Pair(dbo.Pair),
            TimeFrame = dbo.TimeFrame,
            Chart = dbo.Chart.Select(x => x.ToModel()).ToList(),
        };
    }
}