﻿using System.Text.Json;
using Domain.StrategyProcessor;
using Infrastructure.Json;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.Mappers;

internal static class StrategyParameterMap
{
    private static readonly JsonSerializerOptions Options = JsonOptions.JsonSerializerOptions;

    public static StrategyParameter ToModel(this StrategyParameterDbo parameterDbo)
    {
        return new StrategyParameter(parameterDbo.Name,
            JsonSerializer.Deserialize(parameterDbo.ParameterJson, parameterDbo.ParameterType, Options)
            ?? throw new InvalidOperationException()
        );
    }

    public static StrategyParameterDbo ToDbo(this StrategyParameterDto candle)
    {
        return new StrategyParameterDbo
        {
            Name = candle.Name,
            ParameterJson = candle.JsonValue,
            ParameterType = candle.ParameterType
        };
    }

    public static StrategyParameterDto ToDto<T>(this StrategyParameter<T> strategyParameters)
    {
        return new StrategyParameterDto
        {
            Name = strategyParameters.Name,
            JsonValue = JsonSerializer.Serialize((T) strategyParameters.Value, Options),
            ParameterType = typeof(T)
        };
    }

    public static StrategyParameter ToModel(this StrategyParameterDto strategyStartDto)
    {
        return new StrategyParameter(strategyStartDto.Name,
            JsonSerializer.Deserialize(strategyStartDto.JsonValue, strategyStartDto.ParameterType, Options)
            ?? throw new InvalidOperationException()
        );
    }

    public static StrategyParameterDto ToDto(this ParameterDbo parameterDbo)
    {
        return new StrategyParameterDto
        {
            Name = parameterDbo.Name,
            JsonValue = parameterDbo.ParameterValue.RootElement.GetRawText(),
            ParameterType = Type.GetType(parameterDbo.TypeName) ??
                            throw new InvalidOperationException($"Can't get type of parameter {parameterDbo.TypeName}")
        };
    }

    public static StrategyParameterDbo ToDbo<T>(this StrategyParameter<T> strategyParameter)
    {
        return new StrategyParameterDbo
        {
            Name = strategyParameter.Name,
            ParameterJson = JsonSerializer.Serialize(strategyParameter.Value, Options),
            ParameterType = typeof(T),
        };
    }
}