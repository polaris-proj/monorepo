﻿using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.Mappers;

public static class ExchangePairTimeFrameMap
{
    public static ExchangePairTimeFrame ToModel(this ExchangePairTimeFrameDto strategyStartEvent)
    {
        return new ExchangePairTimeFrame(
            strategyStartEvent.Exchange,
            new Pair(strategyStartEvent.Pair),
            strategyStartEvent.Timeframe
        );
    }

    public static ExchangePairTimeFrameDto ToDto(this ExchangePairTimeFrame strategyStartEventDto)
    {
        return new ExchangePairTimeFrameDto(
            strategyStartEventDto.Exchange,
            new Pair(strategyStartEventDto.Pair),
            strategyStartEventDto.TimeFrame
        );
    }

    public static ExchangePairTimeFrameDbo ToDbo(this ExchangePairTimeFrame strategyStartEventDto)
    {
        return new ExchangePairTimeFrameDbo
        {
            Exchange = strategyStartEventDto.Exchange,
            Pair = new Pair(strategyStartEventDto.Pair),
            TimeFrame = strategyStartEventDto.TimeFrame
        };
    }
}