﻿using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.DAL.Models;

namespace StrategyProcessor.Infrastructure.Mappers;

public static class PointMap
{
    public static PointDbo ToDbo(this Point point)
    {
        return new PointDbo
        {
            Value = point.Value,
            TimeStamp = point.TimeStamp,
        };
    }

    public static Point ToModel(this PointDbo dbo)
    {
        return new Point(dbo.TimeStamp, dbo.Value);
    }
}