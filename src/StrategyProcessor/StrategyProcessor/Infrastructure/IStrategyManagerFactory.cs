﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using StrategyProcessor.Strategy;

namespace StrategyProcessor.Infrastructure;

internal interface IStrategyManagerFactory
{
    public StrategyManager<TStrategy, TConnector> Create<TStrategy, TConnector>
    (
        Guid runId,
        IReadOnlyList<StrategyParameter> parameters,
        Dictionary<Exchange, TConnector> connectors,
        IReadOnlyList<ExchangePairTimeFrame> allowedSources,
        IChartDataStorage chartDataStorage,
        bool storeInDatabase
    )
        where TStrategy : BaseStrategy<TConnector>, new()
        where TConnector : IConnector;
}