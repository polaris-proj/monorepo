﻿using Domain.StrategyProcessor;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

/// <inheritdoc />
/// <summary>Save strategy artifacts like (charts, marks) to db</summary>
public class ChartDataStorage : IChartDataStorage
{
    public ChartDataStorage(Guid runId, IChartRepository chartRepository, INotesRepository notesRepository)
    {
        _runId = runId;
        _chartRepository = chartRepository;
        _notesRepository = notesRepository;
    }

    public void AddChartDot(string name, string? description, Point point, Pair pair, TimeFrame timeFrame)
    {
        _chartRepository.AddDotToChartAsync(_runId, name, description, pair, timeFrame, point);
    }

    public void CreateNote(Note note)
    {
        _notesRepository.AddNotesAsync(_runId, new List<Note>(1) {note});
    }

    public ValueTask DisposeAsync()
    {
        return ValueTask.CompletedTask;
    }

    private readonly Guid _runId;
    private readonly IChartRepository _chartRepository;
    private readonly INotesRepository _notesRepository;
}