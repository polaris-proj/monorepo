﻿namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

public class RunStrategyArtifactsRepository
{
    public RunStrategyArtifactsRepository(IRunStrategyRepository runStrategyRepository,
        IChartRepository chartRepository,
        INotesRepository notesRepository, IOrderRepository orderRepository)
    {
        RunStrategyRepository = runStrategyRepository;
        ChartRepository = chartRepository;
        NotesRepository = notesRepository;
        OrderRepository = orderRepository;
    }

    public IRunStrategyRepository RunStrategyRepository { get; }
    public IChartRepository ChartRepository { get; }
    public INotesRepository NotesRepository { get; }
    public IOrderRepository OrderRepository { get; }
}