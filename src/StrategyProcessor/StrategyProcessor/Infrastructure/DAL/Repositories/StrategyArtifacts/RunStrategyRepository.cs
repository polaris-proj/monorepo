﻿using Dapper;
using Domain.StrategyProcessor;
using Npgsql;
using StrategyProcessor.Infrastructure.DAL.Models;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

public class RunStrategyRepository : IRunStrategyRepository
{
    public RunStrategyRepository(NpgsqlDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public async Task<Guid> CreateAsync(RunOfStrategyDbo item)
    {
        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();

        const string sql =
            """
            INSERT INTO "RunOfStrategyDbo" ("Id", "StrategyId", "StatusOfRun")
            VALUES (@Id, @StrategyId, @StatusOfRun)
            """;
        await connection.ExecuteAsync(sql, item);
        return item.Id;
    }

    public async Task<RunOfStrategyDbo?> ReadAsync(Guid runId)
    {
        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();

        const string sql = """
                           SELECT * FROM "RunOfStrategyDbo"
                           WHERE "Id" = @Id
                           """;
        return await connection.QueryFirstOrDefaultAsync<RunOfStrategyDbo>(sql, new {Id = runId});
    }

    public async Task UpdateStatusAsync(Guid id, StatusOfRun statusOfRun)
    {
        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();

        const string sql = """
                           UPDATE "RunOfStrategyDbo" SET "StatusOfRun" = @StatusOfRun
                           WHERE "Id" = @Id
                           """;
        var updatedRows = await connection.ExecuteAsync(sql, new {Id = id, StatusOfRun = statusOfRun});
        if (updatedRows == 0)
        {
            throw new InvalidOperationException($"TestOfStrategyDbo with id {id} not found");
        }
    }


    private readonly NpgsqlDataSource _dataSource;
}