﻿using Domain.Connectors;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

public interface IOrderRepository
{
    Task AddOrdersAsync(Guid runId, IEnumerable<OrderDto> orders);
    Task CloseOrderAsync(OrderId orderId);
    Task<IEnumerable<OrderDto>> ReadAsync(Guid runId, Pair pair, TimeFrame timeFrame, DateTimeRange dateTimeRange);
}