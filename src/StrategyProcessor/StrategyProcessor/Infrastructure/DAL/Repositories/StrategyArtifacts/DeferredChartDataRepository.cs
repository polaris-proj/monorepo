﻿using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using Domain.StrategyProcessor;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

/// <inheritdoc cref="IChartDataStorage" />
/// <summary>Save strategy artifacts like (charts, orders, marks) to db when dispose to reduce amount of requests</summary>
public class DeferredChartDataRepository : IChartDataStorage //todo IDisposableAsync
{
    public DeferredChartDataRepository(Guid runId, IChartRepository chartRepository, INotesRepository notesRepository)
    {
        _runId = runId;
        _chartRepository = chartRepository;
        _notesRepository = notesRepository;
    }

    public void AddChartDot(string name, string? description, Point point, Pair pair, TimeFrame timeFrame)
    {
        _chartsCache.AddOrUpdate((name, description, pair, timeFrame),
            _ => [point],
            (_, y) =>
            {
                var newlist = y.ToList();
                newlist.Add(point);
                return newlist;
            });
    }

    public void CreateNote(Note note)
    {
        _notesCache.Add(note);
    }

    public async ValueTask DisposeAsync()
    {
        var sw = Stopwatch.StartNew(); 
        var charts = _chartsCache.Select(x => new ChartDto
            {
                Name = x.Key.Item1,
                Description = x.Key.Item2,
                Pair = x.Key.Item3,
                TimeFrame = x.Key.Item4,
                Chart = x.Value,
            })
            .ToList();

        await Task.WhenAll(
            _chartRepository.AddChartsAsync(_runId, charts),
            _notesRepository.AddNotesAsync(_runId, _notesCache)
        );

        Console.WriteLine($"Write Deffered chart {sw.ElapsedMilliseconds}");
    }

    private readonly Guid _runId;
    private readonly IChartRepository _chartRepository;
    private readonly INotesRepository _notesRepository;
    private readonly ConcurrentDictionary<(string, string?, Pair, TimeFrame), List<Point>> _chartsCache = new();
    private readonly List<Note> _notesCache = new();
}