﻿using Microsoft.EntityFrameworkCore;
using StrategyProcessor.Infrastructure.DAL.Models;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;

internal class StrategyManagerRepository : IStrategyManagerRepository
{
    public StrategyManagerRepository(IDbContextFactory<StrategyProcessorContext> applicationContext)
    {
        _dbContextFactory = applicationContext;
    }

    public async Task<IReadOnlyCollection<StrategyManagerDbo>> ReadAll()
    {
        await using var db = await _dbContextFactory.CreateDbContextAsync();
        var items = await db.StrategyManagers.Include(x=>x.ExchangePairTimeFrameDbos)
                                             .ToListAsync();
        return items;
    }

    public async Task<StrategyManagerDbo?> ReadAsync(Guid id)
    {
        await using var db = await _dbContextFactory.CreateDbContextAsync();
        return await db.StrategyManagers.Include(x=>x.ExchangePairTimeFrameDbos)
                                        .FirstOrDefaultAsync(u => u.RunId == id);
    }
    
    public async Task<IReadOnlyCollection<StrategyManagerDbo>> ReadLocked()
    {
        await using var context = await _dbContextFactory.CreateDbContextAsync();
        await using var transaction = await context.Database.BeginTransactionAsync();
        
        var items = await context.StrategyManagers.Include(x=>x.ExchangePairTimeFrameDbos)
                                                  .ToListAsync();
        
        await transaction.CommitAsync();
        return items;
    }

   public async Task<Guid> CreateAsync(StrategyManagerDbo item)
    {
        await using var db = await _dbContextFactory.CreateDbContextAsync();
        await db.StrategyManagers.AddAsync(item);
        await db.SaveChangesAsync();
        return item.RunId;
    }

    public async Task UpdateAsync(StrategyManagerDbo item)
    {
        await using var db = await _dbContextFactory.CreateDbContextAsync();
        db.Update(item);
        await db.SaveChangesAsync();
    }

    private readonly IDbContextFactory<StrategyProcessorContext> _dbContextFactory;
}