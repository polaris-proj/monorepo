﻿using StrategyProcessor.Infrastructure.DAL.Models;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;

public interface IStrategyManagerRepository
{
    Task<IReadOnlyCollection<StrategyManagerDbo>> ReadAll();
    Task<StrategyManagerDbo?> ReadAsync(Guid id);
    Task<Guid> CreateAsync(StrategyManagerDbo item);
    Task UpdateAsync(StrategyManagerDbo item);
}