﻿using StrategyProcessor.Infrastructure.DAL.Models;

namespace StrategyProcessor.Infrastructure.DAL.Repositories;

public interface IStrategyRepository
{
    Task<StrategyDbo?> ReadByIdAsync(Guid id);
    Task<StrategyDbo?> ReadByNameAsync(string name);
    Task<List<StrategyDbo>> ReadAllAsync();
    Task<Guid> CreateAsync(StrategyDbo item);
    void Update(StrategyDbo item);
    Task SaveAsync();
}