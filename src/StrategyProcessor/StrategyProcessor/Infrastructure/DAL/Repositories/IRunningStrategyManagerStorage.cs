﻿using StrategyProcessor.Strategy;

namespace StrategyProcessor.Infrastructure.DAL.Repositories;

internal interface IRunningStrategyManagerStorage
{
    void AddStrategyManager(IStrategyManager strategyManager);
    ValueTask RemoveStrategyManager(Guid runId);
    IReadOnlyCollection<IStrategyManager> StrategyManagers { get; }
}