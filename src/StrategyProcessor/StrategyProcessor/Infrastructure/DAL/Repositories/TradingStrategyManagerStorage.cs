﻿using StrategyProcessor.Strategy;

namespace StrategyProcessor.Infrastructure.DAL.Repositories;

internal class TradingStrategyManagerStorage : IRunningStrategyManagerStorage
{
    public void AddStrategyManager(IStrategyManager strategyManager) => UserStrategyManagers.Add(strategyManager);
    public async ValueTask RemoveStrategyManager(Guid runId)
    {
        var strategyManager = UserStrategyManagers.Find(x => x.RunId == runId);
        if (strategyManager != null)
        {
            UserStrategyManagers.Remove(strategyManager);
            await strategyManager.DisposeAsync();
        }
        else
        {
            throw new KeyNotFoundException($"StrategyManager with id {runId} not found");
        }
    }

    public IReadOnlyCollection<IStrategyManager> StrategyManagers => UserStrategyManagers;
    private List<IStrategyManager> UserStrategyManagers { get; } = new();
}