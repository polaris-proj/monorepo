﻿using System.Text.Json;
using Infrastructure;
using Infrastructure.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Logging;
using StrategyProcessor.Infrastructure.DAL.Models;

namespace StrategyProcessor.Infrastructure.DAL;

public sealed class StrategyProcessorContext : DbContext
{
    public DbSet<StrategyManagerDbo> StrategyManagers { get; set; } = null!;

    public StrategyProcessorContext(PostgresParameters postgresParameters, ILoggerFactory loggerFactory)
    {
        _loggerFactory = loggerFactory;
        _connectionString = postgresParameters.FullConnectionString;
        //Database.EnsureDeleted();
        Database.EnsureCreated();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<StrategyDbo>()
            .HasKey(x => x.Id);

        modelBuilder.Entity<StrategyDbo>()
            .HasIndex(x => x.InternalName);

        modelBuilder.Entity<StrategyDbo>()
            .HasMany(x => x.Properties)
            .WithOne()
            .HasForeignKey(x => x.StrategyDboId)
            .OnDelete(DeleteBehavior.Cascade);

        modelBuilder.Entity<StrategyManagerDbo>()
            .HasKey(x => x.RunId);

        modelBuilder.Entity<ExchangePairTimeFrameDbo>()
            .HasKey(x => x.Id);

        modelBuilder.Entity<StrategyManagerDbo>()
            .HasMany(x => x.ExchangePairTimeFrameDbos)
            .WithOne()
            .HasForeignKey(x => x.StrategyManagerDboId)
            .OnDelete(DeleteBehavior.Cascade);
    }

    protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
    {
        configurationBuilder.Properties<Type>().HaveConversion<FullTypeConverter>();
        configurationBuilder.Properties<JsonDocument>().HaveConversion<JsonConverter>();
        configurationBuilder.Properties<Pair>().HaveConversion<PairConverter>();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(_connectionString)
            .UseLoggerFactory(_loggerFactory);
    }

    private readonly string _connectionString;
    private readonly ILoggerFactory _loggerFactory;

    private class PairConverter() : ValueConverter<Pair, string>(v => v.ToString(), v => new Pair(v));
}