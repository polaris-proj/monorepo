﻿using System.Data;
using Dapper;
using Infrastructure;
using Microsoft.Extensions.Logging;
using Npgsql;

namespace StrategyProcessor.Infrastructure.DAL;

public class Migrations : StartUpService
{
    public Migrations(NpgsqlDataSource dataSource, ILogger<Migrations> logger)
    {
        _dataSource = dataSource;
        _logger = logger;
    }

    public override async Task Start()
    {
        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();
        
        await MigrateRunStrategyArtifactsRepository(connection);
    }

    private async Task MigrateRunStrategyArtifactsRepository(IDbConnection connection)
    {
        var migrations = new[]
        {
            """
            CREATE TABLE IF NOT EXISTS "RunOfStrategyDbo"
            (
                "Id" UUID PRIMARY KEY,
                "StrategyId" UUID NOT NULL,
                "StatusOfRun" INT NOT NULL DEFAULT 0
            )
            """,
            """
                        CREATE TABLE IF NOT EXISTS "ChartDbo"
                        (
                            "Id" bigserial PRIMARY KEY,
                            "Name" VARCHAR(50) NOT NULL,
                            "Description" VARCHAR(200),
                            "Pair" VARCHAR(30) NOT NULL,
                            "TimeFrame" INT NOT NULL,
                            "RunOfStrategyDboId" UUID NOT NULL
                        )
            """,
            """
            CREATE TABLE IF NOT EXISTS "PointDbo"
            (
                "Id" bigserial PRIMARY KEY,
                "TimeStamp" BIGINT NOT NULL,
                "Value" NUMERIC NOT NULL,
                "ChartDboId" bigint NOT NULL,
                FOREIGN KEY ("ChartDboId") REFERENCES "ChartDbo"("Id")
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS "NoteDbo"
            (
                "Id" bigserial PRIMARY KEY,
                "Pair" VARCHAR(30) NOT NULL,
                "TimeFrame" INT NOT NULL,
                "TimeStamp" BIGINT NOT NULL,
                "Price" NUMERIC NOT NULL,
                "Text" VARCHAR(1000),
                "Icon" INT NOT NULL,
                "RunOfStrategyId" UUID NOT NULL
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS "OrderDbo"
            (
                "Id" BIGSERIAL PRIMARY KEY,
                "Pair" VARCHAR(30) NOT NULL,
                "Type" INT NOT NULL,
                "Status" INT NOT NULL,
                "Price" NUMERIC,
                "Amount" NUMERIC NOT NULL,
                "Leverage" INT,
                "TakePrice" NUMERIC,
                "StopPrice" NUMERIC,
                "OrderId" VARCHAR(50),
                "CreatedAt" TIMESTAMPTZ,
                "UpdatedAt" TIMESTAMPTZ,
                "RunOfStrategyId" UUID NOT NULL
            );
            """
        };

        var counter = 0;
        foreach (var migration in migrations)
        {
            try
            {
                await connection.ExecuteAsync(migration);
                _logger.LogInformation("Migration {counter} completed", counter);
            }
            catch (Exception)
            {
                _logger.LogCritical("Migration {counter} failed", counter);
                throw;
            }
        }
    }

    public override int Priority => int.MaxValue;
    private readonly ILogger<Migrations> _logger;
    private readonly NpgsqlDataSource _dataSource;
}