﻿

// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
#pragma warning disable CS8618
namespace StrategyProcessor.Infrastructure.DAL.Models;

public class StrategyParameterDbo
{
    public Guid Id { get; set; }
    public Guid StrategyManagerDboId { get; set; }

    public virtual StrategyManagerDbo StrategyManagerDbo { get; set; }
    public string Name { get; set; }
    public string ParameterJson { get; set; }
    public Type ParameterType { get; set; }
}