﻿using System.ComponentModel.DataAnnotations;

namespace StrategyProcessor.Infrastructure.DAL.Models;

public class ChartDbo
{
    public ulong Id { get; set; }
    [StringLength(50)] public required string Name { get; set; }
    [StringLength(200)] public string? Description { get; set; }
    [StringLength(30)] public required string Pair { get; set; }
    public required TimeFrame TimeFrame { get; set; }
    public ICollection<PointDbo> Chart { get; set; } = new List<PointDbo>();

    public Guid TestOfStrategyDboId { get; set; }
}