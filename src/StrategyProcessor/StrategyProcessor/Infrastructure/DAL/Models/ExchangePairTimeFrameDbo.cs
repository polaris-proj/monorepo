﻿using System.ComponentModel.DataAnnotations;

namespace StrategyProcessor.Infrastructure.DAL.Models;

public class ExchangePairTimeFrameDbo
{
    public Guid Id { get; set; }
    public required Exchange Exchange { get; set; }
    [MaxLength(50)] public required string Pair { get; set; }
    public required TimeFrame TimeFrame { get; set; }
    public Guid StrategyManagerDboId { get; set; }
}