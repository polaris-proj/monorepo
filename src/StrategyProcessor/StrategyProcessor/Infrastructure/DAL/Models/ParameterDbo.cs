﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json;

namespace StrategyProcessor.Infrastructure.DAL.Models;

public class ParameterDbo
{
    public Guid Id { get; set; }
    [StringLength(100)] public required string Name { get; set; }
    [StringLength(100)] public required string TypeName { get; set; }
    public required JsonDocument ParameterValue { get; set; }
    public Guid StrategyId { get; set; }
    public Guid StrategyDboId { get; set; }
}