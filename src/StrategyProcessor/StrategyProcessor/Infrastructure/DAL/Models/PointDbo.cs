﻿namespace StrategyProcessor.Infrastructure.DAL.Models;

public class PointDbo
{
    public ulong Id { get; set; }
    public required long TimeStamp { get; set; }
    public required decimal Value { get; set; }

    public ulong ChartDboId { get; set; }
}