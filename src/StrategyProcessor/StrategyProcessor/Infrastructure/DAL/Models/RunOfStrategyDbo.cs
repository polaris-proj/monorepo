﻿using Domain.StrategyProcessor;

namespace StrategyProcessor.Infrastructure.DAL.Models;

public class RunOfStrategyDbo
{
    public Guid Id { get; set; }//TestId
    public required Guid StrategyId { get; set; }
    public StatusOfRun StatusOfRun { get; set; } = StatusOfRun.Created;
    
    public ICollection<NoteDbo> Notes { get; set; } = new List<NoteDbo>();
    public ICollection<ChartDbo> Charts { get; set; } = new List<ChartDbo>();
    public ICollection<OrderDbo> Orders { get; set; } = new List<OrderDbo>();
}