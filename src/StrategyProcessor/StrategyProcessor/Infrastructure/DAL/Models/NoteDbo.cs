﻿using System.ComponentModel.DataAnnotations;
using Domain.StrategyProcessor;

namespace StrategyProcessor.Infrastructure.DAL.Models;

public class NoteDbo
{
    public ulong Id { get; set; }
    public required string Pair { get; set; }
    public required TimeFrame TimeFrame { get; set; }
    public required long TimeStamp { get; set; }
    public required decimal Price { get; set; }
    [StringLength(1000)] public string? Text { get; set; }
    public Icon Icon { get; set; }
    public Guid RunOfStrategyId { get; set; }
}
