﻿using System.Text.Json;

namespace StrategyProcessor.Infrastructure.DAL.Models;

public class StrategyManagerDbo
{
    public required Guid RunId { get; set; }
    public required Guid UserId { get; set; }
    public required Type StrategyType { get; set; }
    public required JsonDocument StartStrategyParametersJson { get; set; }
    public required JsonDocument StrategyManagerState { get; set; }
    public ICollection<ExchangePairTimeFrameDbo> ExchangePairTimeFrameDbos = new List<ExchangePairTimeFrameDbo>();
}