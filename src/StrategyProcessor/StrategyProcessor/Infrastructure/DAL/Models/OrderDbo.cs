﻿using System.ComponentModel.DataAnnotations;
using Domain.Connectors;
using Domain.StrategyProcessor;

namespace StrategyProcessor.Infrastructure.DAL.Models;

public class OrderDbo
{
    public ulong Id { get; set; }
    public required Pair Pair { get; set; } // Ticker/Ticker
    public OrderType Type { get; set; }
    public OrderStatus Status { get; set; }
    public decimal? Price { get; set; }
    public decimal Amount { get; set; }
    public int? Leverage { get; set; }

    public decimal? TakePrice { get; set; }
    public decimal? StopPrice { get; set; }
    [StringLength(50)] public string? OrderId { get; set; }//todo add index

    public DateTime? CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }

    public Guid RunOfStrategyId { get; set; }
}