﻿using System.Linq;
using ProtoBuf.Grpc;
using StrategyProcessor.Infrastructure.DAL.Repositories;
using StrategyProcessor.Infrastructure.Mappers;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Controllers;

internal class GetStrategiesService : IGetStrategiesService

{
    public GetStrategiesService(IStrategyRepository strategyRepository)
    {
        _strategyRepository = strategyRepository;
    }


    public async Task<StrategyDto[]> GetStrategies(CallContext context = default)
    {
        var strategies = await _strategyRepository.ReadAllAsync();
        var mapped = strategies.Select(x => x.ToDto()).ToArray();
        return mapped;
        //todo add cache
    }

    private readonly IStrategyRepository _strategyRepository;
}