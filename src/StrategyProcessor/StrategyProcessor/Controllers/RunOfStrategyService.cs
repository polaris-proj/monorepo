﻿using System.Linq;
using Domain.StrategyProcessor;
using ProtoBuf.Grpc;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Controllers;

public class RunOfStrategyService(
    IRunStrategyRepository runStrategyRepository,
    IChartRepository chartRepository,
    INotesRepository notesRepository,
    IOrderRepository orderRepository)
    : IRunOfStrategyService
{
    public async Task<TestOfStrategyDto> GetRunResult(TestOfStrategyArgument arg, CallContext context = default)
    {
        var dbo = await runStrategyRepository.ReadAsync(arg.RunId);
        if (dbo is null)
        {
            throw new KeyNotFoundException($"Can't find test with id: {arg.RunId}");
        }

        var result = new TestOfStrategyDto {Status = dbo.StatusOfRun};

        if (dbo.StatusOfRun == StatusOfRun.Completed)
        {
            var notes = notesRepository.ReadAsync(arg.RunId, arg.Pair, arg.TimeFrame, arg.DateTimeRange);
            var chart = chartRepository.ReadAsync(arg.RunId, arg.Pair, arg.TimeFrame, arg.DateTimeRange);
            var orders = orderRepository.ReadAsync(arg.RunId, arg.Pair, arg.TimeFrame, arg.DateTimeRange);
            await Task.WhenAll(notes, chart, orders);
            result.Notes = (await notes).OrderBy(x => x.TimeStamp).ToList();
            result.Orders = (await orders).OrderBy(x => x.CreatedAt).ToList();
            result.Charts = (await chart).ToList();
        }

        return result;
    }
}