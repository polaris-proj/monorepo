﻿using System.Diagnostics;
using System.Linq;
using Domain.Connectors;
using Domain.StrategyProcessor;
using Infrastructure.Grpc;
using ProtoBuf.Grpc;
using ReadOnlyCollectionsExtensions;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Infrastructure.DAL.Repositories;
using StrategyProcessor.Infrastructure.Mappers;
using StrategyProcessor.Services;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Controllers;

internal class StrategyServiceGrpc(
    IStartStopStrategyServiceTypedFactory startStopStrategyServiceTypedFactory,
    IDarkMagic darkMagic,
    IStrategyRepository strategyRepository
)
    : IStrategyServiceGrpc
{
    public async Task<Answer<Guid>> Start(StrategyStartDto arguments, CallContext context = default)
    {
        var sw = Stopwatch.StartNew();

        var allowedSources = arguments.DataSources.Select(x => x.ToModel()).ToReadOnlyList();

        var strategyDbo = await strategyRepository.ReadByIdAsync(arguments.StrategyId);
        if (strategyDbo is null)
        {
            throw new ArgumentOutOfRangeException($"Unknown strategyId: {arguments.StrategyId}");
        }

        var strategyType = darkMagic.GetTypeOfStrategy(strategyDbo.InternalName);
        Validate(allowedSources, strategyType);

        var startStopStrategyService = startStopStrategyServiceTypedFactory.GetInstance(strategyType);

        var runId = await startStopStrategyService.StartNewStrategyAsync(GetStartStrategyParameters(arguments, allowedSources));

        Console.WriteLine($"grpc Service {sw.ElapsedMilliseconds}");
        return new Answer<Guid>(runId);
    }

    private static StartStrategyParameters GetStartStrategyParameters(StrategyStartDto args,
        IReadOnlyList<ExchangePairTimeFrame> allowedSources)
    {
        var parameters = args.Parameters.Select(x => x.ToDbo()).ToReadOnlyList();
        var initialBalance = new List<Balance> {new("USDT", args.InitialBalance, 0)};
        return new StartStrategyParameters(
            args.StrategyId, args.UserId, args.RunMode, args.EnableOrderConfirmation, allowedSources,
            parameters, initialBalance, new DateTimeRange(args.StartTime, args.EndTime));
    }
    

    private void Validate(IReadOnlyList<ExchangePairTimeFrame> allowedSources, Type strategyType)
    {
        if (!(allowedSources.All(x => x.Exchange.IsFutures()) || allowedSources.All(x => x.Exchange.IsSpot())))
            throw new Exception("All connectors should be of only one type (futures/spot)");

        if (strategyType.BaseType is null)
            throw new ArgumentException("Unknown type of strategy");

        if (!(strategyType.BaseType.GetGenericArguments()[0] == typeof(ISpotConnector) &&
              allowedSources[0].Exchange.IsSpot()
              || strategyType.BaseType.GetGenericArguments()[0] == typeof(IFuturesConnector) &&
              allowedSources[0].Exchange.IsFutures()))
        {
            throw new ArgumentOutOfRangeException(
                $"Type of strategy connector ({strategyType.BaseType.GetGenericArguments()[0].ToString}) should equals type of dataSource {allowedSources[0].Exchange}");
        }
    }

    public Task StopStrategy(StrategyStopDto arguments, CallContext context = default)
    {
        throw new NotImplementedException();
        // var _startStopStrategyService = _startStopStrategyServiceTypedFactory.GetInstance(strategyType);
        // await _startStopStrategyService.StopStrategyAsync(@event);
    }
}