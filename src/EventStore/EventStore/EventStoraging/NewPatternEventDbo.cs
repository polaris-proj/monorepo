﻿namespace EventStorage.EventStoraging;

public class NewPatternEventDbo
{
    public NewPatternEventDbo()
    {
        
    }
    
    public required int TimeFrame { get; set; }
    public required long TimeStamp { get; set; }
    public required string RecognitionResult { get; set; }

}