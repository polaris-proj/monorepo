﻿using EventStore.Shared;

namespace EventStorage.EventStoraging;

public interface IEventRepository<TEvent>
{
    Task SaveEventAsync(IEnumerable<TEvent> events);
    Task<IEnumerable<TEvent>> GetEventAsync(GetRangeArgument getRangeArgument);
}