﻿using Dapper;
using Domain;
using Domain.Patterns.Objects;
using EventStore.Shared;
using Npgsql;
using PostgreSQLCopyHelper;

namespace EventStorage.EventStoraging;

public class NewPatternEventRepository(NpgsqlDataSource dataSource) //: EventRepositoryBase(dataSource)
{
    public async Task<IEnumerable<NewPatternEvent>> GetEventAsync(GetRangeArgument rangeArgument)
    {
        var tableName = GetTableName(rangeArgument.Exchange, rangeArgument.Pair);
        string sql = $"""
                      SELECT timeframe,timestamp, recognitionresult
                      FROM {tableName}
                      WHERE timeframe = @TimeFrame
                        AND timestamp >= @TimeStampRangeBegin
                        AND timestamp <= @TimeStampRangeEnd
                      """;

        await using var connection = dataSource.CreateConnection();
        await connection.OpenAsync();
        await CreateTableIfNotExistAsync(connection, tableName);

        var result = await connection.QueryAsync<NewPatternEventDbo>(
            sql,
            new
            {
                Pair = rangeArgument.Pair.ToString(),
                TimeFrame = (int) rangeArgument.TimeFrame,
                TimeStampRangeBegin = rangeArgument.Start,
                TimeStampRangeEnd = rangeArgument.End
            }
        );

        return result.Select(eventDbo => eventDbo.Map(rangeArgument.Exchange, rangeArgument.Pair));
    }

    public async Task SaveEventsAsync(IEnumerable<NewPatternEvent> events)
    {
        await using var connection = dataSource.CreateConnection();
        await connection.OpenAsync();

        foreach (var @event in events)
        {
            await SaveEventAsync(connection, @event);
        }
    }

    public async Task SaveEventAsync(NpgsqlConnection connection, NewPatternEvent @event)
    {
        var tableName = GetTableName(@event.Exchange, @event.Pair);
        await CreateTableIfNotExistAsync(connection, tableName);

        var eventCopyHelper = GetEventCopyHelper(tableName);

        await eventCopyHelper.SaveAllAsync(connection, new[] {@event.Map()});
    }

    private async Task CreateTableIfNotExistAsync(NpgsqlConnection connection, string tableName,
        CancellationToken ct = default)
    {
        var queries = new[]
        {
            $@"
                CREATE TABLE IF NOT EXISTS ""{tableName}""
                (
                    Timeframe         int,
                    Timestamp         bigint,
                    RecognitionResult text
                );

            ",
            $@"
                 CREATE INDEX IF NOT EXISTS {tableName}_timeFrame_timestamp_idx
                        ON ""{tableName}"" (timeFrame, timestamp)
            ",
        };

        foreach (var query in queries)
        {
            await connection.ExecuteAsync(query);
            ct.ThrowIfCancellationRequested();
        }
    }

    private static string GetTableName(Exchange exchange, Pair pair) =>
        $"patterns_{exchange.ToString()}_{pair.ToStringWithOutSlash()}".ToLower();


    private PostgreSQLCopyHelper<NewPatternEventDbo> GetEventCopyHelper(string tableName) =>
        new PostgreSQLCopyHelper<NewPatternEventDbo>(tableName)
            .UsePostgresQuoting()
            // .MapText(nameof(NewPatternEventDbo.Exchange).ToLower(), x => x.Exchange.ToString())
            // .MapText(nameof(NewPatternEventDbo.Pair).ToLower(), x => x.Pair.ToString())
            .MapInteger(nameof(NewPatternEventDbo.TimeFrame).ToLower(), x => x.TimeFrame)
            .MapBigInt(nameof(NewPatternEventDbo.TimeStamp).ToLower(), x => x.TimeStamp)
            .MapText(nameof(NewPatternEventDbo.RecognitionResult).ToLower(), x => x.RecognitionResult);
}