﻿using Infrastructure;
using Infrastructure.Grpc;

namespace EventStore.Client;

public class EventStoreClientParameters(IVault vault) : IParameters, IGrpcServiceParameters
{
    public string Address { get; } = vault.GetValue("EventStore:gRPC:Url");
}