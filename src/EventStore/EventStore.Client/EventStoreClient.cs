﻿using Domain.Patterns.Objects;
using Domain.StrategyProcessor;
using EventStore.Shared;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using ProtoBuf.Grpc.Client;

namespace EventStore.Client;

public class EventStoreClient(EventStoreClientChannel channel, ILogger<EventStoreClient> logger):IEventStoreClient
{
    public async Task<IEnumerable<NewPatternEvent>> GetRange(Guid userId, ExchangePairTimeFrame key, long start, long end)
    {
        var service = _channel.CreateGrpcService<IPatternAccessServiceGrpc>();
        return await service.GetRangeAsync(new GetRangeArgument(userId,
            new ExchangePairTimeFrame(key.Exchange, key.Pair, key.TimeFrame), start, end));
    }

    // public async IAsyncEnumerable<NewPatternEvent> GetRangeStream(Guid userId,
    //     (Domain.Exchange Exchange, Domain.Pair Pair, TimeFrames.TimeFrame TimeFrame) key, long start, long end)
    // {
    //     var candleAccessServiceGrpc = _channel.CreateGrpcService<IPatternAccessServiceGrpc>();
    //     var result = candleAccessServiceGrpc.GetRangeStream(new GetRangeArgument(userId, new ExchangePairTimeFrame(key.Exchange, key.Pair, key.TimeFrame), start, end));
    //
    //     var sw = Stopwatch.StartNew();
    //     await foreach (var candlesBatch in result.Buffer(10))
    //     {
    //         foreach (var candle in candlesBatch)
    //             yield return candle;
    //         logger.LogTrace($"пачку прочитал и отправил {sw.ElapsedMilliseconds}");
    //     }
    // }

    private readonly GrpcChannel _channel = channel.Channel;
}

public interface IEventStoreClient
{
    Task<IEnumerable<NewPatternEvent>> GetRange(Guid userId, ExchangePairTimeFrame key, long start, long end);
}