﻿using System.ServiceModel;
using Domain.Patterns.Objects;
using ProtoBuf.Grpc;

namespace EventStore.Shared;

[ServiceContract]
public interface IPatternAccessServiceGrpc
{
    [OperationContract]
    Task<IReadOnlyList<NewPatternEvent>> GetRangeAsync(GetRangeArgument argument, CallContext context = default);
}