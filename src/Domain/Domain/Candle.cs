﻿using System.Runtime.Serialization;

namespace Domain;

[DataContract]
public record Candle
{
    public Candle(long timeStamp, decimal open, decimal high, decimal low, decimal close, decimal volume=0)
    {
        TimeStamp = timeStamp;
        Open = open;
        High = high;
        Low = low;
        Close = close;
        Volume = volume;
    }

    public Candle()
    {
    }

    [DataMember(Order = 1)] public long TimeStamp { get; set; }
    [DataMember(Order = 2)] public decimal Open { get; set; }
    [DataMember(Order = 3)] public decimal High { get; set; }
    [DataMember(Order = 4)] public decimal Low { get; set; }
    [DataMember(Order = 5)] public decimal Close { get; set; }
    [DataMember(Order = 6)] public decimal Volume { get; set; }
}