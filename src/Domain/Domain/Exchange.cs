﻿using System.Collections.Generic;

namespace Domain;

public enum Exchange
{
    BinanceSpot = 0,
    BinanceSpotTest = 4,
    BinanceFutures = 3,
    BinanceFuturesTest = 5,
    Poloniex = 1,
}

public static class ConnectorExtensions
{
    private static readonly Dictionary<Exchange, ConnectorProperties> СonnectorProperties = new()
    {
        {Exchange.BinanceSpot, new(true, false)},
        {Exchange.BinanceFutures, new(false, false)},
        {Exchange.BinanceSpotTest, new(true, true)},
        {Exchange.BinanceFuturesTest, new(false, true)},
        {Exchange.Poloniex, new(true, false)},
    };

    public static bool IsSpot(this Exchange connector) => СonnectorProperties[connector].IsSpot;
    public static bool IsFutures(this Exchange connector) => !connector.IsSpot();
    public static bool IsTestApi(this Exchange connector) => СonnectorProperties[connector].IsTestApi;

    private record ConnectorProperties(bool IsSpot, bool IsTestApi);
}