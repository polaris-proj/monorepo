﻿using System;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using Domain.Connectors;

namespace Domain;

[DataContract]
public record Pair
{
    [JsonConstructor]
    public Pair(Ticker first, Ticker second)
    {
        if (first == second)
        {
            throw new ArgumentException("Tickers should be different");
        }

        First = first;
        Second = second;
    }

    public Pair(string pair)
    {
        var t = pair.Split('/');
        if (t.Length != 2)
        {
            throw new ArgumentException("Invalid pair");
        }

        First = t[0];
        Second = t[1];
    }

    public static implicit operator string(Pair pair)
    {
        return pair.First + "/" + pair.Second;
    }

    public override string ToString()
    {
        return First + '/' + Second;
    }

    public string ToStringWithOutSlash()
    {
        return First + Second;
    }

    [DataMember(Order = 1)] public Ticker First { get; }
    [DataMember(Order = 2)] public Ticker Second { get; }

    #pragma warning disable CS8618
    private Pair() { }//for protobuf don't delete
}