﻿namespace Domain.StrategyProcessor;

public enum StatusOfRun
{
    Created = 0,
    Running = 1,
    Completed = 2,
    Stopped = 3,
    Failed = 4
}