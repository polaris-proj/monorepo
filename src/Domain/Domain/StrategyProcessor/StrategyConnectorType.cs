﻿namespace Domain.StrategyProcessor;

public enum StrategyConnectorType
{
    Spot = 0,
    Futures = 1
}