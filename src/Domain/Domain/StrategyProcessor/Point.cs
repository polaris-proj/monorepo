﻿using System.Runtime.Serialization;

namespace Domain.StrategyProcessor;

[DataContract]
public class Point
{
    public Point(long timeStamp, decimal value)
    {
        TimeStamp = timeStamp;
        Value = value;
    }

    [DataMember(Order = 1)] public long TimeStamp { get; set; }
    [DataMember(Order = 2)] public decimal Value { get; set; }

    private Point() { }//for protobuf don't delete
}