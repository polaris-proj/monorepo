﻿namespace Domain.StrategyProcessor;

public enum OrderType
{
    Buy,
    Sell,
    Long,
    Short,
    BuyLimit,
    SellLimit
}

public enum MarketOrder
{
    Buy,
    Sell
}

public enum LimitOrder
{
    BuyLimit,
    SellLimit
}

public enum FuturesOrder
{
    Long,
    Short,
}

public enum DealStatus
{
    Open,
    Close,
    Created,
    Liquidated,
}