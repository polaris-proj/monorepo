﻿using Domain.Connectors;

namespace Domain.StrategyProcessor;

//todo сделать так, чтоб работало
public class AutoSyncOrder : Order
{
    public Pair Pair { get; }

    public new OrderStatus OrderStatus
    {
        get
        {
            UpdateState();
            return _orderStatus;
        }
        private set => _orderStatus = value;
    }

    public new decimal Price
    {
        get
        {
            UpdateState();
            return _price;
        }
        private set => _price = value;
    }

    public new decimal FilledAmount
    {
        get
        {
            UpdateState();
            return _filledAmount;
        }
        private set => _filledAmount = value;
    }


    public AutoSyncOrder(IConnector connector, OrderId id, Pair pair, OrderType type, decimal price, decimal amount,
        UpdatePolicy updatePolicy = UpdatePolicy.LimitRps) : base(price, amount)
    {
        _connector = connector;
        _id = id;
        _updatePolicy = updatePolicy;
        Pair = pair;
        UpdateState();

        OrderType = type;
        Price = price;
        Amount = amount;
        _orderStatus = OrderStatus.Open;
    }

    private void UpdateState()
    {
        var info = _connector.GetOrderInfo(Pair, _id).GetAwaiter().GetResult();
        Price = info.Price;
        _orderStatus = info.OrderStatus;
        FilledAmount = info.FilledAmount;
    }


    private OrderStatus _orderStatus;
    private decimal _price;
    private decimal _filledAmount;
    private readonly IConnector _connector;
    private readonly UpdatePolicy _updatePolicy;
    private readonly OrderId _id;
}

public enum UpdatePolicy
{
    LimitRps
}