﻿using System.Runtime.Serialization;

namespace Domain.StrategyProcessor;

[DataContract]
public record ExchangePairTimeFrame
{
    [DataMember(Order = 1)] public Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public Pair Pair { get; set; }
    [DataMember(Order = 3)] public TimeFrame TimeFrame { get; set; }

    public ExchangePairTimeFrame(Exchange exchange, Pair pair, TimeFrame timeFrame)
    {
        Exchange = exchange;
        Pair = pair;
        TimeFrame = timeFrame;
    }

#pragma warning disable CS8618
    private ExchangePairTimeFrame() { } // for protobuf don't delete
}