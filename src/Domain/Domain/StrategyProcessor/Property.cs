﻿using System;

namespace Domain.StrategyProcessor;

public class Property
{
    public Property(Type type, object value)
    {
        Type = type;
        Value = value;
    }

    public Type Type { get; set; }
    public object Value { get; set; }
}