﻿namespace Domain.StrategyProcessor;

public enum Icon
{
    Default = 0,
    Buy = 1,
    Sell = 2,
}