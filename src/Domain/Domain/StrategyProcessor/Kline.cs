﻿namespace Domain.StrategyProcessor;

public record Kline(Pair Pair, TimeFrame TimeFrame, long TimeStamp, decimal Open, decimal High, decimal Low,
    decimal Close, decimal Volume) : Candle(TimeStamp, Open, High, Low, Close, Volume);