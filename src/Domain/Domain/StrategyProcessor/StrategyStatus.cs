﻿namespace Domain.StrategyProcessor;

public enum StrategyStatus
{
    Created,
    Opened,
    Closed
}