﻿using System;

namespace Domain.StrategyProcessor;

[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
public class StrategyParameterAttribute : Attribute
{
    public StrategyParameterAttribute(string name)
    {
        Name = name;
    }

    public string Name { get; set; }
}