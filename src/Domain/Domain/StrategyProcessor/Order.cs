﻿using System;
using System.Runtime.Serialization;
using Domain.Connectors;

namespace Domain.StrategyProcessor;

[DataContract]
public class Order
{
    public Order(DateTime createTime, DateTime? updateTime, decimal price, decimal amount, decimal filledAmount,
        OrderType orderType, OrderStatus orderStatus)
    {
        CreateTime = createTime;
        UpdateTime = updateTime;
        Price = price;
        Amount = amount;
        FilledAmount = filledAmount;
        OrderType = orderType;
        OrderStatus = orderStatus;
    }

    [DataMember(Order = 1)] public DateTime CreateTime { get; set; }
    [DataMember(Order = 2)] public DateTime? UpdateTime { get; set; }
    [DataMember(Order = 3)] public decimal Price { get; set; }
    [DataMember(Order = 4)] public decimal Amount { get; set; }
    [DataMember(Order = 5)] public decimal FilledAmount { get; set; }
    [DataMember(Order = 6)] public OrderType OrderType { get; set; }
    [DataMember(Order = 7)] public OrderStatus OrderStatus { get; set; }
    [DataMember(Order = 8)] public OrderId OrderId { get; set; } = new(null);


    public Order(decimal price, decimal amount)
    {
        Price = price;
        Amount = amount;
    }
    //нафига нужен такой конструктор?

    public Order(decimal price, decimal amount, OrderType orderType, OrderStatus orderStatus = OrderStatus.Open)
    {
        Price = price;
        Amount = amount;
        OrderType = orderType;
        OrderStatus = orderStatus;
    }

    public Order(decimal price, decimal baseAmount, decimal quoteAmount, OrderType orderType,
        OrderStatus orderStatus = OrderStatus.Open)
    {
        Price = price;
        Amount = baseAmount;
        FilledAmount = quoteAmount;
        OrderType = orderType;
        OrderStatus = orderStatus;
    }

    public override string ToString()
    {
        return $"{OrderType} {Amount} coins for {Price} every, ({OrderStatus}) ({FilledAmount}/{Amount})";
    }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private Order(){} //for protobuf don't delete
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
}