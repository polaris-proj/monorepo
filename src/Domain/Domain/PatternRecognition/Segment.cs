﻿using Domain.StrategyProcessor;

namespace Domain.PatternRecognition;

public class Segment
{
    public Point FirstPoint;
    public Point SecondPoint;

    public Segment(long firstTimeStamp, decimal firstPrice, long secondTimeStamp, decimal secondPrice)
    {
        FirstPoint = new Point(firstTimeStamp, firstPrice);
        SecondPoint = new Point(secondTimeStamp, secondPrice);
    }

    public Segment(Point firstPoint, Point secondPoint)
    {
        FirstPoint = firstPoint;
        SecondPoint = secondPoint;
    }
}