﻿namespace Domain.PatternRecognition;

public class Accumulation
{
    //public double VolumeLevel = 0;
    public long StartTimeStamp { get; set; }
    public decimal LowPrice { get; set; }
    public long EndTimeStamp { get; set; }
    public decimal HighPrice { get; set; }
    public AccumulationType Type { get; set; }
}

public enum AccumulationType
{
    Rectangle,
    Triangle,
    Сonstriction,
    CompressionUp,
    CompressionDown
}