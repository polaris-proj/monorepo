﻿using System.Runtime.Serialization;

namespace Domain.Patterns.Objects;

[DataContract]
public class NewCandleEvent : ITradeEvent
{
    public NewCandleEvent(Candle candle, Pair pair, TimeFrame timeFrame, Exchange exchange, long timeStamp)
    {
        Candle = candle;
        Pair = pair;
        TimeFrame = timeFrame;
        Exchange = exchange;
        TimeStamp = timeStamp;
    }

    [DataMember(Order = 1)] public Candle Candle { get; set; }
    [DataMember(Order = 2)] public Pair Pair { get; set; }
    [DataMember(Order = 3)] public TimeFrame TimeFrame { get; set; }
    [DataMember(Order = 4)] public Exchange Exchange { get; set; }
    [DataMember(Order = 5)] public long TimeStamp { get; set; }
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private NewCandleEvent()//for protobuf don't delete
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    {
    }
}