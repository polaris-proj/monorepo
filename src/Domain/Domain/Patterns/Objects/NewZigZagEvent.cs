﻿namespace Domain.Patterns.Objects;

public class NewZigZagEvent : ITradeEvent
{
    public decimal Value { get; set; }

    //public MovingAverage.Type type;
    public required Pair Pair { get; set; }
    public TimeFrame TimeFrame { get; set; }
    public Exchange Exchange { get; set; }
    public long TimeStamp { get; set; }
}