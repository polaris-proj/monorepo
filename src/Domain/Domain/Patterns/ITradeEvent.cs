﻿namespace Domain.Patterns;

public interface ITradeEvent
{
    Exchange Exchange { get; }
    Pair Pair { get; }
    TimeFrame TimeFrame { get; }
    long TimeStamp { get; }
}