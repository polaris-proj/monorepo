﻿using Domain.StrategyProcessor;

namespace Domain.Patterns;

public static class ExchangePairTimeFrameMapper
{
    public static ExchangePairTimeFrame ToExchangePairTimeframe(this ITradeEvent tradeEvent) =>
        new(tradeEvent.Exchange, tradeEvent.Pair, tradeEvent.TimeFrame);
}