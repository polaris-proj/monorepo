﻿using System.Threading.Tasks;
using Domain.StrategyProcessor;

namespace Domain.Connectors;

public interface IFuturesConnector : IConnector
{
    public Task<OrderId> CreateFuturesOrder(Pair pair, FuturesOrder orderType, int leverage, PlacementOrder entry,
        PlacementOrder? take = null, PlacementOrder? stop = null);
}