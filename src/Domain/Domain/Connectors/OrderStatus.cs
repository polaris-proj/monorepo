﻿namespace Domain.Connectors;

public enum OrderStatus
{
    Open,
    PartiallyFilled,
    Close,
    Created
}