﻿using System.Threading.Tasks;
using Domain.StrategyProcessor;

namespace Domain.Connectors;

public interface ISpotConnector : IConnector
{
    Task<OrderId> CreateMarketOrder(Pair pair, MarketOrder orderType, decimal amount);

    Task<OrderId> CreateLimitOrder(Pair pair, LimitOrder orderType, PlacementOrder entry);
}