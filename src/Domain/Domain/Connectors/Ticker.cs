﻿using System.Runtime.Serialization;

namespace Domain.Connectors;

[DataContract]
public record Ticker
{
    public Ticker(string Name) => this.Name = Name;

    [DataMember(Order = 1)] public string Name { get; set; }

    public static implicit operator string(Ticker pair)
    {
        return pair.Name;
    }

    public static implicit operator Ticker(string pairName)
    {
        return new(pairName);
    }
    
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private Ticker() { } // for protobuf dont delete
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

}