﻿using System.Runtime.Serialization;

namespace Domain.Connectors;

/// <summary>
/// Id ордера на бирже
/// может быть <c>null</c>, если Id не существует
/// </summary>
[DataContract]
public class OrderId
{
    public OrderId(string? id)
    {
        Id = id;
    }

    [DataMember(Order = 1)] public string? Id { get; set; }
    
    private OrderId(){}//for protobuf don't delete
}