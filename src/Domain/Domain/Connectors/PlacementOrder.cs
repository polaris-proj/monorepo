﻿using System.Runtime.Serialization;

namespace Domain.Connectors;

[DataContract]
public record PlacementOrder
{
    public PlacementOrder(decimal Price, decimal Amount)
    {
        this.Price = Price;
        this.Amount = Amount;
    }

    [DataMember(Order = 1)] public decimal Price { get; set; }
    [DataMember(Order = 2)] public decimal Amount { get; set; }

    private PlacementOrder() { } //for protobuf don't delete
}