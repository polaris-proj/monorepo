﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.StrategyProcessor;

namespace Domain.Connectors;

public interface IConnector : IAsyncDisposable
{
    Task<Dictionary<Pair, decimal>> GetPrices();
    Task<IReadOnlyList<Balance>> GetCoinsAmount();
    Task<IReadOnlyList<Candle>> GetCandles(Pair pair, TimeFrame timeFrame, DateTimeRange range);

    Task<IReadOnlyList<Order>> GetOrdersPerPair(Pair pair);
    
    Task<Order> GetOrderInfo(Pair pair, OrderId id);
    Task CancelOrder(Pair pair, OrderId orderId);
}