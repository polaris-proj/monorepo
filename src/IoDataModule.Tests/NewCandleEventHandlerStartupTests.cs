﻿using System.ComponentModel.DataAnnotations;
using Domain;
using Domain.StrategyProcessor;
using ExchangeConnectors.Connectors;
using Infrastructure;
using Infrastructure.RTQConnector;
using Infrastructure.Tests;
using IODataModule.Services;
using IODataModule.Services.ExchangeConnectors;
using Microsoft.Extensions.Logging;

namespace IoDataModule.Tests
{
    [TestFixture]
    public class NewCandleEventHandlerStartupTests
    {
        public NewCandleEventHandlerStartupTests()
        {
            var vault = new Mock<IVault>();
            vault.Setup(x => x.GetValue(It.IsAny<string>())).Returns(string.Empty);
            
            var kafkaParameters = new KafkaParameters(vault.Object);

            _connector = new Mock<IInternalSpotConnector>();
            _connectorFactory = new Mock<IConnectorFactory>();
            _connectorFactory.Setup(x => x.GetSpotConnectorAsync(It.IsAny<Guid>(), It.IsAny<Exchange>()))
                .ReturnsAsync(() => _connector.Object);

            _candleEventHandlerStartup = new NewCandleEventHandlerStartup(
                _connectorFactory.Object,
                _loggerFactory,
                kafkaParameters);
        }

        [Test]
        [CustomAutoData]
        public async Task ExecuteAsync_ShouldCreateNewCandleEventHandler(
            [MinLength(3)] (Pair, decimal)[] prices)
        {
            prices = prices.DistinctBy(x => x.Item1).ToArray();
            var userId = Guid.Parse("9b257c73-241c-438e-9d2d-46834afcb244");
            var exchange = Exchange.BinanceSpot;
            _connector.Setup(x => x.GetPrices()).ReturnsAsync(() => prices.ToDictionary(k => k.Item1, v => v.Item2));

            var cts = new CancellationTokenSource(TimeSpan.FromMilliseconds(1000));
            await _candleEventHandlerStartup.StartAsync(cts.Token);

            _connectorFactory.Verify(x => x.GetSpotConnectorAsync(userId, exchange), Times.Once);
            _connector.Verify(x => x.GetPrices(), Times.Once);
            _connector.Verify(
                x => x.SubscribeOnNewKlines(It.IsAny<IReadOnlyList<Pair>>(), It.IsAny<IReadOnlyList<TimeFrame>>(),
                    It.IsAny<Action<Kline>>()), Times.Once);
        }

        [Test]
        public async Task StopAsync_ShouldStopNewCandleEventHandlers()
        {
            await _candleEventHandlerStartup.StopAsync(CancellationToken.None);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            _loggerFactory.Dispose();
            _candleEventHandlerStartup.Dispose();
        }

        private readonly LoggerFactory _loggerFactory = new();
        private readonly Mock<IConnectorFactory> _connectorFactory;
        private readonly NewCandleEventHandlerStartup _candleEventHandlerStartup;
        private readonly Mock<IInternalSpotConnector> _connector;
    }
}