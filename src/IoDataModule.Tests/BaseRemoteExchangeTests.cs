using Domain;
using Domain.Connectors;
using IODataModule.Services.ExchangeConnectors;

namespace IoDataModule.Tests;

public class BaseRemoteExchangeTests
{
    [Test]
    public async Task GetCoinsAmount_ShouldReturnCoinsAmount()
    {
        var connector = new Mock<IConnector>();
        var funcInvokeAmount = 0;
        var exchange = Exchange.BinanceSpot;
        var userId = Guid.NewGuid();
        var connectorSelector = (Guid usId, Exchange ex) =>
        {
            funcInvokeAmount++;
            exchange.Should().Be(ex);
            usId.Should().Be(userId);
            return Task.FromResult(connector.Object);
        };
        var baseRemoteExchange = new TestRemoteExchange(connectorSelector);

        await baseRemoteExchange.GetCoinsAmount(exchange, userId);

        funcInvokeAmount.Should().Be(1);
        connector.Verify(x => x.GetCoinsAmount(), Times.Once);
    }

    [Test]
    public async Task GetPrices_ShouldReturnPrices()
    {
        var connector = new Mock<IConnector>();
        var funcInvokeAmount = 0;
        var exchange = Exchange.BinanceSpot;
        var userId = Guid.NewGuid();
        var connectorSelector = (Guid usId, Exchange ex) =>
        {
            funcInvokeAmount++;
            exchange.Should().Be(ex);
            usId.Should().Be(userId);
            return Task.FromResult(connector.Object);
        };
        var baseRemoteExchange = new TestRemoteExchange(connectorSelector);

        await baseRemoteExchange.GetPrices(exchange, userId);

        connector.Verify(x => x.GetPrices(), Times.Once);
        funcInvokeAmount.Should().Be(1);
    }

    [Test]
    public async Task GetCandles_ShouldReturnCandles()
    {
        var connector = new Mock<IConnector>();
        var funcInvokeAmount = 0;
        var exchange = Exchange.BinanceSpot;
        var userId = Guid.NewGuid();
        var connectorSelector = (Guid usId, Exchange ex) =>
        {
            funcInvokeAmount++;
            exchange.Should().Be(ex);
            usId.Should().Be(userId);
            return Task.FromResult(connector.Object);
        };
        var baseRemoteExchange = new TestRemoteExchange(connectorSelector);

        var pair = new Pair("BTC", "USDT");
        var timeFrame = TimeFrame.h1;
        var range = new DateTimeRange(DateTime.Now.AddHours(-1), DateTime.Now);

        await baseRemoteExchange.GetCandles(exchange, userId, pair, timeFrame, range);

        funcInvokeAmount.Should().Be(1);
        connector.Verify(x => x.GetCandles(pair, timeFrame, range), Times.Once);
    }

    [Test]
    public async Task GetCurrentOrdersPerPair_ShouldReturnCurrentOrdersPerPair()
    {
        var connector = new Mock<IConnector>();
        var funcInvokeAmount = 0;
        var exchange = Exchange.BinanceSpot;
        var userId = Guid.NewGuid();
        var connectorSelector = (Guid usId, Exchange ex) =>
        {
            funcInvokeAmount++;
            exchange.Should().Be(ex);
            usId.Should().Be(userId);
            return Task.FromResult(connector.Object);
        };
        var baseRemoteExchange = new TestRemoteExchange(connectorSelector);

        var pair = new Pair("BTC", "USDT");

        await baseRemoteExchange.GetCurrentOrdersPerPair(exchange, userId, pair);

        funcInvokeAmount.Should().Be(1);
        connector.Verify(x => x.GetOrdersPerPair(pair), Times.Once);
    }

    [Test]
    public async Task GetOrderInfo_ShouldReturnOrderInfo()
    {
        var connector = new Mock<IConnector>();
        var funcInvokeAmount = 0;
        var exchange = Exchange.BinanceSpot;
        var userId = Guid.NewGuid();
        var connectorSelector = (Guid usId, Exchange ex) =>
        {
            funcInvokeAmount++;
            exchange.Should().Be(ex);
            usId.Should().Be(userId);
            return Task.FromResult(connector.Object);
        };
        var baseRemoteExchange = new TestRemoteExchange(connectorSelector);

        var pair = new Pair("BTC", "USDT");
        var orderId = new OrderId("12345");

        await baseRemoteExchange.GetOrderInfo(exchange, userId, pair, orderId);

        funcInvokeAmount.Should().Be(1);
        connector.Verify(x => x.GetOrderInfo(pair, orderId), Times.Once);
    }

    [Test]
    public async Task CancelOrder_ShouldNotThrowException()
    {
        var connector = new Mock<IConnector>();
        var funcInvokeAmount = 0;
        var exchange = Exchange.BinanceSpot;
        var userId = Guid.NewGuid();
        var connectorSelector = (Guid usId, Exchange ex) =>
        {
            funcInvokeAmount++;
            exchange.Should().Be(ex);
            usId.Should().Be(userId);
            return Task.FromResult(connector.Object);
        };
        var baseRemoteExchange = new TestRemoteExchange(connectorSelector);

        var pair = new Pair("BTC", "USDT");
        var orderId = new OrderId("12345");

        var action = async () => { await baseRemoteExchange.CancelOrder(exchange, userId, pair, orderId); };

        await action.Should().NotThrowAsync();
        funcInvokeAmount.Should().Be(1);
        connector.Verify(x => x.CancelOrder(pair, orderId), Times.Once);
    }

    private class TestRemoteExchange(Func<Guid, Exchange, Task<IConnector>> getConnector)
        : BaseRemoteExchange(getConnector);
}