﻿using AutoFixture;
using Domain;
using ExchangeConnectors.Connectors;
using Infrastructure.Extensions;
using Infrastructure.Tests;
using IODataModule.Infrastructure.DAL.Models;
using IODataModule.Infrastructure.DAL.Repositories;
using IODataModule.Infrastructure.Mappers;
using IODataModule.Services;
using IODataModule.Services.ExchangeConnectors;
using ReadOnlyCollectionsExtensions;

namespace IoDataModule.Tests;

//todo futures тоже бы потестить
public class CandleAccessServiceTests
{
    [Test]
    [CustomAutoData]
    public async Task GetRange_NoCandlesInDb_ShouldSaveToDb(Pair pair, TimeFrame timeFrame)
    {
        var startTime = new DateTime(2020, 10, 1).ToMilliseconds();
        var expectedCandles = GetCandles(timeFrame, startTime);
        var candleRepository = MockCandleRepository(Array.Empty<Candle>());
        var connector = MockInternalSpotConnector(expectedCandles);
        var connectorFactory = new Mock<IConnectorFactory>();
        connectorFactory.Setup(x => x.GetSpotConnectorAsync(It.IsAny<Guid>(), It.IsAny<Exchange>()))
            .ReturnsAsync(() => connector.Object);

        var candleAccessService = new CandleAccessService(candleRepository.Object, connectorFactory.Object);
        var maxTime = expectedCandles.MaxBy(y => y.TimeStamp)!.TimeStamp;
        var candles = await candleAccessService.GetRange(Guid.NewGuid(), Exchange.BinanceSpot, pair, timeFrame,
            startTime.ToDateTime(),
            maxTime.ToDateTime()
        );

        candleRepository.Verify(x => x.GetRangeStream(
            It.Is<(Exchange, Pair, TimeFrame)>(p => p.Item2 == pair && p.Item3 == timeFrame),
            startTime,
            maxTime,
            It.IsAny<CancellationToken>()
        ), Times.Once);

        connector.Verify(x => x.GetCandles(It.IsAny<Pair>(), It.IsAny<TimeFrame>(), It.IsAny<DateTimeRange>()),
            Times.Once);

        candleRepository.Verify(x => x.SaveCandles(It.IsAny<Exchange>(), It.IsAny<Pair>(),
            It.IsAny<IEnumerable<CandleDbo>>(),
            It.IsAny<CancellationToken>()), Times.Once);

        candles.Should().BeEquivalentTo(expectedCandles);
    }

    [Test]
    [CustomAutoData]
    public async Task GetRange_AllCandlesInDb_ShouldNotCallConnector(Pair pair, TimeFrame timeFrame)
    {
        var startTime = new DateTime(2020, 10, 1).ToMilliseconds();
        var expectedCandles = GetCandles(timeFrame, startTime);
        var candleRepository = MockCandleRepository(expectedCandles);
        var connector = new Mock<IInternalSpotConnector>();
        var connectorFactory = new Mock<IConnectorFactory>();
        connectorFactory.Setup(x => x.GetSpotConnectorAsync(It.IsAny<Guid>(), It.IsAny<Exchange>()))
            .ReturnsAsync(() => connector.Object);

        var candleAccessService = new CandleAccessService(candleRepository.Object, connectorFactory.Object);
        var maxTime = expectedCandles.MaxBy(y => y.TimeStamp)!.TimeStamp;
        var candles = await candleAccessService.GetRange(Guid.NewGuid(), Exchange.BinanceSpot, pair, timeFrame,
            startTime.ToDateTime(),
            maxTime.ToDateTime()
        );

        connector.Verify(x => x.GetCandles(It.IsAny<Pair>(), It.IsAny<TimeFrame>(), It.IsAny<DateTimeRange>()),
            Times.Never);

        candleRepository.Verify(x => x.GetRangeStream(
            It.Is<(Exchange, Pair, TimeFrame)>(p => p.Item2 == pair && p.Item3 == timeFrame),
            startTime,
            maxTime,
            It.IsAny<CancellationToken>()
        ), Times.Once);

        candles.Should().BeEquivalentTo(expectedCandles);
    }

    [Test]
    [CustomAutoData]
    public async Task GetRange_CentralPartOfCandlesInDbButNotAll_ShouldCallConnectorGorExtraCandlesAndAddNewCandlesToDb(
        Pair pair, TimeFrame timeFrame)
    {
        var startTime = new DateTime(2020, 10, 1).ToMilliseconds();
        var expectedCandles = GetCandles(timeFrame, startTime);
        var candlesInDb = expectedCandles[33..66];
        var candleRepository = MockCandleRepository(candlesInDb);
        var connector = MockInternalSpotConnector(expectedCandles);
        var connectorFactory = new Mock<IConnectorFactory>();
        connectorFactory.Setup(x => x.GetSpotConnectorAsync(It.IsAny<Guid>(), It.IsAny<Exchange>()))
            .ReturnsAsync(() => connector.Object);


        var candleAccessService = new CandleAccessService(candleRepository.Object, connectorFactory.Object);
        var maxTime = expectedCandles.MaxBy(y => y.TimeStamp)!.TimeStamp;
        var candles = await candleAccessService.GetRange(Guid.NewGuid(), Exchange.BinanceSpot, pair, timeFrame,
            startTime.ToDateTime(),
            maxTime.ToDateTime()
        );


        connector.Verify(x => x.GetCandles(It.IsAny<Pair>(), It.IsAny<TimeFrame>(), It.IsAny<DateTimeRange>()),
            Times.Exactly(2));

        candleRepository.Verify(x => x.GetRangeStream(
            It.Is<(Exchange, Pair, TimeFrame)>(p => p.Item2 == pair && p.Item3 == timeFrame),
            startTime,
            maxTime,
            It.IsAny<CancellationToken>()
        ), Times.Once);

        var firstPartOfCandles = expectedCandles[..33].Select(c => c.ToDbo(timeFrame));
        candleRepository.Verify(
            x => x.SaveCandles(It.IsAny<Exchange>(), It.IsAny<Pair>(), firstPartOfCandles,
                It.IsAny<CancellationToken>()),
            Times.Once);

        var secondPartOfCandles = expectedCandles[66..].Select(c => c.ToDbo(timeFrame));
        candleRepository.Verify(
            x => x.SaveCandles(It.IsAny<Exchange>(), It.IsAny<Pair>(), secondPartOfCandles,
                It.IsAny<CancellationToken>()),
            Times.Once);

        candleRepository.Verify(
            x => x.SaveCandles(It.IsAny<Exchange>(), It.IsAny<Pair>(), It.IsAny<IEnumerable<CandleDbo>>(),
                It.IsAny<CancellationToken>()),
            Times.Exactly(2));

        candles.Should().BeEquivalentTo(expectedCandles);
    }

    [Test]
    [CustomAutoData]
    public async Task GetRange_LeftPartOfCandlesInDbButNotAll_ShouldCallConnectorGorExtraCandlesAndAddNewCandlesToDb(
        Pair pair, TimeFrame timeFrame)
    {
        var startTime = new DateTime(2020, 10, 1).ToMilliseconds();
        var expectedCandles = GetCandles(timeFrame, startTime);
        var candlesInDb = expectedCandles[..66];
        var candleRepository = MockCandleRepository(candlesInDb);
        var connector = MockInternalSpotConnector(expectedCandles);

        var connectorFactory = new Mock<IConnectorFactory>();
        connectorFactory.Setup(x => x.GetSpotConnectorAsync(It.IsAny<Guid>(), It.IsAny<Exchange>()))
            .ReturnsAsync(() => connector.Object);


        var candleAccessService = new CandleAccessService(candleRepository.Object, connectorFactory.Object);
        var maxTime = expectedCandles.MaxBy(y => y.TimeStamp)!.TimeStamp;
        var candles = await candleAccessService.GetRange(Guid.NewGuid(), Exchange.BinanceSpot, pair, timeFrame,
            startTime.ToDateTime(),
            maxTime.ToDateTime()
        );


        connector.Verify(x => x.GetCandles(It.IsAny<Pair>(), It.IsAny<TimeFrame>(), It.IsAny<DateTimeRange>()),
            Times.Exactly(1));

        candleRepository.Verify(x => x.GetRangeStream(
            It.Is<(Exchange, Pair, TimeFrame)>(p => p.Item2 == pair && p.Item3 == timeFrame),
            startTime,
            maxTime,
            It.IsAny<CancellationToken>()
        ), Times.Once);

        var secondPartOfCandles = expectedCandles[66..].Select(c => c.ToDbo(timeFrame));
        candleRepository.Verify(
            x => x.SaveCandles(It.IsAny<Exchange>(), It.IsAny<Pair>(), secondPartOfCandles,
                It.IsAny<CancellationToken>()),
            Times.Once);

        candleRepository.Verify(
            x => x.SaveCandles(It.IsAny<Exchange>(), It.IsAny<Pair>(), It.IsAny<IEnumerable<CandleDbo>>(),
                It.IsAny<CancellationToken>()),
            Times.Exactly(1));

        candles.Should().BeEquivalentTo(expectedCandles);
    }

    [Test]
    [CustomAutoData]
    public async Task GetRangeStream_AllDataInDatabase_ShouldNotCallConnector(Pair pair, TimeFrame timeFrame)
    {
        var startTime = new DateTime(2020, 10, 1).ToMilliseconds();
        var expectedCandles = GetCandles(timeFrame, startTime);
        var candleRepository = MockCandleRepository(expectedCandles);
        var connector = new Mock<IInternalSpotConnector>();
        var connectorFactory = new Mock<IConnectorFactory>();
        connectorFactory.Setup(x => x.GetSpotConnectorAsync(It.IsAny<Guid>(), It.IsAny<Exchange>()))
            .ReturnsAsync(() => connector.Object);

        var candleAccessService = new CandleAccessService(candleRepository.Object, connectorFactory.Object);
        var maxTime = expectedCandles.MaxBy(y => y.TimeStamp)!.TimeStamp;
        var candles = await candleAccessService.GetRangeStream(Guid.NewGuid(), Exchange.BinanceSpot, pair, timeFrame,
            startTime.ToDateTime(),
            maxTime.ToDateTime()
        ).ToListAsync();

        connector.Verify(x => x.GetCandles(It.IsAny<Pair>(), It.IsAny<TimeFrame>(), It.IsAny<DateTimeRange>()),
            Times.Never);

        candleRepository.Verify(x => x.GetRangeStream(
            It.Is<(Exchange, Pair, TimeFrame)>(p => p.Item2 == pair && p.Item3 == timeFrame),
            It.IsAny<long>(),
            It.IsAny<long>(),
            It.IsAny<CancellationToken>()
        ), Times.Once);

        candles.Should().BeEquivalentTo(expectedCandles);
    }

    [Test]
    [CustomAutoData]
    public async Task GetRangeStream_DatabaseHaveSomeData_ShouldCallConnectorGorExtraCandlesAndAddNewCandlesToDb(
        Pair pair, TimeFrame timeFrame)
    {
        var startTime = new DateTime(2020, 10, 1).ToMilliseconds();
        var expectedCandles = GetCandles(timeFrame, startTime);
        var candlesInDb = expectedCandles[..66];
        var candleRepository = MockCandleRepository(candlesInDb, true);
        var connector = MockInternalSpotConnector(expectedCandles);
        var connectorFactory = new Mock<IConnectorFactory>();
        connectorFactory.Setup(x => x.GetSpotConnectorAsync(It.IsAny<Guid>(), It.IsAny<Exchange>()))
            .ReturnsAsync(() => connector.Object);


        var candleAccessService = new CandleAccessService(candleRepository.Object, connectorFactory.Object);
        var maxTime = expectedCandles.MaxBy(y => y.TimeStamp)!.TimeStamp;
        var candles = await candleAccessService.GetRangeStream(Guid.NewGuid(), Exchange.BinanceSpot, pair, timeFrame,
            startTime.ToDateTime(),
            maxTime.ToDateTime()
        ).ToListAsync();

        candleRepository.Verify(x => x.GetRangeStream(
            It.Is<(Exchange, Pair, TimeFrame)>(p => p.Item2 == pair && p.Item3 == timeFrame),
            startTime,
            maxTime,
            It.IsAny<CancellationToken>()
        ), Times.Once);

        candles.Should().BeEquivalentTo(expectedCandles);
    }

    private static Mock<IInternalSpotConnector> MockInternalSpotConnector(Candle[] expectedCandles)
    {
        var connector = new Mock<IInternalSpotConnector>();
        connector.Setup(x => x.GetCandles(It.IsAny<Pair>(), It.IsAny<TimeFrame>(), It.IsAny<DateTimeRange>()))
            .ReturnsAsync((Pair _, TimeFrame _, DateTimeRange range) =>
            {
                return expectedCandles.Where(x =>
                        x.TimeStamp >= range.Begin.ToMilliseconds() && x.TimeStamp <= range.End.ToMilliseconds())
                    .ToReadOnlyList();
            });
        return connector;
    }

    private static Mock<ICandleRepository> MockCandleRepository(Candle[] candlesInDb, bool isOldDataSaved = false)
    {
        var candleRepository = new Mock<ICandleRepository>();
        candleRepository.Setup(x => x.GetRangeStream(It.IsAny<(Exchange, Pair, TimeFrame)>(), It.IsAny<long>(),
                It.IsAny<long>(), It.IsAny<CancellationToken>()))
            .Returns(((Exchange, Pair, TimeFrame) _, long begin, long end, CancellationToken _) =>
            {
                return candlesInDb.Where(x => x.TimeStamp >= begin && x.TimeStamp <= end).ToAsyncEnumerable();
            });

        candleRepository.Setup(x => x.IsOldDataSaved(It.IsAny<Exchange>(), It.IsAny<Pair>(), It.IsAny<TimeFrame>(),
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(() => isOldDataSaved);

        candleRepository.Setup(x => x.GetMaxTimestamp(It.IsAny<Exchange>(), It.IsAny<Pair>(), It.IsAny<TimeFrame>(),
            It.IsAny<CancellationToken>())).ReturnsAsync(() => candlesInDb.MaxBy(x => x.TimeStamp)?.TimeStamp ?? 0);
        return candleRepository;
    }

    private static Candle[] GetCandles(TimeFrame timeFrame, long startTime)
    {
        var fixture = new Fixture();
        var candles = fixture.CreateMany<Candle>(99).ToArray();
        var currentTime = startTime;
        foreach (var candle in candles)
        {
            candle.TimeStamp = currentTime;
            currentTime += (long) timeFrame.GetSeconds() * 1000;
        }

        return candles;
    }
}