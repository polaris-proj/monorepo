from aiogram import Dispatcher

from .resources import ResourcesMiddleware
from .user import TelegramUserMiddleware


def setup(dp: Dispatcher):
    dp.setup_middleware(ResourcesMiddleware())
    dp.setup_middleware(TelegramUserMiddleware())
