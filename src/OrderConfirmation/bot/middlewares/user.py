import logging

from aiogram import types
from aiogram.dispatcher.middlewares import BaseMiddleware
from sqlalchemy.ext.asyncio import AsyncSession

from src.OrderConfirmation.service import user_repository


class TelegramUserMiddleware(BaseMiddleware):
    """
    Middleware for providing a `User` object
    """

    def __init__(self):
        """
        Initializes self
        """

        self._logger = logging.getLogger("users_middleware")

        super().__init__()

    async def _provide_user(self, telegram_id: int, username: str, fullname: str, data: dict) -> dict:
        """
        Fetches and returns user
        """

        if "db_session" not in data:
            raise RuntimeError("AsyncSession not found.")

        db_session: AsyncSession = data.get("db_session")

        data["user"] = await user_repository.get_or_create_telegram_user(
            db_session, telegram_id, user_id=None, username=username, fullname=fullname
        )
        self._logger.debug(f"User middleware: {data['user']}")
        return data

    async def on_pre_process_message(self, message: types.Message, data: dict):
        """
        Method for preprocessing messages (provides user)
        :param message: A message
        :param data: A data from another middleware
        :return: None
        """

        return await self._provide_user(
            message.from_user.id, message.from_user.username, message.from_user.full_name, data
        )

    async def on_pre_process_callback_query(self, query: types.CallbackQuery, data: dict):
        """
        Method for preprocessing callback-queries (provides user)
        :param data:
        :param query:
        :return:
        """

        return await self._provide_user(query.from_user.id, query.from_user.username, query.from_user.full_name, data)
