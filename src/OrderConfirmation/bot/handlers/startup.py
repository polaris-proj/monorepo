import asyncio
import logging

from aiogram import Bot, Dispatcher, types
from grpc import aio
from sqlalchemy_utils import create_database, database_exists

from src.OrderConfirmation.database import engine
from src.OrderConfirmation.grpc_python_server import OrderConfirmation_pb2_grpc
from src.OrderConfirmation.grpc_service.order_confirmation import OrderConfirmation
from src.OrderConfirmation.models.base import Base
from src.OrderConfirmation.settings.db import service_database_settings
from src.OrderConfirmation.settings.log import logging_settings

BOT_COMMANDS = [
    types.BotCommand("start", "Запуск бота"),
]

# bot = None


def configure_logging():
    """
    Configures logging
    """

    level = logging.INFO

    if logging_settings.debug:
        level = logging.DEBUG

    logging.basicConfig(level=level, format=logging_settings.format)


def setup_cache():
    """
    Configures cache
    """
    logging.info("Setting up cache...")
    # cashews.setup("mem://")


async def setup_db():
    """
    Initializes db (creates tables, etc...)
    """
    logging.info("Initializing database...")
    if not database_exists(service_database_settings.postgresql_url):
        create_database(service_database_settings.postgresql_url)

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


async def setup_commands(bot: Bot):
    """
    Configures bot-commands
    """

    logging.info("Setting up commands.")

    await bot.set_my_commands(BOT_COMMANDS)


async def notify_admins(bot: Bot):
    """
    Notifying admins about start
    """
    # for admin_id in settings.bot.admins:
    #     await bot.send_message(admin_id, "Bot started")
    pass


async def serve(bot: Bot):
    server = aio.server()
    OrderConfirmation_pb2_grpc.add_OrderConfirmationServicer_to_server(OrderConfirmation(bot), server)
    listen_addr = "[::]:9997"
    server.add_insecure_port(listen_addr)
    logging.info(f"Starting grpc server on {listen_addr}")
    await server.start()
    await server.wait_for_termination()


async def start_grpc_server(bot: Bot):
    asyncio.create_task(serve(bot))


def startup_handler_factory() -> callable:
    async def inner_func(dp: Dispatcher):
        """
        Startup handler
        """
        configure_logging()

        # setup_cache()
        await setup_db()
        await setup_commands(dp.bot)
        await notify_admins(dp.bot)
        await start_grpc_server(dp.bot)
        logging.info("Starting bot.")

    return inner_func
