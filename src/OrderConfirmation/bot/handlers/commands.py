import logging

from aiogram import Dispatcher, types
from sqlalchemy.ext.asyncio import AsyncSession

from src.OrderConfirmation.models.telegram_user import TelegramUser
from src.OrderConfirmation.service.invite_user_key_repository import delete_user_by_invite_key, get_user_by_invite_key
from src.OrderConfirmation.service.user_repository import update_telegram_user

logger = logging.getLogger(__name__)


async def start(message: types.Message, user: TelegramUser, db_session: AsyncSession):
    """
    Handles `/start` cmd
    """
    if message.text.endswith("/start"):
        return await message.answer("Я тебя не знаю. Получи ссылку через личный кабинет")
    invite_key = message.text.split()[1]

    invite_user_key = await get_user_by_invite_key(db_session, invite_key)
    if invite_user_key is None:
        return await message.answer("Нет такого юзера с таким инвайт ключом")

    await update_telegram_user(db_session, user, user_id=invite_user_key.user_id)
    await delete_user_by_invite_key(db_session, invite_key=invite_key)
    return await message.answer(f"Я тебя запомнил, твой user_id={invite_user_key.user_id}")


def setup(dp: Dispatcher):
    """
    Setups commands-handlers
    """
    dp.register_message_handler(start, commands=["start"])
