import logging

from aiogram import Bot

logger = logging.getLogger(__name__)


async def send_message(bot: Bot, telegram_id: int, text: str):
    logger.debug(f"Send message to {telegram_id}: '{text}'")
    return await bot.send_message(chat_id=telegram_id, text=text)
