from protobuf_net import bcl_pb2 as _bcl_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class ExchangeDto(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    Binance: _ClassVar[ExchangeDto]
    Poloniex: _ClassVar[ExchangeDto]

class MessengerType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    Telegram: _ClassVar[MessengerType]

class OrderConfirmationStatus(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    None: _ClassVar[OrderConfirmationStatus]
    Confirmed: _ClassVar[OrderConfirmationStatus]
    UnConfirmed: _ClassVar[OrderConfirmationStatus]

class OrderType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    Buy: _ClassVar[OrderType]
    Sell: _ClassVar[OrderType]
    Long: _ClassVar[OrderType]
    Short: _ClassVar[OrderType]
    BuyLimit: _ClassVar[OrderType]
    SellLimit: _ClassVar[OrderType]

class Status(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    Error: _ClassVar[Status]
    Ok: _ClassVar[Status]
Binance: ExchangeDto
Poloniex: ExchangeDto
Telegram: MessengerType
None: OrderConfirmationStatus
Confirmed: OrderConfirmationStatus
UnConfirmed: OrderConfirmationStatus
Buy: OrderType
Sell: OrderType
Long: OrderType
Short: OrderType
BuyLimit: OrderType
SellLimit: OrderType
Error: Status
Ok: Status

class Answer(_message.Message):
    __slots__ = ["Status", "Message"]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    MESSAGE_FIELD_NUMBER: _ClassVar[int]
    Status: Status
    Message: str
    def __init__(self, Status: _Optional[_Union[Status, str]] = ..., Message: _Optional[str] = ...) -> None: ...

class GetLinkToTelegramBotArgument(_message.Message):
    __slots__ = ["UserId"]
    USERID_FIELD_NUMBER: _ClassVar[int]
    UserId: str
    def __init__(self, UserId: _Optional[str] = ...) -> None: ...

class Answer_List_MessengerLogin(_message.Message):
    __slots__ = ["Status", "Message", "Value"]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    MESSAGE_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    Status: Status
    Message: str
    Value: _containers.RepeatedCompositeFieldContainer[MessengerLogin]
    def __init__(self, Status: _Optional[_Union[Status, str]] = ..., Message: _Optional[str] = ..., Value: _Optional[_Iterable[_Union[MessengerLogin, _Mapping]]] = ...) -> None: ...

class Answer_List_OrderWithConfirmation(_message.Message):
    __slots__ = ["Status", "Message", "Value"]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    MESSAGE_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    Status: Status
    Message: str
    Value: _containers.RepeatedCompositeFieldContainer[OrderWithConfirmation]
    def __init__(self, Status: _Optional[_Union[Status, str]] = ..., Message: _Optional[str] = ..., Value: _Optional[_Iterable[_Union[OrderWithConfirmation, _Mapping]]] = ...) -> None: ...

class ChangeUserNameArgument(_message.Message):
    __slots__ = ["UserId", "MessengerLogins"]
    USERID_FIELD_NUMBER: _ClassVar[int]
    MESSENGERLOGINS_FIELD_NUMBER: _ClassVar[int]
    UserId: _bcl_pb2.Guid
    MessengerLogins: _containers.RepeatedCompositeFieldContainer[MessengerLogin]
    def __init__(self, UserId: _Optional[_Union[_bcl_pb2.Guid, _Mapping]] = ..., MessengerLogins: _Optional[_Iterable[_Union[MessengerLogin, _Mapping]]] = ...) -> None: ...

class CreateOrderConfirmation(_message.Message):
    __slots__ = ["Exchange", "UserId", "Pair", "OrderType", "Price", "Amount", "Confirmable"]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    USERID_FIELD_NUMBER: _ClassVar[int]
    PAIR_FIELD_NUMBER: _ClassVar[int]
    ORDERTYPE_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    AMOUNT_FIELD_NUMBER: _ClassVar[int]
    CONFIRMABLE_FIELD_NUMBER: _ClassVar[int]
    Exchange: ExchangeDto
    UserId: _bcl_pb2.Guid
    Pair: PairDto
    OrderType: OrderType
    Price: _bcl_pb2.Decimal
    Amount: _bcl_pb2.Decimal
    Confirmable: bool
    def __init__(self, Exchange: _Optional[_Union[ExchangeDto, str]] = ..., UserId: _Optional[_Union[_bcl_pb2.Guid, _Mapping]] = ..., Pair: _Optional[_Union[PairDto, _Mapping]] = ..., OrderType: _Optional[_Union[OrderType, str]] = ..., Price: _Optional[_Union[_bcl_pb2.Decimal, _Mapping]] = ..., Amount: _Optional[_Union[_bcl_pb2.Decimal, _Mapping]] = ..., Confirmable: bool = ...) -> None: ...

class GetOrdersArgument(_message.Message):
    __slots__ = ["StrategyRunId"]
    STRATEGYRUNID_FIELD_NUMBER: _ClassVar[int]
    StrategyRunId: _bcl_pb2.Guid
    def __init__(self, StrategyRunId: _Optional[_Union[_bcl_pb2.Guid, _Mapping]] = ...) -> None: ...

class GetUserNameArgument(_message.Message):
    __slots__ = ["UserId"]
    USERID_FIELD_NUMBER: _ClassVar[int]
    UserId: _bcl_pb2.Guid
    def __init__(self, UserId: _Optional[_Union[_bcl_pb2.Guid, _Mapping]] = ...) -> None: ...

class MessengerLogin(_message.Message):
    __slots__ = ["MessengerType", "Login"]
    MESSENGERTYPE_FIELD_NUMBER: _ClassVar[int]
    LOGIN_FIELD_NUMBER: _ClassVar[int]
    MessengerType: MessengerType
    Login: str
    def __init__(self, MessengerType: _Optional[_Union[MessengerType, str]] = ..., Login: _Optional[str] = ...) -> None: ...

class OrderWithConfirmation(_message.Message):
    __slots__ = ["Order", "Status"]
    ORDER_FIELD_NUMBER: _ClassVar[int]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    Order: CreateOrderConfirmation
    Status: OrderConfirmationStatus
    def __init__(self, Order: _Optional[_Union[CreateOrderConfirmation, _Mapping]] = ..., Status: _Optional[_Union[OrderConfirmationStatus, str]] = ...) -> None: ...

class PairDto(_message.Message):
    __slots__ = ["Pair"]
    PAIR_FIELD_NUMBER: _ClassVar[int]
    Pair: str
    def __init__(self, Pair: _Optional[str] = ...) -> None: ...

class SendMessageArgument(_message.Message):
    __slots__ = ["Message"]
    MESSAGE_FIELD_NUMBER: _ClassVar[int]
    Message: str
    def __init__(self, Message: _Optional[str] = ...) -> None: ...
