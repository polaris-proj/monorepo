import hashlib
import uuid

from src.OrderConfirmation.settings.salt import salt_settings
from src.OrderConfirmation.settings.telegram import telegram_settings


def get_invite_key(guid: str) -> str:
    return hashlib.sha224(
        guid.encode("utf-8") + str(uuid.uuid4()).encode() + salt_settings.guid.encode("utf-8")
    ).hexdigest()


def get_invite_link_by_key(invite_key: str) -> str:
    return f"tg://resolve?domain={telegram_settings.nickname}&start={invite_key}"


def get_invite_link_by_guid(guid: str) -> str:
    return f"tg://resolve?domain={telegram_settings.nickname}&start={get_invite_key(guid)}"
