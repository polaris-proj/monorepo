﻿using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;
using Grpc.Net.Client;

namespace OrderConfirmation.Client;

public class OrderSenderClient : IOrderSenderClient
{
    public OrderSenderClient(Guid userId, Exchange exchange, Guid runId,
        OrderSenderClientParameters clientParameters, GrpcChannel? channel)
    {
        Address = clientParameters.Address;
        _userId = userId;
        _exchange = exchange;
        _runId = runId;
        _channel = channel;
    }

    public Task<OrderId> CreateMarketOrder(Pair pair, MarketOrder orderType, decimal amount)
    {
        using (var channel = _channel ?? GrpcChannel.ForAddress(Address))
        {
            var client = new OrderConfirmation.OrderConfirmationClient(channel);
            var answer = client.SendMessage(new SendMessageArgument());

            if (answer.Status == Status.Error)
                throw new Exception(answer.Message);

            //return answer.Message;
        }

        throw new NotImplementedException();
    }

    public Task<OrderId> CreateLimitOrder(Pair pair, LimitOrder orderType, PlacementOrder entry)
    {
        using (var channel = _channel ?? GrpcChannel.ForAddress(Address))
        {
            var client = new OrderConfirmation.OrderConfirmationClient(channel);
            var answer = client.SendMessage(new SendMessageArgument());

            if (answer.Status == Status.Error)
                throw new Exception(answer.Message);

            //return answer.Message;
        }

        throw new NotImplementedException();
    }

    public Task<OrderId> CreateFuturesOrder(Pair pair, FuturesOrder orderType, int leverage, PlacementOrder entry,
        PlacementOrder? take = null, PlacementOrder? stop = null)
    {
        using (var channel = _channel ?? GrpcChannel.ForAddress(Address))
        {
            var client = new OrderConfirmation.OrderConfirmationClient(channel);
            var answer = client.SendMessage(new SendMessageArgument());

            if (answer.Status == Status.Error)
                throw new Exception(answer.Message);

            //return answer.Message;
        }

        throw new NotImplementedException();
    }

    private readonly Guid _userId;
    private readonly Exchange _exchange;
    private readonly Guid _runId;
    private readonly GrpcChannel? _channel;
    private readonly string Address;
}