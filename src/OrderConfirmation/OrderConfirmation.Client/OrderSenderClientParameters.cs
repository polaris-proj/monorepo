﻿using Infrastructure;

namespace OrderConfirmation.Client;

public class OrderSenderClientParameters : IParameters
{
    public OrderSenderClientParameters(IVault vault)
    {
        Address = vault.GetValue("OrderConfirmation:gRPC:Url");
    }

    public string Address { get; }
}