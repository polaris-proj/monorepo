import uuid
from datetime import datetime

from sqlalchemy import BigInteger, Boolean, Column, DateTime, Integer, String
from sqlalchemy.dialects.postgresql import UUID

from .base import Base


class TelegramUser(Base):
    __tablename__ = "TelegramUser"

    telegram_id = Column(BigInteger, primary_key=True, index=True)
    user_id = Column(UUID(as_uuid=True), nullable=True)
    username = Column(String, default=None)
    fullname = Column(String, default=None)
    created_at = Column(DateTime, default=datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __str__(self):
        return f"{self.__class__.__name__}(tg_id={self.telegram_id}, user_id={self.user_id}, username={self.username}, fullname={self.fullname})"
