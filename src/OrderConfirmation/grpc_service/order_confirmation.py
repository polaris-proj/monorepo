import logging
import uuid

from grpc import aio
from sqlalchemy.ext.asyncio import AsyncSession

from src.OrderConfirmation.bot.senders.order_notification import send_message
from src.OrderConfirmation.database import engine
from src.OrderConfirmation.grpc_python_server import OrderConfirmation_pb2, OrderConfirmation_pb2_grpc
from src.OrderConfirmation.helpers.mapper import get_invite_key, get_invite_link_by_key
from src.OrderConfirmation.service.invite_user_key_repository import upsert_invite_key


class OrderConfirmation(OrderConfirmation_pb2_grpc.OrderConfirmation):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger("OrderConfirmationGrpc")
        self._db_session = AsyncSession(engine)

    async def SendMessage(self, request: OrderConfirmation_pb2.SendMessageArgument, context: aio.ServicerContext):
        self._logger.debug(f"gRPC: SendMessage '{request.Message}'")
        res = await send_message(bot=self.bot, telegram_id=953772570, text=request.Message)
        return OrderConfirmation_pb2.Answer(Status=OrderConfirmation_pb2.Status.Ok, Message=res.as_json())

    async def GetLinkToTelegramBot(
        self, request: OrderConfirmation_pb2.GetLinkToTelegramBotArgument, context: aio.ServicerContext
    ):
        self._logger.debug(f"gRPC: GetLinkToTelegramBot '{request.UserId}'")
        invite_key = get_invite_key(guid=request.UserId)
        await upsert_invite_key(self._db_session, user_id=uuid.UUID(request.UserId), invite_key=invite_key)
        self._logger.debug(f"gRPC: Created invite_user_key '{invite_key}'")
        secret_link = get_invite_link_by_key(invite_key)
        return OrderConfirmation_pb2.Answer(Status=OrderConfirmation_pb2.Status.Ok, Message=secret_link)
