import logging
import uuid
from datetime import datetime
from typing import Any, List, Optional, Sequence, Tuple

from sqlalchemy import Row, delete, select, update
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.ext.asyncio import AsyncSession

from src.OrderConfirmation.models.invite_user_key import InviteUserKey
from src.OrderConfirmation.utils.misc import save_commit, save_execute

logger = logging.getLogger(__name__)


@save_execute
async def create_invite_key(session: AsyncSession, user_id: uuid.UUID, invite_key: str) -> Optional[InviteUserKey]:
    invite_user_key = InviteUserKey(user_id=user_id, invite_key=invite_key)
    logger.debug(f"InvKey: {user_id=}, {invite_key=}")
    session.add(invite_user_key)
    await save_commit(session)
    await session.refresh(invite_user_key)
    return invite_user_key


@save_execute
async def upsert_invite_key(session: AsyncSession, user_id: uuid.UUID, invite_key: str) -> Optional[InviteUserKey]:
    stmt = insert(InviteUserKey).values(user_id=user_id, invite_key=invite_key)
    stmt = stmt.on_conflict_do_update(
        index_elements=[InviteUserKey.user_id], set_=dict(invite_key=invite_key, updated_at=datetime.utcnow())
    ).returning(InviteUserKey)

    invite_user_key = await session.execute(stmt)
    await save_commit(session)
    return invite_user_key


@save_execute
async def get_user_by_user_id(session: AsyncSession, user_id: uuid.UUID) -> Optional[InviteUserKey]:
    stmt = select(InviteUserKey).where(InviteUserKey.user_id == user_id)
    result = await session.execute(stmt)

    return result.scalar_one_or_none()


@save_execute
async def get_user_by_invite_key(session: AsyncSession, invite_key: str) -> Optional[InviteUserKey]:
    stmt = select(InviteUserKey).where(InviteUserKey.invite_key == invite_key)
    result = await session.execute(stmt)

    return result.scalar_one_or_none()


@save_execute
async def delete_user_by_invite_key(session: AsyncSession, invite_key: str) -> None:
    if await get_user_by_invite_key(session, invite_key) is None:
        raise ValueError("Invalid user-id passed.")

    stmt = delete(InviteUserKey).where(InviteUserKey.invite_key == invite_key)

    await session.execute(stmt)
    await save_commit(session)
