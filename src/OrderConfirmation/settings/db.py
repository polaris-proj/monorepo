from pydantic import Field, SecretStr

from .base import AdvancedBaseSettings


class ServiceDatabaseSettings(AdvancedBaseSettings):
    host: str
    user: str
    password: str
    db: str
    port: int = Field(default="5432")

    class Config:
        env_prefix = "POSTGRES_"

    @property
    def async_postgresql_url(self) -> str:
        return f"postgresql+asyncpg://{self.user}:{self.password}@{self.host}:{self.port}/{self.db}"

    @property
    def postgresql_url_default(self) -> str:
        return f"postgresql://{self.user}:{self.password}@{self.host}:{self.port}/polarisdb"

    @property
    def postgresql_url(self) -> str:
        return f"postgresql://{self.user}:{self.password}@{self.host}:{self.port}/{self.db}"


service_database_settings = ServiceDatabaseSettings()
