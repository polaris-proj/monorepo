from .base import AdvancedBaseSettings


class SaltSettings(AdvancedBaseSettings):
    guid: str

    class Config:
        env_prefix = "SALT_"


salt_settings = SaltSettings()
