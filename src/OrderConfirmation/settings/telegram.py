from .base import AdvancedBaseSettings


class TelegramSettings(AdvancedBaseSettings):
    token: str
    nickname: str

    class Config:
        env_prefix = "TELEGRAM_"


telegram_settings = TelegramSettings()
