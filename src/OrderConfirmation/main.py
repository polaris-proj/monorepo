from aiogram import executor

from src.OrderConfirmation.bot.bot import get_app
from src.OrderConfirmation.bot.handlers.startup import startup_handler_factory


def start():
    startup_handler = startup_handler_factory()

    executor.start_polling(dp, skip_updates=True, on_startup=startup_handler)


if __name__ == "__main__":
    dp = get_app()
    start()
