﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using SkbKontur.TypeScript.ContractGenerator;
using TypeScriptClientGenerator;
using WebApi.Controllers;

var outPath = GetOutPath();


var apiAssembly = Assembly.GetAssembly(typeof(BaseController)) ??
                  throw new NullReferenceException("Cant get assembly of Api");

var controllers = apiAssembly.GetTypes()
    .Where(type => typeof(Controller).IsAssignableFrom(type)) //filter controllers
    .ToArray();

var generator = new TypeScriptGenerator(
    new TypeScriptGenerationOptions
    {
        NullabilityMode = NullabilityMode.NullableReference,
        UseTypeImports = true,
    },
    new CustomGenerator(),
    new RootTypesProvider(controllers)
);

generator.GenerateFiles(outPath);

return;

string GetOutPath()
{
    var path = Environment.CurrentDirectory;
    var pathToCommonFolder = Directory.GetParent(path)?.Parent?.Parent?.Parent?.Parent?.Parent?.FullName;
    if (pathToCommonFolder == null)
    {
        throw new NullReferenceException("Cant get path to common folder");
    }

    var pathToClients = Path.Combine(pathToCommonFolder, "Frontend", "src", "apiClients");

    if (!Directory.Exists(pathToClients))
    {
        throw new DirectoryNotFoundException(
            "Can't find directory for apiClients in frontend project. Check path to frontend project.");
    }

    return pathToClients;
}