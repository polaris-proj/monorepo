namespace Infrastructure.Tests;

[TestFixture]
public class EnumEnumeratorTests
{
    private enum TestEnum
    {
        Value1,
        Value2,
        Value3
    }

    [Test]
    public void GetEnums_ReturnsAllEnums()
    {
        TestEnum[] expected = {TestEnum.Value1, TestEnum.Value2, TestEnum.Value3};

        var actual = EnumEnumerator.GetEnums<TestEnum>();

        actual.Should().BeEquivalentTo(expected);
    }

    [Test]
    public void GetEnums_ReturnsEmptyArray_WhenEnumHasNoValues()
    {
        var actual = EnumEnumerator.GetEnums<EmptyEnum>();

        actual.Should().BeEmpty();
    }

    private enum EmptyEnum
    {
        // No values
    }

    [Test]
    public void GetEnums_ReturnsEnumsInCorrectOrder()
    {
        OrderedEnum[] expected = {OrderedEnum.Value1, OrderedEnum.Value2, OrderedEnum.Value3};

        var actual = EnumEnumerator.GetEnums<OrderedEnum>();

        actual.Should().BeEquivalentTo(expected);
    }

    private enum OrderedEnum
    {
        Value2 = 2,
        Value1 = 1,
        Value3 = 3
    }
}