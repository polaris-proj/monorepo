﻿using Infrastructure.EventDriven;

namespace Infrastructure.Tests.EventDrivenTests;

[TestFixture]
public class RealTimeDataSourceTests
{
    [Test]
    public async Task RegisterEventDestination_RegisterAction_RegisterSuccessfully()
    {
        var source = new RealTimeDataSource();
        var actionCalled = false;

        void ActionHandler(string data)
        {
            actionCalled = true;
            data.Should().Be("Test data");
        }

        source.RegisterEventDestination<string>(ActionHandler);
        await source.SendDataToHandlers("Test data");

        actionCalled.Should().BeTrue();
    }

    [Test]
    public async Task RegisterEventDestination_RegisterTypeAndAction_RegisterSuccessfully()
    {
        var source = new RealTimeDataSource();
        var actionCalled = false;

        void ActionHandler(int x)
        {
            actionCalled = true;
        }

        source.RegisterEventDestination<int>(ActionHandler);
        await source.SendDataToHandlers(123);

        actionCalled.Should().BeTrue();
    }

    [Test]
    public async Task RegisterEventDestination_RegisterActionWithExistingType_OverrideExistingHandler()
    {
        var source = new RealTimeDataSource();
        var action1Called = false;
        var action2Called = false;

        void ActionHandler1(string data)
        {
            action1Called = true;
        }

        void ActionHandler2(string data)
        {
            action2Called = true;
        }

        source.RegisterEventDestination<string>(ActionHandler1);
        source.RegisterEventDestination<string>(ActionHandler2);

        await source.SendDataToHandlers("Test data");

        action1Called.Should().BeTrue();
        action2Called.Should().BeTrue();
    }
}