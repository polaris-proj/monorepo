﻿using Infrastructure.EventDriven;

namespace Infrastructure.Tests.EventDrivenTests;

[TestFixture]
public class HistoricalDataSourceTests
{
    [Test]
    public async Task SendEvents_SendCollectionOfEvents_DataSentToHandlers()
    {
        var source = new HistoricalDataSource();
        var events = new List<string> {"Event 1", "Event 2", "Event 3"};
        var handledEvents = new List<string>();

        source.RegisterEventDestination<string>(@event => { handledEvents.Add(@event); });

        await source.SendEvents(events);

        handledEvents.Should().BeEquivalentTo(events);
    }

    [Test]
    public async Task SendEvent_SendSingleEvent_DataSentToHandlers()
    {
        var source = new HistoricalDataSource();
        var @event = 123;
        var handledEvent = 0;

        source.RegisterEventDestination<int>(x => { handledEvent = x; });

        await source.SendEvent(@event);

        handledEvent.Should().Be(@event);
    }

    [Test]
    public void SendEvents_NoEventHandlers_NoExceptionThrown()
    {
        var source = new HistoricalDataSource();
        var events = new List<int> {1, 2, 3};

        Assert.DoesNotThrow(() => source.SendEvents(events).Wait());
    }

    [Test]
    public void SendEvent_NoEventHandlers_NoExceptionThrown()
    {
        var source = new HistoricalDataSource();
        var @event = "Test event";

        Assert.DoesNotThrow(() => source.SendEvent(@event).Wait());
    }
}