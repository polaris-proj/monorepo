﻿using System.Collections.Concurrent;
using System.Diagnostics;

namespace Infrastructure.Tests;

public class CollectionsExtensionsTests
{
    [Test]
    public async Task ProcessInParallel_ShouldProcessAllItems()
    {
        var items = Enumerable.Range(0, 10).ToArray();
        var processedItems = new List<int>();
        Func<int, Task> taskFactory = async item =>
        {
            await Task.Delay(100); // Simulate work
            processedItems.Add(item);
        };

        await items.ProcessInParallel(taskFactory, 2);

        items.OrderBy(i => i).Should().BeEquivalentTo(processedItems.OrderBy(i => i));
    }

    [Test]
    public async Task ProcessInParallel_ShouldLimitDegreeOfParallelism()
    {
        var items = Enumerable.Range(0, 10);
        var concurrentTasks = 0;
        var maxConcurrentTasks = 0;
        Func<int, Task> taskFactory = async item =>
        {
            concurrentTasks++;
            maxConcurrentTasks = Math.Max(maxConcurrentTasks, concurrentTasks);
            await Task.Delay(100); // Simulate work
            concurrentTasks--;
        };

        await items.ProcessInParallel(taskFactory, 2);

        maxConcurrentTasks.Should().Be(2);
    }
    
    [Test]
    public async Task ProcessInParallel_ShouldCompleteAllTasksEvenIfOneFails()
    {
        // Arrange
        var items = Enumerable.Range(0, 10);
        var processedItems = new ConcurrentBag<int>();
        Func<int, Task> taskFactory = async item =>
        {
            await Task.Delay(100); // Simulate work
            if (item == 5)
            {
                throw new Exception("Test exception");
            }
            processedItems.Add(item);
        };
        
        Exception? exception = null;
        try
        {
            await items.ProcessInParallel(taskFactory, 2);
        }
        catch (Exception ex)
        {
            exception = ex;
        }
        
        exception.Should().NotBeNull();
        processedItems.Count.Should().Be(9);
    }
    [Test]
    public async Task ProcessInParallel_ShouldExecuteFasterWithHigherDegreeOfParallelism()
    {
        var items = Enumerable.Range(0, 100).ToArray();
        Func<int, Task> taskFactory = async item =>
        {
            await Task.Delay(100); // Simulate work
        };
        
        var stopwatch = Stopwatch.StartNew();
        await items.ProcessInParallel(taskFactory, 2);
        stopwatch.Stop();
        var elapsedWithTwoTasks = stopwatch.Elapsed;

        stopwatch.Restart();
        await items.ProcessInParallel(taskFactory, 10);
        stopwatch.Stop();
        var elapsedWithTenTasks = stopwatch.Elapsed;

        elapsedWithTenTasks.Should().BeLessThan(elapsedWithTwoTasks);
    }
    

  
}