﻿using System.Text;
using System.Text.Json;
using Infrastructure.Json;

namespace Infrastructure.Tests.JsonConvertersTests;

[TestFixture]
public class DateOnlyJsonConverterTests
{
    [Test]
    public void Read_ConvertsJsonStringToDateOnly()
    {
        var converter = new DateOnlyJsonConverter();
        var reader = new Utf8JsonReader(Encoding.UTF8.GetBytes("\"2022-01-01\""));

        reader.Read(); // Move to the Value token
        var result = converter.Read(ref reader, typeof(DateOnly), new JsonSerializerOptions());

        result.Should().Be(new DateOnly(2022, 1, 1));
    }

    [Test]
    public void Write_ConvertsDateOnlyToJsonString()
    {
        var converter = new DateOnlyJsonConverter();
        var memWriter = new MemoryStream();
        var writer = new Utf8JsonWriter(memWriter);

        var date = new DateOnly(2022, 12, 31);
        converter.Write(writer, date, new JsonSerializerOptions());
        writer.Flush();

        var json = Encoding.UTF8.GetString(memWriter.ToArray());
        json.Should().Be("\"2022-12-31\"");
    }

    [Test]
    public void Roundtrip_ConvertsDateOnlyToJsonStringAndBack()
    {
        var converter = new DateOnlyJsonConverter();
        var options = new JsonSerializerOptions();
        options.Converters.Add(converter);

        var date = new DateOnly(2022, 6, 15);

        var json = JsonSerializer.Serialize(date, options);
        var result = JsonSerializer.Deserialize<DateOnly>(json, options);

        result.Should().Be(date);
    }
}