﻿using System.Text;
using System.Text.Json;
using Infrastructure.Json;

namespace Infrastructure.Tests.JsonConvertersTests;

[TestFixture]
public class TypeJsonConverterTests
{
    [Test]
    public void Read_ConvertsJsonStringToType()
    {
        var converter = new TypeJsonConverter();
        var reader = new Utf8JsonReader("\"System.Int32\""u8);

        reader.Read(); // Move to the Value token
        var result = converter.Read(ref reader, typeof(Type), new JsonSerializerOptions());

        result.Should().Be(typeof(int));
    }

    [Test]
    public void Write_ConvertsTypeToJsonString()
    {
        var converter = new TypeJsonConverter();
        var memWriter = new MemoryStream();
        var writer = new Utf8JsonWriter(memWriter);

        var type = typeof(string);
        converter.Write(writer, type, new JsonSerializerOptions());
        writer.Flush();

        var json = Encoding.UTF8.GetString(memWriter.ToArray());
        json.Should().Contain("System.String");
    }

    [Test]
    public void Roundtrip_ConvertsTypeToJsonStringAndBack()
    {
        var converter = new TypeJsonConverter();
        var options = new JsonSerializerOptions();
        options.Converters.Add(converter);

        var type = typeof(DateTime);

        var json = JsonSerializer.Serialize(type, options);
        var result = JsonSerializer.Deserialize<Type>(json, options);

        result.Should().Be(type);
    }
}