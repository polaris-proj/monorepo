﻿using Microsoft.Extensions.Configuration;
using Moq;

namespace Infrastructure.Tests;

public class VaultTests
{
    [SetUp]
    public void SetUp()
    {
        _mockConfig = new Mock<IConfiguration>();
        _vault = new Vault(_mockConfig.Object);
    }

    [Test]
    public void GetValue_ReturnsConfigValue_WhenKeyExists()
    {
        var configKey = "Settings:Key";
        var configValue = "Value";

        _mockConfig.Setup(c => c[configKey]).Returns(configValue);

        string result = _vault.GetValue(configKey);

        result.Should().Be(configValue);
    }

    [Test]
    public void GetValue_ThrowsException_WhenKeyDoesNotExist()
    {
        var configKey = "Settings:Key";

        _mockConfig.Setup(c => c[configKey]).Returns((string) null!);

        Action action = () => _vault.GetValue(configKey);

        action.Should().Throw<VaultKeyNotFoundException>()
            .WithMessage($"Key: {configKey} not found");
    }

    private Vault _vault = null!;
    private Mock<IConfiguration> _mockConfig = null!;
}