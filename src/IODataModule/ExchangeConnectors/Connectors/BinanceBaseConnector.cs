﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Binance.Net;
using Binance.Net.Clients;
using Binance.Net.Enums;
using Binance.Net.Interfaces;
using Binance.Net.Objects.Models.Spot;
using Binance.Net.Objects.Models.Spot.Socket;
using CryptoExchange.Net.Authentication;
using CryptoExchange.Net.Interfaces;
using CryptoExchange.Net.Objects;
using CryptoExchange.Net.Sockets;
using Domain.StrategyProcessor;
using Infrastructure;
using Infrastructure.Extensions;
using Infrastructure.Metric;
using Microsoft.Extensions.Logging;
using ReadOnlyCollectionsExtensions;

namespace ExchangeConnectors.Connectors;

public abstract class BinanceBaseConnector : IAsyncDisposable
{
    protected BinanceBaseConnector(string key, string secret, ILogger logger, bool isTestApi = false)
    {
        Logger = logger;
        var credentials = new ApiCredentials(key, secret);

        RestClient = new BinanceRestClient(x =>
        {
            x.ApiCredentials = credentials;
            x.SpotOptions.RateLimiters = new List<IRateLimiter>
            {
                new RateLimiter().AddEndpointLimit("klines", 19, TimeSpan.FromSeconds(1)),
                new RateLimiter().AddTotalRateLimit(1200, TimeSpan.FromMinutes(1)),
            };
            x.SpotOptions.RateLimitingBehaviour = RateLimitingBehaviour.Wait;
            x.Environment = isTestApi ? BinanceEnvironment.Testnet : BinanceEnvironment.Live;
        });
    }

    public Task UnsubscribeOnNewKlines(Pair ticker, TimeFrame tf, Action<Kline> deleg)
    {
        RemoveSubscription(Events, ticker, tf, deleg);
        return Task.CompletedTask;
    }

    private void RemoveSubscription(ConcurrentDictionary<(Pair, TimeFrame), List<Action<Kline>>> dict, Pair ticker,
        TimeFrame tf, Action<Kline> deleg)
    {
        if (dict.TryGetValue((ticker, tf), out var listDeleg))
            listDeleg.Remove(deleg);
    }

    protected virtual async Task SubscribeOnNewKlines(IReadOnlyList<Pair> ticker, IReadOnlyList<TimeFrame> tf,
        Action<Kline> eventHandler,
        Func<IEnumerable<string>,
            IEnumerable<KlineInterval>,
            Action<DataEvent<IBinanceStreamKlineData>>,
            CancellationToken, Task<CallResult<UpdateSubscription>>> subscribeToKlines)
    {
        await ValidateStringToPairConversion();
        var timeFrames = tf.Select(x => x.GetKlineInterval()).ToReadOnlyList();
        var names = ticker.Select(x => x.First + x.Second).ToReadOnlyList();

        foreach (var (pair, timeFrame) in ticker.SelectMany(x => tf.Select(y => (x, y))))
        {
            AddSubscription(Events, pair, timeFrame, eventHandler);
        }
        //todo подписка на 2300 пар за 210 сек это долго!

        var batchSize = 16; //8 - 417684 16 - 214419 32 -825143 20 -1318822

        var timeout = 0;
        var count = 0;
        var sw2 = Stopwatch.StartNew();
        foreach (var pairsBatch in names.Batch(batchSize))
        {
            var batchedPairs = pairsBatch.ToArray();
            var isSuccess = false;
            var amount = 3;
            CallResult<UpdateSubscription>? subscriptionResult = null;
            while (!isSuccess && amount-- > 0)
            {
                var sw = Stopwatch.StartNew();
                try
                {
                    subscriptionResult = await subscribeToKlines(batchedPairs, timeFrames, obj =>
                    {
                        if (!obj.Data.Data.Final) return;

                        var pair = StringToPairConversion[obj.Data.Symbol];
                        var timeFrame = obj.Data.Data.Interval.GetTimeFrame();
                        var kline = (BinanceStreamKline) obj.Data.Data;

                        eventHandler(new Kline(pair, timeFrame, obj.Timestamp.ToMilliseconds(), kline.OpenPrice,
                            kline.HighPrice,
                            kline.LowPrice, kline.ClosePrice, kline.Volume
                        ));
                    }, CancellationToken.None);
                    
                    isSuccess = subscriptionResult.Success;
                    if (isSuccess)
                    {
                        subscriptionResult.Data.ConnectionLost += () => { Console.WriteLine("Connection lost"); };
                        subscriptionResult.Data.ConnectionRestored += _ => { Console.WriteLine("Connection restored"); };
                        count += batchSize;
                        Logger.LogTrace($"Подписан на {count} пар");
                    }
                    else
                    {
                        Logger.LogError($"Subscription failed retry {amount}");
                    }
                }
                catch (Exception e)
                {
                    Logger.LogError(e, "Error while subscribing to {0}", string.Join(' ', batchedPairs));
                }

                if (sw.ElapsedMilliseconds < timeout)
                    await Task.Delay(timeout - (int) sw.ElapsedMilliseconds);
            }

            if (subscriptionResult is not null && !subscriptionResult.Success)
            {
                throw new Exception(subscriptionResult.Error?.Message);
            }
        }

        Console.WriteLine(sw2.ElapsedMilliseconds);
    }

    private void AddSubscription(ConcurrentDictionary<(Pair, TimeFrame), List<Action<Kline>>> dict, Pair ticker,
        TimeFrame tf, Action<Kline> deleg)
    {
        var dictByTicker = dict.GetOrAdd((ticker, tf), new List<Action<Kline>>());
        dictByTicker.Add(deleg);
    }

    protected virtual async Task<IReadOnlyList<Candle>> GetCandles(Pair pair, TimeFrame timeFrame, DateTimeRange range,
        Func<string, KlineInterval, DateTime, DateTime, int, Task<WebCallResult<IEnumerable<IBinanceKline>>>>
            candleSelector)
    {
        var candles = new ConcurrentBag<Candle>();
        var startTime = range.Begin;
        var convertedTimeFrame = timeFrame.GetKlineInterval();
        const int amountOfParallelTasks = 25;
        const int candlesPerRequest = 1000;
        var work = true;

        while (work)
        {
            var tasks = new List<Task>(amountOfParallelTasks);
            for (var i = 0; i < amountOfParallelTasks; i++)
            {
                var endTime = (startTime.ToMilliseconds() + candlesPerRequest * 1000 * (long) timeFrame).ToDateTime();

                var time = startTime == range.Begin
                    ? startTime
                    : (startTime.ToMilliseconds() + 1)
                    .ToDateTime(); //On first iteration include left timePoint and not on next iterations

                if (endTime > range.End)
                    endTime = range.End;

                if (time > range.End)
                {
                    work = false;
                    break;
                }

                tasks.Add(candleSelector(pair.ToStringWithOutSlash(), convertedTimeFrame, time, endTime,
                        candlesPerRequest)
                    //  .WithRetries()//TODO ретраи не работаю, как будто хттп клиент умирает и его нельзя переиспользовать
                    .ContinueWith(x => AddCandles(x, range.End, candles))); //TODO использовать c# Channel ?
                startTime = endTime;
            }

            await Task.WhenAll(tasks);
        }

        return candles.IsEmpty ? new List<Candle>() : candles.OrderBy(x => x.TimeStamp).ToReadOnlyList();
    }

    private async Task AddCandles(Task<WebCallResult<IEnumerable<IBinanceKline>>> task, DateTime end,
        ConcurrentBag<Candle> candles)
    {
        var callResult = await task;

        if (callResult.Data is null)
            throw new Exception("Че ты в аргументы передал?");

        if (!callResult.Data.Any())
        {
            MetricClient.GetCounter("ZeroCandlesFromExchange").WithLabels("method").Inc();
            return;
        }

        foreach (var candle in callResult.Data)
        {
            if (candle.OpenTime > end)
                break;

            var timeOfCandle = candle.OpenTime.ToMilliseconds();
            var newCandle = new Candle(timeOfCandle, candle.OpenPrice, candle.HighPrice,
                candle.LowPrice, candle.ClosePrice, candle.Volume);

            candles.Add(newCandle);
        }
    }

    protected virtual async Task<Dictionary<Pair, decimal>> GetPrices(
        Func<Task<WebCallResult<IEnumerable<BinancePrice>>>> priceGetter)
    {
        await ValidateStringToPairConversion();
        var dict = new Dictionary<Pair, decimal>();
        foreach (var pair in (await priceGetter()).Data)
        {
            if (StringToPairConversion.TryGetValue(pair.Symbol, out var value))
                dict.Add(value, pair.Price);
            else
                Logger.LogWarning($"Unknown pair in convertations process {pair.Symbol}");
        }

        return dict;
    }

    protected virtual async Task ValidateStringToPairConversion()
    {
        if (StringToPairConversion.Count == 0)
            StringToPairConversion = await GetStringToPairConversionAsync();
    }

    protected virtual async Task<Dictionary<string, Pair>> GetStringToPairConversionAsync()
    {
        Logger.LogTrace("Getting pairs for pair to string conversion");
        var spotSymbols = RestClient.SpotApi.ExchangeData.GetExchangeInfoAsync();
        var usdFuturesSymbols = RestClient.UsdFuturesApi.ExchangeData.GetExchangeInfoAsync();
        var coinsFuturesSymbols = RestClient.CoinFuturesApi.ExchangeData.GetExchangeInfoAsync();


        var pairs = new List<(string BaseAsset, string QuoteAsset)>();

        var spotPairs = await spotSymbols;
        if (spotPairs.Success)
            pairs.AddRange(spotPairs.Data.Symbols.Select(x => (x.BaseAsset, x.QuoteAsset)));
        else
            Logger.LogError($"Can't receive {nameof(spotPairs)} {spotPairs.Error}");

        var usdFuturesPairs = await usdFuturesSymbols;
        if (usdFuturesPairs.Success)
            pairs.AddRange(usdFuturesPairs.Data.Symbols.Select(x => (x.BaseAsset, x.QuoteAsset)));
        else
            Logger.LogError($"Can't receive {nameof(usdFuturesPairs)} {usdFuturesPairs.Error}");

        var coinsFuturesPairs = await coinsFuturesSymbols;
        if (coinsFuturesPairs.Success)
            pairs.AddRange(coinsFuturesPairs.Data.Symbols.Select(x => (x.BaseAsset, x.QuoteAsset)));
        else
            Logger.LogError($"Can't receive {nameof(coinsFuturesPairs)} {coinsFuturesPairs.Error}");

        return pairs.Distinct()
            .ToDictionary(k => k.BaseAsset + k.QuoteAsset, v => new Pair(v.BaseAsset, v.QuoteAsset));
    }

    public virtual ValueTask DisposeAsync()
    {
        RestClient.Dispose();
        return ValueTask.CompletedTask;
    }

    protected readonly ILogger Logger;
    //todo добавить поддержку прокси
    protected readonly BinanceRestClient RestClient;
    protected static Dictionary<string, Pair> StringToPairConversion = new();
    protected readonly ConcurrentDictionary<(Pair, TimeFrame), List<Action<Kline>>> Events = new();
}