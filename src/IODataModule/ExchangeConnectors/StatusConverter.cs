﻿using System;
using Binance.Net.Enums;
using CryptoExchange.Net.CommonObjects;
using Domain.StrategyProcessor;
using LibraryOrderStatus = Binance.Net.Enums.OrderStatus;
using OrderStatus = Domain.Connectors.OrderStatus;

namespace ExchangeConnectors;

public static class StatusConverter
{
    public static OrderType GetOrderType(CommonOrderSide side, CommonOrderType type)
    {
        return (side, type) switch
        {
            (CommonOrderSide.Buy, CommonOrderType.Market) => OrderType.Buy,
            (CommonOrderSide.Sell, CommonOrderType.Market) => OrderType.Sell,
            (CommonOrderSide.Buy, CommonOrderType.Limit) => OrderType.BuyLimit,
            (CommonOrderSide.Sell, CommonOrderType.Limit) => OrderType.SellLimit,
            _ => throw new ArgumentOutOfRangeException(nameof(side), side, null)
        };
    }
    
    public static OrderType GetFuturesOrderType(CommonOrderSide side, CommonOrderType type)
    {
        return (side, type) switch
        {
            (CommonOrderSide.Buy, CommonOrderType.Market) => OrderType.Buy,
            (CommonOrderSide.Sell, CommonOrderType.Market) => OrderType.Sell,
            (CommonOrderSide.Buy, CommonOrderType.Limit) => OrderType.Long,
            (CommonOrderSide.Sell, CommonOrderType.Limit) => OrderType.Short,
            _ => throw new ArgumentOutOfRangeException(nameof(side), side, null)
        };
    }

    public static OrderSide GetOrderSide(this FuturesOrder futuresOrder)
    {
        return futuresOrder == FuturesOrder.Long ? OrderSide.Buy : OrderSide.Sell;
    }

    public static CommonOrderSide ToCommonOrderSide(this OrderSide orderSide)
    {
        return orderSide switch
        {
            OrderSide.Buy => CommonOrderSide.Buy,
            OrderSide.Sell => CommonOrderSide.Sell,
            _ => throw new ArgumentOutOfRangeException(nameof(orderSide), orderSide, null)
        };
    }

    public static CommonOrderType ToCommonOrderType(this FuturesOrderType futuresOrderType)
    {
        return futuresOrderType switch
        {
            FuturesOrderType.Limit => CommonOrderType.Limit,
            FuturesOrderType.Market => CommonOrderType.Market,
            FuturesOrderType.Stop => CommonOrderType.Other,
            FuturesOrderType.StopMarket => CommonOrderType.Market,
            FuturesOrderType.TakeProfit => CommonOrderType.Other,
            FuturesOrderType.TakeProfitMarket => CommonOrderType.Market,
            FuturesOrderType.TrailingStopMarket => CommonOrderType.Market,
            FuturesOrderType.Liquidation => CommonOrderType.Other,
            _ => throw new ArgumentOutOfRangeException(nameof(futuresOrderType), futuresOrderType, null)
        };
    }

    public static OrderStatus GetOrderStatus(this CommonOrderStatus status)
    {
        return status switch
        {
            CommonOrderStatus.Filled => OrderStatus.Close,
            CommonOrderStatus.Active => OrderStatus.Open,
            CommonOrderStatus.Canceled => OrderStatus.Close,
            _ => throw new Exception("ну и зачем ты сюда такой тип ордеров отправил?")
        };
    }
    
    public static OrderStatus GetOrderStatus(this LibraryOrderStatus status)
    {
        return status switch
        {
            LibraryOrderStatus.Filled => OrderStatus.Close,
            LibraryOrderStatus.New => OrderStatus.Open,
            LibraryOrderStatus.Canceled => OrderStatus.Close,
            _ => throw new Exception("ну и зачем ты сюда такой тип ордеров отправил?")
        };
    }
    
}