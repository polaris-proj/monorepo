﻿using System;
using Binance.Net.Enums;

namespace ExchangeConnectors;

public static class TimeFrameConverter
{
    public static TimeFrame GetTimeFrame(this KlineInterval timeFrame)
    {
        return timeFrame switch
        {
            KlineInterval.OneMinute => TimeFrame.m1,
            KlineInterval.ThreeMinutes => TimeFrame.m3,
            KlineInterval.FiveMinutes => TimeFrame.m5,
            KlineInterval.FifteenMinutes => TimeFrame.m15,
            KlineInterval.ThirtyMinutes => TimeFrame.m30,
            KlineInterval.OneHour => TimeFrame.h1,
            KlineInterval.FourHour => TimeFrame.h4,
            KlineInterval.TwelveHour => TimeFrame.h12,
            KlineInterval.OneDay => TimeFrame.D1,
            KlineInterval.ThreeDay => TimeFrame.D3,
            KlineInterval.OneWeek => TimeFrame.W1,
            KlineInterval.OneMonth => TimeFrame.Mo1,
            _ => throw new ArgumentOutOfRangeException($"Unknown timeframe! {timeFrame}")
        };
    }

    public static KlineInterval GetKlineInterval(this TimeFrame timeFrame)
    {
        return timeFrame switch
        {
            TimeFrame.m1 => KlineInterval.OneMinute,
            TimeFrame.m3 => KlineInterval.ThreeMinutes,
            TimeFrame.m5 => KlineInterval.FiveMinutes,
            TimeFrame.m15 => KlineInterval.FifteenMinutes,
            TimeFrame.m30 => KlineInterval.ThirtyMinutes,
            TimeFrame.h1 => KlineInterval.OneHour,
            TimeFrame.h4 => KlineInterval.FourHour,
            TimeFrame.h12 => KlineInterval.TwelveHour,
            TimeFrame.D1 => KlineInterval.OneDay,
            TimeFrame.D3 => KlineInterval.ThreeDay,
            TimeFrame.W1 => KlineInterval.OneWeek,
            TimeFrame.Mo1 => KlineInterval.OneMonth,
            _ => throw new ArgumentOutOfRangeException($"Unknown timeframe! {timeFrame}")
        };
    }
}