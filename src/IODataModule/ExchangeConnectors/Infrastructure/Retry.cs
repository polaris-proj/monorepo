﻿/*using System;
using System.Threading.Tasks;
using CryptoExchange.Net.Objects;
using Infrastructure.Metric;
using MethodTimer;
using Polly;

namespace ExchangeConnectors.Infrastructure;

internal static class Retry
{
    [Time]
    public static async Task<WebCallResult<T>> WithRetries<T>(this Task<WebCallResult<T>> task, int count = 1000)
    {
        var retry = Policy
            .HandleResult<WebCallResult<T>>(x => !x.Success)
            .WaitAndRetryAsync(count,
                retryAttempt => TimeSpan.FromMilliseconds(100 * retryAttempt),
                (_, timespan) =>
                {
                    MetricClient.SendTimerData("RetryAttemptNumber", timespan.TotalMilliseconds / 100);
                    Console.WriteLine($"RetryAttempt {timespan.TotalMilliseconds / 100}");
                });


        return await retry.ExecuteAsync(async () => await task);
    }
}*/