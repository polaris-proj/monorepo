﻿/*using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.Metric;
using MethodTimer;

namespace ExchangeConnectors.Infrastructure;

public class ResponseQueue //todo возможно эта реализация лучше https://stackoverflow.com/questions/72435221/restricting-sendasync-calls-to-5-messages-per-second
{
    public ResponseQueue(int rps = 15)
    {
        _rps = rps;
        _sleepTime = TimeSpan.FromMilliseconds(1000 / _rps);
        SetupTimer();
    }

    private void SetupTimer()
    {
        _timer = new Timer(StartExecute, null, 0, 1000);
    }

    private void StartExecute(object _)
    {
        // var sw = Stopwatch.StartNew();
        var amount = 0;
        for (var i = 0; i < _rps; i++)
        {
            if (_queue.TryDequeue(out var semaphoreSlim))
            {
                semaphoreSlim.Release();
                amount++;
                Thread.Sleep(_sleepTime); //для равномерного распределения запросов в рамках 1 секунды
            }
        }
        // Console.WriteLine(sw.ElapsedMilliseconds);

        MetricClient.SendTimerData("CurrentRps", amount);
        MetricClient.SendTimerData("RequestedRps", _rps);
        MetricClient.SendTimerData("RequestQueueLag", _queue.Count);
    }


    [Time]
    public async Task<T> Send<T>(Func<Task<T>> command)
    {
        var semaphore = new SemaphoreSlim(0);
        _queue.Enqueue(semaphore);

        await semaphore.WaitAsync();
        return await command();
    }

    private static Timer _timer;

    //todo переписать этот костыль на TaskSheduler
    private readonly int _rps;
    private readonly TimeSpan _sleepTime;
    private readonly ConcurrentQueue<SemaphoreSlim> _queue = new();
}*/