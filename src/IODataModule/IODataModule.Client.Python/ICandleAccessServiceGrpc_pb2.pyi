from protobuf_net import bcl_pb2 as _bcl_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class Exchange(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    BinanceSpot: _ClassVar[Exchange]
    BinanceFutures: _ClassVar[Exchange]
    Poloniex: _ClassVar[Exchange]

class Status(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    Error: _ClassVar[Status]
    Ok: _ClassVar[Status]

class TimeFrame(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    ZzzzDontuse: _ClassVar[TimeFrame]
    m1: _ClassVar[TimeFrame]
    m3: _ClassVar[TimeFrame]
    m5: _ClassVar[TimeFrame]
    m15: _ClassVar[TimeFrame]
    m30: _ClassVar[TimeFrame]
    h1: _ClassVar[TimeFrame]
    h4: _ClassVar[TimeFrame]
    h12: _ClassVar[TimeFrame]
    D1: _ClassVar[TimeFrame]
    D3: _ClassVar[TimeFrame]
    W1: _ClassVar[TimeFrame]
    Mo1: _ClassVar[TimeFrame]
BinanceSpot: Exchange
BinanceFutures: Exchange
Poloniex: Exchange
Error: Status
Ok: Status
ZzzzDontuse: TimeFrame
m1: TimeFrame
m3: TimeFrame
m5: TimeFrame
m15: TimeFrame
m30: TimeFrame
h1: TimeFrame
h4: TimeFrame
h12: TimeFrame
D1: TimeFrame
D3: TimeFrame
W1: TimeFrame
Mo1: TimeFrame

class Answer_Array_CandleDto(_message.Message):
    __slots__ = ["Status", "Message", "Value"]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    MESSAGE_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    Status: Status
    Message: str
    Value: _containers.RepeatedCompositeFieldContainer[CandleDto]
    def __init__(self, Status: _Optional[_Union[Status, str]] = ..., Message: _Optional[str] = ..., Value: _Optional[_Iterable[_Union[CandleDto, _Mapping]]] = ...) -> None: ...

class Answer_CandleDto(_message.Message):
    __slots__ = ["Status", "Message", "Value"]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    MESSAGE_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    Status: Status
    Message: str
    Value: CandleDto
    def __init__(self, Status: _Optional[_Union[Status, str]] = ..., Message: _Optional[str] = ..., Value: _Optional[_Union[CandleDto, _Mapping]] = ...) -> None: ...

class CandleAccessServiceGetArgument(_message.Message):
    __slots__ = ["Exchange", "Pair", "TimeFrame", "TimeStamp"]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    PAIR_FIELD_NUMBER: _ClassVar[int]
    TIMEFRAME_FIELD_NUMBER: _ClassVar[int]
    TIMESTAMP_FIELD_NUMBER: _ClassVar[int]
    Exchange: Exchange
    Pair: PairDto
    TimeFrame: TimeFrame
    TimeStamp: int
    def __init__(self, Exchange: _Optional[_Union[Exchange, str]] = ..., Pair: _Optional[_Union[PairDto, _Mapping]] = ..., TimeFrame: _Optional[_Union[TimeFrame, str]] = ..., TimeStamp: _Optional[int] = ...) -> None: ...

class CandleDto(_message.Message):
    __slots__ = ["TimeStamp", "Open", "High", "Low", "Close"]
    TIMESTAMP_FIELD_NUMBER: _ClassVar[int]
    OPEN_FIELD_NUMBER: _ClassVar[int]
    HIGH_FIELD_NUMBER: _ClassVar[int]
    LOW_FIELD_NUMBER: _ClassVar[int]
    CLOSE_FIELD_NUMBER: _ClassVar[int]
    TimeStamp: int
    Open: _bcl_pb2.Decimal
    High: _bcl_pb2.Decimal
    Low: _bcl_pb2.Decimal
    Close: _bcl_pb2.Decimal
    def __init__(self, TimeStamp: _Optional[int] = ..., Open: _Optional[_Union[_bcl_pb2.Decimal, _Mapping]] = ..., High: _Optional[_Union[_bcl_pb2.Decimal, _Mapping]] = ..., Low: _Optional[_Union[_bcl_pb2.Decimal, _Mapping]] = ..., Close: _Optional[_Union[_bcl_pb2.Decimal, _Mapping]] = ...) -> None: ...

class GetRangeArgument(_message.Message):
    __slots__ = ["Exchange", "Pair", "TimeFrame", "Start", "End"]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    PAIR_FIELD_NUMBER: _ClassVar[int]
    TIMEFRAME_FIELD_NUMBER: _ClassVar[int]
    START_FIELD_NUMBER: _ClassVar[int]
    END_FIELD_NUMBER: _ClassVar[int]
    Exchange: Exchange
    Pair: PairDto
    TimeFrame: TimeFrame
    Start: int
    End: int
    def __init__(self, Exchange: _Optional[_Union[Exchange, str]] = ..., Pair: _Optional[_Union[PairDto, _Mapping]] = ..., TimeFrame: _Optional[_Union[TimeFrame, str]] = ..., Start: _Optional[int] = ..., End: _Optional[int] = ...) -> None: ...

class PairDto(_message.Message):
    __slots__ = ["Pair"]
    PAIR_FIELD_NUMBER: _ClassVar[int]
    Pair: str
    def __init__(self, Pair: _Optional[str] = ...) -> None: ...
