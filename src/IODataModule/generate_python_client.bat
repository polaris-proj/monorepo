mkdir "../IODataModule/IODataModule.Shared/Protos/protobuf-net/"
copy /y "../protobuf-net/" "../IODataModule/IODataModule.Shared/Protos/protobuf-net/"


python -m grpc_tools.protoc --proto_path=./IODataModule.Shared/Protos/ --python_out=./IODataModule.Client.Python/ --grpc_python_out=./IODataModule.Client.Python/ --pyi_out=./IODataModule.Client.Python/ ./IODataModule.Shared/Protos/ICandleAccessServiceGrpc.proto