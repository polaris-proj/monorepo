﻿using Infrastructure;
using Infrastructure.Metric;
using IODataModule.Services;
using Prometheus;

namespace IODataModule;

internal class Program
{
    public static async Task Main(string[] args)
    {
        var app = SetUp(args);
        Metrics.SuppressDefaultMetrics();
        Metrics.DefaultRegistry.SetStaticLabels(new Dictionary<string, string>
        {
            {"service_name", "IoDataModule"}
        });
        await PrepareServices(app);


        Task.Run(async () =>
        {
            
            using var scope = app.Services.CreateScope();
            var patternEventLoader = scope.ServiceProvider.GetRequiredService<PatternEventLoader>();
            await patternEventLoader.StartSendCandles();
        });

        await app.RunAsync();
    }

    private static async Task PrepareServices(IHost app)
    {
        var logger = app.Services.GetRequiredService<ILogger<Program>>();

        using var scope = app.Services.CreateScope();
        foreach (var service in scope.ServiceProvider.GetServices<StartUpService>()
                     .OrderByDescending(x => x.Priority))
        {
            await service.Start();
            logger.LogTrace($"{service.GetType().Name} Completed");
        }
    }

    public static IHost SetUp(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        builder.ConfigureOpenTelemetry("IoDataModule");
       
        builder.Services.ConfigureServices();

        var app = builder.Build();
        app.ConfigureApp();
        return app;
    }
    
}