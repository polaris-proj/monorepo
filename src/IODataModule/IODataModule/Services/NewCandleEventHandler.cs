﻿using Confluent.Kafka;
using Domain.Patterns.Objects;
using Domain.StrategyProcessor;
using ExchangeConnectors.Connectors;
using Infrastructure.Extensions;
using Infrastructure.RTQConnector;
using Prometheus;

namespace IODataModule.Services;

public class NewCandleEventHandler
{
    public NewCandleEventHandler(
        IInternalConnector connector,
        Exchange exchange,
        IReadOnlyList<Pair> pairs,
        IReadOnlyList<TimeFrame> timeFrames,
        KafkaParameters kafkaParameters,
        ILoggerFactory logger
    )
    {
        _logger = logger.CreateLogger<NewCandleEventHandler>();
        _pairs = pairs;
        _timeFrames = timeFrames;
        _connector = connector;
        _exchange = exchange;

        _handler = x => Handle(x).GetAwaiter().GetResult(); // если завернуть в таск ран, то получаем дедлок. Т.к выжирается пул потоков?

        var cc = kafkaParameters.GetClientConfig();
        var config = new ProducerConfig(cc)
        {
            AllowAutoCreateTopics = true,
            LingerMs = 5,
            CompressionType = CompressionType.Lz4,
        };

        _producer = new ProducerBuilder<Null, NewCandleEvent>(config)
            .SetValueSerializer(new MyProtoSerializer<NewCandleEvent>())
            .Build();

        _candleLag = Metrics.CreateSummary("candle_lag", string.Empty,
            ["Exchange", "TimeFrame"],
            new SummaryConfiguration
            {
                Objectives = new[]
                {
                    new QuantileEpsilonPair(0.5, 0.05),
                    new QuantileEpsilonPair(0.9, 0.05),
                    new QuantileEpsilonPair(0.95, 0.01),
                    new QuantileEpsilonPair(0.99, 0.005),
                }
            });
    }


    public async Task StartAsync()
    {
        _logger.LogTrace("Начинаю выполнение регистрации хэндлеров");
        try
        {
            await _connector.SubscribeOnNewKlines(_pairs, _timeFrames, _handler);
        }
        catch (Exception e)
        {
            _logger.LogCritical(e, "Ошибка при регистрации хэндлеров");
            throw;
        }
    }

    //todo сделать так чтоб этот метод работал
    public void Stop()
    {
        foreach (var pair in _pairs)
        {
            foreach (var tf in _timeFrames)
            {
                _connector.UnsubscribeOnNewKlines(pair, tf, _handler);
            }
        }

        _cts.Cancel();
        // IsStarted = false;
    }

    private async Task Handle(Kline candle)
    {
        var candleTime = (long) Math.Round((decimal) candle.TimeStamp / (candle.TimeFrame.GetSeconds() * 1000))
                         * candle.TimeFrame.GetSeconds() * 1000;

        var currentTime = DateTime.UtcNow.ToMilliseconds();
        var lag = currentTime - candleTime;


        var candleEvent = new NewCandleEvent(
            new Candle(candle.TimeStamp, candle.Open, candle.High, candle.Low, candle.Close, candle.Volume),
            candle.Pair, candle.TimeFrame, _exchange, candle.TimeStamp);

        try
        {
            await _producer.ProduceAsync(TopicName, new Message<Null, NewCandleEvent> {Value = candleEvent});
            _candleLag.WithLabels(_exchange.ToString(), candle.TimeFrame.ToString()).Observe(lag);
        }
        catch (ProduceException<long, NewCandleEvent> e)
        {
            _logger.LogCritical(e.ToString());
            throw;
        }
    }

    private Action<Kline> _handler;
    private readonly ILogger _logger;
    private readonly ICollector<ISummary> _candleLag;
    private readonly IReadOnlyList<Pair> _pairs;
    private readonly IReadOnlyList<TimeFrame> _timeFrames;
    private readonly IInternalConnector _connector;
    private readonly Exchange _exchange;
    private readonly IProducer<Null, NewCandleEvent> _producer;
    private readonly CancellationTokenSource _cts = new();
    private static readonly string TopicName = Topic.Candle.ToString();
}