﻿using Confluent.Kafka;
using Domain.Patterns.Objects;
using Infrastructure.RTQConnector;

namespace IODataModule.Services;

public class PatternEventLoader
{
    public PatternEventLoader(
        ICandleAccessService candleAccessService,
        KafkaParameters kafkaParameters,
        ILogger<PatternEventLoader> logger)
    {
        _candleAccessService = candleAccessService;
        _logger = logger;

        var cc = kafkaParameters.GetClientConfig();
        var config = new ProducerConfig(cc)
        {
            AllowAutoCreateTopics = true,
            LingerMs = 5,
            CompressionType = CompressionType.Lz4,
        };

        _producer = new ProducerBuilder<Null, NewCandleEvent>(config)
            .SetValueSerializer(new MyProtoSerializer<NewCandleEvent>())
            .Build();
    }


    public async Task StartSendCandles()
    {
        var exchange = Exchange.BinanceFutures;
        var userId = Guid.Parse("9b257c73-241c-438e-9d2d-46834afcb244");

        var pairs = new List<Pair> {new("BTC", "USDT"), new("ETH", "USDT"), new("BNB", "USDT"), new("LTC", "USDT")};
        var timeFrames = new List<TimeFrame> {TimeFrame.h4, TimeFrame.h1};
        var pairsAndTimeFrames = pairs.Zip(timeFrames, (pair, timeFrame) => (pair, timeFrame)).ToList();


        foreach (var (pair, timeFrame) in pairsAndTimeFrames)
        {
            var candles = await _candleAccessService.GetRange(userId, exchange, pair, timeFrame,
                DateTime.Now.AddMonths(-24),
                DateTime.Now);
            foreach (var candle in candles)
            {
                await Handle(candle, exchange, pair, timeFrame);
            }

        }
    }

    private async Task Handle(Candle candle, Exchange exchange, Pair pair, TimeFrame timeFrame)
    {
        var candleEvent = new NewCandleEvent(candle, pair, timeFrame, exchange, candle.TimeStamp);

        try
        {
            await _producer.ProduceAsync(TopicName, new Message<Null, NewCandleEvent> {Value = candleEvent});
        }
        catch (ProduceException<long, NewCandleEvent> e)
        {
            _logger.LogCritical(e.ToString());
            throw;
        }
    }


    private readonly IProducer<Null, NewCandleEvent> _producer;
    private readonly ICandleAccessService _candleAccessService;
    private readonly ILogger<PatternEventLoader> _logger;
    private static readonly string TopicName = Topic.CandlesForLoadPatterns.ToString();
}