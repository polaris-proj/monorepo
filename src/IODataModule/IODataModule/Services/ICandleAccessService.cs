﻿namespace IODataModule.Services;

public interface ICandleAccessService
{
    Task<Candle[]> GetRange(Guid userId, Exchange exchange, Pair pair, TimeFrame timeFrame, DateTime startTime,
        DateTime endTime, CancellationToken cancellationToken = default);

    IAsyncEnumerable<Candle> GetRangeStream(Guid userId, Exchange exchange, Pair pair, TimeFrame timeFrame,
        DateTime startTime, DateTime endTime, CancellationToken ct = default);
}