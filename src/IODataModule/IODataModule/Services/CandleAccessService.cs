﻿using System.Diagnostics;
using System.Runtime.CompilerServices;
using Domain.Connectors;
using Infrastructure.Extensions;
using IODataModule.Infrastructure.DAL.Repositories;
using IODataModule.Infrastructure.Mappers;
using IODataModule.Services.ExchangeConnectors;

namespace IODataModule.Services;

internal class CandleAccessService(
    ICandleRepository candleRepository,
    IConnectorFactory connectorFactory,
    ILoggerFactory? logger = null
)
    : ICandleAccessService
{
    public async Task<Candle[]> GetRange(Guid userId, Exchange exchange, Pair pair, TimeFrame timeFrame,
        DateTime startTime, DateTime endTime, CancellationToken ct = default)
    {
        //try to reduce window
        var reducedEndTime = Math.Min(DateTime.UtcNow.ToMilliseconds(), endTime.ToMilliseconds());
        var sw = Stopwatch.StartNew();

        var candles = await candleRepository
            .GetRangeStream((exchange, pair, timeFrame), startTime.ToMilliseconds(), reducedEndTime, ct)
            .ToListAsync(ct);

        Console.WriteLine($"Время получ свечей из бд {sw.ElapsedMilliseconds}ms");
        sw.Reset();
        var expectedAmountOfCandles = (reducedEndTime - startTime.ToMilliseconds()) / ((long) timeFrame * 1000);

        if (expectedAmountOfCandles > candles.Count)
        {
            if (candles.Count > 0) //если нужно добавить данных в бд
            {
                var minTimeStamp = long.MaxValue;
                var maxTimeStamp = long.MinValue;

                if (candles.Count > 0)
                {
                    maxTimeStamp = candles.Max(x => x.TimeStamp);
                    minTimeStamp = candles.Min(x => x.TimeStamp);
                }

                Console.WriteLine($"Нашли мин + макс {sw.ElapsedMilliseconds}ms");
                sw.Restart();

                var isOldDataSaved = await candleRepository.IsOldDataSaved(exchange, pair, timeFrame, ct);
                _logger?.LogTrace($"Получили инфу о старых данных{sw.ElapsedMilliseconds}ms");
                sw.Reset();

                if (!isOldDataSaved && Math.Abs(minTimeStamp - startTime.ToMilliseconds()) >= (long) timeFrame * 1000)
                {
                    var startTimeArg = startTime;
                    var endTimeArg = (minTimeStamp - 1).ToDateTime();

                    var partOfCandles = await GetCandlesAndSaveToDatabase(
                        exchange, userId, timeFrame, pair, startTimeArg, endTimeArg, ct);

                    var expectedAmountOfCandlesInPart =
                        (endTimeArg.ToMilliseconds() - startTimeArg.ToMilliseconds()) / ((long) timeFrame * 1000);

                    if (expectedAmountOfCandlesInPart > partOfCandles.Count)
                        await candleRepository.SetOldDataSaved(exchange, pair, timeFrame, ct);

                    candles.AddRange(partOfCandles);
                }

                if (Math.Abs(maxTimeStamp - reducedEndTime) >= (long) timeFrame * 1000)
                {
                    var startTimeArg = (maxTimeStamp + 1).ToDateTime();
                    var endTimeArg = reducedEndTime.ToDateTime();

                    var partOfCandles = await GetCandlesAndSaveToDatabase(
                        exchange, userId, timeFrame, pair, startTimeArg, endTimeArg, ct);
                    candles.AddRange(partOfCandles);
                }
            }
            else
            {
                var partOfCandles = await GetCandlesAndSaveToDatabase(
                    exchange, userId, timeFrame, pair, startTime, reducedEndTime.ToDateTime(), ct);

                var expectedAmountOfCandlesInPart =
                    (reducedEndTime - startTime.ToMilliseconds()) / ((long) timeFrame * 1000);

                if (expectedAmountOfCandlesInPart > partOfCandles.Count)
                    await candleRepository.SetOldDataSaved(exchange, pair, timeFrame, ct);

                candles.AddRange(partOfCandles);
            }
        }

        sw.Restart();
        candles.Sort(Comparer<Candle>.Create((x, y) => x.TimeStamp.CompareTo(y.TimeStamp)));
        Console.WriteLine($"Сортанули свечи {sw.ElapsedMilliseconds}ms");


        return candles.ToArray();
    }

    public async IAsyncEnumerable<Candle> GetRangeStream(Guid userId, Exchange exchange, Pair pair, TimeFrame timeFrame,
        DateTime startTime, DateTime endTime, [EnumeratorCancellation] CancellationToken ct = default)
    {
        //try to reduce window
        var reducedEndTime = Math.Min(DateTime.UtcNow.ToMilliseconds(), endTime.ToMilliseconds());

        var isOldDataSaved = await candleRepository.IsOldDataSaved(exchange, pair, timeFrame, ct);
        if (isOldDataSaved)
        {
            var candlesFromDb = await candleRepository.GetRangeStream((exchange, pair, timeFrame), startTime.ToMilliseconds(), reducedEndTime, ct)
                    .ToListAsync(cancellationToken: ct);

            foreach (var candle in candlesFromDb)
                yield return candle;

            var maxTimestampInDb = await candleRepository.GetMaxTimestamp(exchange, pair, timeFrame, ct);
            if (maxTimestampInDb + (long) timeFrame * 1000 <= reducedEndTime)
            {
                var candles = await GetRange(userId, exchange, pair, timeFrame, (maxTimestampInDb + 1).ToDateTime(),
                    reducedEndTime.ToDateTime(), ct);
                foreach (var candle in candles)
                    yield return candle;
            }
        }
        else
        {
            var candles = await GetRange(userId, exchange, pair, timeFrame, startTime, endTime, ct);
            foreach (var candle in candles)
            {
                yield return candle;
                ct.ThrowIfCancellationRequested();
            }
        }
    }

    private async Task<List<Candle>> GetCandlesAndSaveToDatabase(Exchange exchange, Guid userId,
        TimeFrame timeFrame, Pair pair, DateTime startTime, DateTime endTime, CancellationToken ct)
    {
        var sw = Stopwatch.StartNew();
        var connector = await GetConnectorService(userId, exchange);
        var answer = await connector.GetCandles(pair, timeFrame, new DateTimeRange(startTime, endTime));

        Console.WriteLine($"Забрали свечи с биржи {sw.ElapsedMilliseconds}ms");
        sw.Restart();

        var candleModels = answer.Select(x => x.ToDbo(timeFrame)).ToList();
        Console.WriteLine($"мапнули свечи {sw.ElapsedMilliseconds}ms");
        sw.Restart();
        if (candleModels.Count > 0)
        {
            await candleRepository.SaveCandles(exchange, pair, candleModels, ct); //cache to db
            Console.WriteLine($"Залили свечи в бд {sw.ElapsedMilliseconds}ms");
            sw.Restart();
        }

        return answer.ToList();
    }

    private async Task<IConnector> GetConnectorService(Guid userId, Exchange exchange)
    {
        return exchange.IsSpot()
            ? await connectorFactory.GetSpotConnectorAsync(userId, exchange)
            : await connectorFactory.GetFuturesConnectorAsync(userId, exchange);
    }

    private readonly ILogger? _logger = logger?.CreateLogger<CandleAccessService>();
}