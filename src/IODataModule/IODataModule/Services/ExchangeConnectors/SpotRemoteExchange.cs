﻿using Domain.Connectors;
using Domain.StrategyProcessor;

namespace IODataModule.Services.ExchangeConnectors;

internal class SpotRemoteExchange(IConnectorFactory connectorFactory)
    : BaseRemoteExchange(async (x, y) => await connectorFactory.GetSpotConnectorAsync(x, y))
{
    public async Task<OrderId> CreateMarketOrder(Exchange exchange, Guid userId, Pair pair, MarketOrder orderType,
        decimal amount)
    {
        var connector = await connectorFactory.GetSpotConnectorAsync(userId, exchange);
        return await connector.CreateMarketOrder(pair, orderType, amount);
    }

    public async Task<OrderId> CreateLimitOrder(Exchange exchange, Guid userId, Pair pair, LimitOrder orderType,
        PlacementOrder entry)
    {
        var connector = await connectorFactory.GetSpotConnectorAsync(userId, exchange);
        return await connector.CreateLimitOrder(pair, orderType, entry);
    }
}