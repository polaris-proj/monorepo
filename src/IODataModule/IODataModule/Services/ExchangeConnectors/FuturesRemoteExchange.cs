﻿using Domain.Connectors;
using Domain.StrategyProcessor;

namespace IODataModule.Services.ExchangeConnectors;

internal class FuturesRemoteExchange(IConnectorFactory connectorFactory)
    : BaseRemoteExchange(async (x, y) => await connectorFactory.GetFuturesConnectorAsync(x, y))
{
    public async Task<OrderId> CreateFuturesOrder(Exchange exchange, Guid userId, Pair pair, FuturesOrder orderType,
        int leverage, PlacementOrder entry, PlacementOrder? take = null, PlacementOrder? stop = null)
    {
        var connector = await connectorFactory.GetFuturesConnectorAsync(userId, exchange);
        return await connector.CreateFuturesOrder(pair, orderType, leverage, entry, take, stop);
    }
}