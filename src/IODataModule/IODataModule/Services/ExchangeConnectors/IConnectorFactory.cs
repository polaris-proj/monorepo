﻿using ExchangeConnectors.Connectors;

namespace IODataModule.Services.ExchangeConnectors;

public interface IConnectorFactory
{
    Task<IInternalSpotConnector> GetSpotConnectorAsync(Guid userId, Exchange exchange);
    Task<IInternalFuturesConnector> GetFuturesConnectorAsync(Guid userId, Exchange exchange);
}