using Domain.Connectors;
using Domain.StrategyProcessor;

namespace IODataModule.Services.ExchangeConnectors;

internal abstract class BaseRemoteExchange : IBaseRemoteExchange
{
    internal BaseRemoteExchange(Func<Guid, Exchange, Task<IConnector>> getConnector)
    {
        _getConnector = getConnector;
    }

    public async Task<IReadOnlyList<Balance>> GetCoinsAmount(Exchange exchange, Guid userId)
    {
        var connector = await _getConnector(userId, exchange);
        return await connector.GetCoinsAmount();
    }

    public async Task<Dictionary<Pair, decimal>> GetPrices(Exchange exchange, Guid userId)
    {
        var connector = await _getConnector(userId, exchange);
        return await connector.GetPrices();
    }

    public async Task<IReadOnlyList<Candle>> GetCandles(Exchange exchange, Guid userId, Pair pair,
        TimeFrame timeFrame, DateTimeRange range, CancellationToken ct = default)
    {
        var connector = await _getConnector(userId, exchange);

        ct.ThrowIfCancellationRequested();
        return await connector.GetCandles(pair, timeFrame, range);
    }

    public async Task<IReadOnlyList<Order>> GetCurrentOrdersPerPair(Exchange exchange, Guid userId, Pair pair)
    {
        var connector = await _getConnector(userId, exchange);
        return await connector.GetOrdersPerPair(pair);
    }

    public async Task<Order> GetOrderInfo(Exchange exchange, Guid userId, Pair pair, OrderId orderId)
    {
        var connector = await _getConnector(userId, exchange);
        return await connector.GetOrderInfo(pair, orderId);
    }

    public async Task CancelOrder(Exchange exchange, Guid userId, Pair pair, OrderId orderId)
    {
        var connector = await _getConnector(userId, exchange);
        await connector.CancelOrder(pair, orderId);
    }
    
    private readonly Func<Guid, Exchange, Task<IConnector>> _getConnector;
}