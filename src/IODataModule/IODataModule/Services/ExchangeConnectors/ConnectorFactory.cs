﻿using ExchangeConnectors.Connectors;
using IODataModule.Infrastructure.DAL.Repositories;
using IODataModule.Shared.Dto;
using Microsoft.Extensions.Caching.Memory;

namespace IODataModule.Services.ExchangeConnectors;

internal class ConnectorFactory(
    IUserExchangeKeysRepository userExchangeKeysRepository,
    IMemoryCache memoryCache,
    ILoggerFactory loggerFactory
)
    : IConnectorFactory
{
    public async Task<IInternalSpotConnector> GetSpotConnectorAsync(Guid userId, Exchange exchange)
    {
        if (!exchange.IsSpot())
        {
            throw new ArgumentOutOfRangeException($"{exchange} should be spot exchange");
        }

        _logger.LogTrace($"Creating {exchange.ToString()} connector for {userId}");

        if (!memoryCache.TryGetValue((exchange, userId), out ExchangeKeyDto? keyInfo))
        {
            keyInfo = await userExchangeKeysRepository.GetExchangeKeyAsync(userId, exchange);
            if (keyInfo is null)
            {
                _logger.LogWarning($"Can't get keys for {userId} and {exchange}");
                throw new ArgumentNullException($"Can't get keys for {userId} and {exchange}");
            }
        }

        memoryCache.Set((exchange, userId), keyInfo, TimeSpan.FromMinutes(15));
        return GetSpotConnector(exchange, keyInfo!.Key, keyInfo.Secret);
    }

    public async Task<IInternalFuturesConnector> GetFuturesConnectorAsync(Guid userId, Exchange exchange)
    {
        if (!exchange.IsFutures())
        {
            _logger.LogWarning($"{exchange} should be futures exchange");
            throw new ArgumentOutOfRangeException($"{exchange} should be futures exchange");
        }

        _logger.LogTrace($"Creating {exchange.ToString()} connector for {userId}");

        if (!memoryCache.TryGetValue((exchange, userId), out ExchangeKeyDto? keyInfo))
        {
            keyInfo = await userExchangeKeysRepository.GetExchangeKeyAsync(userId, exchange);
            if (keyInfo is null)
            {
                _logger.LogWarning($"Can't get keys for {userId} and {exchange}");
                throw new ArgumentNullException($"Can't get keys for {userId} and {exchange}");
            }
        }

        memoryCache.Set((exchange, userId), keyInfo, TimeSpan.FromMinutes(15));
        return GetFuturesConnector(exchange, keyInfo!.Key, keyInfo.Secret);
    }

    private IInternalSpotConnector GetSpotConnector(Exchange exchange, string key, string secret)
    {
        return exchange switch
        {
            Exchange.BinanceSpot => new BinanceSpotConnector(key, secret, loggerFactory),
            Exchange.BinanceSpotTest => new BinanceSpotConnector(key, secret, loggerFactory, isTestApi: true),
            _ => throw new ArgumentOutOfRangeException(nameof(exchange), exchange, null)
        };
    }

    private IInternalFuturesConnector GetFuturesConnector(Exchange exchange, string key, string secret)
    {
        return exchange switch
        {
            Exchange.BinanceFutures => new BinanceFuturesConnector(key, secret, loggerFactory),
            Exchange.BinanceFuturesTest => new BinanceFuturesConnector(key, secret, loggerFactory, true),
            _ => throw new ArgumentOutOfRangeException(nameof(exchange), exchange, null)
        };
    }


    private readonly ILogger _logger = loggerFactory.CreateLogger<ConnectorFactory>();
}