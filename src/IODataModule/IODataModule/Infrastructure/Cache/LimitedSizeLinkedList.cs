﻿/*namespace IODataModule.Infrastructure.Cache;

//todo потокобезопасным сделать
public class LimitedSizeLinkedList<T>
{
    private readonly LinkedList<T> _list;
    private readonly int _limit;

    public LimitedSizeLinkedList(int limit)
    {
        _limit = limit;
        _list = new LinkedList<T>();
    }

    public void AddLast(T item)
    {
        if (_limit == 0) return;
        if (_list.Count == _limit)
            _list.RemoveFirst();
        _list.AddLast(item);
    }

    public T GetLast()
    {
        if (_list.Last == null)
            throw new Exception("Queue is empty");
        return _list.Last.Value;
    }

    public T GetFirst()
    {
        if (_list.First == null)
            throw new Exception("Queue is empty");
        return _list.First.Value;
    }

    public IEnumerable<T> GetAllValues()
    {
        return _list;
    }

    public int Count => _list.Count;
}*/