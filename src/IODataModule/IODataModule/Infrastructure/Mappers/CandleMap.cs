﻿using IODataModule.Infrastructure.DAL.Models;

namespace IODataModule.Infrastructure.Mappers;

public static class CandleMap
{
    public static Candle ToModel(this CandleDbo dbo) =>
        new(dbo.TimeStamp, dbo.Open, dbo.High, dbo.Low, dbo.Close, dbo.Volume);

    public static CandleDbo ToDbo(this Candle candle, TimeFrame timeFrame) =>
        new(timeFrame, candle.TimeStamp, candle.Open, candle.High, candle.Low, candle.Close, candle.Volume);
}