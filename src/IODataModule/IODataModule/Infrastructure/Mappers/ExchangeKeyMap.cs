﻿using IODataModule.Infrastructure.DAL.Models;
using IODataModule.Shared.Dto;

namespace IODataModule.Infrastructure.Mappers;

public static class ExchangeKeyMap
{
    public static ExchangeKeyDto ToDto(this ExchangeKeyDbo x)
    {
        return new ExchangeKeyDto
        {
            Exchange = x.Exchange,
            Key = x.Key,
            Secret = x.Token,
        };
    }

    public static ExchangeKeyDbo ToDbo(this ExchangeKeyDto x)
    {
        return new ExchangeKeyDbo
        {
            Exchange = x.Exchange,
            Key = x.Key,
            Token = x.Secret,
        };
    }
}