﻿namespace IODataModule.Infrastructure.DAL.Models;

public class UserExchangeKeysDbo
{
    public Guid UserId { get; set; }

    public ICollection<ExchangeKeyDbo> ExchangeKeys { get; set; } = new List<ExchangeKeyDbo>();
}