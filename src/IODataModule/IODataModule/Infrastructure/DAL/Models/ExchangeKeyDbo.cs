﻿using System.ComponentModel.DataAnnotations;

namespace IODataModule.Infrastructure.DAL.Models;

public class ExchangeKeyDbo
{
    public Guid Id { get; set; }
    public required Exchange Exchange { get; init; }
    [StringLength(100)] public required string Key { get; init; }
    [StringLength(100)] public required string Token { get; init; }
    
    public UserExchangeKeysDbo? UserExchangeKeys { get; set; }
}