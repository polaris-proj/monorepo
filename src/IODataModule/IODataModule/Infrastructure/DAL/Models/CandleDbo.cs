﻿namespace IODataModule.Infrastructure.DAL.Models;

public record CandleDbo
{
    public CandleDbo()
    {
        
    }
    public CandleDbo(TimeFrame TimeFrame, long TimeStamp, decimal Open, decimal High, decimal Low, decimal Close, decimal Volume)
    {
        this.TimeFrame = TimeFrame;
        this.TimeStamp = TimeStamp;
        this.Open = Open;
        this.High = High;
        this.Low = Low;
        this.Close = Close;
        this.Volume = Volume;
    }
    
    public TimeFrame TimeFrame { get; init; }
    public long TimeStamp { get; init; }
    public decimal Open { get; init; }
    public decimal High { get; init; }
    public decimal Low { get; init; }
    public decimal Close { get; init; }
    public decimal Volume { get; init; }
}