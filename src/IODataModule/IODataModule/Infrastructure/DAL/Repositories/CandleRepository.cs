﻿using System.Data;
using System.Runtime.CompilerServices;
using Dapper;
using IODataModule.Infrastructure.DAL.Models;
using Npgsql;
using PostgreSQLCopyHelper;


namespace IODataModule.Infrastructure.DAL.Repositories;

public class CandleRepository(NpgsqlDataSource dataSource) : ICandleRepository
{
    public async Task SaveCandle(Exchange exchange, Pair pair, CandleDbo candle, CancellationToken ct = default)
    {
        var tableName = GetTableName(exchange, pair);
        await using var connection = dataSource.CreateConnection();
        var createTable = CreateTableIfNotExist(connection, tableName, ct);
        var data = new
        {
            // ReSharper disable RedundantAnonymousTypePropertyName
            TimeFrame = candle.TimeFrame,
            TimeStamp = candle.TimeStamp,
            Open = candle.Open,
            High = candle.High,
            Low = candle.Low,
            Close = candle.Close,
            Volume = candle.Volume,
        }; // ReSharper restore RedundantAnonymousTypePropertyName

        var insertQuery = @$"
            INSERT INTO {tableName} (timeframe, timestamp, open, high, low, close, volume)
            VALUES (@TimeFrame, @TimeStamp, @Open, @High, @Low, @Close, @Volume)
            ";
        ct.ThrowIfCancellationRequested();
        await createTable;
        await connection.ExecuteAsync(insertQuery, data);
    }

    public async Task SaveCandles(Exchange exchange, Pair pair, IEnumerable<CandleDbo> candles,
        CancellationToken ct = default)
    {
        var tableName = GetTableName(exchange, pair);
        await using var connection = dataSource.CreateConnection();
        var createTable = CreateTableIfNotExist(connection, tableName, ct);
        var candlesCopyHelper = CreateCandlesCopyHelper(tableName);
        // var insertQuery = @$"
        //     INSERT INTO {tableName} (timeframe, timestamp, open, high, low, close, volume)
        //     VALUES (@{nameof(CandleDbo.TimeFrame)}, @{nameof(CandleDbo.TimeStamp)}, @{nameof(CandleDbo.Open)}, @{nameof(CandleDbo.High)}, @{nameof(CandleDbo.Low)}, @{nameof(CandleDbo.Close)}, @{nameof(CandleDbo.Volume)} )...
        //     ";
        await createTable;

        await connection.OpenAsync(ct);
        await candlesCopyHelper.SaveAllAsync(connection, candles, ct);
        //   await connection.BulkInsertAsync(insertQuery, orderDbos, batchSize:6500);
        // await _connection.BulkInsertAsync(insertQuery, candles, batchSize: 9_000);
    }

    public async IAsyncEnumerable<Candle> GetRangeStream((Exchange, Pair, TimeFrame) key, long start, long end,
        [EnumeratorCancellation] CancellationToken ct = default)
    {
        if (start > end)
            throw new ArgumentException("Start can't be more than end");

        var tableName = GetTableName(key.Item1, key.Item2);
        await using var connection = dataSource.CreateConnection();
        await CreateTableIfNotExist(connection, tableName, ct);

        var query = @$"
                       SELECT open as {nameof(Candle.Open)},
                              high as {nameof(Candle.High)},
                              low as {nameof(Candle.Low)},
                              close as {nameof(Candle.Close)},
                              volume as {nameof(Candle.Volume)},
                              timestamp as {nameof(Candle.TimeStamp)}
                       FROM {tableName}
                       WHERE timeframe = @timeFrame AND timestamp >= @start AND timestamp <= @end
                   ";
        
        var candles = connection.QueryUnbufferedAsync<Candle>(
            query,
            new
            {
                timeFrame = (int) key.Item3,
                start,
                end
            });

        await foreach (var candle in candles)
        {
            yield return candle;
            ct.ThrowIfCancellationRequested();
        }
    }

    public async Task<long> GetMaxTimestamp(Exchange exchange, Pair pair, TimeFrame timeFrame,
        CancellationToken ct = default)
    {
        var candleTableName = GetTableName(exchange, pair);
        var query = GetMaxTimestampQuery(candleTableName, timeFrame);
        ct.ThrowIfCancellationRequested();
        await using var connection = dataSource.CreateConnection();
        return await connection.ExecuteScalarAsync<long>(query);
    }

    public async Task SetOldDataSaved(Exchange exchange, Pair pair, TimeFrame timeFrame, CancellationToken ct = default)
    {
        var pairNumber = await GetOrCreateNumberForPair(pair, ct);

        var data = new
        {
            Exchange = exchange,
            PairNumber = pairNumber,
            TimeFrame = timeFrame,
            IsOldDataSaved = true
        };

        var insertQuery = @"
            INSERT INTO candlesOldDataSaved (exchange, pairNumber, timeFrame, isOldDataSaved)
            VALUES (@Exchange, @PairNumber, @TimeFrame, @IsOldDataSaved)
            ";
        ct.ThrowIfCancellationRequested();
        await using var connection = dataSource.CreateConnection();
        await connection.ExecuteAsync(insertQuery, data);
    }

    public async Task<bool> IsOldDataSaved(Exchange exchange, Pair pair, TimeFrame timeFrame,
        CancellationToken ct = default)
    {
        var pairNumber = await GetOrCreateNumberForPair(pair, ct);

        var searchQuery = @"
            SELECT EXISTS (
                SELECT 1
                FROM candlesOldDataSaved
                WHERE exchange = @Exchange
                    AND pairNumber = @PairNumber
                    AND timeFrame = @TimeFrame
                    AND isOldDataSaved = true
            )";
        ct.ThrowIfCancellationRequested();

        await using var connection = dataSource.CreateConnection();
        var isOldDataSaved = await connection.ExecuteScalarAsync<bool>(searchQuery, new
        {
            Exchange = exchange,
            PairNumber = pairNumber,
            TimeFrame = timeFrame
        });

        return isOldDataSaved;
    }

    public async Task CreateTableIfNotExist(Exchange exchange, Pair pair, CancellationToken ct = default)
    {
        var tableName = GetTableName(exchange, pair);
        await using var connection = dataSource.CreateConnection();
        await CreateTableIfNotExist(connection, tableName, ct);
    }

    private static async Task CreateTableIfNotExist(IDbConnection connection, string tableName, CancellationToken ct = default)
    {
        var queries = new[]
        {
            $@"
                    CREATE TABLE IF NOT EXISTS {tableName} (
                    timeFrame INT,
                    timestamp BIGINT,
                    open numeric,
                    high numeric,
                    low numeric,
                    close numeric,
                    volume numeric,
                    PRIMARY KEY (timeFrame, timestamp)
                );
            ",
            $@"
                     CREATE INDEX IF NOT EXISTS {tableName}_timeFrame_timestamp_idx
                            ON {tableName} (timeFrame, timestamp)
            ",
        };

        foreach (var query in queries)
        {
            await connection.ExecuteAsync(query);
            ct.ThrowIfCancellationRequested();
        }
    }

    private static PostgreSQLCopyHelper<CandleDbo> CreateCandlesCopyHelper(string tableName)
    {
        return new PostgreSQLCopyHelper<CandleDbo>(tableName)
            .MapInteger(nameof(CandleDbo.TimeFrame).ToLower(), x => (int) x.TimeFrame)
            .MapBigInt(nameof(CandleDbo.TimeStamp).ToLower(), x => x.TimeStamp)
            .MapNumeric(nameof(CandleDbo.Open).ToLower(), x => x.Open)
            .MapNumeric(nameof(CandleDbo.High).ToLower(), x => x.High)
            .MapNumeric(nameof(CandleDbo.Low).ToLower(), x => x.Low)
            .MapNumeric(nameof(CandleDbo.Close).ToLower(), x => x.Close)
            .MapNumeric(nameof(CandleDbo.Volume).ToLower(), x => x.Volume);
    }

    private async Task<int> GetOrCreateNumberForPair(Pair pair, CancellationToken ct = default)
    {
        await using var connection = dataSource.CreateConnection();
        var searchQuery = "SELECT pairNumber FROM pairToNumber WHERE pair = @Pair";
        var insertQuery = "INSERT INTO pairToNumber (pair) VALUES (@Pair) RETURNING pairNumber";


        var result = await connection.ExecuteScalarAsync<int?>(searchQuery, new {Pair = pair.ToString()});
        ct.ThrowIfCancellationRequested();
        if (result is not null)
            return result.Value;

        return await connection.ExecuteScalarAsync<int>(insertQuery, new {Pair = pair.ToString()});
    }

    private static string GetMaxTimestampQuery(string candleTableName, TimeFrame timeFrame)
    {
        return @$"
                      SELECT Max(Timestamp)
                       FROM {candleTableName}
                       WHERE timeFrame = {(int) timeFrame}
                   ";
    }

    private static string GetTableName(Exchange exchange, Pair pair) =>
        $"candles_{exchange.ToString()}_{pair.ToStringWithOutSlash()}".ToLower();
}