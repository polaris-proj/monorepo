﻿using IODataModule.Infrastructure.DAL.Models;


namespace IODataModule.Infrastructure.DAL.Repositories;

public interface ICandleRepository
{
    Task SaveCandle(Exchange exchange, Pair pair, CandleDbo candle, CancellationToken ct = default);
    Task SaveCandles(Exchange exchange, Pair pair, IEnumerable<CandleDbo> candles, CancellationToken ct = default);

    IAsyncEnumerable<Candle> GetRangeStream((Exchange, Pair, TimeFrame) key, long start, long end,
        CancellationToken ct = default);

    Task SetOldDataSaved(Exchange exchange, Pair pair, TimeFrame timeFrame, CancellationToken ct = default);
    Task<bool> IsOldDataSaved(Exchange exchange, Pair pair, TimeFrame timeFrame, CancellationToken ct = default);
    Task CreateTableIfNotExist(Exchange exchange, Pair pair, CancellationToken ct = default);
    Task<long> GetMaxTimestamp(Exchange exchange, Pair pair, TimeFrame timeFrame, CancellationToken ct = default);
}