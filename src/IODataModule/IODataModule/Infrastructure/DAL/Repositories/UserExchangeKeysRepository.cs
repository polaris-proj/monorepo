﻿using IODataModule.Infrastructure.DAL.Models;
using IODataModule.Infrastructure.Mappers;
using IODataModule.Shared.Dto;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using ReadOnlyCollectionsExtensions;

namespace IODataModule.Infrastructure.DAL.Repositories;

internal class UserExchangeKeysRepository(
    IDbContextFactory<IoDataModuleContext> pooledFactory,
    ILogger<UserExchangeKeysRepository> logger)
    : IUserExchangeKeysRepository
{
    public async Task<ExchangeKeyDto?> GetExchangeKeyAsync(Guid userId, Exchange exchange)
    {
        return (await GetAllExchangeKeysAsync(userId)).SingleOrDefault(x => x.Exchange == exchange);
    }

    public async Task<IReadOnlyCollection<ExchangeKeyDto>> GetAllExchangeKeysAsync(Guid userId)
    {
        await using var context = await pooledFactory.CreateDbContextAsync();
        return (await GetDbEntryAsync(context, userId))?.ExchangeKeys.Select(x => x.ToDto()).ToReadOnlyList() ??
               Array.Empty<ExchangeKeyDto>();
    }

    public async Task UpsertExchangeKeyAsync(Guid userId, ExchangeKeyDto key, CancellationToken? ctn = null)
    {
        ctn ??= new CancellationTokenSource(TimeSpan.FromMilliseconds(1000)).Token;
        var ct = ctn.Value;
        while (!ct.IsCancellationRequested)
        {
            await using var context = await pooledFactory.CreateDbContextAsync(ct);

            try
            {
                await UpsertExchangeKeyInnerAsync(userId, key, context, ct);
                return;
            }
            catch (DbUpdateException)
            {
                logger.LogTrace("Upsert fail, retrying...");
                await Task.Delay(Random.Shared.Next(0, 5), ct);
            }
        }

        logger.LogError($"Upsert failed. Timeout reached: {ct.IsCancellationRequested}");
    }

    private static async Task UpsertExchangeKeyInnerAsync(Guid userId, ExchangeKeyDto key, IoDataModuleContext context,
        CancellationToken ct)
    {
        var allKeys = await GetDbEntryAsync(context, userId, ct);
        if (allKeys is null)
        {
            allKeys = new UserExchangeKeysDbo
            {
                UserId = userId,
            };
            await context.AddAsync(allKeys, ct);
            await context.SaveChangesAsync(ct);
        }

        var entryToRemove = allKeys.ExchangeKeys.SingleOrDefault(x => x.Exchange == key.Exchange);
        if (entryToRemove != null)
        {
            allKeys.ExchangeKeys.Remove(entryToRemove);
        }

        allKeys.ExchangeKeys.Add(key.ToDbo());


        await context.SaveChangesAsync(ct);
    }

    public async Task RemoveExchangeKeyAsync(Guid userId, Exchange exchange, CancellationToken? ctn = null)
    {
        ctn ??= new CancellationTokenSource(TimeSpan.FromMilliseconds(1000)).Token;
        var ct = ctn.Value;
        await using var context = await pooledFactory.CreateDbContextAsync(ct);

        while (!ct.IsCancellationRequested)
        {
            try
            {
                var allKeys = await GetDbEntryAsync(context, userId, ct);

                var entryToRemove = allKeys?.ExchangeKeys.SingleOrDefault(x => x.Exchange == exchange);
                if (entryToRemove is null)
                {
                    return;
                }

                allKeys?.ExchangeKeys.Remove(entryToRemove);
                await context.SaveChangesAsync(ct);
            }

            catch (PostgresException)
            {
                logger.LogTrace("Delete fail, retrying...");
                await Task.Delay(Random.Shared.Next(0, 5), ct);
            }
        }

        logger.LogError($"Delete failed. Timeout reached: {ct.IsCancellationRequested}");
    }

    private static async Task<UserExchangeKeysDbo?> GetDbEntryAsync(IoDataModuleContext context, Guid userId,
        CancellationToken ct = default)
    {
        return await context.UserExchangeKeys
            .Include(x => x.ExchangeKeys)
            .SingleOrDefaultAsync(x => x.UserId == userId, cancellationToken: ct);
    }
}