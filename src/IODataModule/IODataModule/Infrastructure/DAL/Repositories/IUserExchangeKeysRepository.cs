﻿using IODataModule.Shared.Dto;

namespace IODataModule.Infrastructure.DAL.Repositories
{
    public interface IUserExchangeKeysRepository
    {
        Task<ExchangeKeyDto?> GetExchangeKeyAsync(Guid userId, Exchange exchange);
        Task<IReadOnlyCollection<ExchangeKeyDto>> GetAllExchangeKeysAsync(Guid userId);
        Task UpsertExchangeKeyAsync(Guid userId, ExchangeKeyDto key, CancellationToken? ctn = null);
        Task RemoveExchangeKeyAsync(Guid userId, Exchange exchange, CancellationToken? ctn = null);
    }
}