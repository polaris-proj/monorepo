﻿using Dapper;
using Infrastructure;
using IODataModule.Infrastructure.DAL.Repositories;
using IODataModule.Shared.Dto;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace IODataModule.Infrastructure.DAL;

internal class Migrations(
        NpgsqlDataSource connection,
        IUserExchangeKeysRepository userExchangeKeysRepository,
        IDbContextFactory<IoDataModuleContext> contextFactory,
        ILogger<Migrations> logger
    )
    : StartUpService
{
    public override async Task Start()
    {
        await MigrateUserKeys(userExchangeKeysRepository, contextFactory, logger);//should be first bcs ef check any table for exist to detect old database or not 

        await CreateCandlesTables();
    }

    private static async Task MigrateUserKeys(IUserExchangeKeysRepository userExchangeKeysRepository,
        IDbContextFactory<IoDataModuleContext> contextFactory, ILogger<Migrations> logger)
    {
        logger.LogTrace("Applying efCore Migrations");
        await using (var context = await contextFactory.CreateDbContextAsync())
        {
            await context.Database.EnsureCreatedAsync();
            await context.Database.MigrateAsync();
        }
        logger.LogTrace("Apply efCore Migrations done");
        
        await userExchangeKeysRepository.UpsertExchangeKeyAsync(Guid.Parse("9b257c73-241c-438e-9d2d-46834afcb244"),
            new ExchangeKeyDto
            {
                Exchange = Exchange.BinanceSpot,
                Key = "dm1T5kHtzkbd6v1UcIRom8qawh36XVXYlXirrTQc2pUEHxosukLtLaw6lbH4jrd9",
                Secret = "v7exYJFmHQEiKQSm5sQA3lM2VA1G2YEP5zUGZ1eDkcYM7w4QAB69OtFovUo4icbI"
            },
            new CancellationTokenSource(TimeSpan.FromSeconds(5)).Token);

        await userExchangeKeysRepository.UpsertExchangeKeyAsync(Guid.Parse("9b257c73-241c-438e-9d2d-46834afcb244"),
            new ExchangeKeyDto
            {
                Exchange = Exchange.BinanceFutures,
                Key = "dm1T5kHtzkbd6v1UcIRom8qawh36XVXYlXirrTQc2pUEHxosukLtLaw6lbH4jrd9",
                Secret = "v7exYJFmHQEiKQSm5sQA3lM2VA1G2YEP5zUGZ1eDkcYM7w4QAB69OtFovUo4icbI"
            },
            new CancellationTokenSource(TimeSpan.FromSeconds(5)).Token);
    }

    private async Task CreateCandlesTables()
    {
        var queries = new[]
        {
            """
                CREATE TABLE IF NOT EXISTS candlesOldDataSaved (
                    exchange INT,
                    pairNumber INT,
                    timeFrame INT,
                    isOldDataSaved BOOL,
                    PRIMARY KEY (exchange, pairNumber, timeFrame)
                );
            """,
            """
             CREATE TABLE IF NOT EXISTS pairToNumber (
                 pair TEXT,
                 pairNumber SERIAL,
                 PRIMARY KEY (pairNumber)
             );
            """,
            "CREATE INDEX IF NOT EXISTS idx_Pair ON pairToNumber (pair);",
        };

        var i = 0;
        foreach (var query in queries)
        {
            logger.LogTrace($"Apply dapper migrations {i++}");
            await _connection.ExecuteAsync(query);
        }
    }

    public override int Priority => int.MaxValue;

    private readonly NpgsqlConnection _connection = connection.CreateConnection();
}