﻿using IODataModule.Infrastructure.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace IODataModule.Infrastructure.DAL;

public sealed class IoDataModuleContext(DbContextOptions<IoDataModuleContext> options) : DbContext(options)
{
    public DbSet<UserExchangeKeysDbo> UserExchangeKeys { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ExchangeKeyDbo>().HasKey(x => x.Id);
        modelBuilder.Entity<UserExchangeKeysDbo>().HasKey(x => x.UserId);
        modelBuilder.Entity<UserExchangeKeysDbo>().HasMany(x=>x.ExchangeKeys).WithOne(x=>x.UserExchangeKeys);
    }
}