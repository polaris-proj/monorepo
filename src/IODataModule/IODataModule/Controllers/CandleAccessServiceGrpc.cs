﻿using System.Diagnostics;
using Infrastructure.Extensions;
using IODataModule.Services;
using IODataModule.Shared.CandleAccessService;
using IODataModule.Shared.Dto;
using IODataModule.Shared.IRemoteExchangeConnector;
using ProtoBuf.Grpc;

namespace IODataModule.Controllers;

internal class CandleAccessServiceGrpc(
    ICandleAccessService candleAccessService,
    IRemoteExchangeBaseConnectorGrpc
        remoteExchangeController, //todo заменить на метод сервиса, перед этим перенести логику выбора коннектора в сервис из контроллера
    ILogger<CandleAccessServiceGrpc> logger)
    : ICandleAccessServiceGrpc
{
    public async Task<IReadOnlyList<Candle>> GetRange(GetRangeArgument argument, CallContext context = default)
    {
        return await candleAccessService.GetRange(argument.UserId, argument.Exchange, argument.Pair,
            argument.TimeFrame,
            argument.Start.ToDateTime(), argument.End.ToDateTime());
    }

    public async IAsyncEnumerable<IEnumerable<Candle>> GetRangeStream(GetRangeArgument argument, CallContext context = default)
    {
        var sw = Stopwatch.StartNew();
        var candles = await candleAccessService.GetRange(argument.UserId, argument.Exchange, argument.Pair,
            argument.TimeFrame, argument.Start.ToDateTime(), argument.End.ToDateTime());
        logger.LogTrace($"Прочитал из бд {sw.ElapsedMilliseconds}");
        foreach (var candle in candles.Chunk(60000))
            yield return candle;
        logger.LogTrace($"отправил все {sw.ElapsedMilliseconds}");
    }

    public async Task<IReadOnlyList<Pair>> GetPairs(GetPairsArgument argument, CallContext context = default)
    {
        logger.LogTrace($"User {argument.UserId} get pairs for {argument.Exchange}");
        var prices = await remoteExchangeController.GetPrices(new ExchangeInfo(argument.UserId, argument.Exchange));
        return prices.Select(x => x.Key).ToList();
    }
}