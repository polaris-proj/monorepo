﻿using IODataModule.Infrastructure.DAL.Repositories;
using IODataModule.Shared;
using IODataModule.Shared.Dto;
using ProtoBuf.Grpc;

namespace IODataModule.Controllers;

internal class UserAccessTokenServiceGrpc(IUserExchangeKeysRepository userExchangeKeysExchangeKeysRepository)
    : IUserAccessTokenServiceGrpc
{
    public async Task AddOrUpdateExchangeKey(AddOrUpdateExchangeKey argument, CallContext context = default)
    {
        await userExchangeKeysExchangeKeysRepository.UpsertExchangeKeyAsync(argument.UserId, argument.ExchangeKey);
    }

    public async Task<ExchangeKeyDto> GetExchangeKey(ExchangeKeyArgument exchangeKey, CallContext context = default)
    {
        var key = await userExchangeKeysExchangeKeysRepository.GetExchangeKeyAsync(exchangeKey.UserId, exchangeKey.Exchange);
        return key ?? throw new Exception("Key not found");
    }

    public async Task<ExchangeKeyDto[]> GetAllExchangeKeys(UserId userId, CallContext context = default)
    {
        var userKeys = await userExchangeKeysExchangeKeysRepository.GetAllExchangeKeysAsync(userId.Id);
        return userKeys.ToArray();
    }

    public async Task RemoveExchangeKey(ExchangeKeyArgument exchangeKey, CallContext context = default)
    {
        await userExchangeKeysExchangeKeysRepository.RemoveExchangeKeyAsync(exchangeKey.UserId, exchangeKey.Exchange);
    }
}