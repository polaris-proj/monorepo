using Domain.Connectors;
using IODataModule.Services.ExchangeConnectors;
using IODataModule.Shared.IRemoteExchangeConnector;
using IODataModule.Shared.IRemoteExchangeConnector.Dto;
using ProtoBuf.Grpc;

namespace IODataModule.Controllers.ExchangeConnectors;

internal class FuturesRemoteExchangeController(FuturesRemoteExchange futuresRemoteExchange)
    : IFuturesRemoteExchangeConnectorGrpc
{
    public async Task<OrderId> CreateFuturesOrder(FuturesOrderInfo orderInfo,
        CallContext context = default)
    {
        return await futuresRemoteExchange.CreateFuturesOrder(orderInfo.Exchange, orderInfo.UserId,
            orderInfo.Pair, orderInfo.OrderType, orderInfo.Leverage, orderInfo.Entry,
            orderInfo.Take, orderInfo.Stop);
    }
}