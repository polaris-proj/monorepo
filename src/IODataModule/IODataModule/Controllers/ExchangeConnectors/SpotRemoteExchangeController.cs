using Domain.Connectors;
using IODataModule.Services.ExchangeConnectors;
using IODataModule.Shared.IRemoteExchangeConnector;
using IODataModule.Shared.IRemoteExchangeConnector.Dto;
using ProtoBuf.Grpc;

namespace IODataModule.Controllers.ExchangeConnectors;

internal class SpotRemoteExchangeController(SpotRemoteExchange spotRemoteExchange) : ISpotRemoteExchangeConnectorGrpc
{
    public async Task<OrderId> CreateMarketOrder(MarketOrderInfo marketOrderInfo, CallContext context = default)
    {
        return await spotRemoteExchange.CreateMarketOrder(marketOrderInfo.Exchange, marketOrderInfo.UserId,
            marketOrderInfo.Pair, marketOrderInfo.OrderType,
            marketOrderInfo.Amount);
    }

    public async Task<OrderId> CreateLimitOrder(LimitOrderInfo limitOrderInfo, CallContext context = default)
    {
        return await spotRemoteExchange.CreateLimitOrder(limitOrderInfo.Exchange, limitOrderInfo.UserId,
            limitOrderInfo.Pair, limitOrderInfo.OrderType, limitOrderInfo.Entry);
    }
}