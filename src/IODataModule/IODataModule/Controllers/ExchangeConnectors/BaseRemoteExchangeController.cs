using Domain.Connectors;
using Domain.StrategyProcessor;
using IODataModule.Services;
using IODataModule.Services.ExchangeConnectors;
using IODataModule.Shared.Dto;
using IODataModule.Shared.IRemoteExchangeConnector;
using ProtoBuf.Grpc;

namespace IODataModule.Controllers.ExchangeConnectors;

internal class BaseRemoteExchangeController(SpotRemoteExchange spotRemoteExchange,
        FuturesRemoteExchange futuresRemoteExchange,
        ICandleAccessService candleAccessService
        )
    : IRemoteExchangeBaseConnectorGrpc
{
    public async Task<IReadOnlyList<Balance>> GetCoinsAmount(ExchangeInfo exchangeInfo,
        CallContext context = default)
    {
        var connector = GetConnectorService(exchangeInfo.Exchange);
        return await connector.GetCoinsAmount(exchangeInfo.Exchange, exchangeInfo.UserId);
    }

    public async Task<Dictionary<Pair, decimal>> GetPrices(ExchangeInfo exchangeInfo, CallContext ctx = default)
    {
        var connector = GetConnectorService(exchangeInfo.Exchange);
        return await connector.GetPrices(exchangeInfo.Exchange, exchangeInfo.UserId);
    }

    public async Task<IReadOnlyList<Candle>> GetCandles(CandleInfo candleInfo, CallContext context = default)
    {
        return await candleAccessService.GetRange(candleInfo.UserId, candleInfo.Exchange, candleInfo.Pair,
            candleInfo.TimeFrame, candleInfo.Start, candleInfo.End);
    }

    public async Task<IReadOnlyList<Order>> GetCurrentOrdersPerPair(PairInfo pairInfo,
        CallContext context = default)
    {
        var connector = GetConnectorService(pairInfo.Exchange);
        return await connector.GetCurrentOrdersPerPair(pairInfo.Exchange, pairInfo.UserId, pairInfo.Pair);
    }

    public async Task<Order> GetOrderInfo(OrderInfo orderInfo, CallContext context = default)
    {
        var connector = GetConnectorService(orderInfo.Exchange);
        return await connector.GetOrderInfo(orderInfo.Exchange, orderInfo.UserId, orderInfo.Pair,
            orderInfo.OrderId);
    }

    public async Task CancelOrder(OrderInfo orderInfo, CallContext context = default)
    {
        var connector = GetConnectorService(orderInfo.Exchange);
        await connector.CancelOrder(orderInfo.Exchange, orderInfo.UserId, orderInfo.Pair, orderInfo.OrderId);
    }

    private IBaseRemoteExchange GetConnectorService(Exchange exchange)
    {
        return exchange.IsSpot() ? spotRemoteExchange : futuresRemoteExchange;
    }
}