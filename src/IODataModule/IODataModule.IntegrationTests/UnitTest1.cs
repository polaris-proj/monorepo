using Client;
using Domain;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;
namespace IODataModule.IntegrationTests;

public class Tests: IntegrationTestBase
{
    
    public Tests(GrpcTestFixture<Startup> fixture, ITestOutputHelper outputHelper) : base(fixture, outputHelper)
    {
    }

    [Fact]
    public async Task SayHelloUnaryTest()
    {
        var client = new BaseRemoteConnector(Guid.NewGuid(),Exchange.BinanceSpot,Channel);
        
        // Act
        var response = await client.GetPrices();

        response.Count.Should().BeGreaterThan(0);
    }

}