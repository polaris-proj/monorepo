﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using Grpc.Net.Client;
using IODataModule.Shared.Dto;
using IODataModule.Shared.IRemoteExchangeConnector;

namespace IODataModule.Client.Clients;

public abstract class BaseRemoteConnector(Guid userId, Exchange exchange, IoDataModuleClientChannel channel)
    : IConnector
{
    protected readonly GrpcChannel Channel = channel.Channel;
    protected Exchange Exchange { get; } = exchange;
    protected Guid UserId { get; } = userId;


    public async Task<IReadOnlyList<Balance>> GetCoinsAmount()
    {
        var service = Channel.CreateGrpcService<IRemoteExchangeBaseConnectorGrpc>();
        var dto = new ExchangeInfo(UserId, Exchange);
        return await service.GetCoinsAmount(dto);
    }

    public async Task<IReadOnlyList<Candle>> GetCandles(Pair pair, TimeFrame timeFrame, DateTimeRange range)
    {
        var dto = new CandleInfo(UserId, Exchange, pair, timeFrame, range.Begin, range.End);
        var service = Channel.CreateGrpcService<IRemoteExchangeBaseConnectorGrpc>();
        return await service.GetCandles(dto);
    }

    public async Task<Order> GetOrderInfo(Pair pair, OrderId id)
    {
        var dto = new OrderInfo {UserId = UserId, Exchange = Exchange, Pair = pair, OrderId = id};
        var service = Channel.CreateGrpcService<IRemoteExchangeBaseConnectorGrpc>();
        return await service.GetOrderInfo(dto);
    }

    public async Task CancelOrder(Pair pair, OrderId orderId)
    {
        var dto = new OrderInfo {UserId = UserId, Exchange = Exchange, Pair = pair, OrderId = orderId};
        var service = Channel.CreateGrpcService<IRemoteExchangeBaseConnectorGrpc>();
        await service.CancelOrder(dto);
    }

    public async Task<IReadOnlyList<Order>> GetOrdersPerPair(Pair pair)
    {
        var dto = new PairInfo {UserId = UserId, Exchange = Exchange, Pair = pair,};
        var service = Channel.CreateGrpcService<IRemoteExchangeBaseConnectorGrpc>();
        return await service.GetCurrentOrdersPerPair(dto);
    }

    public async Task<Dictionary<Pair, decimal>> GetPrices()
    {
        var dto = new ExchangeInfo(UserId, Exchange);
        var service = Channel.CreateGrpcService<IRemoteExchangeBaseConnectorGrpc>();
        return await service.GetPrices(dto);
    }

    public virtual ValueTask DisposeAsync()
    {
        return ValueTask.CompletedTask;
    }
}