﻿using System.Diagnostics;
using Domain.StrategyProcessor;
using Grpc.Net.Client;
using Infrastructure;
using IODataModule.Shared.CandleAccessService;

namespace IODataModule.Client.Clients;

public class CandleService(IoDataModuleClientChannel channel, ILogger<CandleService> logger) : ICandleService
{
    public async Task<IEnumerable<Candle>> GetRange(Guid userId, ExchangePairTimeFrame key, long start, long end)
    {
        var calculator = _channel.CreateGrpcService<ICandleAccessServiceGrpc>();
        return await calculator.GetRange(new GetRangeArgument(userId, key, start, end));
    }

    public async IAsyncEnumerable<Candle> GetRangeStream(Guid userId, ExchangePairTimeFrame key, long start, long end)
    {
        var candleAccessServiceGrpc = _channel.CreateGrpcService<ICandleAccessServiceGrpc>();
        var result = candleAccessServiceGrpc.GetRangeStream(new GetRangeArgument(userId, key, start, end));

        var sw = Stopwatch.StartNew();
        await foreach (var candlesBatch in result.Buffer(10))
        {
            foreach (var candle in candlesBatch)
                yield return candle;
            logger.LogTrace($"пачку прочитал и отправил {sw.ElapsedMilliseconds}");
        }
    }

    public async Task<IEnumerable<Pair>> GetPairs(Guid userId, Exchange exchange)
    {
        var service = _channel.CreateGrpcService<ICandleAccessServiceGrpc>();
        return await service.GetPairs(new GetPairsArgument(userId, exchange));

    }

    private readonly GrpcChannel _channel = channel.Channel;
}