﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using IODataModule.Shared.IRemoteExchangeConnector;
using IODataModule.Shared.IRemoteExchangeConnector.Dto;

namespace IODataModule.Client.Clients;

public class RemoteFuturesConnector : BaseRemoteConnector, IFuturesConnector
{
    public RemoteFuturesConnector(Guid userId, Exchange exchange, IoDataModuleClientChannel channel) : base(userId,
        exchange, channel)
    {
    }

    public async Task<OrderId> CreateFuturesOrder(Pair pair, FuturesOrder orderType, int leverage, PlacementOrder entry,
        PlacementOrder? take = null, PlacementOrder? stop = null)
    {
        var dto = new FuturesOrderInfo
        {
            Exchange = Exchange,
            UserId = UserId,
            Pair = pair,
            OrderType = orderType,
            Leverage = leverage,
            Entry = entry,
            Take = take,
            Stop = stop
        };

        var service = Channel.CreateGrpcService<IFuturesRemoteExchangeConnectorGrpc>();
        return await service.CreateFuturesOrder(dto);
    }
}