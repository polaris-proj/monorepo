﻿using Grpc.Net.Client;
using IODataModule.Shared;
using IODataModule.Shared.Dto;

namespace IODataModule.Client.Clients;

public class UserExchangeKeysClient(IoDataModuleClientChannel channel) : IUserExchangeKeysClient
{
    public async Task AddOrUpdateExchangeKey(Guid userId, Exchange exchange, string key, string secret)
    {
        var calculator = _channel.CreateGrpcService<IUserAccessTokenServiceGrpc>();
        await calculator.AddOrUpdateExchangeKey(new AddOrUpdateExchangeKey
        {
            UserId = userId,
            ExchangeKey = new ExchangeKeyDto {Exchange = exchange, Key = key, Secret = secret}
        });
    }

    public async Task<ExchangeKeyDto> GetExchangeKey(Guid userId, Exchange exchange)
    {
        var calculator = _channel.CreateGrpcService<IUserAccessTokenServiceGrpc>();
        return await calculator.GetExchangeKey(new ExchangeKeyArgument(userId, exchange));
    }

    public async Task<ExchangeKeyDto[]> GetAllExchangeKeys(Guid userId)
    {
        var calculator = _channel.CreateGrpcService<IUserAccessTokenServiceGrpc>();
        return await calculator.GetAllExchangeKeys(new UserId {Id = userId});
    }

    public async Task RemoveExchangeKey(Guid userId, Exchange exchange)
    {
        var calculator = _channel.CreateGrpcService<IUserAccessTokenServiceGrpc>();
        await calculator.RemoveExchangeKey(new ExchangeKeyArgument(userId, exchange));
    }

    private readonly GrpcChannel _channel = channel.Channel;
}