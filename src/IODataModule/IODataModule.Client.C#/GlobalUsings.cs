﻿// Global using directives

global using Domain;
global using ProtoBuf.Grpc.Client;
global using static Domain.TimeFrames;