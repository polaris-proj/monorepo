﻿using Grpc.Net.Client;
using Infrastructure.Grpc;

namespace IODataModule.Client;

public class IoDataModuleClientChannel : INamedGrpcChannelWrapper
{
    public GrpcChannel Channel { get; init; } = null!;
}