﻿namespace IODataModule.Shared;

[DataContract]
public class ExchangeKeyArgument
{
    public ExchangeKeyArgument()
    {
    }

    public ExchangeKeyArgument(Guid userId, Exchange exchange)
    {
        UserId = userId;
        Exchange = exchange;
    }

    [DataMember(Order = 1)] public Guid UserId { get; set; }
    [DataMember(Order = 2)] public Exchange Exchange { get; set; }
}