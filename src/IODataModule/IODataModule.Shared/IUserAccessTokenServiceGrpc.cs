﻿using System.ServiceModel;
using IODataModule.Shared.Dto;
using ProtoBuf.Grpc;

namespace IODataModule.Shared;

[ServiceContract]
public interface IUserAccessTokenServiceGrpc
{
    [OperationContract]
    Task<ExchangeKeyDto> GetExchangeKey(ExchangeKeyArgument exchangeKey, CallContext context = default);

    [OperationContract]
    Task<ExchangeKeyDto[]> GetAllExchangeKeys(UserId userId, CallContext context = default);

    [OperationContract]
    Task AddOrUpdateExchangeKey(AddOrUpdateExchangeKey argument, CallContext context = default);

    [OperationContract]
    Task RemoveExchangeKey(ExchangeKeyArgument exchangeKey, CallContext context = default);
}