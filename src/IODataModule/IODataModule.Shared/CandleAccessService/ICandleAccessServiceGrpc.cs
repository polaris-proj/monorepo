﻿using System.ServiceModel;
using ProtoBuf.Grpc;

namespace IODataModule.Shared.CandleAccessService;

[ServiceContract]
public interface ICandleAccessServiceGrpc
{
    [OperationContract]
    Task<IReadOnlyList<Candle>> GetRange(GetRangeArgument argument, CallContext context = default);
    
    [OperationContract]
    IAsyncEnumerable<IEnumerable<Candle>> GetRangeStream(GetRangeArgument argument, CallContext context = default);
    
    [OperationContract]
    Task<IReadOnlyList<Pair>> GetPairs(GetPairsArgument argument, CallContext context = default);
}