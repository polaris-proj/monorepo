﻿namespace IODataModule.Shared.Dto;

[DataContract]
public class ExchangeInfo
{
    public ExchangeInfo(Guid userId, Exchange exchange)
    {
        UserId = userId;
        Exchange = exchange;
    }

    [DataMember(Order = 1)] public Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public Guid UserId { get; set; }

    private ExchangeInfo()//for protobuf don't delete
    {
        
    }
}