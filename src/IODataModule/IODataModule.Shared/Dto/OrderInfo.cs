﻿using Domain.Connectors;

namespace IODataModule.Shared.Dto;

[DataContract]
public class OrderInfo
{
    [DataMember(Order = 1)] public required Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public required Guid UserId { get; set; }
    [DataMember(Order = 3)] public required Pair Pair { get; set; }
    [DataMember(Order = 4)] public required OrderId OrderId { get; set; }
}