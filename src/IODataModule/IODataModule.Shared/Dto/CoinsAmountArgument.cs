﻿namespace IODataModule.Shared.Dto;

[DataContract]
public class CoinsAmountArgument
{
    public CoinsAmountArgument(Exchange exchange, Guid userId)
    {
        Exchange = exchange;
        UserId = userId;
    }
    [DataMember(Order = 1)] public Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public Guid UserId { get; set; }
}