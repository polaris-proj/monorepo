﻿#pragma warning disable CS8618

namespace IODataModule.Shared.Dto;
[DataContract]
public class CandleInfo
{
    private CandleInfo()
    {
        
    }
    public CandleInfo(Guid userId, Exchange exchange, Pair pair, TimeFrame timeFrame, DateTime start, DateTime end)
    {
        UserId = userId;
        Exchange = exchange;
        Pair = pair;
        TimeFrame = timeFrame;
        Start = start;
        End = end;
    }

    [DataMember(Order = 1)] public Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public Guid UserId { get; set; }
    [DataMember(Order = 3)]public Pair Pair { get; set; }
    [DataMember(Order = 4)]public TimeFrame TimeFrame { get; set; }
    [DataMember(Order = 5)] public DateTime Start { get; set; }
    [DataMember(Order = 6)]public DateTime End { get; set; }
}