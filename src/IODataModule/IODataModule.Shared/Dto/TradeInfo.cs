﻿#pragma warning disable CS8618
namespace IODataModule.Shared.Dto;

[DataContract]
public class TradeInfo
{
    public TradeInfo(Guid userId, Exchange exchange, Pair pair, DateTime startTime, DateTime endTime)
    {
        UserId = userId;
        Exchange = exchange;
        Pair = pair;
        StartTime = startTime;
        EndTime = endTime;
    }

    [DataMember(Order = 1)] public Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public Guid UserId { get; set; }
    [DataMember(Order = 3)] public Pair Pair { get; set; }
    [DataMember(Order = 4)] public TimeFrame TimeFrame { get; set; }
    [DataMember(Order = 5)] public DateTime StartTime { get; set; }
    [DataMember(Order = 6)] public DateTime EndTime { get; set; }
}