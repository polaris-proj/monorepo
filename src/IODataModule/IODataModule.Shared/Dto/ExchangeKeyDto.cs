﻿namespace IODataModule.Shared.Dto;

[DataContract]
public record ExchangeKeyDto
{
    [DataMember(Order = 1)] public required Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public required string Key { get; set; }
    [DataMember(Order = 3)] public required string Secret { get; set; }
}