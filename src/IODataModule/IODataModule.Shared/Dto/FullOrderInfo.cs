﻿using Domain.StrategyProcessor;

#pragma warning disable CS8618

namespace IODataModule.Shared.Dto;
[DataContract]
public class FullOrderInfo
{
    public FullOrderInfo(Guid userId, Exchange exchange, Pair pair, OrderType orderType, decimal? price, decimal amount)
    {
        UserId = userId;
        Exchange = exchange;
        Pair = pair;
        OrderType = orderType;
        Price = price;
        Amount = amount;
    }

    [DataMember(Order = 1)] public Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public Guid UserId { get; set; }
    [DataMember(Order = 3)] public Pair Pair { get; set; }
    [DataMember(Order = 4)]public OrderType OrderType { get; set; }
    [DataMember(Order = 5)]public decimal? Price { get; set; }
    [DataMember(Order = 6)] public decimal Amount { get; set; }
}