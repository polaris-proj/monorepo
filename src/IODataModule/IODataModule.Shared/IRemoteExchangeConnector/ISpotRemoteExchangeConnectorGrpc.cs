﻿using System.ServiceModel;
using Domain.Connectors;
using IODataModule.Shared.IRemoteExchangeConnector.Dto;
using ProtoBuf.Grpc;

namespace IODataModule.Shared.IRemoteExchangeConnector;

[ServiceContract]
public interface ISpotRemoteExchangeConnectorGrpc
{
    [OperationContract]
    Task<OrderId> CreateMarketOrder(MarketOrderInfo marketOrderInfo, CallContext context = default);

    [OperationContract]
    Task<OrderId> CreateLimitOrder(LimitOrderInfo limitOrderInfo, CallContext context = default);
}