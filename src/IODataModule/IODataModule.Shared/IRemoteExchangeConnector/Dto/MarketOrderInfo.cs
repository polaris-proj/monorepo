﻿using Domain.StrategyProcessor;

namespace IODataModule.Shared.IRemoteExchangeConnector.Dto;

[DataContract]
public class MarketOrderInfo
{
    [DataMember(Order = 1)] public required Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public required Guid UserId { get; set; }
    [DataMember(Order = 3)] public required Pair Pair { get; set; }
    [DataMember(Order = 4)] public required MarketOrder OrderType { get; set; }
    [DataMember(Order = 5)] public required decimal Amount { get; set; }
}