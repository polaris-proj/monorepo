﻿using System;
using Domain;

namespace Infrastructure;

public static class TimeConstants
{
    public static readonly DateTime Jan1St1970 = new(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    
    public static readonly DateTimeRange WholeDataTimeRange = new(Jan1St1970, DateTime.UtcNow.AddMinutes(1));
}