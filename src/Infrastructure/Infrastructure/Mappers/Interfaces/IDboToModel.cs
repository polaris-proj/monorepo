﻿namespace Infrastructure.Mappers.Interfaces;

public interface IDboToModel<in TIn, out TOut> : IMap
{
    TOut MapDboToModel(TIn parameterDbo);
}