﻿namespace Infrastructure.Mappers.Interfaces;

public interface IDtoToModel<in TIn, out TOut> : IMap
{
    TOut MapDtoToModel(TIn strategyStartDto);
}