﻿namespace Infrastructure.Mappers.Interfaces;

public interface IModelToDto<in TIn, out TOut> : IMap
{
    TOut MapModelToDto(TIn strategyStartEventDto);
}