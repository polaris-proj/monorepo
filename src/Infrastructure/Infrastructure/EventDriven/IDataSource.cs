﻿using System;
using System.Threading.Tasks;

namespace Infrastructure.EventDriven;

public interface IDataSource
{
    void RegisterEventDestination<T>(Action<T> action);
    void RegisterEventDestination<T>(Func<T, Task> action);
}