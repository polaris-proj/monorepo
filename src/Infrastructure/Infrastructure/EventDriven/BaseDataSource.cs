﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.EventDriven;

public abstract class BaseDataSource : IDataSource
{
    public void RegisterEventDestination<T>(Action<T> action)
    {
        RegisterEventDestination<T>(obj =>
        {
            action(obj);
            return Task.CompletedTask;
        });
    }

    public void RegisterEventDestination<T>(Func<T, Task> action)
    {
        var type = typeof(T);
        if (_dataHandlers.TryGetValue(type, out var pipeline))
        {
            pipeline.Add(async x => { await action((T) x); });
        }
        else
        {
            if (!_dataHandlers.TryAdd(type, new List<Func<object, Task>> {async x => { await action((T) x); }}))
            {
                throw new ArgumentOutOfRangeException($"Can't add handler for {nameof(T)}");
            }
        }
    }

    public void RegisterExternalEventSource<T>(IConsumerWithExternalHandlers<T> candleConsumer)
    {
        candleConsumer.RegisterHandler(SendDataToHandlers);
    }

    public async Task SendDataToHandlers<T>(T data)
    {
        var type = typeof(T);
        if (_dataHandlers.TryGetValue(type, out var handlers))
        {
            await Task.WhenAll(handlers.Select(x => x(data!)));
        }
    }

    private readonly ConcurrentDictionary<Type, List<Func<object, Task>>> _dataHandlers = new();
}