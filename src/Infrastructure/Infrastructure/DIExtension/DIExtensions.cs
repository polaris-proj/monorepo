using System;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.DIExtension;

public static class DIExtensions
{
    [Obsolete]
    public static IServiceCollection AddFactory<TService, TImplementation>(this IServiceCollection services)
        where TService : class
        where TImplementation : class, TService
    {
        services.AddScoped<TService, TImplementation>();
        services.AddSingleton<Func<TService>>(x => () => x.CreateScope().ServiceProvider.GetService<TService>() ??
                                                         throw new InvalidOperationException());
        services.AddSingleton<IFactory<TService>, Factory<TService>>();

        return services;
    }

    public static object GetGenericService(
        this IServiceProvider serviceCollection,
        Type genericType,
        params Type[] genericArguments)
    
    {
        var strategyServiceType = genericType.MakeGenericType(genericArguments);
        return serviceCollection.GetRequiredService(strategyServiceType);
    }
}