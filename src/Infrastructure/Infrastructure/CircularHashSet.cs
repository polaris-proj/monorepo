﻿using System;
using System.Collections.Generic;

namespace Infrastructure;

public class CircularHashSet<T>
{
    public CircularHashSet(int capacity)
    {
        if (capacity < 1)
        {
            throw new ArgumentException("Capacity should be positive");
        }
        _hashSet = new HashSet<T>(capacity);
        _buffer = new T[capacity];
    }

    public bool Add(T item)
    {
        var elementIndex = _offset % _buffer.Length;

        _hashSet.Remove(_buffer[elementIndex]);
        if (_offset > _buffer.Length * 2)
        {
            _offset %= _buffer.Length;
        }

        _buffer[elementIndex] = item;
        _offset++;
        return _hashSet.Add(item);
    }

    public bool Contains(T item) => _hashSet.Contains(item);

    private readonly HashSet<T> _hashSet;
    private readonly T[] _buffer;
    private int _offset;
}