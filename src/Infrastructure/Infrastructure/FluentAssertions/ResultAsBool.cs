﻿using System;

namespace Infrastructure.FluentAssertions;

public static class ResultAsBool
{
    public static bool Evaluate(Action assertion)
    {
        assertion();
        return true;
    }
}