﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastructure.DAL.Storage;

public interface IRepository<T, in TKey>
{
    IEnumerable<T> ReadAll();
    Task<List<T>> ReadAllAsync();
    Guid Create(T item);
    Task<Guid> CreateAsync(T item);
    T? Read(TKey id);
    Task<T?> ReadAsync(TKey id);
    IEnumerable<T> Read(Expression<Func<T, bool>> lambda);
    Task<IEnumerable<T>> ReadAsync(Expression<Func<T, bool>> lambda);
    void Update(T item);
    Task UpdateAsync(T item);
    void Delete(TKey id);
    Task DeleteAsync(TKey id);
    void Save();
    Task SaveAsync();
}