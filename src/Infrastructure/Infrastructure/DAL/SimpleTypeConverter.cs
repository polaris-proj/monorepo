﻿using System;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Infrastructure.DAL;

public class SimpleTypeConverter() : ValueConverter<Type, string>(v => v.FullName!, v => Type.GetType(v)!);