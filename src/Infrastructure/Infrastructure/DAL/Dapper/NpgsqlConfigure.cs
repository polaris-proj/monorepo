﻿using Dapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Npgsql;

namespace Infrastructure.DAL.Dapper;

public static class NpgsqlConfigure
{
    public static IServiceCollection ConfigureNpgSql(this IServiceCollection services, bool enableLogging = false)
    {
        services.AddSingleton<NpgsqlDataSource>(x =>
        {
            var loggerFactory = x.GetRequiredService<ILoggerFactory>();
            var logger = loggerFactory.CreateLogger(nameof(NpgsqlConfigure));
            var postgresParameters = x.GetRequiredService<PostgresParameters>();
            var connectionString = postgresParameters.ConnectionStringWithoutDatabaseName;
            var databaseName = postgresParameters.DbName;
            EnsureCreated(connectionString, databaseName, logger);

            var dataSourceBuilder = new NpgsqlDataSourceBuilder(postgresParameters.FullConnectionString);
            if (enableLogging)
            {
                dataSourceBuilder = dataSourceBuilder.UseLoggerFactory(x.GetService<ILoggerFactory>());
                dataSourceBuilder.EnableParameterLogging();
            }

            var builder = dataSourceBuilder.Build();
            return builder;
        });
        return services;
    }

    private static void EnsureCreated(string connectionString, string databaseName, ILogger logger)
    {
        try
        {
            var connection = new NpgsqlConnection(connectionString);
            var databaseExists = connection.ExecuteScalar<bool>($"SELECT 1 FROM pg_database WHERE datname = '{databaseName}'");

            if (!databaseExists)
            {
                connection.Execute($"CREATE DATABASE \"{databaseName}\"");
            }
        }
        catch (PostgresException e)
        {
            logger.LogCritical(e, "Can't create database");
        }
    }
}