﻿using System;
using System.Reflection;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Infrastructure.DAL;

public class FullTypeConverter()
    : ValueConverter<Type, string>(v => Assembly.CreateQualifiedName(v.Assembly.FullName, v.FullName),
        v => Type.GetType(v)!);