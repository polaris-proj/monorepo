﻿using System.IO;
using System.Text;
using System.Text.Json;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Infrastructure.DAL;

public class JsonConverter : ValueConverter<JsonDocument, string>
{
    public JsonConverter() : base(
        v => ToJsonString(v),
        str => JsonDocument.Parse(str, new JsonDocumentOptions()))
    {
    }

    private static string ToJsonString(JsonDocument jdoc)
    {
        using (var stream = new MemoryStream())
        {
            var writer = new Utf8JsonWriter(stream, new JsonWriterOptions {Indented = true});
            jdoc.WriteTo(writer);
            writer.Flush();
            return Encoding.UTF8.GetString(stream.ToArray());
        }
    }
}