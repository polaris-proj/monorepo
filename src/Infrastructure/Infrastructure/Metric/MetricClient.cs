﻿using System.Collections.Concurrent;
using Prometheus;

namespace Infrastructure.Metric;

public static class MetricClient
{
    private static ConcurrentDictionary<string, Gauge> _timers = new();
    private static ConcurrentDictionary<(string, string?), Counter> _counters = new();
    
    static MetricClient()
    {
        using var server = new KestrelMetricServer(7777);
        server.Start();
        //  Collector.StartCollecting();
    }

    public static void SendTimerData(string name, double measure, string? description = null)
    {
        _timers.GetOrAdd(name, x => Metrics.CreateGauge(x, description ?? string.Empty))
            .Set(measure);
    }

    public static Counter GetCounter(string name, string? description = null)
    {
        return _counters.GetOrAdd((name, description), x => Metrics.CreateCounter(x.Item1, x.Item2 ?? string.Empty,
            new CounterConfiguration
            {
                /*
                 StaticLabels = new Dictionary<string, string>
                {
                    {"Service", "BinanceConnector"}
                },
                */
                LabelNames = new[] {"response_code"}
            }
        ));
    }
}