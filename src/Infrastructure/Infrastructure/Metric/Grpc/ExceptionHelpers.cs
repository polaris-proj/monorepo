﻿using System;
using System.Linq;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Prometheus;

namespace Infrastructure.Metric.Grpc;

public static class ExceptionHelpers
{
    public static RpcException Handle<T>(this Exception exception, ServerCallContext context, ILogger<T> logger) =>
        exception switch
        {
            _ => HandleDefault(exception, context, logger)
        };

    private static RpcException HandleDefault<T>(Exception exception, ServerCallContext context, ILogger<T> logger)
    {
        var exceptionType = exception.GetType().Name;
        var method = context.Method.Split('/').Last();
        ExceptionCounter.WithLabels(context.Method, method, context.Host, exceptionType).Inc();

        logger.LogError(exception, "An error occurred");
        return new RpcException(new Status(StatusCode.Internal, exception.Message), context.ResponseTrailers);
    }

    private static readonly Counter ExceptionCounter = Metrics.CreateCounter("grpc_requests_errors_total", string.Empty,
        ["endpoint", "method", "instance", "exceptionType"]);
}