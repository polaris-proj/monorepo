﻿namespace Infrastructure.Metric;

public class MetricSettings(IVault vault) : IParameters
{
    public ushort Port { get; } = ushort.Parse(vault.GetValue("Settings:MetricServerPort"));
}