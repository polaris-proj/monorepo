﻿using Grpc.Net.Client;

namespace Infrastructure.Grpc;

public interface INamedGrpcChannelWrapper
{
    public GrpcChannel Channel { get; init; }
}