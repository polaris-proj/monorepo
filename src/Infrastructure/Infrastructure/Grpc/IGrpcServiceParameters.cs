﻿namespace Infrastructure.Grpc;

public interface IGrpcServiceParameters
{
    public string Address { get; }
}