﻿using System;

namespace Infrastructure;

public static class EnumEnumerator
{
    public static TEnum[] GetEnums<TEnum>() where TEnum : Enum
    {
        return (TEnum[]) Enum.GetValues(typeof(TEnum));
    }
}