﻿using System.Threading.Tasks;

namespace Infrastructure;

public abstract class StartUpService
{
    public abstract Task Start();
    public virtual int Priority => 0;//more -> firster
}