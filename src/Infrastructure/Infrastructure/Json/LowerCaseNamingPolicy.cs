﻿using System.Text.Json;

namespace Infrastructure.Json;

public class LowerCaseNamingPolicy : JsonNamingPolicy
{
    public override string ConvertName(string name) => name.ToLower();
}