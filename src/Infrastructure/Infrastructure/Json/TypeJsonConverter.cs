﻿using System;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Infrastructure.Json;

public class TypeJsonConverter : JsonConverter<Type>
{
    public override Type Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var value = reader.GetString() ?? throw new InvalidOperationException("can't get string");
        return Type.GetType(value) ?? throw new InvalidOperationException("Can't convert string to type");
    }

    public override void Write(Utf8JsonWriter writer, Type value, JsonSerializerOptions options)
    {
        var typeAsString = Assembly.CreateQualifiedName(value.Assembly.FullName, value.FullName);
        writer.WriteStringValue(typeAsString);
    }
}