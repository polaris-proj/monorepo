﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;

namespace Infrastructure.RTQConnector;

public abstract class Consumer<T>
{
    protected abstract Topic TopicName { get; }
    protected readonly ConsumerConfig Config;

    public Consumer(KafkaParameters kafkaParameters)
    {
        var cc = kafkaParameters.GetClientConfig();
        Config = new ConsumerConfig(cc)
        {
            GroupId = "test-consumer-group",
            AutoOffsetReset = AutoOffsetReset.Latest,
            AllowAutoCreateTopics = true
        };
    }

    public virtual void SubscribeOnMessages(CancellationToken ct)
    {
        Task.Run(() =>
        {
            using var c = new ConsumerBuilder<Ignore, T>(Config)
                .SetValueDeserializer(new MyJsonDeserializer<T>())
                .Build();
            c.Subscribe(TopicName.ToString());
            Console.WriteLine($"Поднимаю консюмер для {TopicName.ToString()}");
            while (!ct.IsCancellationRequested)
            {
                var consumeResult = c.Consume(ct);
                Handle(consumeResult.Message.Value);
            }
        }, ct);
    }

    public virtual void Handle(T @event)
    {
    }
}