﻿namespace Infrastructure.RTQConnector;

public enum Topic
{
    Candle = 0,
    CandlesForLoadPatterns=8,
    PatternRecognition = 12,

    // ReSharper disable once InconsistentNaming
    PP = 1,
    Slom = 2,
    Accumulation = 3,
    StrategyStartEvent = 4,
    StartStrategy = 5,
    StopStrategy = 6,
    NewCandleBatchEvent=7,
}