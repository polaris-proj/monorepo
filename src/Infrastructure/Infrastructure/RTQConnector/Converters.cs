﻿using System;
using System.IO;
using System.Text.Json;
using Confluent.Kafka;
using Infrastructure.Json;
using ProtoBuf;
using SerializationContext = Confluent.Kafka.SerializationContext;

namespace Infrastructure.RTQConnector;

internal static class OptionsJsonSerializer
{
    public static readonly JsonSerializerOptions Options;

    static OptionsJsonSerializer()
    {
        Options = new JsonSerializerOptions(JsonSerializerDefaults.Web);
        Options.Converters.Add(new DateOnlyJsonConverter());
        Options.Converters.Add(new TimeOnlyJsonConverter());
        Options.Converters.Add(new TypeJsonConverter());
    }
}

public class MyJsonSerializer<T> : ISerializer<T>
{
    public byte[] Serialize(T data, SerializationContext context)
    {
        return JsonSerializer.SerializeToUtf8Bytes(data, OptionsJsonSerializer.Options);
    }
}

public class MyJsonDeserializer<T> : IDeserializer<T>
{
    public T Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context)
    {
        if (isNull)
            throw new MessageNullException();
        return JsonSerializer.Deserialize<T>(data, OptionsJsonSerializer.Options) ??
               throw new InvalidOperationException();
    }
}

public class MyProtoSerializer<T> : ISerializer<T>
{
    public byte[] Serialize(T data, SerializationContext context)
    {
        var stream = new MemoryStream();
        Serializer.Serialize(stream, data);
        return stream.ToArray();
    }
}

public class MyProtoDeserializer<T> : IDeserializer<T>
{
    public T Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context)
    {
        if (isNull)
            throw new MessageNullException();
        
        return Serializer.Deserialize<T>(data);
    }
}