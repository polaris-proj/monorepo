﻿using System.Threading.Tasks;

namespace Infrastructure.RTQConnector;

public interface IProducer<in T>
{
    void Send(Topic topic, T data);
    Task SendAsync(Topic topic, T data);
}