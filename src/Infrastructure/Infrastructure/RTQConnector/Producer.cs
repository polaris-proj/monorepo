﻿using System;
using System.Threading.Tasks;
using Confluent.Kafka;

namespace Infrastructure.RTQConnector;

public class Producer<T> : IProducer<T>
{
    private readonly IProducer<Null, T> _producer;

    public Producer(KafkaParameters kafkaParameters)
    {
        var cc = kafkaParameters.GetClientConfig();
        var config = new ProducerConfig(cc)
        {
            AllowAutoCreateTopics = true,
            LingerMs = 5,
        };

        _producer = new ProducerBuilder<Null, T>(config)
            .SetValueSerializer(new MyJsonSerializer<T>())
            .Build();
    }

    private void Send(string topicName, T data)
    {
        _producer.Produce(topicName, new Message<Null, T> {Value = data}, DeliveryHandler);
        _producer.Flush();
    }
    
    private async Task SendAsync(string topicName, T data)
    {
        try
        {
            await _producer.ProduceAsync(topicName, new Message<Null, T> {Value = data});
        }
        catch (ProduceException<Null,T> e)
        {
            Console.WriteLine(e);
            throw;
        }
        //_producer.Flush();
    }


    private void DeliveryHandler(DeliveryReport<Null, T> obj)
    {
        if (obj.Status != PersistenceStatus.Persisted)
        {
            Send(obj.Topic, obj.Value);
        }
    }

    public void Send(Topic topic, T data)
    {
        Send(topic.ToString(), data);
    }
    
    public async Task SendAsync(Topic topicName, T data)
    {
        await SendAsync(topicName.ToString(), data);
    }
    
    public void Dispose()
    {
        _producer.Dispose();
    }
}