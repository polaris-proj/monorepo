﻿using System;
using Domain;

namespace Infrastructure.Extensions;

public static class Extensions
{
    public static bool Contains(this DateTimeRange dateTimeRange, DateTime dateTime)
        => dateTimeRange.Begin <= dateTime && dateTime <= dateTimeRange.End;
}