﻿using System.Text.RegularExpressions;

namespace WebApi.Infrastructure;

public static class ValidateEmail
{
    private static readonly Regex EmailValid = new(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

    public static bool IsEmailValid(string email)
    {
        return EmailValid.IsMatch(email);
    }
}