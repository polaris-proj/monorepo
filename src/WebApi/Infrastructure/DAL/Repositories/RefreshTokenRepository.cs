﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApi.Infrastructure.DAL.Dbo;

namespace WebApi.Infrastructure.DAL.Repositories;

public class RefreshTokenRepository(IDbContextFactory<WebApiApplicationContext> context)
{
    public async Task AddOrUpdateAsync(RefreshTokenDbo token)
    {
        await using var context1 = await context.CreateDbContextAsync();
        var existingToken = await context1.RefreshTokenDbos.FirstOrDefaultAsync(t => t.TokenString == token.TokenString);

        // it's not concurrent, so sometimes we throw errors
        if (existingToken == null)
        {
            await context1.AddAsync(token);
        }
        else
        {
            existingToken.ExpireAt = token.ExpireAt;
            existingToken.Login = token.Login;
        }

        await context1.SaveChangesAsync();
    }

    public async Task<RefreshTokenDbo?> ReadAsync(string tokenString)
    {
        await using var context1 = await context.CreateDbContextAsync();
        return await context1.RefreshTokenDbos.FirstOrDefaultAsync(x => x.TokenString == tokenString);
    }

    public async Task RemoveExpired(DateTime now)
    {
        await using var context1 = await context.CreateDbContextAsync();
        await context1.RefreshTokenDbos.Where(t => t.ExpireAt < now).ExecuteDeleteAsync();
    }

    public async Task RemoveByLogin(string login)
    {
        await using var context1 = await context.CreateDbContextAsync();
        await context1.RefreshTokenDbos.Where(t => t.Login == login).ExecuteDeleteAsync();
    }
}