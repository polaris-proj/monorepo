﻿using Infrastructure.DAL.Storage;
using WebApi.Infrastructure.DAL.Dbo;

namespace WebApi.Infrastructure.DAL.Repositories;

public class StrategyUserPresentationRepository : RepositoryWithGuid<StrategyDbo, WebApiApplicationContext>
{
    public StrategyUserPresentationRepository(WebApiApplicationContext context) : base(context)
    {
    }
}