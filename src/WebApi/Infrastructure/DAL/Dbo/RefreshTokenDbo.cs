﻿using System;
using System.ComponentModel.DataAnnotations;
using WebApi.Infrastructure.Auth;

namespace WebApi.Infrastructure.DAL.Dbo;

public class RefreshTokenDbo
{
    [MaxLength(100)] public required string Login { get; set; }

    [MaxLength(100)] public required string TokenString { get; set; }

    public DateTime ExpireAt { get; set; }

    public static RefreshTokenDbo FromDto(RefreshToken refreshToken) =>
        new()
        {
            Login = refreshToken.Login,
            TokenString = refreshToken.TokenString,
            ExpireAt = refreshToken.ExpireAt
        };
}