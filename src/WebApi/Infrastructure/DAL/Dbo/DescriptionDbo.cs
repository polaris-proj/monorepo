﻿using System;

namespace WebApi.Infrastructure.DAL.Dbo;

public class DescriptionDbo
{
    public Guid Id { get; set; }
    public Guid StrategyUserPresentationDboId { get; set; }
    public string Title { get; set; }
    public string Text { get; set; }
}