﻿using System;
using System.Collections.Generic;
using WebApi.Models;

namespace WebApi.Infrastructure.DAL.Dbo;

public class RoleDbo
{
    public Guid Id { get; set; }
    public Role Role { get; set; }

    public ICollection<UserDbo> Users { get; set; } = new List<UserDbo>();
}

