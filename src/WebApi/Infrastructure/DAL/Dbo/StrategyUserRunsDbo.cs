﻿using System;
using System.Collections.Generic;

namespace WebApi.Infrastructure.DAL.Dbo;

public class StrategyUserRunsDbo
{
    public required Guid UserId { get; set; }
    public required Guid StrategyId { get; set; }
    public ICollection<StartTestOfStrategyParametersDbo> RunsId { get; set; } = new List<StartTestOfStrategyParametersDbo>();
    
}