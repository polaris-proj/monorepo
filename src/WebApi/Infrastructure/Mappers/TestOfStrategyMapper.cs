﻿using System.Linq;
using Domain.StrategyProcessor;
using Infrastructure.Extensions;
using StrategyProcessor.Shared;
using WebApi.Models.ViewModel;

namespace WebApi.Infrastructure.Mappers;

internal static class TestOfStrategyMapper
{
    public static TestOfStrategyViewModel ToViewModel(this TestOfStrategyDto dto)
    {
        return new TestOfStrategyViewModel
        {
            Notes = dto.Notes.Select(y => y.ToViewModel()),
            Orders = dto.Orders.Select(order => order.ToViewModel()),
            Charts = dto.Charts.Select(chart => chart.ToViewModel())
        };
    }

    internal static NoteViewModel ToViewModel(this Note dto)
    {
        return new NoteViewModel
        {
            TimeStamp = dto.TimeStamp,
            Price = dto.Price,
            Text = dto.Text,
            Icon = dto.Icon,
        };
    }

    internal static OrderViewModel ToViewModel(this OrderDto dto)
    {
        return new OrderViewModel
        {
            CurrencyPair = dto.Pair.ToViewModel(),
            Type = dto.Type,
            Status = dto.Status,
            Price = dto.Price??0,
            Amount = dto.Amount,
            OrderId = dto.OrderId,
            CreatedAt = dto.CreatedAt.ToMilliseconds(),
            UpdatedAt = dto.UpdatedAt,
            Leverage = dto.Leverage,
            TakePrice = dto.TakePrice,
            StopPrice = dto.StopPrice
        };
    }

    internal static ChartViewModel ToViewModel(this ChartDto dto)
    {
        return new ChartViewModel
        {
            Name = dto.Name,
            Description = dto.Description,
            Chart = dto.Chart,
            TimeFrame = dto.TimeFrame,
            Pair = dto.Pair.ToViewModel()
        };
    }
}