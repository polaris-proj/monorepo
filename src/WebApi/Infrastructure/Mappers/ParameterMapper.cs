﻿using System;
using System.Collections.Generic;
using StrategyProcessor.Shared;
using WebApi.Models;

namespace WebApi.Infrastructure.Mappers;

public static class ParameterMapper
{
    public static ParameterModel ToModel(this StrategyParameterDto strategyStartDto)
    {
        return new ParameterModel
        {
            Name = strategyStartDto.Name,
            TypeOfValue = strategyStartDto.ParameterType.ToParameterType(),
            Value = strategyStartDto.JsonValue,
        };
    }

    private static ParameterType ToParameterType(this Type type)
    {
        if (NumericTypes.Contains(type)) return ParameterType.Number;
        if (type == typeof(bool)) return ParameterType.Bool;
        return ParameterType.String;
    }

    private static readonly HashSet<Type> NumericTypes =
    [
        typeof(byte), typeof(short), typeof(int), typeof(long),
        typeof(sbyte), typeof(ushort), typeof(uint), typeof(ulong),
        typeof(float), typeof(double), typeof(decimal)
    ];

    public enum ParameterType
    {
        String,
        Number,
        Bool,
    }
}