﻿using WebApi.Infrastructure.DAL.Dbo;
using WebApi.Models;

namespace WebApi.Infrastructure.Mappers;

public static class DescriptionMapper
{
    public static DescriptionDbo ToDbo(this Description strategyStartEventDto)
    {
        return new DescriptionDbo
        {
            Text = strategyStartEventDto.Text,
            Title = strategyStartEventDto.Title
        };
    }

    public static Description ToModel(this DescriptionDbo parameterDbo)
    {
        return new Description
        {
            Text = parameterDbo.Text,
            Title = parameterDbo.Title
        };
    }
}