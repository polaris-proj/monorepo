﻿using System;
using System.Text.Json;
using Infrastructure.Json;
using WebApi.Infrastructure.DAL.Dbo;
using WebApi.Models.InputModel;
using WebApi.Models.ViewModel;
using DateTimeRange = WebApi.Models.ViewModel.DateTimeRange;

namespace WebApi.Infrastructure.Mappers;

public static class StartTestOfStrategyParametersMapper
{
    public static TestResult ToView(this StartTestOfStrategyParametersDbo parametersDbo)
    {
        var isTest = parametersDbo.IsTest;
        DateTimeRange duration;
        if (isTest)
        {
            var parameters =
                parametersDbo.ModelJson.Deserialize<StartTestOfStrategyModel>(JsonOptions.JsonSerializerOptions) ??
                throw new JsonException("Can't parse parameters from json");
            duration = new DateTimeRange {Begin = parameters.StartTime, End = parameters.EndTime};
        }
        else
        {
            var parameters =
                parametersDbo.ModelJson.Deserialize<StartStrategyModel>(JsonOptions.JsonSerializerOptions) ??
                throw new JsonException("Can't parse parameters from json");
            
            duration = new DateTimeRange {Begin = parameters.StartTime, End = DateTime.MaxValue};
        }

        return new TestResult
        {
            RunId = parametersDbo.RunId,
            TimeOfRun = parametersDbo.TimeOfRun.DateTime,
            Profit = 0,
            Duration = duration
        };
    }
}