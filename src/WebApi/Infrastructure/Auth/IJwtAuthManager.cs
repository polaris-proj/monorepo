﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApi.Infrastructure.Auth;

public interface IJwtAuthManager
{
    Task<JwtAuthResult> GenerateTokens(string username, Claim[] claims, DateTime now);
    Task<JwtAuthResult> Refresh(string refreshToken, string accessToken, DateTime now);
    Task RemoveExpiredRefreshTokens(DateTime now);
    Task RemoveRefreshTokenByUserName(string userName);
}