﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebApi.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RefreshTokenDbos",
                columns: table => new
                {
                    TokenString = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Login = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    ExpireAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefreshTokenDbos", x => x.TokenString);
                });

            migrationBuilder.CreateTable(
                name: "RoleDbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Role = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleDbo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StrategyUserPresentation",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(150)", maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StrategyUserPresentation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StrategyUserRunsDbos",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    StrategyId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StrategyUserRunsDbos", x => new { x.UserId, x.StrategyId });
                });

            migrationBuilder.CreateTable(
                name: "UserDbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Login = table.Column<string>(type: "character varying(150)", maxLength: 150, nullable: false),
                    Email = table.Column<string>(type: "character varying(150)", maxLength: 150, nullable: false),
                    PasswordHash = table.Column<string>(type: "character varying(150)", maxLength: 150, nullable: false),
                    ExtraFields_Photo = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDbo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DescriptionDbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    StrategyUserPresentationDboId = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: false),
                    Text = table.Column<string>(type: "text", nullable: false),
                    StrategyDboId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DescriptionDbo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DescriptionDbo_StrategyUserPresentation_StrategyDboId",
                        column: x => x.StrategyDboId,
                        principalTable: "StrategyUserPresentation",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "StartTestOfStrategyParametersDbos",
                columns: table => new
                {
                    RunId = table.Column<Guid>(type: "uuid", nullable: false),
                    StrategyId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsTest = table.Column<bool>(type: "boolean", nullable: false),
                    ModelJson = table.Column<string>(type: "text", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StartTestOfStrategyParametersDbos", x => x.RunId);
                    table.ForeignKey(
                        name: "FK_StartTestOfStrategyParametersDbos_StrategyUserRunsDbos_User~",
                        columns: x => new { x.UserId, x.StrategyId },
                        principalTable: "StrategyUserRunsDbos",
                        principalColumns: new[] { "UserId", "StrategyId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    RoleId = table.Column<Guid>(type: "uuid", nullable: false),
                    AssignmentDateTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => new { x.RoleId, x.UserId });
                    table.ForeignKey(
                        name: "FK_UserRole_RoleDbo_RoleId",
                        column: x => x.RoleId,
                        principalTable: "RoleDbo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRole_UserDbo_UserId",
                        column: x => x.UserId,
                        principalTable: "UserDbo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DescriptionDbo_StrategyDboId",
                table: "DescriptionDbo",
                column: "StrategyDboId");

            migrationBuilder.CreateIndex(
                name: "IX_RefreshTokenDbos_Login",
                table: "RefreshTokenDbos",
                column: "Login");

            migrationBuilder.CreateIndex(
                name: "IX_StartTestOfStrategyParametersDbos_UserId_StrategyId",
                table: "StartTestOfStrategyParametersDbos",
                columns: new[] { "UserId", "StrategyId" });

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_UserId",
                table: "UserRole",
                column: "UserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DescriptionDbo");

            migrationBuilder.DropTable(
                name: "RefreshTokenDbos");

            migrationBuilder.DropTable(
                name: "StartTestOfStrategyParametersDbos");

            migrationBuilder.DropTable(
                name: "UserRole");

            migrationBuilder.DropTable(
                name: "StrategyUserPresentation");

            migrationBuilder.DropTable(
                name: "StrategyUserRunsDbos");

            migrationBuilder.DropTable(
                name: "RoleDbo");

            migrationBuilder.DropTable(
                name: "UserDbo");
        }
    }
}
