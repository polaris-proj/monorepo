using System.Threading.Tasks;
using Infrastructure.Metric;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;

namespace WebApi;

public class Program
{
    public static async Task Main(string[] args)
    {
        var app = SetUp(args);
        await app.RunAsync();
    }

    private static IHost SetUp(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        builder.ConfigureOpenTelemetry("WebApi");
        builder.Services.ConfigureServices(builder.Environment);

        var app = builder.Build();
        app.ConfigureApp(app.Environment);

        return app;
    }
}