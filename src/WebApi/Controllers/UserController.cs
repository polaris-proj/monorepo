﻿using System;
using System.Threading.Tasks;
using Domain;
using IODataModule.Client.Clients;
using IODataModule.Shared.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MoreLinq;
using WebApi.Infrastructure.DAL.Repositories;
using WebApi.Models.InputModel;
using WebApi.Models.ViewModel;

namespace WebApi.Controllers;

[Route("user")]
[Authorize(Roles = "User")]
public class UserController : BaseController
{
    public UserController(UserRepository userDataRepository, IUserExchangeKeysClient userExchangeKeysClient)
    {
        _userDataRepository = userDataRepository;
        _userExchangeKeysClient = userExchangeKeysClient;
    }

    [HttpGet("exchangeKeys")]
    public async Task<ActionResult<ExchangeKeyDto[]>> GetExchangeKeys()
    {
        var userId = GetUserIdOrNull();
        if (userId is null)
            return Unauthorized();

        var user = await _userDataRepository.ReadAsync(userId.Value);
        if (user is null)
            return Unauthorized();

        var keys = await _userExchangeKeysClient.GetAllExchangeKeys((Guid) userId);
        var privatedKeys = keys.Pipe(x =>
        {
            x.Key = MakePrivate(x.Key);
            x.Secret = MakePrivate(x.Secret);
        });
        return Ok(privatedKeys);
    }

    [HttpPost("exchangeKeys")]
    //todo убрать передачу ид в аргументе и принимать секрет с токеном в теле а не в квери параметрах
    public async Task<ActionResult> AddExchangeKey([FromBody] ExchangeKey exchangeKey)
    {
        var userId = GetUserIdOrNull();
        if (userId is null)
            return Unauthorized();

        var user = await _userDataRepository.ReadAsync(userId.Value);
        if (user is null)
            return Unauthorized();

        await _userExchangeKeysClient.AddOrUpdateExchangeKey((Guid) userId, exchangeKey.Exchange, exchangeKey.Secret,
            exchangeKey.Token);
        return Ok();
    }

    [HttpDelete("exchangeKeys")]
    public async Task<ActionResult> RemoveExchangeKey([FromQuery] Exchange exchange)
    {
        var userId = GetUserIdOrNull();
        if (userId is null)
            return Unauthorized();

        var user = await _userDataRepository.ReadAsync(userId.Value);
        if (user is null)
            return Unauthorized();

        await _userExchangeKeysClient.RemoveExchangeKey((Guid) userId, exchange);
        return Ok();
    }

    [HttpGet("info")]
    public async Task<ActionResult<User>> GetUserInfo()
    {
        var userId = GetUserIdOrNull();
        if (userId is null)
        {
            return Unauthorized();
        }

        var user = await _userDataRepository.ReadAsync(userId.Value);
        if (user is null)
        {
            return Unauthorized();
        }

        return Ok(new User
        {
            Login = user.Login,
            Photo = user.ExtraFields.Photo
        });
    }

    internal static string MakePrivate(string text)
    {
        return text[..5] + "******" + text[^5..];
    }

    private readonly UserRepository _userDataRepository;
    private readonly IUserExchangeKeysClient _userExchangeKeysClient;
}