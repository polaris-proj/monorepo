﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.StrategyProcessor;
using EventStore.Client;
using Infrastructure;
using Infrastructure.Extensions;
using IODataModule.Client.Clients;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Infrastructure.Mappers;
using WebApi.Models.ViewModel;
using Pair = Domain.Pair;
using PairOut = WebApi.Models.ViewModel.Pair;

namespace WebApi.Controllers;

[Route("exchange")]
[Authorize(Roles = "User")]
public class ExchangeDataController : BaseController
{
    public ExchangeDataController(ICandleService candleService, EventStoreClient eventStoreClient,
        ILogger<ExchangeDataController> logger)
    {
        _candleService = candleService;
        _eventStoreClient = eventStoreClient;
        _logger = logger;
    }

    [HttpGet("{exchange}/pairs")]
    public async Task<ActionResult<IEnumerable<PairOut>>> GetPairsForExchange(Exchange exchange)
    {
        var userId = GetUserIdOrNull()!.Value;

        var pairs = await _candleService.GetPairs(userId, exchange);
        return Ok(pairs.Select(x => x.ToViewModel()));
    }

    [HttpGet("pairs")]
    public async Task<Dictionary<Exchange, IEnumerable<PairOut>>> GetPairs()
    {
        var userId = GetUserIdOrNull()!.Value;

        var exchanges = EnumEnumerator.GetEnums<Exchange>().Where(x => !x.IsTestApi()).ToArray();
        var pairs = await Task.WhenAll(exchanges.Select(async x =>
        {
            try
            {
                return await _candleService.GetPairs(userId, x);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error while getting pairs for exchange {0}", x);
                return Array.Empty<Pair>();
            }
        }));
        var pairsByExchange = pairs.Select((x, i) => (exchanges[i], x))
            .ToDictionary(k => k.Item1, v => v.Item2.Select(x => x.ToViewModel()));

        return pairsByExchange;
    }

    [HttpGet("{exchange}/candles")]
    public async Task<ActionResult<Candle[]>> GetCandles([Required] Exchange exchange, [Required] string firstTicker,
        [Required] string secondTicker, [Required] TimeFrame timeFrame,
        [Required] DateTime startTime, [Required] DateTime endTime)
    {
        var sw = Stopwatch.StartNew();
        var key = new ExchangePairTimeFrame(exchange, new Pair(firstTicker, secondTicker), timeFrame);

        var userId = GetUserIdOrNull().Value;

        var rawCandles =
            await _candleService.GetRange(userId, key, startTime.ToMilliseconds(), endTime.ToMilliseconds());

        var candles = rawCandles
            .OrderBy(x => x.TimeStamp)
            .ToArray();
        _logger.LogTrace("GetCandles: {0} ms", sw.ElapsedMilliseconds);
        return Ok(candles);
    }

    [HttpGet("{exchange}/patterns")]
    public async Task<ActionResult<PatternModel[]>> GetPatterns(Exchange exchange, string firstTicker,
        string secondTicker, TimeFrame timeFrame, DateTime startTime, DateTime endTime)
    {
        var sw = Stopwatch.StartNew();
        var key = new ExchangePairTimeFrame(exchange, new Pair(firstTicker, secondTicker), timeFrame);

        var userId = GetUserIdOrNull().Value;


        var rawCandles =
            await _candleService.GetRange(userId, key, startTime.ToMilliseconds(), endTime.ToMilliseconds());

        var patternEvents =
            await _eventStoreClient.GetRange(userId, key, startTime.ToMilliseconds(), endTime.ToMilliseconds());

        var patterns = patternEvents
            .DistinctBy(x => x.TimeStamp)
            .Select(x => new PatternModel()
            {
                Name = x.RecognitionResult.MostProbablePattern.ToString(),
                PatternPoints = x.RecognitionResult.UsedCandles.Select(y =>
                        new Point(y.TimeStamp, rawCandles.FirstOrDefault(z => z.TimeStamp == y.TimeStamp)?.Close ?? 0))
                    .OrderBy(y => y.TimeStamp)
                    .ToList()
            })
            .ToArray();
        _logger.LogTrace("GetPatterns: {0} ms", sw.ElapsedMilliseconds);
        // return Ok(Enumerable.Range(0, 10).Select(_ => GenerateRandomPattern(candles.OrderBy(x => random.Next()).Take(5).ToArray()
        // )).ToArray());
        return patterns;
    }

    private readonly ICandleService _candleService;
    private readonly EventStoreClient _eventStoreClient;
    private readonly ILogger<ExchangeDataController> _logger;
}