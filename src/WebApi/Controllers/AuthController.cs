using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using WebApi.Infrastructure;
using WebApi.Infrastructure.Auth;
using WebApi.Infrastructure.DAL.Dbo;
using WebApi.Infrastructure.DAL.Repositories;
using WebApi.Models;
using WebApi.Models.InputModel;
using WebApi.Models.ViewModel;

namespace WebApi.Controllers;

[Route("auth")]
public class AuthController : Controller
{
    public AuthController(UserRepository userRepository, IJwtAuthManager jwtAuthManager)
    {
        _userRepository = userRepository;
        _jwtAuthManager = jwtAuthManager;
    }

    [AllowAnonymous]
    [HttpPost("login")]
    public async Task<ActionResult<LoginResult>> Login([FromBody] LoginData loginData)
    {
        var login = loginData.Login;
        var password = loginData.Password;

        if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
        {
            return Forbid();
        }

        var user = await _userRepository.GetByLoginAsync(login);
        if (user == default)
        {
            return Forbid();
        }

        var passwordValidator = new PasswordHasher<UserDbo>();
        var isPasswordValid = passwordValidator.VerifyHashedPassword(null!, user.PasswordHash, password);
        if (isPasswordValid != PasswordVerificationResult.Failed)
        {
            var claims = new List<Claim>
            {
                new(ClaimTypes.Name, user.Login),
                new(ClaimTypes.NameIdentifier, user.Id.ToString())
            };
            var roles = user.Roles.Select(role => new Claim(ClaimTypes.Role, role.Role.ToString())).ToList();
            claims.AddRange(roles);


            var jwtResult = await _jwtAuthManager.GenerateTokens(login, claims.ToArray(), DateTime.UtcNow);
            if (jwtResult is not null)
                return Ok(new LoginResult
                {
                    Login = login,
                    Roles = roles.Select(x => x.Value).ToArray(),
                    AccessToken = jwtResult.AccessToken,
                    RefreshToken = jwtResult.RefreshToken.TokenString
                });
        }

        return Forbid();
    }

    [Authorize]
    [HttpPost("logout")]
    public async Task<ActionResult> Logout()
    {
        var userName = User.Identity?.Name ?? throw new InvalidOperationException();
        await _jwtAuthManager.RemoveRefreshTokenByUserName(userName);
        return Ok();
    }

    [AllowAnonymous]
    [HttpPost("register")]
    public async Task<ActionResult> Register([FromBody] [Required] RegisterData registerData)
    {
        var email = registerData.Email;
        var login = registerData.Login;
        var password = registerData.Password;

        if (email is null || login is null || password is null)
            return BadRequest();

        email = email.ToLowerInvariant();

        if (!ValidateEmail.IsEmailValid(email) ||
            email.Length >= 64 ||
            login.Length is <= 4 or >= 32 ||
            password.Length is <= 12 or >= 64)
            return StatusCode(412);

        var user = await _userRepository.GetByLoginAsync(login);
        if (user is not null) return Forbid();

        var passwordHasher = new PasswordHasher<UserDbo>();
        user = new UserDbo
        {
            Login = login,
            Email = email,
            ExtraFields = new UserExtraFields(),
            Roles = new List<RoleDbo> {new() {Role = Role.User}},
            PasswordHash = passwordHasher.HashPassword(null, password),
        };

        await _userRepository.AddAsync(user);
        return Ok();
    }

    [HttpPost("refresh-token")]
    public async Task<ActionResult<LoginResult>> RefreshToken([Required] string refreshToken)
    {
        try
        {
            var login = User.Identity?.Name;
            if (string.IsNullOrWhiteSpace(refreshToken)) return Unauthorized();

            if (!Request.Headers.TryGetValue(HeaderNames.Authorization, out var authHeader) ||
                authHeader.ToString().Length < 8)
            {
                return Unauthorized("No Authorization Header");
            }

            var accessToken = authHeader.ToString()[7..];
            var jwtResult = await _jwtAuthManager.Refresh(refreshToken, accessToken, DateTime.UtcNow);
            return Ok(new LoginResult
            {
                Login = jwtResult.RefreshToken.Login,
                Roles = User.FindAll(ClaimTypes.Role).Select(x => x.Value).ToArray(),
                AccessToken = jwtResult.AccessToken,
                RefreshToken = jwtResult.RefreshToken.TokenString
            });
        }
        catch (SecurityTokenException e)
        {
            return Unauthorized(e.Message); // return 401 so that the client side can redirect the user to login page
        }
    }

    private readonly UserRepository _userRepository;
    private readonly IJwtAuthManager _jwtAuthManager;
}