﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Domain;
using Domain.StrategyProcessor;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StrategyProcessor.Client;
using StrategyProcessor.Shared;
using WebApi.Infrastructure.DAL.Dbo;
using WebApi.Infrastructure.DAL.Repositories;
using WebApi.Infrastructure.Mappers;
using WebApi.Models;
using WebApi.Models.InputModel;
using WebApi.Models.ViewModel;
using DateTimeRange = Domain.DateTimeRange;
using Pair = Domain.Pair;

namespace WebApi.Controllers;

[Authorize(Roles = "User")]
[Route("strategies")]
public class StrategyController : BaseController
{
    public StrategyController(
        StrategyUserPresentationRepository strategyUserPresentationRepository,
        IStrategyProcessorClient strategyProcessorClient,
        TestParametersRepository testParametersRepository,
        StrategyUserRunsRepository strategyUserRunsRepository
    )
    {
        _strategyUserPresentationRepository = strategyUserPresentationRepository;
        _strategyProcessorClient = strategyProcessorClient;
        _testParametersRepository = testParametersRepository;
        _strategyUserRunsRepository = strategyUserRunsRepository;
    }

    [HttpPost("start")]
    public async Task<ActionResult<Guid>> StartStrategy([FromBody] [Required] StartStrategyModel arg)
    {
        var userId = GetUserIdOrNull();
        if (userId is null)
        {
            return Unauthorized();
        }

        var allStrategiesAnswer = await _strategyProcessorClient.GetAllStrategies();

        var strategy = allStrategiesAnswer.FirstOrDefault(x => x.Id == arg.StrategyId);
        if (strategy is null)
        {
            return NoContent();
        }

        //и тут всякие обращения, которые будут юзать общий контекст, который создался в юзинге
        var msg = new StrategyStartDto
        {
            StrategyId = arg.StrategyId,
            Parameters = await GetParametersValue(arg.StrategyId, arg.Parameters),
            DataSources = arg.AllowedSource
                .SelectMany(x =>
                    x.PairTimeFrame.Select(
                        y => new ExchangePairTimeFrameDto(x.Exchange, y.Pair.ToDomain(), y.TimeFrame)))
                .ToArray(),
            UserId = userId.Value,
            RunMode = RunMode.Real,
            EnableOrderConfirmation = false,
        };

        var result = await _strategyProcessorClient.StartStrategy(msg);

        await _strategyUserRunsRepository.AddStrategyRealRun(arg, userId.Value, result.Value);

        return Ok(result);
    }

    [HttpPost("startTest")]
    public async Task<ActionResult<Guid>> StartTestOfStrategy([FromBody] StartTestOfStrategyModel arg)
    {
        if (!ModelState.IsValid)
        {
            throw new InvalidDataException(string.Join(Environment.NewLine, ModelState.Values
                                                 .SelectMany(x => x.Errors)
                                                 .Select(x => x.ErrorMessage))
            );
        }
        
        
        var userId = GetUserIdOrNull();

        if (userId is null)
            return Unauthorized();

        if (arg is null or {IsRealTimeTestMode: true, StartTime.Year: > 10, EndTime.Year: > 10})
            return Forbid();

        var allStrategiesAnswer = await _strategyProcessorClient.GetAllStrategies();

        var strategy = allStrategiesAnswer.FirstOrDefault(x => x.Id == arg.StrategyId);
        if (strategy is null)
            return NotFound("Strategy not found");

        //и тут всякие обращения, которые будут юзать общий контекст, который создался в юзинге
        var msg = new StrategyStartDto
        {
            StrategyId = arg.StrategyId,
            Parameters = await GetParametersValue(arg.StrategyId, arg.Parameters),
            DataSources = arg.AllowedSource
                .SelectMany(x =>
                    x.PairTimeFrame.Select(
                        y => new ExchangePairTimeFrameDto(x.Exchange, y.Pair.ToDomain(), y.TimeFrame)))
                .ToArray(),
            UserId = userId.Value,
            EnableOrderConfirmation = arg.EnableOrderConfirmation,
            InitialBalance = arg.InitialBalance > 0 ? arg.InitialBalance : 1000,
            EndTime = arg.EndTime,
            StartTime = arg.StartTime,
            RunMode = arg.IsRealTimeTestMode ? RunMode.RealDataTest : RunMode.HistoryTest
        };

        var result = await _strategyProcessorClient.StartStrategy(msg);

        await _strategyUserRunsRepository.AddStrategyTestRun(arg, msg.UserId, result.Value);

        return Ok(result);
    }

    [HttpGet("result/{runId:guid}")]
    public async Task<ActionResult<TestOfStrategyViewModel>> GetResultOfExperiment(
        Guid runId,
        [FromQuery] Exchange exchange,
        [FromQuery] string firstTicker,
        [FromQuery] string secondTicker,
        [FromQuery] TimeFrame timeFrame,
        [FromQuery] DateTime startTime,
        [FromQuery] DateTime endTime
    )
    {
        var result = await _strategyProcessorClient.GetTestResult(
            runId,
            new Pair(firstTicker, secondTicker),
            timeFrame,
            new DateTimeRange(startTime, endTime)
        );
        return Ok(result.ToViewModel());
    }

    [HttpGet("{strategyId:guid}/parameters")]
    public async Task<ActionResult<ParameterModel[]>> GetStrategyParameters(Guid strategyId)
    {
        var allStrategiesAnswer = await _strategyProcessorClient.GetAllStrategies();

        var strategy = allStrategiesAnswer.FirstOrDefault(x => x.Id == strategyId);
        if (strategy is null)
            return NoContent();

        return Ok(strategy.Parameters.Select(x => x.ToModel()).ToArray());
    }

    [HttpGet("testRun/{runId:guid}")]
    public async Task<ActionResult<StartTestOfStrategyModel>> GetTestStrategyParameters(Guid runId)
    {
        var startInfo = await _testParametersRepository.ReadAsync(runId);

        if (startInfo is null)
        {
            return NotFound();
        }

        return Ok(startInfo);
    }

    [HttpGet("{strategyId:guid}/realRuns")]
    public async Task<ActionResult<Guid[]>> GetStrategyRealRuns(Guid strategyId)
    {
        var userId = GetUserIdOrNull();

        if (userId is null)
        {
            return Unauthorized();
        }

        var ids = await _strategyUserRunsRepository.GetStrategyRealRuns(userId.Value, strategyId);

        return Ok(ids);
    }

    [HttpGet("{strategyId:guid}/testRuns")]
    public async Task<ActionResult<IEnumerable<TestResult>>> GetStrategyTestRuns(Guid strategyId)
    {
        var userId = GetUserIdOrNull();

        if (userId is null)
        {
            return Unauthorized();
        }

        var ids = await _strategyUserRunsRepository.GetStrategyTestRuns(userId.Value, strategyId);

        return Ok(ids);
    }

    #region StrategyInfo

    [HttpGet("{strategyId:guid}/strategyInfo")]
    public async Task<ActionResult<StrategyUserPresentation>> GetStrategyInfo(Guid strategyId)
    {
        var strategy = await _strategyUserPresentationRepository.ReadAsync(strategyId);
        if (strategy is null)
            return NoContent();

        var model = strategy.ToModel();
        // model.Charts = CalculateStrategyCharts();9
        return Ok(model);
    }


    [HttpPost("{strategyId:guid}/strategyInfo")]
    public async Task<IActionResult> CreateStrategyInfo(
        [FromBody] [Required] StrategyUserPresentationEditable strategyUserPresentationEditable)
    {
        var allStrategies = await _strategyProcessorClient.GetAllStrategies();
        var strategy = allStrategies.FirstOrDefault(x => x.Id == strategyUserPresentationEditable.Id);
        if (strategy is null)
        {
            return NotFound();
        }

        var strategyUserPresentationDbo = new StrategyDbo
        {
            Id = strategyUserPresentationEditable.Id,
            Descriptions = strategyUserPresentationEditable.Descriptions.Select(x => x.ToDbo()).ToList(),
            Name = strategyUserPresentationEditable.Name
        };

        await _strategyUserPresentationRepository.CreateAsync(strategyUserPresentationDbo);
        return Ok();
    }

    [HttpPut("{strategyId:guid}/strategyInfo")]
    public async Task<ActionResult> EditStrategyInfo(
        [FromBody] [Required] StrategyUserPresentationEditable strategyUserPresentationEditable)
    {
        var strategy = await _strategyUserPresentationRepository.ReadAsync(strategyUserPresentationEditable.Id);
        if (strategy is null)
            return NoContent();

        strategy.Descriptions = strategyUserPresentationEditable.Descriptions.Select(x => x.ToDbo()).ToList();
        strategy.Name = strategyUserPresentationEditable.Name;

        await _strategyUserPresentationRepository.UpdateAsync(strategy);
        return Ok();
    }

    #endregion

    [HttpGet]
    public async Task<ActionResult<StrategyWithParameters[]>> GetStrategyList()
    {
        var allStrategiesAnswer = await _strategyProcessorClient.GetAllStrategies();
        return Ok(allStrategiesAnswer.Select(x => x.ToModel()).ToArray());
    }

    [HttpGet("{strategyId:guid}")]
    public async Task<ActionResult<StrategyWithParameters>> GetStrategy(Guid strategyId)
    {
        var allStrategiesAnswer = await _strategyProcessorClient.GetAllStrategies();
        var strategy = allStrategiesAnswer.FirstOrDefault(x => x.Id == strategyId);
        if (strategy is null)
            return NoContent();
        var view = strategy.ToModel();
        return Ok(view);
    }

    private async Task<StrategyParameterDto[]> GetParametersValue(Guid strategyId, ParameterModel[] parameterModels)
    {
        var allStrategiesAnswer = await _strategyProcessorClient.GetAllStrategies();
        var strategy = allStrategiesAnswer.FirstOrDefault(x => x.Id == strategyId);
        if (strategy is null)
            throw new ArgumentOutOfRangeException(nameof(strategyId), strategyId, "Strategy not found");

        foreach (var parameterModel in parameterModels)
        {
            var parameter = strategy.Parameters.FirstOrDefault(x => x.Name == parameterModel.Name);
            if (parameter is null)
                continue;

            //validate
            var val = JsonSerializer.Deserialize(parameterModel.Value, parameter.ParameterType);
            if (val is null) throw new FormatException("Invalid parameter value");

            parameter.JsonValue = parameterModel.Value;
        }

        return strategy.Parameters;
    }

    private readonly StrategyUserPresentationRepository _strategyUserPresentationRepository;
    private readonly IStrategyProcessorClient _strategyProcessorClient;
    private readonly TestParametersRepository _testParametersRepository;
    private readonly StrategyUserRunsRepository _strategyUserRunsRepository;
}