﻿using System;
using System.Text.Json;


namespace WebApi.Models;

public class Parameter
{
    public string Name { get; set; }
    public Type TypeOfValue { get; set; }
    public JsonDocument Value { get; set; }
}