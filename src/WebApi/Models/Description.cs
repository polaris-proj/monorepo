﻿namespace WebApi.Models;

#pragma warning disable CS8618
public class Description
{
    public string Title { get; set; }
    public string Text { get; set; }
}