﻿using System.ComponentModel.DataAnnotations;
using Domain;

namespace WebApi.Models;

public class ExchangeSources
{
    [Required] public Exchange Exchange { get; set; }
    [Required] public PairTimeFrame[] PairTimeFrame { get; set; }
}