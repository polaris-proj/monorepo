﻿using System;

namespace WebApi.Models;

public class StrategyUserPresentation
{
    public Guid Id { get; set; }
    public string? Name { get; set; }
    public Description[]? Descriptions { get; set; }
}