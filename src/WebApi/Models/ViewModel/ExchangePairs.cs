﻿using System.Collections.Generic;


namespace WebApi.Models.ViewModel;

public class ExchangePairs
{
    public Dictionary<string, List<string>> ExchangesPairs { get; set; }
}