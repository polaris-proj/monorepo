﻿namespace WebApi.Models.ViewModel;

public class NoteGroup
{
    public required Pair Pair { get; set; }
    public required TimeFrame TimeFrame { get; set; }
    public required NoteViewModel[] Notes { get; set; }
}