﻿using System;
using Domain.StrategyProcessor;

namespace WebApi.Models.ViewModel;

public class StrategyWithParameters
{
    public Guid Id { get; set; }
    public required string InternalName { get; set; }
    public required StrategyConnectorType ConnectorType { get; set; }
    public virtual ParameterModel[] Properties { get; set; } = Array.Empty<ParameterModel>();
}