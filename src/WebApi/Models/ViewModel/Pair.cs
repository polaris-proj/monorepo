﻿namespace WebApi.Models.ViewModel;

public class Pair
{
    public required string First { get; set; }
    public required string Second { get; set; }
}