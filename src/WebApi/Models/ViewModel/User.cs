﻿namespace WebApi.Models.ViewModel;

public class User
{
    public required string Login { get; set; }
    public string? Photo { get; set; }
}