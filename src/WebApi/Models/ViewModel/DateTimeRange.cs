﻿using System;

namespace WebApi.Models.ViewModel;

public class DateTimeRange
{
    public required DateTime Begin { get; set; }
    public required DateTime End { get; set; }
}