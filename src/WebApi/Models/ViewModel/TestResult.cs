﻿using System;

namespace WebApi.Models.ViewModel;

public class TestResult
{
    public required Guid RunId { get; set; }
    public required DateTime TimeOfRun { get; set; }
    public required decimal Profit { get; set; }
    public required DateTimeRange Duration { get; set; }
}