﻿using System.Collections.Generic;

namespace WebApi.Models.ViewModel;

public class TestOfStrategyViewModel
{
    public required IEnumerable<NoteViewModel> Notes { get; set; }

    public required IEnumerable<OrderViewModel> Orders { get; set; }

    public required IEnumerable<ChartViewModel> Charts { get; set; }
}