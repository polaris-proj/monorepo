﻿using System.Text.Json.Serialization;

// ReSharper disable UnusedAutoPropertyAccessor.Global
#pragma warning disable CS8618

namespace WebApi.Models.ViewModel;

public class LoginResult
{
    [JsonPropertyName("login")] public string Login { get; set; }
    [JsonPropertyName("roles")] public string[] Roles { get; set; }
    [JsonPropertyName("accessToken")] public string AccessToken { get; set; }
    [JsonPropertyName("refreshToken")] public string RefreshToken { get; set; }
}