﻿using System;


namespace WebApi.Models.InputModel;

public class CandleRequest
{
    public required string Pair { get; set; }
    public required TimeFrame TimeFrame { get; set; }

    public required DateTime StartTime { get; set; }
    public required DateTime EndTime { get; set; }
}