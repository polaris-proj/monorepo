﻿using Domain;

namespace WebApi.Models.InputModel;

public class ExchangeKey
{
    public required Exchange Exchange { get; set; }
    public required string Secret { get; set; }
    public required string Token { get; set; }
}