﻿using System;
using Domain;

// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
#pragma warning disable CS8618
namespace WebApi.Models.InputModel;

public class StartEndForStrategyModel
{
    public Guid StrategyId { get; set; }
    public Exchange Exchange { get; set; }
    public Pair Pair { get; set; }
    public TimeFrames.TimeFrame TimeFrame { get; set; }

    public DateTime StartTime { get; set; }
    public DateTime EndTime { get; set; }
}