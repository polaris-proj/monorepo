﻿using System;

namespace WebApi.Models.InputModel;

public class StartTestOfStrategyModel : StartStrategyModel
{
    public decimal InitialBalance { get; set; }
    public bool IsRealTimeTestMode { get; set; }
    public bool EnableOrderConfirmation { get; set; }
    public DateTime EndTime { get; set; }
}