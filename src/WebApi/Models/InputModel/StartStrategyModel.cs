﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.InputModel;

public class StartStrategyModel
{
    [Required] public Guid StrategyId { get; set; }
    [Required] public ParameterModel[] Parameters { get; set; }
    [Required] public ExchangeSources[] AllowedSource { get; set; }
    [Required] public DateTime StartTime { get; set; }
}