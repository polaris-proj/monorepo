﻿using System;
using System.Collections.Generic;
using System.Text;
using EventStore.Client;
using Infrastructure;
using Infrastructure.Grpc;
using Infrastructure.Json;
using Infrastructure.Mappers.Interfaces;
using Infrastructure.Metric;
using IODataModule.Client;
using IODataModule.Client.Clients;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using OrderConfirmation.Client;
using Prometheus;
using StrategyProcessor.Client;
using WebApi.Infrastructure.Auth;
using WebApi.Infrastructure.DAL;
using WebApi.Infrastructure.DAL.Repositories;

namespace WebApi;

public static class Startup
{
    public static void ConfigureServices(this IServiceCollection services, IWebHostEnvironment env)
    {
        services.AddHealthChecks();
        services.AddLogging(x => x.AddConsole());
        services.AddCors();
        services.AddDistributedMemoryCache();
        services.AddSession();
        services.AddEndpointsApiExplorer();
        services.AddDbContextFactory<WebApiApplicationContext>();

        services.AddScoped<StrategyUserRunsRepository>();
        services.AddScoped<StrategyUserPresentationRepository>();
        services.AddScoped<TestParametersRepository>();
        services.AddSingleton<UserRepository>();
        services.AddSingleton<RefreshTokenRepository>();

        services.AddScoped<ICandleService, CandleService>();

        services.ConfigureGrpcChannel<IoDataModuleClientChannel, IoDataModuleGrpcServiceParameters>(env);
        services.ConfigureGrpcChannel<EventStoreClientChannel, EventStoreClientParameters>(env);
        services.ConfigureGrpcChannel<StrategyProcessorClientChannel, StrategyProcessorClientParameters>(env);

        services.AddScoped<IStrategyProcessorClient, StrategyProcessorClient>();
        services.AddScoped<EventStoreClient>();
        services.AddScoped<IUserSettingsClient, UserSettingsClient>();
        services.AddScoped<IUserExchangeKeysClient, UserExchangeKeysClient>();

        services.AddSingleton<IJwtAuthManager, JwtAuthManager>();
        services.AddSingleton<IVault, Vault>();

        services.Scan(i =>
        {
            i.FromCallingAssembly()
                .AddClasses(cl => cl.AssignableTo<StartUpService>()).As<StartUpService>().WithScopedLifetime()
                .AddClasses(cl => cl.AssignableTo<IMap>()).AsSelfWithInterfaces().WithSingletonLifetime();

            i.FromAssembliesOf(typeof(CandleService), typeof(OrderSenderClient), typeof(InfrastructureAssembly),
                              typeof(StrategyProcessorClient), typeof(EventStoreClient))
                .AddClasses(cl => cl.AssignableTo<IParameters>()).AsSelf().WithSingletonLifetime();
        });


        services.AddHostedService<JwtRefreshTokenCache>();

        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo {Title = "My API", Version = "v1"});
            c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Description = @"JWT Authorization header using the Bearer scheme.
                      Enter 'Bearer' [space] and then your token in the text input below.
                      Example: 'Bearer 12345abcdef'",
                Name = "Authorization",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer"
            });

            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme {Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "Bearer"}},
                    new string[] { }
                }
            });
        });
        services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = true;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = JwtTokenConfig.Issuer,
                    ValidAudience = JwtTokenConfig.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtTokenConfig.Secret)),
                    ClockSkew = TimeSpan.FromMinutes(1)
                };
            });

        services.AddControllers()
            .AddJsonOptions(x => JsonOptions.Setup(x.JsonSerializerOptions));

        var port = services.BuildServiceProvider(validateScopes: true).GetRequiredService<MetricSettings>().Port;

        if (env.IsProduction())
        {
            services.AddMetricServer(x => x.Port = port);
        }
    }

    public static void ConfigureApp(this IApplicationBuilder app, IWebHostEnvironment context)
    {
        app.UseCors(builder => builder.AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod()
        );

        if (context.IsProduction())
        {
            SetupMetrics(app);
        }
        
        if (context.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        
        app.UseSession();
        app.AddBearerHeaderHandlerMiddleware();

        app.UseRouting();
        app.UseAuthentication();
        app.UseAuthorization();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapSwagger();
            endpoints.MapControllers();
            endpoints.MapHealthChecks("health");
        });
    }

    private static void AddBearerHeaderHandlerMiddleware(this IApplicationBuilder app)
    {
        app.Use(async (ctx, next) =>
        {
            var token = ctx.Session.GetString("Token");
            if (!string.IsNullOrEmpty(token))
                ctx.Request.Headers.Append("Authorization", "Bearer " + token);
            await next();
        });
    }

    private static void SetupMetrics(IApplicationBuilder app)
    {
        Metrics.SuppressDefaultMetrics();
        Metrics.DefaultRegistry.SetStaticLabels(new Dictionary<string, string>
        {
            {"service_name", "WebApi"}
        });
        
        app.UseHttpMetrics();
    }
}