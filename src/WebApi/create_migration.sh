#!/bin/bash
MIGRATION_NAME=$1
dotnet ef migrations add --project WebApi.csproj --startup-project WebApi.csproj --context WebApi.Infrastructure.DAL.WebApiApplicationContext --configuration Debug $MIGRATION_NAME --output-dir Infrastructure\Migrations