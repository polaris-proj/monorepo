﻿using System.Net;
using System.Net.Http.Json;
using FluentAssertions;
using IODataModule.Shared.Dto;
using Microsoft.AspNetCore.WebUtilities;
using NUnit.Framework;
using WebApi.FunctionalTests.Mocks;

namespace WebApi.FunctionalTests;

public class UserControllerTests : BaseTests
{
    [Test]
    public async Task AddExchangeKey_ShouldFail401_WhenNoAuth()
    {
        using var client = NoAuthHttpClient;

        var resp = await client.PostAsync("user/AddExchangeKey", null);

        resp.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }
    
    [Test]
    public async Task AddExchangeKey_ShouldSucceed()
    {
        using var client = AuthHttpClient;

        var uri = "user/AddExchangeKey";

        uri = QueryHelpers.AddQueryString(uri, new Dictionary<string, string?>
        {
            ["exchange"] = "1",
            ["secret"] = "some_secret",
            ["token"] = "some_token"
        });
        
        var resp = await client.PostAsync(uri, null);

        resp.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Test]
    public async Task GetUserInfo_ShouldFailed401_WhenNoAuth()
    {
        using var client = NoAuthHttpClient;

        var resp = await client.GetAsync("user/GetUserInfo");

        resp.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }
    
    [Test]
    public async Task GetUserInfo_ShouldSuccess()
    {
        using var client = AuthHttpClient;

        var resp = await client.GetAsync("user/GetUserInfo");

        resp.StatusCode.Should().Be(HttpStatusCode.OK);

        var content = await resp.Content.ReadFromJsonAsync<Guid>();

        content.Should().NotBeEmpty();
    }

    [Test]
    public async Task GetLinkToTelegramBot_ShouldSucceed()
    {
        using var client = AuthHttpClient;

        var resp = await client.GetAsync("user/GetLinkToTelegramBot");

        resp.StatusCode.Should().Be(HttpStatusCode.OK);

        var content = await resp.Content.ReadAsStringAsync();
        content.Should().Be(MockUserSettingsClient.MockTelegramLink);
    }
    
    [Test]
    public async Task GetLinkToTelegramBot_ShouldFail401_WhenNoAuth()
    {
        using var client = NoAuthHttpClient;

        var resp = await client.GetAsync("user/GetLinkToTelegramBot");

        resp.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }

    [Test]
    public async Task SendMessage_ShouldSucceed()
    {
        using var client = NoAuthHttpClient;

        var resp = await client.PostAsync("user/SendMessage", JsonContent.Create("something"));

        resp.StatusCode.Should().Be(HttpStatusCode.OK);

        var content = await resp.Content.ReadAsStringAsync();
        content.Should().Be(MockUserSettingsClient.MockMessage);
    }

    [Test]
    public async Task GetExchangeKeys_ShouldSuccess()
    {
        using var client = AuthHttpClient;

        var resp = await client.GetAsync("user/ExchangeKeys");

        resp.StatusCode.Should().Be(HttpStatusCode.OK);

        var content = await resp.Content.ReadFromJsonAsync<ExchangeKeyDto[]>();
        content.Should().BeEquivalentTo(MockUserExchangeClient.GetPrivateExchangeKeys());
    }
    
    [Test]
    public async Task GetExchangeKeys_ShouldFail401_WhenNoAuth()
    {
        using var client = NoAuthHttpClient;

        var resp = await client.GetAsync("user/ExchangeKeys");

        resp.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }
    
    [Test]
    public async Task RemoveExchangeKeys_ShouldSuccess()
    {
        using var client = AuthHttpClient;

        var resp = await client.DeleteAsync("user/ExchangeKey?exchange=0");

        resp.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    
    [Test]
    public async Task RemoveExchangeKeys_ShouldFail401_WhenNoAuth()
    {
        using var client = NoAuthHttpClient;

        var resp = await client.DeleteAsync("user/ExchangeKey?exchange=0");

        resp.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }
}