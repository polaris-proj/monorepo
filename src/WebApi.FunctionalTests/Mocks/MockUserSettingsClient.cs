﻿using OrderConfirmation.Client;

namespace WebApi.FunctionalTests.Mocks;

public class MockUserSettingsClient : IUserSettingsClient
{
    public const string MockMessage = "test_message_return";
    public const string MockTelegramLink = "test_link_return";
    
    public Task<string> SendMessage(string message)
    {
        return Task.FromResult(MockMessage);
    }

    public Task<string> GetLinkToTelegramBot(Guid userId)
    {
        return Task.FromResult(MockTelegramLink);
    }
}