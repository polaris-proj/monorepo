﻿using Domain;
using IODataModule.Client.Clients;
using IODataModule.Shared.Dto;
using WebApi.Controllers;

namespace WebApi.FunctionalTests.Mocks;

public class MockUserExchangeClient : IUserExchangeKeysClient
{
    private static readonly ExchangeKeyDto[] ExchangeKeys = {
        new()
        {
            Exchange = Exchange.Poloniex,
            Key = "some_key_567f61ed-f0ed-4cd1-a885-1a62e3524c41",
            Secret = "some_secret_567f61ed-f0ed-4cd1-a885-1a62e3524c41"
        },
        new()
        {
            Exchange = Exchange.Poloniex,
            Key = "some_key_4ac40faa-5a9e-49c7-8cf3-76e505f9b40e",
            Secret = "some_secret_4ac40faa-5a9e-49c7-8cf3-76e505f9b40e"
        }
    };

    public static ExchangeKeyDto[] GetPrivateExchangeKeys()
    {
        return ExchangeKeys.Select(k => 
            new ExchangeKeyDto
            {
                Exchange = k.Exchange,
                Key = UserController.MakePrivate(k.Key),
                Secret = UserController.MakePrivate(k.Secret)
            })
            .ToArray();
    }
    
    public Task<ExchangeKeyDto> GetExchangeKey(Guid userId, Exchange exchange)
    {
        return Task.FromResult(ExchangeKeys[0]);
    }

    public Task<ExchangeKeyDto[]> GetAllExchangeKeys(Guid userId)
    {
        return Task.FromResult(ExchangeKeys);
    }

    public Task AddOrUpdateExchangeKey(Guid userId, Exchange exchange, string secret, string key)
    {
        return Task.CompletedTask;
    }

    public Task RemoveExchangeKey(Guid userId, Exchange exchange)
    {
        return Task.CompletedTask;
    }
}