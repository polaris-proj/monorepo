using System.Net;
using System.Net.Http.Headers;
using Dapper;
using FluentAssertions;
using Infrastructure;
using IODataModule.Client.Clients;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Npgsql;
using NUnit.Framework;
using OrderConfirmation.Client;
using WebApi.FunctionalTests.Mocks;
using WebApi.Models.InputModel;
using WebApi.Models.ViewModel;

namespace WebApi.FunctionalTests;

public abstract class BaseTests
{
    private const string Environment = "FunctionalTests";
    
    protected const string
        BaseLogin = "baseuser",
        BaseEmail = "baseuser@mail.com",
        BasePassword = "basepassword_123";

    protected string BaseAuthToken = null!;
    
    [SetUp]
    public void BaseSetup()
    {
    }

    [TearDown]
    public void BaseTearDown()
    {
    }

    [OneTimeSetUp]
    public async Task BaseOneTimeSetup()
    {
        WebApiFactory = GetWebApiFactory();
        Scope = WebApiFactory.Services.CreateScope();
        DropTables().GetAwaiter().GetResult();
        await CreateBaseUser();
    }

    [OneTimeTearDown]
    public void BaseOneTimeTearDown()
    {
        DropTables().GetAwaiter().GetResult();
        Scope.Dispose();
    }

    protected async Task DropTables()
    {
        var postgresParameters = Scope.ServiceProvider.GetRequiredService<PostgresParameters>();
        var connectionString = postgresParameters.FullConnectionString;

        await using var connection = new NpgsqlConnection(connectionString);
        await connection.ExecuteAsync("DROP SCHEMA public CASCADE; CREATE SCHEMA public;");
    }

    private WebApplicationFactory<Program> GetWebApiFactory()
    {
        // mocking other service cause testing them in other tests
        return new WebApplicationFactory<Program>()
            .WithWebHostBuilder(b =>
            {
                b.UseEnvironment(Environment);
                b.ConfigureTestServices(s =>
                {
                    s.AddScoped<IUserSettingsClient, MockUserSettingsClient>();
                    s.AddScoped<IUserExchangeKeysClient, MockUserExchangeClient>();
                });
            });
    }

    private async Task CreateBaseUser()
    {
        var registerData = new RegisterData
        {
            Login = BaseLogin,
            Password = BasePassword,
            Email = BaseEmail
        };

        var loginData = new LoginData
        {
            Login = BaseLogin,
            Password = BasePassword
        };


        using var client = NoAuthHttpClient;

        var registerContent = JsonContent.Create(registerData);
        var registerResp = await client.PostAsync("register", registerContent);
        registerResp.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var loginContent = JsonContent.Create(loginData);
        var loginResp = await client.PostAsync("login", loginContent);
        loginResp.StatusCode.Should().Be(HttpStatusCode.OK);

        var body = await loginResp.Content.ReadFromJsonAsync<LoginResult>();
        body.Should().NotBeNull();

        BaseAuthToken = body!.AccessToken;
    }
    
    protected HttpClient AuthHttpClient => GetAuthClient(BaseAuthToken);

    protected HttpClient NoAuthHttpClient => WebApiFactory.CreateClient();

    protected HttpClient GetAuthClient(string accessToken)
    {
        var client = NoAuthHttpClient;
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
        return client;
    }
    
    internal WebApplicationFactory<Program> WebApiFactory;
    protected IServiceScope Scope = null!;
}