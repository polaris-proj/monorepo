﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.PatternRecognition;
using Domain.StrategyProcessor;

namespace PatternRecognition.Algorithms;

public class SliceAlgorithm
{
    public const decimal DifferentInPercent = 0.7m;
    public const int MinAmountCandles = 5; // >3
    public const int MaxAmountCandles = 20; // >2
    public const int MinCountTouchOfPrice = 3;
    public const int MinAmountOfCombinations = 1;
    public const decimal StableFactor = 0.10m;

    public static (decimal, decimal) CalcFactors(List<Point> twoDots)
    {
        if (twoDots[1].TimeStamp - twoDots[0].TimeStamp == 0)
            return (0, twoDots[0].Value);
        return ((twoDots[0].Value - twoDots[1].Value) / (twoDots[1].TimeStamp - twoDots[0].TimeStamp),
            (twoDots[0].TimeStamp * twoDots[1].Value - twoDots[1].TimeStamp * twoDots[0].Value) /
            (twoDots[1].TimeStamp - twoDots[0].TimeStamp));
    }

    public static List<Point> GetTouches(Point border, List<Candle> dots, Direction dir)
    {
        var factorB = border.Value;
        var touches = new List<Point> {new(0, 0)};
        for (var i = 0; i < dots.Count; i++)
            if (dir == Direction.Up)
            {
                var expectedPrice = factorB;
                var priceDelta = Math.Abs(expectedPrice - dots[i].High);
                if (priceDelta <= DifferentInPercent * expectedPrice
                    && -touches.Last().TimeStamp + dots[i].TimeStamp >= 86400000 * 2)
                    touches.Add(new Point(dots[i].TimeStamp, dots[i].High));
            }
            else
            {
                var expectedPrice = factorB;
                var priceDelta = Math.Abs(expectedPrice - dots[i].Low);
                if (priceDelta <= DifferentInPercent * expectedPrice
                    && -touches.Last().TimeStamp + dots[i].TimeStamp >= 86400000 * 2)
                    touches.Add(new Point(dots[i].TimeStamp, dots[i].Low));
            }

        touches.RemoveAt(0);
        return touches;
    }

    //TODO interface
    public static List<Accumulation> FindBoxes(List<Candle> zigZag)
    {
        var boxes = new List<Accumulation>();
        for (var i = 0; i < zigZag.Count; i++)
        for (var j = Math.Min(zigZag.Count, i + MaxAmountCandles); j > i + MinAmountCandles; j--)
        {
            var section = zigZag.GetRange(i, j - i);
            var y = IsAccum(section);
            if (y.Item1)
            {
                var foundedBox = new Accumulation
                {
                    StartTimeStamp = section.First().TimeStamp,
                    LowPrice = y.Item2,
                    EndTimeStamp = section.Last().TimeStamp,
                    HighPrice = y.Item3,
                    Type = AccumulationType.Rectangle
                };
                //Можно оптимизировать ,если не создавать foundedBox
                if (boxes.Count > 0 && boxes.Last().EndTimeStamp >= foundedBox.StartTimeStamp)
                {
                    var last = boxes.Last();
                    var newBox = new Accumulation
                    {
                        StartTimeStamp = Math.Min(last.StartTimeStamp, foundedBox.StartTimeStamp),
                        LowPrice = Math.Min(last.LowPrice, foundedBox.LowPrice),
                        EndTimeStamp = Math.Max(last.EndTimeStamp, foundedBox.EndTimeStamp),
                        HighPrice = Math.Max(last.HighPrice, foundedBox.HighPrice),
                        Type = AccumulationType.Rectangle
                    };
                    boxes.RemoveAt(boxes.Count - 1);
                    boxes.Add(newBox);
                }
                else
                {
                    boxes.Add(foundedBox);
                }
            }
        }

        return boxes;
    }

    private static (bool, decimal, decimal) IsAccum(List<Candle> section)
    {
        const int resolution = 40;
        var (atl, ath) = GetMaxsAndMins(section);
        var delta = (ath.Value - atl.Value) / (resolution - 1);
        var list = new int[resolution];

        foreach (var candle in section)
        {
            if (delta == 0)
                continue;

            var startN = (int) ((candle.Low - atl.Value) / delta);
            var endN = (int) ((candle.High - atl.Value) / delta);

            try
            {
                for (var step = startN; step <= endN; step++)
                    list[step] += 1;
            }
            catch (Exception)
            {
                Console.WriteLine();
            }

            startN = (int) ((Math.Min(candle.Open, candle.Close) - atl.Value) / delta);
            endN = (int) ((Math.Max(candle.Open, candle.Close) - atl.Value) / delta);


            for (var step = startN; step <= endN; step++)
            {
                try
                {
                    list[step] += 1;
                }
                catch (Exception)
                {
                    Console.WriteLine();
                }
            }
        }

        var median = MidArifm(list.Select(x => (decimal) x).ToList());
        var box = new List<decimal>();
        for (var a = 0; a < list.Length; a++)
            if (list[a] >= median)
                box.Add(a * delta + atl.Value);

        return (CheckProjectionOfBox(0.65m, section) && IsStable(section)
                                                     && CheckSquareOfBox(0.36m, section), box.Min(), box.Max());
    }

    private static bool CheckProjectionOfBox(decimal kFactor, List<Candle> section)
    {
        const int resolution = 40;
        var (atl, ath) = GetMaxsAndMins(section);
        var delta = (ath.Value - atl.Value) / (resolution - 1);
        var list = new int[resolution];

        foreach (var candle in section)
        {
            if (delta == 0)
                continue;

            var startN = (int) ((candle.Low - atl.Value) / delta);
            var endN = (int) ((candle.High - atl.Value) / delta);
            for (var step = startN; step <= endN; step++)
                list[step] += 1;
            startN = (int) ((Math.Min(candle.Open, candle.Close) - atl.Value) / delta);
            endN = (int) ((Math.Max(candle.Open, candle.Close) - atl.Value) / delta);
            for (var step = startN; step <= endN; step++)
                list[step] += 1;
        }

        var median = MidArifm(list.Select(x => (decimal) x).ToList());
        var box = new List<decimal>();
        for (var a = 0; a < list.Length; a++)
            if (list[a] >= median)
                box.Add(a * delta + atl.Value);

        return resolution * kFactor >= box.Count();
    }

    private static bool CheckSquareOfBox(decimal kFactor, List<Candle> section)
    {
        var sectionSquare = (section.Select(x => x.High).Max() - section.Select(x => x.Low).Min()) * section.Count;
        var candlesSquare = 0m;
        foreach (var t in section)
            //candlesSquare += (Math.Abs(t.Open - t.Close)*0.4m + Math.Abs(t.High - t.Low) *0.6m);
            candlesSquare += (Math.Abs(t.Open - t.Close) + Math.Abs(t.High - t.Low)) / 2;

        return candlesSquare >= sectionSquare * kFactor;
    }

    private static decimal MidArifm(List<decimal> sourceNumbers)
    {
        return sourceNumbers.Sum() / sourceNumbers.Count;
    }

    private static bool IsStable(List<Candle> section)
    {
        var medianDelta = new List<decimal>();
        decimal maxMedian = 0;
        foreach (var t in section)
        {
            medianDelta.Add(t.High - t.Close);
            maxMedian = Math.Max(maxMedian, t.High - t.Close);
        }

        var median = GetMedian(medianDelta);
        return maxMedian * StableFactor < median;
    }

    private static void ExtendBox(List<Candle> zigZag, List<Candle> section)
    {
        var a = section.OrderBy(x => x.High).ToList();
        var topBorder = a[^1].High; // firstMax, secondMax
        var b = section.OrderBy(x => x.Low).ToList();
        var lowBorder = b[0].Low;
        var firstIndex = section.IndexOf(section.First());
        var lastIndex = section.IndexOf(section.Last());

        for (var i = lastIndex - 1; i < zigZag.Count; i++)
            if (lowBorder <= zigZag[i].Low * 0.98m && zigZag[i].High * 1.02m <= topBorder)
                section.Add(zigZag[i]);
            else
                break;

        for (var i = firstIndex - 1; i >= 0; i--)
            if (lowBorder <= zigZag[i].Low * 0.98m && zigZag[i].High * 1.02m <= topBorder)
                section.Add(zigZag[i]);
            else
                break;
    }

    private static decimal GetMedian(List<decimal> sourceNumbers)
    {
        if (sourceNumbers == null || sourceNumbers.Count == 0)
            throw new Exception("Median of empty array not defined.");

        var sortedPNumbers = new List<decimal>(sourceNumbers);
        sortedPNumbers.Sort();
        var size = sortedPNumbers.Count;
        var mid = size / 2;
        var median = size % 2 != 0 ? sortedPNumbers[mid] : (sortedPNumbers[mid] + sortedPNumbers[mid - 1]) / 2;
        return median;
    }

    private static (Point, Point) GetMaxsAndMins(List<Candle> candles)
    {
        Candle? maxCandle = null;
        var maxPrice = decimal.MinValue;

        Candle? minCandle = null;
        var minPrice = decimal.MaxValue;

        foreach (var candle in candles)
        {
            var prices = new[] {candle.Close, candle.Open, candle.High, candle.Low};
            var max = prices.Max();
            var min = prices.Min();

            if (max > maxPrice)
            {
                maxPrice = max;
                maxCandle = candle;
            }

            if (min < minPrice)
            {
                minPrice = min;
                minCandle = candle;
            }
        }

        if (maxCandle is null || minCandle is null)
            throw new Exception();


        var highDots = new Point(maxCandle.TimeStamp, maxPrice);
        var lowDots = new Point(minCandle.TimeStamp, minPrice);
        return (lowDots, highDots);
    }

    public enum Direction
    {
        Up,
        Down
    }
}