﻿using System;
using System.Collections.Generic;
using Domain.PatternRecognition;
using Domain.StrategyProcessor;

namespace PatternRecognition.Algorithms;

internal class TrendDetector
{
    public static event Action<Point> PpDetected = null!;
    public static event Action<Point> SlomDetected = null!;

    //public static event Action PPUpDetected;
    //public static event Action SlomUpDetected;
    //public static event Action PPDownDetected;
    //public static event Action SlomDownDetected;

    public static List<Segment> TrendDetect(List<Point> dots)
    {
        if (dots.Count < 2)
            throw new Exception("Мало данных");

        var result = new List<Segment>();
        var lastPpNum = -1;
        var lastSlomNum = -1;

        var startTrend = dots[0].Value < dots[2].Value ? Trend.Up : Trend.Down;
        var currentTrend = startTrend;

        for (var n = 3; n < dots.Count; n++)
        {
            if (currentTrend == Trend.Up && dots[n - 1].Value > dots[n - 0].Value &&
                dots[n - 1].Value > dots[n - 2].Value)
            {
                if (dots[n - 1].Value > dots[n - 3].Value)
                    lastPpNum = n - 2;
                //Console.WriteLine(dots[lastPPNum]);

                if (dots[n - 0].Value > dots[n - 2].Value && dots[n - 1].Value > dots[n - 3].Value)
                {
                    lastSlomNum = n;
                }
                //Console.WriteLine(dots[lastSlomNum]);

                else if (dots[n - 2].Value > dots[n - 0].Value)
                {
                    //Console.WriteLine($"ПП в short {dots[n - 2]}");
                    if (lastPpNum != -1)
                    {
                        result.Add(new Segment(dots[lastPpNum].TimeStamp, dots[lastPpNum].Value,
                            dots[lastPpNum].TimeStamp + 500000000, dots[lastPpNum].Value));

                        PpDetected(new Point(dots[lastPpNum].TimeStamp, dots[lastPpNum].Value));
                        //PPDownDetected();
                    }

                    if (lastSlomNum != -1)
                    {
                        result.Add(new Segment(dots[lastSlomNum].TimeStamp, dots[lastSlomNum].Value,
                            dots[lastSlomNum].TimeStamp + 500000000, dots[lastSlomNum].Value));

                        SlomDetected(new Point(dots[lastSlomNum].TimeStamp, dots[lastSlomNum].Value));
                        //SlomDownDetected();
                        //Console.WriteLine($"Слом в short {lastSlom}");
                    }

                    currentTrend = Trend.Down;
                }
            }

            if (currentTrend == Trend.Down && dots[n - 1].Value < dots[n - 0].Value &&
                dots[n - 1].Value < dots[n - 2].Value)
            {
                if (dots[n - 1].Value < dots[n - 3].Value)
                    lastPpNum = n - 2;
                //Console.WriteLine(dots[lastPPNum]);

                if (dots[n - 0].Value < dots[n - 2].Value && dots[n - 1].Value < dots[n - 3].Value)
                {
                    lastSlomNum = n;
                }

                else if (dots[n - 2].Value < dots[n - 0].Value)
                {
                    //Console.WriteLine($"ПП в short {dots[n - 2]}");
                    if (lastPpNum != -1)
                    {
                        result.Add(new Segment(dots[lastPpNum].TimeStamp, dots[lastPpNum].Value,
                            dots[lastPpNum].TimeStamp + 500000000, dots[lastPpNum].Value));

                        PpDetected(new Point(dots[lastPpNum].TimeStamp, dots[lastPpNum].Value));
                        //PPUpDetected();
                    }

                    if (lastSlomNum != -1)
                    {
                        result.Add(new Segment(dots[lastSlomNum].TimeStamp, dots[lastSlomNum].Value,
                            dots[lastSlomNum].TimeStamp + 500000000, dots[lastSlomNum].Value));

                        SlomDetected(new Point(dots[lastSlomNum].TimeStamp, dots[lastSlomNum].Value));
                        //SlomUpDetected();
                    }

                    currentTrend = Trend.Up;
                }
            }
        }

        //for (int n = 4; n < dots.Count; n++)
        //{
        //    if (dots[n - 0].Price < dots[n - 2].Price
        //            && dots[n - 2].Price > dots[n - 4].Price
        //            && dots[n - 1].Price > dots[n - 2].Price)
        //    {
        //        if (dots[n - 3].Price <= dots[n - 1].Price)
        //        {
        //            Console.Write(dots[n - 2].Price);
        //            Console.WriteLine(new Mark(dots[n - 2].TimeStamp, "Down PP", 0, string.Empty, 0));
        //        }
        //        else
        //        {
        //            Console.Write(dots[n - 2].Price);
        //            Console.WriteLine(new Mark(dots[n - 2].TimeStamp, "Down Slom", 0, string.Empty, 0));
        //        }
        //        currentTrend = Trend.Down;
        //    }
        //    if (dots[n - 0].Price > dots[n - 2].Price
        //            && dots[n - 2].Price < dots[n - 4].Price
        //            && dots[n - 1].Price < dots[n - 2].Price)
        //    {
        //        if (dots[n - 3].Price >= dots[n - 1].Price)
        //        {
        //            Console.Write(dots[n - 2].Price);
        //            Console.WriteLine(new Mark(dots[n - 2].TimeStamp, "Up PP", 0, string.Empty, 0));
        //        }
        //        else
        //        {
        //            Console.Write(dots[n - 2].Price);
        //            Console.WriteLine(new Mark(dots[n - 2].TimeStamp, "Up Slom", 0, string.Empty, 0));
        //        }
        //        currentTrend = Trend.Up;
        //    }
        //}
        return result;
    }

    public enum Trend
    {
        Down,
        Up,
        Side
    }
}