﻿// ReSharper disable InconsistentNaming
namespace PatternRecognition.Shared;

public class NewPatternEventDto
{
    public int exchange { get; set; }
    public PairDto pair { get; set; }
    public int timeframe { get; set; }
    public long timestamp { get; set; }
    public RecognitionResultDto recognition_result { get; set; }
}