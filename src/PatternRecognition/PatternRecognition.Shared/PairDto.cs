﻿// ReSharper disable InconsistentNaming
namespace PatternRecognition.Shared;

public class PairDto
{
    public string first { get; set; }
    public string second { get; set; }
}