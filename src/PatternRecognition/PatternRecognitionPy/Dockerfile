FROM python:3.11.7-slim as dependencies

RUN apt-get update && apt-get install -y --no-install-recommends \
    make \
    python3-dev && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /opt

COPY ./pyproject.toml ./poetry.lock /opt/

ARG POETRY_VERSION

RUN pip install --no-cache-dir poetry==${POETRY_VERSION} && \
    poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi --only main

FROM dependencies as production

COPY ./pattern_recognizer /opt/pattern_recognizer
COPY ./resources /opt/resources
COPY ./grpc_python_server /opt/grpc_python_server

WORKDIR /opt

ENTRYPOINT ["poetry", "run", "python", "-m", "pattern_recognizer"]

FROM dependencies as development

RUN poetry install

WORKDIR /opt

CMD ["bash"]