from unittest.mock import create_autospec, Mock

import pytest
from confluent_kafka import Producer

from grpc_python_server.ITest_pb2 import Exchange, TimeFrame
from pattern_recognizer.infra.kafka.new_candle_event_handler import NewCandleEventHandler
from pattern_recognizer.models.candle import Candle
from pattern_recognizer.models.new_candle_event import NewCandleEvent
from pattern_recognizer.models.pair import Pair
from pattern_recognizer.models.pattern import PatternType
from pattern_recognizer.predictor.data_models import RecognitionResult
from pattern_recognizer.predictor.factory import create_recognizer
from pattern_recognizer.predictor.recognizer import PatternRecognizer


@pytest.fixture
def new_candle_event_handler() -> NewCandleEventHandler:
    handler = NewCandleEventHandler(pattern_recognizer=create_recognizer(), window_size=32)
    handler.producer = create_autospec(Producer)
    handler.producer.produce = Mock()
    handler._pattern_recognizer = create_autospec(PatternRecognizer)

    def fake_recognize(candles: list):
        if len(candles) < 6:
            return RecognitionResult.failed()

        return RecognitionResult(
            used_candle_timestamps=list(range(len(candles))),
            pattern_to_proba={
                PatternType.NO_PATTERN: 0.1,
                PatternType.UP_IMPULSE_OR_DIAG_EXPAND: 0.9,
            },
            most_probable_pattern=PatternType.UP_IMPULSE_OR_DIAG_EXPAND,
            is_successful=True,
        )

    handler._pattern_recognizer.recognize = Mock(side_effect=fake_recognize)
    return handler


@pytest.mark.parametrize(
    "candles",
    [
        [(1, 0.5), (2, 0.4), (3, 0.0), (4, 0.2), (5, 0.7), (6, 1.0)],
    ]
)
def test_handle_event(new_candle_event_handler: NewCandleEventHandler, candles):
    produce_method: Mock = new_candle_event_handler.producer.produce

    for candle in candles:
        new_candle_event_handler.handle_message(event=NewCandleEvent(
            exchange=Exchange.BinanceSpotTest,
            pair=Pair(first='BTC', second='USD'),
            timeframe=TimeFrame.h1,
            candle=Candle(timestamp=candle[0], close=candle[1], open=0.1, high=0.1, low=0.1, volume=0.1)
        ))

    produce_method.assert_called_once()
