from torch import nn

from pattern_recognizer.predictor.network.rbm import RBM


class Classifier(nn.Module):
    def __init__(self, backbone, embed_size, num_classes):
        super().__init__()
        self.backbone = backbone
        self.head = nn.Linear(embed_size, num_classes)

    def forward(self, x):
        for name, module in self.backbone.items():
            x = module(x)
        return self.head(x)


def create_perceptron_classifier(models: list[RBM], num_classes: int, activation_cls=nn.ReLU) -> Classifier:
    modules = nn.ModuleDict()
    out_size = None

    for i, model in enumerate(models):
        v = model.h.data.squeeze().cpu()
        W = model.W.data.cpu()
        fc = nn.Linear(W.shape[1], W.shape[0], bias=True)
        fc.weight.data = W.clone().requires_grad_()
        fc.bias.data = v.clone().requires_grad_()

        modules[f"linear{i + 1}"] = fc
        modules[f"act{i + 1}"] = activation_cls()

        out_size = W.shape[0]

    return Classifier(modules, out_size, num_classes)
