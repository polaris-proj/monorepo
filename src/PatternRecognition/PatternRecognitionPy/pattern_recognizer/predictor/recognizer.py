import numpy as np
import torch

from pattern_recognizer.models.candle import Candle
from pattern_recognizer.models.pattern import PatternType
from pattern_recognizer.predictor.preprocessing import (
    min_max_normalize_short_candle,
    plr_vip_short_candle,
)
from pattern_recognizer.predictor.data_models import RecognitionResult, ShortCandle
from pattern_recognizer.predictor.network.bp import Classifier


class PatternRecognizer:
    def __init__(
            self,
            classifier: Classifier,
            num_points: int,
            device: torch.device | str = "cpu",
    ) -> None:
        self._device = device
        self._classifier = classifier.to(device).eval()
        self._num_points = num_points
        self._idx_to_pattern: dict[int, PatternType] = {i: p for i, p in enumerate(PatternType)}  # type: ignore[misc]

    def recognize(self, candles: list[Candle]) -> RecognitionResult:
        if len(candles) < self._num_points:
            return RecognitionResult.failed("Недостаточно свечей для предсказания")

        plr_indices = plr_vip_short_candle(candles, point_count=self._num_points)
        extracted_candles: list[Candle] = [c for i, c in enumerate(candles) if i in plr_indices]
        normalized = min_max_normalize_short_candle(extracted_candles)
        if normalized is None:
            return RecognitionResult.failed("График не отличается от прямой линии")

        probas = self._predict_proba(normalized)
        max_idx = int(np.argmax(probas))

        pattern_to_proba: dict[PatternType, float] = {}
        for i, pattern in self._idx_to_pattern.items():
            pattern_to_proba[pattern] = probas[i]

        return RecognitionResult(
            used_candles=extracted_candles,
            pattern_to_proba=pattern_to_proba,
            most_probable_pattern=self._idx_to_pattern[max_idx],
            is_successful=True,
        )

    def _predict_proba(self, normalized_candles: list[ShortCandle]) -> list[float]:
        raw_data: list[float] = []
        for candle in normalized_candles:
            raw_data.append(candle.timestamp)
            raw_data.append(candle.price)

        input_tensor = torch.tensor([raw_data], device=self._device, dtype=torch.float32)
        with torch.inference_mode():
            preds = self._classifier(input_tensor).cpu()

        return torch.softmax(preds, dim=-1)[0].numpy().tolist()
