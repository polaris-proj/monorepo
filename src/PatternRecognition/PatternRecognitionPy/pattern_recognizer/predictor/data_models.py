import typing as tp
from dataclasses import dataclass

from pattern_recognizer.models.candle import Candle
from pattern_recognizer.models.pattern import PatternType


@dataclass
class ShortCandle:
    price: float
    timestamp: float


@dataclass
class RecognitionResult:
    used_candles: list[Candle]
    pattern_to_proba: dict[PatternType, float]
    most_probable_pattern: PatternType
    is_successful: bool
    fail_reason: str | None = None

    @classmethod
    def failed(cls, reason: str | None = None) -> tp.Self:
        return cls(
            used_candles=[],
            pattern_to_proba={},
            most_probable_pattern=PatternType.NO_PATTERN,
            is_successful=False,
            fail_reason=reason,
        )
