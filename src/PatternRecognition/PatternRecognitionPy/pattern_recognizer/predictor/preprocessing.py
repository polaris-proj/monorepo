from pattern_recognizer.models.candle import Candle
from pattern_recognizer.predictor.data_models import ShortCandle


def min_max_normalize_short_candle(window: list[Candle]) -> list[ShortCandle] | None:
    min_val = min(candle.low for candle in window)
    max_val = max(candle.low for candle in window)
    min_ts = min(candle.timestamp for candle in window)
    max_ts = max(candle.timestamp for candle in window)

    if abs(max_val - min_val) < 1e-8:
        return None

    normalized_candles = []
    for candle in window:
        normalized_candles.append(ShortCandle(
            price=(candle.low - min_val) / (max_val - min_val),
            timestamp=(candle.timestamp - min_ts) / (max_ts - min_ts),
        ))

    return normalized_candles


def plr_vip_short_candle(window: list[Candle], point_count: int, min_dist: int = 1) -> list[int]:
    important_points_indexes = [0, len(window) - 1]
    important_points_count = 2
    while important_points_count < point_count:
        segments: list[list[int]] = []
        for i in range(len(important_points_indexes) - 1):
            idx1, idx2 = important_points_indexes[i:i + 2]
            segments.append([idx1, idx2])

        segments.sort(key=lambda x: abs(window[x[1]].timestamp - window[x[0]].timestamp), reverse=True)

        for i in range(len(segments)):
            segment = segments[i]
            if important_points_count == point_count:
                break
            max_dist: float = -1.
            max_idx: int = -1
            vip_1 = segment[0]
            vip_2 = segment[1]
            for point_idx in range(vip_1 + 1, vip_2):
                dist = vertical_distance_short_candle(window, vip_1, vip_2, point_idx)
                if dist > max_dist:
                    max_dist = dist
                    max_idx = point_idx
            if max_idx != -1:
                important_points_count += 1
                segment.insert(1, max_idx)

        important_points_indexes = sorted(list(set([point for segment in segments for point in segment])))

    return important_points_indexes


def vertical_distance_short_candle(
        window: list[Candle],
        start_point_idx: int,
        end_point_idx: int,
        point_idx: int,
) -> float:
    x1, y1 = window[start_point_idx].timestamp, window[start_point_idx].low
    x2, y2 = window[end_point_idx].timestamp, window[end_point_idx].low
    x0, y0 = window[point_idx].timestamp, window[point_idx].low

    y_line = y1 + (y2 - y1) * (x0 - x1) / (x2 - x1)

    return abs(y0 - y_line)
