from grpc_python_server.ITest_pb2 import Pair as ProtobufPair
from pattern_recognizer.models.pair import Pair as InternalPair


class PairMapper:
    @staticmethod
    def map(pair: ProtobufPair) -> InternalPair:
        return InternalPair(pair.First.Name, pair.Second.Name)
