from grpc_python_server.protobuf_net.bcl_pb2 import Decimal


class DecimalMapper:
    @staticmethod
    def decimal_to_float(decimal: Decimal) -> float: # todo fix
        lo = decimal.lo
        hi = decimal.hi
        sign = 1 if (decimal.signScale & 0x80000000) == 0 else -1
        scale = decimal.signScale & 0x00FF0000 >> 16
        return sign * (lo + hi * 2 ** 32) / 10 ** scale
