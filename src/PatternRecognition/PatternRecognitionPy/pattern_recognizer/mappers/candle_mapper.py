from grpc_python_server.ITest_pb2 import (
    Candle as ProtobufCandle,
)
from pattern_recognizer.mappers.decimal_mapper import DecimalMapper
from pattern_recognizer.models.candle import Candle as InternalCandle


class CandleMapper:
    @staticmethod
    def map(candle: ProtobufCandle) -> InternalCandle:
        return InternalCandle(
            open=DecimalMapper.decimal_to_float(candle.Open),
            close=DecimalMapper.decimal_to_float(candle.Close),
            high=DecimalMapper.decimal_to_float(candle.High),
            low=DecimalMapper.decimal_to_float(candle.Low),
            volume=DecimalMapper.decimal_to_float(candle.Volume),
            timestamp=candle.TimeStamp,
        )
