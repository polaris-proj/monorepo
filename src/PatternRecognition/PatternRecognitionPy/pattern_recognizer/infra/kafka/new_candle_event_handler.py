import dataclasses
import json
import os

from confluent_kafka import Producer

from pattern_recognizer.dto.new_pattern_event import NewPatternEvent
from pattern_recognizer.infra.kafka.candle_kafka_reader import CandleKafkaReader
from pattern_recognizer.infra.kafka.event_window import EventWindow, EventKey
from pattern_recognizer.models.pattern import PatternType
from pattern_recognizer.models.candle import Candle
from pattern_recognizer.models.new_candle_event import NewCandleEvent
from pattern_recognizer.predictor.data_models import ShortCandle
from pattern_recognizer.predictor.recognizer import PatternRecognizer


class NewCandleEventHandler:
    def __init__(self, pattern_recognizer: PatternRecognizer, window_size: int) -> None:
        env_vars = {
            "bootstrap.servers": "BootstrapServers",
            "security.protocol": "SecurityProtocol",
            "sasl.username": "SaslUsername",
            "sasl.password": "SaslPassword",
            "sasl.mechanism": "SaslMechanism",
        }

        consumer_conf = {
            "group.id": "pattern_recognition",
            "auto.offset.reset": "smallest"
        }

        producer_conf = {}

        for conf_key, env_var in env_vars.items():
            env_value = os.getenv(env_var)
            if env_value is not None:
                consumer_conf[conf_key] = env_value
                producer_conf[conf_key] = env_value

        self.data_reader = CandleKafkaReader(consumer_conf)
        self.producer = Producer(producer_conf)
        self.events = EventWindow(window_size)
        self.event_counter = 0

        self._pattern_recognizer = pattern_recognizer

    def start(self):
        self.data_reader.start(self.handle_message)

    def handle_message(self, event: NewCandleEvent):
        event_key = EventKey(exchange=event.exchange, pair=event.pair, timeframe=event.timeframe)

        self.events.add_event(event_key, event.candle)

        new_event = self.recognize_pattern(event)
        if new_event is None:
            return

        data = json.dumps(dataclasses.asdict(new_event), ensure_ascii=False)

        self.producer.produce("PatternRecognition", key=None, value=data)

        self.event_counter += 1
        if self.event_counter % 500 == 0:
            self.producer.flush()
            self.event_counter = 0

    def recognize_pattern(self, event: NewCandleEvent) -> NewPatternEvent | None:
        is_removing_last = False
        event_key = EventKey(exchange=event.exchange, pair=event.pair, timeframe=event.timeframe)

        while True:
            window: list[Candle] = self.events.get_events(event_key)

            recognition_result = self._pattern_recognizer.recognize(window)
            if not recognition_result.is_successful:
                return None

            if recognition_result.most_probable_pattern is not PatternType.NO_PATTERN:
                self.events.leave_only_latest(event_key)
                return NewPatternEvent(
                    exchange=event.exchange,
                    pair=event.pair,
                    timeframe=event.timeframe,
                    timestamp=event.candle.timestamp,
                    recognition_result=recognition_result,
                )

            max_size = self.events.window_size
            if len(window) < max_size and not is_removing_last:
                return None

            is_removing_last = True
            self.events.remove_oldest(event_key)
