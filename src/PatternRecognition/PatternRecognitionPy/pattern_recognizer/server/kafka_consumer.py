import os

from pattern_recognizer.infra.kafka.new_candle_event_handler import (
    NewCandleEventHandler,
)
from pattern_recognizer.predictor.factory import create_recognizer


def run_consumer():
    window_size = int(os.getenv("PATTERN_RECOGNITION_WINDOW_SIZE", "64"))

    consumer = NewCandleEventHandler(create_recognizer(), window_size=window_size)
    consumer.start()
