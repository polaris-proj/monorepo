import enum


class PatternType(enum.StrEnum):
    NO_PATTERN = "NoPattern"
    DOWN_IMPULSE_OR_DIAG_EXPAND = "DownImpulseOrDiagExpand"
    DOWN_DIAG_SHRINK = "DownDiagShrink"
    UP_IMPULSE_OR_DIAG_EXPAND = "UpImpulseOrDiagExpand"
    UP_DIAG_SHRINK = "UpDiagShrink"
