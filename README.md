# TradingBot



## Запуск :


1. run `start_infrastructure.sh` or `start_infrastructure.bat` for Windows`
2. go to `src\PatternRecognition\PatternRecognitionPy` and execute `docker compose up production -d`
3. (optionaly) U can run services for monitoring

## Запуск сервисов для мониторинга:

1. `cd monitoring`
2. `sudo chmod -R 0777 log/` (on unix)
3. `sudo docker compose up -d`




## Запуск сервисов

1. run `.sh` or `start_services_debug.bat` for windows




Swagger: `https://localhost:7139/swagger/index.html`